﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Security;
using System.Web.UI;

namespace Longview
{
    public partial class reset : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string user = Request.QueryString["user"];
            if (user == null)
            {
                Response.StatusCode = 400;
                Response.End();
                return;
            }

            if (Membership.GetUser(user, false) == null)
            {
                Response.StatusCode = 400;
                Response.End();
                return;
            }

            string token = Request.QueryString["token"];
            if(token == null)
            {
                Response.StatusCode = 400;
                Response.End();
                return;
            }

            if (!IsValidToken(token, user))
            {
                Response.StatusCode = 400;
                Response.End();
                return;
            }
        }

        protected bool IsValidToken(string token, string user)
        {
            MembershipUser usr = Membership.GetUser(user, false);
            if (usr.Comment == null)
                return false;

            string[] separators = { ";;" };
            string salt = usr.Comment.Split(separators, StringSplitOptions.None)[0];
            byte[] buf = new byte[] { };

            using (var sha256 = System.Security.Cryptography.SHA256Managed.Create())
            {
                buf = Encoding.UTF8.GetBytes(string.Concat(DateTime.Today, user, salt));
                buf = sha256.ComputeHash(buf);
            }

            string HashBase64 = Convert.ToBase64String(buf);
            if (token != HashBase64)
                return false;

            return true;
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (Password1.Text.Length < 6)
            {
                CustomValidator1.IsValid = false;
                CustomValidator1.ErrorMessage = "Password must be at least 6 characters.";
                return;
            }

            if (Password1.Text.Length > 12)
            {
                CustomValidator1.IsValid = false;
                CustomValidator1.ErrorMessage = "Password cannot be more than 12 characters.";
                return;
            }

            string Valid = @"^(?=.*\d)(?=.*[a-zA-Z])(?!.*[\s\W_]).{6,12}$";
            if (!Regex.IsMatch(Password1.Text, Valid))
            {
                CustomValidator1.IsValid = false;
                CustomValidator1.ErrorMessage = "Password must contain at least 1 letter and 1 number. Password cannot contain spaces or symbols.";
                return;
            }

            string user = Request.QueryString["user"];
            if (user == null)
            {
                Response.StatusCode = 400;
                Response.End();
                return;
            }

            MembershipUser usr = Membership.GetUser(user, false);
            if (usr == null)
            {
                Response.StatusCode = 400;
                Response.End();
                return;
            }

            string token = Request.QueryString["token"];
            if (token == null)
            {
                Response.StatusCode = 400;
                Response.End();
                return;
            }

            if (!IsValidToken(token, user))
            {
                Response.StatusCode = 400;
                Response.End();
                return;
            }

            usr.ChangePassword(usr.ResetPassword(), Password1.Text);
            form1.InnerHtml = @"<h1 style=""font-family: Raleway; text-align: center"">Password reset successful. You will be redirected...</h1>";
            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "redirectJS",
            "setTimeout(function() { window.location.replace('/') }, 3000);", true);
            usr.Comment = "";
            Membership.UpdateUser(usr);
        }
    }
}