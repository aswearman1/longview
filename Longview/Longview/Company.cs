﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;

namespace Longview
{
    public static class Company
    {
        public struct Employee
        {
            public string LastName;
            public string FirstName;
            public string Position;
            public DateTime StartDate;
            public decimal WeeklyHours;
            public decimal HourlyRate;
            public int Rewards;
            public int Recommendations;
            public int Reprimands;
            public int CorrectiveActions;
            public decimal HWRate;
            public bool SCA;

            public Employee(string lastname, string firstname, string position, DateTime startdate, decimal weeklyhours, decimal hourlyrate, 
                int rewards, int recommendations, int reprimands, int correctiveactions, decimal hwrate, bool sca)
            {
                LastName = lastname;
                FirstName = firstname;
                Position = position;
                StartDate = startdate;
                WeeklyHours = weeklyhours;
                HourlyRate = hourlyrate;
                Rewards = rewards;
                Recommendations = recommendations;
                Reprimands = reprimands;
                CorrectiveActions = correctiveactions;
                HWRate = hwrate;
                SCA = sca;
            }
        }

        private static decimal Revenue = 0;
        private static decimal Expenses = 0;
        private static Dictionary<int, Employee> employees = new Dictionary<int, Employee> { };
        public static Dictionary<int, Employee> GetEmployees() { return employees; }
        public static Employee GetEmployeeById(int id) { return GetEmployees()[id];  }

        public static void AddFinancial(decimal[] d)
        {
            if (d.Length != 9)
                return;

            Expenses += d[0] + d[1] + d[2] + d[3] + d[4] + d[5];
            Revenue += d[6] + d[7] + d[8];
        }

        public static decimal GetExpenses()
        {
            return Expenses;
        }

        public static decimal GetRevenue()
        {
            return Revenue;
        }

        public static void PopulateEmployees()
        {
            using (MySqlConnection mysqlCon = new MySqlConnection(ConfigurationManager.ConnectionStrings["LocalMySqlServer"].ConnectionString))
            {
                mysqlCon.Open();
                using (MySqlCommand sqlCommand = new MySqlCommand("SELECT EmployeeID, LastName, FirstName, Position, StartDate, WeeklyHours, HourlyRate," +
                    "Rewards, Recommendations, Reprimands, CorrectiveActions, `H&W Rate`, SCA FROM company_employee;", mysqlCon))
                {
                    using (MySqlDataReader dataReader = sqlCommand.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            Employee e = new Employee(dataReader.GetString(1), dataReader.GetString(2), dataReader.GetString(3),
                                dataReader.IsDBNull(4) ? DateTime.Now : dataReader.GetDateTime(4),
                                dataReader.IsDBNull(5) ? 0 : dataReader.GetDecimal(5),
                                dataReader.IsDBNull(6) ? 0 : dataReader.GetDecimal(6),
                                dataReader.IsDBNull(7) ? 0 : dataReader.GetInt32(7),
                                dataReader.IsDBNull(8) ? 0 : dataReader.GetInt32(8),
                                dataReader.IsDBNull(9) ? 0 : dataReader.GetInt32(9),
                                dataReader.IsDBNull(10) ? 0 : dataReader.GetInt32(10),
                                dataReader.IsDBNull(11) ? 0 : dataReader.GetDecimal(11),
                                dataReader.GetString(12) == "no" ? false : true);
                            employees.Add(dataReader.GetInt32(0), e);
                        }
                    }
                }
                mysqlCon.Close();
            }
        }

        public static void EditEmployee(int id, string lastname, string firstname, string position, DateTime startdate, decimal weeklyhours, 
            decimal hourlyrate, int rewards, int recommendations, int reprimands, int correctiveactions, decimal hwrate)
        {
            Employee e = new Employee(lastname, firstname, position, startdate, weeklyhours, hourlyrate, rewards, recommendations, reprimands, correctiveactions, hwrate, false);
            employees[id] = e;
            using (MySqlConnection mysqlCon = new MySqlConnection(ConfigurationManager.ConnectionStrings["LocalMySqlServer"].ConnectionString))
            {
                mysqlCon.Open();
                using (MySqlCommand sqlCommand = new MySqlCommand("UPDATE company_employee SET LastName = @param1, " +
                    "FirstName = @param2, Position = @param3, StartDate = @param4, WeeklyHours = @param5, HourlyRate = @param6, Rewards = @param7," +
                    "Recommendations = @param8, Reprimands = @param9, CorrectiveActions = @param10," +
                    " `H&W Rate` = @param11, SCA = @param12 WHERE EmployeeID = @param0;", mysqlCon))
                {
                    sqlCommand.Parameters.AddWithValue("@param0", id);
                    sqlCommand.Parameters.AddWithValue("@param1", lastname);
                    sqlCommand.Parameters.AddWithValue("@param2", firstname);
                    sqlCommand.Parameters.AddWithValue("@param3", position);
                    sqlCommand.Parameters.AddWithValue("@param4", startdate);
                    sqlCommand.Parameters.AddWithValue("@param5", weeklyhours);
                    sqlCommand.Parameters.AddWithValue("@param6", hourlyrate);
                    sqlCommand.Parameters.AddWithValue("@param7", rewards);
                    sqlCommand.Parameters.AddWithValue("@param8", recommendations);
                    sqlCommand.Parameters.AddWithValue("@param9", reprimands);
                    sqlCommand.Parameters.AddWithValue("@param10", correctiveactions);
                    sqlCommand.Parameters.AddWithValue("@param11", hwrate);
                    sqlCommand.Parameters.AddWithValue("@param12", "no");
                    sqlCommand.ExecuteNonQuery();
                }
                mysqlCon.Close();
            }
        }

        public static void AddEmployee(int id, Employee employee)
        {
            employees.Add(id, employee);
            using (MySqlConnection mysqlCon = new MySqlConnection(ConfigurationManager.ConnectionStrings["LocalMySqlServer"].ConnectionString))
            {
                mysqlCon.Open();
                using (MySqlCommand sqlCommand = new MySqlCommand("INSERT INTO company_employee VALUES (@param0, @param1, @param2, " +
                    "@param3, @param4," +
                    "@param5, @param6, @param7, @param8, @param9, @param10, @param11, @param12);", mysqlCon))
                {
                    sqlCommand.Parameters.AddWithValue("@param0", id);
                    sqlCommand.Parameters.AddWithValue("@param1", employee.LastName);
                    sqlCommand.Parameters.AddWithValue("@param2", employee.FirstName);
                    sqlCommand.Parameters.AddWithValue("@param3", employee.Position);
                    sqlCommand.Parameters.AddWithValue("@param4", employee.StartDate);
                    sqlCommand.Parameters.AddWithValue("@param5", employee.WeeklyHours);
                    sqlCommand.Parameters.AddWithValue("@param6", employee.HourlyRate);
                    sqlCommand.Parameters.AddWithValue("@param7", employee.Rewards);
                    sqlCommand.Parameters.AddWithValue("@param8", employee.Recommendations);
                    sqlCommand.Parameters.AddWithValue("@param9", employee.Reprimands);
                    sqlCommand.Parameters.AddWithValue("@param10", employee.CorrectiveActions);
                    sqlCommand.Parameters.AddWithValue("@param11", employee.HWRate);
                    sqlCommand.Parameters.AddWithValue("@param12", "no");
                    sqlCommand.ExecuteNonQuery();
                }
                mysqlCon.Close();
            }
        }

        public static void RemoveEmployee(int id)
        {
            employees.Remove(id);
            using (MySqlConnection mysqlCon = new MySqlConnection(ConfigurationManager.ConnectionStrings["LocalMySqlServer"].ConnectionString))
            {
                mysqlCon.Open();
                using (MySqlCommand sqlCommand = new MySqlCommand("DELETE FROM company_employee WHERE EmployeeID = @param0;", mysqlCon))
                {
                    sqlCommand.Parameters.AddWithValue("@param0", id);
                    sqlCommand.ExecuteNonQuery();
                }
                mysqlCon.Close();
            }
        }
    }
}