﻿using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using System.Web.Security;
using System.Linq;
using System.Configuration;

namespace Longview
{
    public static class ProjectsManager
    {
        private static Dictionary<string, Project> roles = new Dictionary<string, Project> { };
        private static Dictionary<int, string> roleList = new Dictionary<int, string> { };
        private static Dictionary<string, decimal[]> yearlyFinances = new Dictionary<string, decimal[]> { };
        private static Dictionary<string, decimal[]> monthlyFinances = new Dictionary<string, decimal[]> { };

        public static void PopulateRoles(bool refresh)
        {
            if (refresh)
            {
                roles.Clear();
                roleList.Clear();
            }

            if (roles.Count > 0)
                return;

            Project p = new Longview.Project(0, "Overview", "Anthony Long", "", "", "", DateTime.Now.ToShortDateString(),
                DateTime.Now.ToShortDateString(), DateTime.Now.ToShortDateString());
            roles.Add("Overview", p);
            roleList.Add(0, "Overview");

            using (MySqlConnection mysqlCon = new MySqlConnection(ConfigurationManager.ConnectionStrings["LocalMySqlServer"].ConnectionString))
            {
                mysqlCon.Open();
                using (MySqlCommand sqlCommand = new MySqlCommand("SELECT `name`, project.id, project_manager, project_phone," +
                    " project_email, summary, completion_date, details, conclusion, project_scope, product_scope, progress_author," +
                    " budget, last_updated, progress_percentage, start_date" +
                    " FROM project JOIN my_aspnet_roles ON my_aspnet_roles.id = project.id;", mysqlCon))
                {
                    using (MySqlDataReader dataReader = sqlCommand.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            //1 project id; 2 project manager; 3 manager phone #; 4 manager email; 5 project summary;
                            //6 estimated completion date; 7 project details; 8 pr conclusion; 9 pr project scope;
                            //10 pr product scope; 11 pr author; 12 project budget; 13 pr last update 15 start date
                            p = new Longview.Project(dataReader.GetInt32(1), dataReader.GetString(0), dataReader.GetString(2),
                                dataReader.GetString(3), dataReader.GetString(4), dataReader.IsDBNull(5) ? "" : dataReader.GetString(5),
                                dataReader.GetString(15), dataReader.GetString(6), dataReader.GetString(13), dataReader.IsDBNull(7) ? "" : dataReader.GetString(7),
                                dataReader.IsDBNull(8) ? "" : dataReader.GetString(8), dataReader.IsDBNull(9) ? "" : dataReader.GetString(9),
                                dataReader.IsDBNull(10) ? "" : dataReader.GetString(10), dataReader.IsDBNull(11) ? "" : dataReader.GetString(11),
                                dataReader.GetDecimal(12), 0, dataReader.GetInt32(14));
                            roles.Add(dataReader.GetString(0), p);
                            roleList.Add(dataReader.GetInt32(1), dataReader.GetString(0));
                        }
                    }
                }
                mysqlCon.Close();

                CalculateFinances();
            }
        }

        public static void CalculateFinances()
        {
            yearlyFinances.Clear();
            monthlyFinances.Clear();
            foreach(KeyValuePair<string, Project> kvp in roles)
            {
                IEnumerable<KeyValuePair<string, decimal[]>> d = kvp.Value.getFinancial().Where(x => x.Key.Contains(DateTime.Now.Year.ToString()));
                foreach(KeyValuePair<string, decimal[]> keyval in d)
                {
                    if (!monthlyFinances.ContainsKey(keyval.Key))
                        monthlyFinances.Add(keyval.Key, new decimal[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 });
                    monthlyFinances[keyval.Key][0] += keyval.Value[0];
                    monthlyFinances[keyval.Key][1] += keyval.Value[1];
                    monthlyFinances[keyval.Key][2] += keyval.Value[2];
                    monthlyFinances[keyval.Key][3] += keyval.Value[3];
                    monthlyFinances[keyval.Key][4] += keyval.Value[4];
                    monthlyFinances[keyval.Key][5] += keyval.Value[5];
                    monthlyFinances[keyval.Key][6] += keyval.Value[6];
                    monthlyFinances[keyval.Key][7] += keyval.Value[7];
                    monthlyFinances[keyval.Key][8] += keyval.Value[8];

                    string monthyear = keyval.Key.Split(' ')[1] + " " + keyval.Key.Split(' ')[2];
                    if (!yearlyFinances.ContainsKey(monthyear))
                        yearlyFinances.Add(monthyear, new decimal[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 });
                    yearlyFinances[monthyear][0] += keyval.Value[0];
                    yearlyFinances[monthyear][1] += keyval.Value[1];
                    yearlyFinances[monthyear][2] += keyval.Value[2];
                    yearlyFinances[monthyear][3] += keyval.Value[3];
                    yearlyFinances[monthyear][4] += keyval.Value[4];
                    yearlyFinances[monthyear][5] += keyval.Value[5];
                    yearlyFinances[monthyear][6] += keyval.Value[6];
                    yearlyFinances[monthyear][7] += keyval.Value[7];
                    yearlyFinances[monthyear][8] += keyval.Value[8];
                }
            }
        }

        public static Dictionary<string, decimal[]> GetYearlyFinances()
        {
            return yearlyFinances;
        }

        public static Dictionary<string, decimal[]> GetMonthlyFinances()
        {
            return monthlyFinances;
        }

        public static Project GetRoleByName(string s)
        {
            if (roles.Count == 0 || !roles.ContainsKey(s))
                return null;

            return roles[s];
        }

        public static string GetRoleNameById(int i)
        {
            if (roleList.Count == 0 || !roleList.ContainsKey(i))
                return null;

            return roleList[i];
        }

        public static bool AddRole(string s, string contact_name, string contact_phone, string contact_email, DateTime start_date, DateTime end_date, decimal budget)
        {
            if (s == "Overview")
                return false;

            try
            {
                Roles.CreateRole(s);
                using (MySqlConnection mysqlCon = new MySqlConnection(ConfigurationManager.ConnectionStrings["LocalMySqlServer"].ConnectionString))
                {
                    mysqlCon.Open();
                    using (MySqlCommand sqlCommand = new MySqlCommand("INSERT INTO project (id, project_manager, project_phone, " +
                        "project_email, start_date, completion_date, budget) " +
                        "VALUES (@param1,@param2,@param3,@param4,@param5,@param6,@param7);", mysqlCon))
                    {
                        int id = 0;
                        using (MySqlCommand cmd = new MySqlCommand("SELECT id FROM my_aspnet_roles WHERE name = ?param1;", mysqlCon))
                        {
                            cmd.Parameters.AddWithValue("param1", s);
                            id = (int)cmd.ExecuteScalar();
                        }

                        sqlCommand.Parameters.AddWithValue("@param1", id);
                        sqlCommand.Parameters.AddWithValue("@param2", contact_name);
                        sqlCommand.Parameters.AddWithValue("@param3", contact_phone);
                        sqlCommand.Parameters.AddWithValue("@param4", contact_email);
                        sqlCommand.Parameters.AddWithValue("@param5", start_date);
                        sqlCommand.Parameters.AddWithValue("@param6", end_date);
                        sqlCommand.Parameters.AddWithValue("@param7", budget);
                        sqlCommand.ExecuteNonQuery();
                    }
                    mysqlCon.Close();
                }
            }
            catch (Exception ex)
            {
                Roles.DeleteRole(s);
                return false;
            }

            if (Roles.RoleExists(s))
                PopulateRoles(true);

            return true;
        }

        public static bool RemoveRole(string s)
        {
            try
            {
                using (MySqlConnection mysqlCon = new MySqlConnection(ConfigurationManager.ConnectionStrings["LocalMySqlServer"].ConnectionString))
                {
                    mysqlCon.Open();
                    int id = 0;
                    using (MySqlCommand sqlCommand = new MySqlCommand("SELECT id FROM my_aspnet_roles WHERE name = ?param1;", mysqlCon))
                    {
                        sqlCommand.Parameters.AddWithValue("param1", s);
                        id = (int)sqlCommand.ExecuteScalar();
                    }

                    using (MySqlCommand sqlCommand = new MySqlCommand("DELETE FROM project WHERE id = ?param1;", mysqlCon))
                    {
                        sqlCommand.Parameters.AddWithValue("param1", id);
                        sqlCommand.ExecuteNonQuery();
                    }

                    using (MySqlCommand sqlCommand = new MySqlCommand("DELETE FROM project_actions WHERE project_id = ?param1;", mysqlCon))
                    {
                        sqlCommand.Parameters.AddWithValue("param1", id);
                        sqlCommand.ExecuteNonQuery();
                    }

                    using (MySqlCommand sqlCommand = new MySqlCommand("DELETE FROM project_files WHERE project_id = ?param1;", mysqlCon))
                    {
                        sqlCommand.Parameters.AddWithValue("param1", id);
                        sqlCommand.ExecuteNonQuery();
                    }

                    using (MySqlCommand sqlCommand = new MySqlCommand("DELETE FROM project_financial WHERE project_id = ?param1;", mysqlCon))
                    {
                        sqlCommand.Parameters.AddWithValue("param1", id);
                        sqlCommand.ExecuteNonQuery();
                    }

                    using (MySqlCommand sqlCommand = new MySqlCommand("DELETE FROM project_issue WHERE project_id = ?param1;", mysqlCon))
                    {
                        sqlCommand.Parameters.AddWithValue("param1", id);
                        sqlCommand.ExecuteNonQuery();
                    }

                    using (MySqlCommand sqlCommand = new MySqlCommand("DELETE FROM project_milestone WHERE project_id = ?param1;", mysqlCon))
                    {
                        sqlCommand.Parameters.AddWithValue("param1", id);
                        sqlCommand.ExecuteNonQuery();
                    }

                    using (MySqlCommand sqlCommand = new MySqlCommand("DELETE FROM project_risk WHERE project_id = ?param1;", mysqlCon))
                    {
                        sqlCommand.Parameters.AddWithValue("param1", id);
                        sqlCommand.ExecuteNonQuery();
                    }

                    using (MySqlCommand sqlCommand = new MySqlCommand("DELETE FROM project_recruiting WHERE project_id = ?param1;", mysqlCon))
                    {
                        sqlCommand.Parameters.AddWithValue("param1", id);
                        sqlCommand.ExecuteNonQuery();
                    }

                    using (MySqlCommand sqlCommand = new MySqlCommand("DELETE FROM project_recruiting_info WHERE project_id = ?param1;", mysqlCon))
                    {
                        sqlCommand.Parameters.AddWithValue("param1", id);
                        sqlCommand.ExecuteNonQuery();
                    }

                    using (MySqlCommand sqlCommand = new MySqlCommand("DELETE FROM project_employee WHERE project_id = ?param1;", mysqlCon))
                    {
                        sqlCommand.Parameters.AddWithValue("param1", id);
                        sqlCommand.ExecuteNonQuery();
                    }

                    mysqlCon.Close();
                }
                Roles.DeleteRole(s);
            }
            catch (Exception ex)
            {
                return false;
            }

            if (!Roles.RoleExists(s))
                PopulateRoles(true);

            return true;
        }

        public static Dictionary<string, Project> GetAllRoles()
        {
            return roles;
        }
    }
}
