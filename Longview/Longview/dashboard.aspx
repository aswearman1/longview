﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="Dashboard.aspx.cs" Inherits="Longview.Dashboard" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <table width="100%" height="80px" style="background: linear-gradient(to right, rgba(1,28,86,1) 0%,rgba(255,255,255,0) 100%);">
		<tr>
		<td width="5%"></td>
			<td style="width: 100%; height: 100%;  font-family: raleway; font-size: 36pt; font-weight: 600; color: white; text-shadow: 2px 2px black;">
				<a name="top" id="dashText">Overview</a>
			</td>
		</tr>
	</table>
	
	<table width="100%" cellspacing="0">
	<tbody>	
			<tr>
			<td style="vertical-align: top; margin-top: 0px; width: 180px; background: linear-gradient(to bottom, rgba(51,51,51,1) 0%,rgba(51,51,51,1) 32%,rgba(180,180,180,1) 75%,rgba(255,255,255,0) 100%); padding: 0;">
				<ul id="links" style="list-style: none; padding-left: 0px; margin-top: 0px;">
                    <li style="background-color: #0D0435; width: 180px; word-wrap: break-word; display: block; margin-bottom: 1px;">
						<a href="#top" id="ov" style="text-shadow: 2px 2px black; padding-left: 15px; padding-right: 15px; padding-top: 13px; padding-bottom: 13px; font-size: 12pt; color: white; display: block; height: 100%; width: 100%; font-family: raleway; text-decoration: none; font-weight: 600;" 
                        onMouseOver="this.style.backgroundColor='#5b719f'" onMouseOut="onhoverout(this)" onclick="overview(this)">
						Overview
						</a>
					</li>

                    <li style="background-color: #40537b; width: 180px; word-wrap: break-word; display: block; margin-bottom: 1px;">
						<a href="#top" id="cs" style="text-shadow: 2px 2px black; padding-left: 15px; padding-right: 15px; padding-top: 13px; padding-bottom: 13px; font-size: 12pt; color: white; display: block; height: 100%; width: 100%; font-family: raleway; text-decoration: none; font-weight: 600;" 
                        onMouseOver="this.style.backgroundColor='#5b719f'" onMouseOut="onhoverout(this)" onclick="CorporateStatus(this)">
						Corporate Status
						</a>
					</li>

                    <li style="background-color: #40537b; width: 180px; word-wrap: break-word; display: block; margin-bottom: 1px;">
						<a href="#top" id="ps" style="text-shadow: 2px 2px black; padding-left: 15px; padding-right: 15px; padding-top: 13px; padding-bottom: 13px; font-size: 12pt; color: white; display: block; height: 100%; width: 100%; font-family: raleway; text-decoration: none; font-weight: 600;" 
                        onMouseOver="this.style.backgroundColor='#5b719f'" onMouseOut="onhoverout(this)" onclick="Project(this)">
						Program Status
						</a>
					</li>

                    <% if (User.IsInRole("Employee") || User.IsInRole("Administrators") || User.IsInRole("Manager"))
                        { %>
                    <li style="background-color: #40537b; width: 180px; word-wrap: break-word; display: block; margin-bottom: 1px;">
						<a id="cus" href="#top" style="text-shadow: 2px 2px black; padding-left: 15px; padding-right: 15px; padding-top: 13px; padding-bottom: 13px; font-size: 12pt; color: white; display: block; height: 100%; width: 100%;  font-family: raleway; text-decoration: none; font-weight: 600;"
						onMouseOver="this.style.backgroundColor='#5b719f'" onMouseOut="onhoverout(this)" onclick="customer(this)">
					    Customer
						</a>
					</li>

                    <li style="background-color: #40537b; width: 180px; word-wrap: break-word; display: block; margin-bottom: 1px;">
						<a id="emp" href="#top" style="text-shadow: 2px 2px black; padding-left: 15px; padding-right: 15px; padding-top: 13px; padding-bottom: 13px; font-size: 12pt; color: white; display: block; height: 100%; width: 100%;  font-family: raleway; text-decoration: none; font-weight: 600;"
						onMouseOver="this.style.backgroundColor='#5b719f'" onMouseOut="onhoverout(this)" onclick="employee(this)">
					    Employee
						</a>
					</li>
                    <% } %>

                    <% if (User.IsInRole("Administrators"))
                        { %>
                    <li style="background-color: #40537b; width: 180px; word-wrap: break-word; display: block; margin-bottom: 1px;">
						<a id="ao" href="#top" style="text-shadow: 2px 2px black; padding-left: 15px; padding-right: 15px; padding-top: 13px; padding-bottom: 13px; font-size: 12pt; color: white; display: block; height: 100%; width: 100%;  font-family: raleway; text-decoration: none; font-weight: 600;"
						onMouseOver="this.style.backgroundColor='#5b719f'" onMouseOut="onhoverout(this)" onclick="admin(this)">
						Administrative Options
						</a>
					</li>
                    <% } %>
				</ul>
			</td>
			
			<td style="padding: 0;">
                <div id="overview" style="background: linear-gradient(to bottom, rgba(191,191,191,1) 0%,rgba(232,232,232,1) 13%,rgba(244,244,244,1) 100%); height: 1000px;">
                    <iframe id="frame" src="Overview.aspx" scrolling="no" style="height: 100%; width: 100%; overflow-y: hidden;"></iframe>
                </div>
			</td>
			</tr>
		</tbody>
	</table>

    <asp:Literal ID="Literal3" runat="server"></asp:Literal>
    <script>
        var active = document.getElementById("ov");

        if (sessionStorage.getItem("activeId") != null) {
            var el = document.getElementById(sessionStorage.getItem("activeId"));
            if (el != null)
                el.click();
        }

        <% if(User.IsInRole("Employee") || 
            User.IsInRole("Administrators") || 
            User.IsInRole("Manager")) { %>
        function employee(e)
        {
            var element = document.getElementById("overview");

            element.innerHTML = '<iframe src="employee.aspx" style="height: 100%; width: 100%;"></iframe>';
            document.getElementById("dashText").innerHTML = 'Employee';

            setactive(e);
        }

        function CorporateStatus(e) {
            var element = document.getElementById("overview");

            element.innerHTML = '<iframe src="CorporateStatus.aspx" style="height: 100%; width: 100%;"></iframe>';
            document.getElementById("dashText").innerHTML = 'Corporate Status';

            setactive(e);
        }
        <% } %>

        function Project(e) {
            var element = document.getElementById("overview");

            element.innerHTML = '<iframe src="Project.aspx" style="height: 100%; width: 100%;"></iframe>';
            document.getElementById("dashText").innerHTML = 'Program Status';

            setactive(e);
        }

        function overview(e) {
            var element = document.getElementById("overview");

            element.innerHTML = '<iframe src="Overview.aspx" style="height: 100%; width: 100%;"></iframe>';
            document.getElementById("dashText").innerHTML = 'Overview';

            setactive(e);
        }

        <% if(User.IsInRole("Administrators")) { %>
        function admin(e) {
            var element = document.getElementById("overview");

            element.innerHTML = '<iframe src="Admin.aspx" style="height: 100%; width: 100%;"></iframe>';
            document.getElementById("dashText").innerHTML = 'Administrative Options';

            setactive(e);
        }
        <% } %>

        <% if(User.IsInRole("Customer") || 
            User.IsInRole("Administrators") || 
            User.IsInRole("Manager")) { %>
        function customer(e) {
            var element = document.getElementById("overview");

            element.innerHTML = '<iframe src="customer.aspx" style="height: 100%; width: 100%;"></iframe>';
            document.getElementById("dashText").innerHTML = 'Customer';

            setactive(e);
        }
        <% } %>

        function setactive(element) {
            active.style.backgroundColor = '#40537b';
            element.style.backgroundColor = '#0D0435';
            sessionStorage.setItem("activeId", element.id);
            active = element;
        }

        function onhoverout(el)
        {
            var col = el != active ? '#40537b' : '#0D0435';

            el.style.backgroundColor = col;
        }
    </script>
</asp:Content>
