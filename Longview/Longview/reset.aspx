﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Reset.aspx.cs" Inherits="Longview.reset" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <style type="text/css">
        #Password1 {
            width: 220px;
        }
        #Password2 {
            width: 220px;
        }
    </style>
    <link href="Content/bootstrap.css" rel="stylesheet" id="bootstrap-css" />
</head>
<body>
    <form id="form1" runat="server">
        <div style="font-family: Raleway;">
            <table>
                <tr>
                    <td colspan="3" align="center">
                        <h2>Reset Password</h2>
                    </td>
                </tr>

                <tr>
                    <td>
                        New Password: 
                    </td>

                    <td>
                        <asp:TextBox ID="Password1" runat="server" TextMode="Password" MaxLength="12"></asp:TextBox>
                    </td>

                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="A password is required." ControlToValidate="Password1" ValidationGroup="CompPass">*</asp:RequiredFieldValidator>
                    </td>
                </tr>

                <tr>
                    <td>
                        Confirm Password: 
                    </td>

                    <td>
                        <asp:TextBox ID="Password2" runat="server" TextMode="Password"></asp:TextBox>
                    </td>

                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="A password confirmation is required." ControlToValidate="Password2" ValidationGroup="CompPass">*</asp:RequiredFieldValidator>
                    </td>
                </tr>

                <tr>
                    <td>
                        <asp:Button ID="Button1" CssClass="btn-default btn-sm" runat="server" Text="Submit" CausesValidation="true" ValidationGroup="CompPass" BackColor="#0D0435" ForeColor="White" OnClick="Button1_Click" />
                    </td>
                </tr>

                <tr>
                    <td colspan="3">
                        <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="CustomValidator" ControlToValidate="Password1" ValidationGroup="CompPass" Display="None"></asp:CustomValidator>
                        <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Both passwords must match." ControlToCompare="Password2" ControlToValidate="Password1" Display="None" ValidationGroup="CompPass"></asp:CompareValidator>
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="CompPass" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
