﻿using MySql.Data.MySqlClient;
using System;
using System.Configuration;
using System.Net;

namespace Longview
{
    public partial class Overview2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!User.Identity.IsAuthenticated)
            { 
                Response.Redirect("Default.aspx");
                return;
            }

            string notes = "";
            string surveyt = "";
            if(getData(out notes, out surveyt))
            {
                notesText.InnerHtml = notes;
                survey.Src = surveyt;
            }

            if(User.IsInRole("Administrators"))
            {
                Button1.Visible = true;
                Button2.Visible = true;
            }
        }

        protected bool getData(out string notes, out string survey)
        {
            string nt = "";
            string sv = "";

            using (MySqlConnection mysqlCon = new MySqlConnection(ConfigurationManager.ConnectionStrings["LocalMySqlServer"].ConnectionString))
            {
                try
                {
                    mysqlCon.Open();
                    using (MySqlCommand sqlCommand = new MySqlCommand("SELECT notes, data1 FROM tab_data WHERE tab_name = ?param1;", mysqlCon))
                    {
                        sqlCommand.Parameters.AddWithValue("param1", "Overview");
                        using (MySqlDataReader dataReader = sqlCommand.ExecuteReader())
                        {
                            if (dataReader != null)
                            {
                                while (dataReader.Read())
                                {
                                    nt = dataReader.GetString(0);
                                    sv = dataReader.GetString(1);
                                }
                            }
                        }
                    }
                    mysqlCon.Close();
                }
                catch (Exception ex)
                {
                    notes = "";
                    survey = "";
                    mysqlCon.Close();
                    return false;
                }
            }

            notes = nt;
            survey = sv;

            return true;
        }



        protected void Button1_Click(object sender, EventArgs e)
        {
            if (User.IsInRole("Administrators"))
            {
                if (notesText.Visible)
                {
                    Button1.Text = "Save";
                    notesText.Visible = false;
                    TextBox1.Text = notesText.InnerText;
                    TextBox1.Visible = true;
                }
                else
                {
                    Button1.Text = "Edit";
                    notesText.Visible = true;
                    SetNotes(TextBox1.Text);
                    TextBox1.Visible = false;
                }
                UpdatePanel1.Update();
            }
        }

        protected void SetNotes(string s)
        {
            using (MySqlConnection mysqlCon = new MySqlConnection(ConfigurationManager.ConnectionStrings["LocalMySqlServer"].ConnectionString))
            {
                mysqlCon.Open();
                try
                {
                    using (MySqlCommand sqlCommand = new MySqlCommand("UPDATE tab_data SET notes = @param1 WHERE tab_name = @param2;", mysqlCon))
                    {
                        sqlCommand.Parameters.AddWithValue("@param1", s);
                        sqlCommand.Parameters.AddWithValue("@param2", "Overview");

                        int affected = sqlCommand.ExecuteNonQuery();
                        if (affected > 0)
                        {
                            notesText.InnerText = TextBox1.Text;
                        }
                    }
                }
                finally
                {
                    mysqlCon.Close();
                }
            }
        }

        protected void SetSurvey(string s)
        {
            if (s == survey.Src)
                return;

            WebRequest webRequest = WebRequest.Create(s);
            if (webRequest.RequestUri.Host != "www.surveymonkey.com")
                return;

            using (MySqlConnection mysqlCon = new MySqlConnection(ConfigurationManager.ConnectionStrings["LocalMySqlServer"].ConnectionString))
            {
                mysqlCon.Open();
                try
                {
                    using (MySqlCommand sqlCommand = new MySqlCommand("UPDATE tab_data SET data1 = @param1 WHERE tab_name = @param2;", mysqlCon))
                    {
                        sqlCommand.Parameters.AddWithValue("@param1", s);
                        sqlCommand.Parameters.AddWithValue("@param2", "Overview");

                        int affected = sqlCommand.ExecuteNonQuery();
                        if (affected > 0)
                        {
                            survey.Src = s;
                        }
                    }
                }
                finally
                {
                    mysqlCon.Close();
                }
            }
        }

        

        protected void Button2_Click(object sender, EventArgs e)
        {
            if (!User.IsInRole("Administrators"))
                return;

            if (Button2.Text == "Edit")
            {
                survey.Visible = false;
                SurveyInput.Visible = true;
                Button2.Text = "Save";
                SurveyURL.Text = survey.Src;
            }
            else
            {
                survey.Visible = true;
                SurveyInput.Visible = false;
                Button2.Text = "Edit";
                SetSurvey(SurveyURL.Text);
            }

            UpdatePanel2.Update();
        }
    }
}