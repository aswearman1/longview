﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Progress.aspx.cs" Inherits="Longview.progress" EnableEventValidation = "false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="Content/bootstrap.css" rel="stylesheet" id="bootstrap-css">
    <style type="text/css">
            .auto-style1 {
                height: 23px;
            }
            .auto-style2 {
                height: 10px;
            }

            @font-face { font-family: "Raleway"; src: url('/fonts/Raleway-Regular.ttf'); }
            @font-face { font-family: "Raleway"; src: url('/fonts/Raleway-SemiBold.ttf'); font-weight: 500; }
            @font-face { font-family: "Raleway"; src: url('/fonts/Raleway-Bold.ttf'); font-weight: bold; }

            .dropdown {
                position: relative;
                display: inline-block;
            }

            .dropdown-content {
                visibility: hidden;
                position: absolute;
                background-color: #f9f9f9;
                min-width: 160px;
                box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
                z-index: 1;
                right: 0;
                border-color: gray;

                transition: visibility 0.5s;
            }

            .dropdown-item img
            {
                float: left;
            }

            .dropdown-item
            {
                padding: 10px;
            }

            .dropdown-item:hover
            {
                background-color: lightgray;
            }

            .dropdown:focus {
                pointer-events: none;
                background-color: lightgray;
            }

            .dropdown-content:focus
            {
                display: block;
            }

            .dropdown:focus .dropdown-content {
                visibility: visible;
                outline: none;
                pointer-events: auto;

                transition: visibility 0.5s;
            }

            .navItem
            {
                height: 100%;
                font-size: 19px;
                padding: 16px 25px;
                display: block;
                text-align: center;
                color: #2f415f;
            }

            .navItem:hover
            {
                background: rgba(0,0,0,0.3);
                color: #FFF;
                text-align: center;
            }
        </style>
</head>
<body id="Panel1" style="margin: 0; background-color: white;" runat="server">
    <header> 
        <table width="100%" style="font-family: Raleway;">
		    <tr>
			    <td width="5%"></td>
			    <td width="10%" valign="middle" align="left"><a href="/"><img src="/images/longview.png" style="height: 67px;" alt=""/></a></td>
			    <td align="right">
                    <div style="width: auto; height: 100%; display: inline-block; border-right: 1px solid black;">
                        <nav style="height: 100%;">
                            <ul style="list-style: none; margin: 0; padding: 0;">
                                <li>
                                    <a class="navItem" href="dashboard.aspx" style="text-decoration: none;">
                                        Dashboard
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    <% if (Page.User.Identity.IsAuthenticated)
                        { %>
                    <div class="dropdown" tabindex="0" style="outline: none; padding: 0 5px;" onclick="this.focus();">
                        <div style="height: 50px; cursor: pointer;">
                            <asp:Label ID="Username" runat="server" Text="Label" Font-Size="14pt"></asp:Label><img src="Images/user.png" width="50px" height="50px" />
                        </div>
                            
                        <div class="dropdown-content" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="Settings" runat="server" style="width: 200px;">&nbsp;&nbsp;&nbsp;<img src="Images/settings.png" />Account Settings</a>
                            <a class="dropdown-item" href="#" runat="server" onserverclick="Button1_OnClick"><img src="Images/logout.png" />Log Out</a>
                        </div>
                    </div>
                    <% } %>
			    </td>
		        <td width="5%"></td>
		    </tr>
	    </table>
    </header>
    <form id="form1" runat="server">
        <div style="padding: 0 50px 50px 50px; height: 100%; color: #383838;">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <table style="width: 100%;">	
		    <tbody>

			    <tr>
				    <td colspan="4">
					    <h1 style="text-align: center;">Progress Report</h1>
				    </td>
			    </tr>

                <tr>
                    <td colspan="4" align="center">
                        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Download"/>
                    </td>
                </tr>

                <tr>
                    <td class="auto-style2">
                        
                        &nbsp;</td>
                </tr>
		
			    <tr>
				    <td colspan="4">
					    <h2 style="margin: 0;">Project Info</h2><hr />
				    </td>
			    </tr>
		
			    <tr>
				    <td width="300px" class="auto-style1">
					    <b id="pProjectName" runat="server">Project Name</b>
				    </td>

				    <td width="20px" class="auto-style1">

				    </td>

				    <td class="auto-style1">
					    <b id="pProjectManager" runat="server">Project Manager</b>
				    </td>

                    <td width="20px" class="auto-style1"></td>
			    </tr>
		
			    <tr>
				    <td>
                        <asp:Literal ID="pName" runat="server"></asp:Literal>
				    </td>
				
				    <td>
					
				    </td>
				
				    <td>
					    <asp:Literal ID="pManager" runat="server"></asp:Literal>
				    </td>
			    </tr>
			
			    <tr>
				    <td colspan="4" style="height: 5px;"></td>
			    </tr>
			
			    <tr>
				    <td>
					    <b>Author</b>
				    </td>
				
				    <td width="20px"></td>
				
				    <td>
					    <b>Last Updated</b>
				    </td>

                    <td width="20px"></td>
			    </tr>
			
			    <tr>
				    <td>
                        <asp:Literal ID="pAuthor" runat="server"></asp:Literal>
				    </td>
				
				    <td width="20px"></td>
				
				    <td>
                        <asp:Literal ID="pDate" runat="server"></asp:Literal>
				    </td>

                    <td width="20px"></td>
			    </tr>
			
			    <tr>
				    <td colspan="4" style="height: 5px;"></td>
			    </tr>

                <tr>
                    <td class="auto-style1">
                        <b>Project Started</b></td>

                    <td>

                    </td>

                    <td>
					    <b>Estimated Completion Date</b>
				    </td>
                </tr>

                <tr>
                    <td class="auto-style1">
                        <asp:Literal ID="pProjectStart" runat="server"></asp:Literal>
                    </td>

                    <td class="auto-style1">

                    </td>

                    <td class="auto-style1">
                        <asp:Literal ID="pCompletion" runat="server"></asp:Literal>
                    </td>
                </tr>

                <tr>
				    <td colspan="4" style="height: 20px;"></td>
			    </tr>
			
			    <tr>
				    <td colspan="4">
					    <h2 style="margin: 0;">Project Summary</h2><hr />
				    </td>
			    </tr>

                <tr>
                    <td colspan="4"><b>Summary</b></td>
                </tr>

                <tr>
                    <td colspan="4">
                        <asp:Literal ID="pSummary" runat="server"></asp:Literal>
                    </td>
                </tr>

                <tr>
				    <td colspan="4" style="height: 10px;"></td>
			    </tr>

                <tr>
                    <td colspan="4"><b>Project Scope</b></td>
                </tr>

                <tr>
                    <td colspan="4">
                        <asp:Literal ID="pProjectScope" runat="server"></asp:Literal>
                    </td>
                </tr>

                <tr>
				    <td colspan="4" style="height: 10px;"></td>
			    </tr>

                <tr>
                    <td colspan="4"><b>Product Scope</b></td>
                </tr>

                <tr>
                    <td colspan="4">
                        <asp:Literal ID="pProductScope" runat="server"></asp:Literal>
                    </td>
                </tr>

                <tr>
				    <td colspan="4" style="height: 10px;"></td>
			    </tr>
			
			    <tr>
				    <td class="auto-style1">
					    <b>Project Progress</b>
				    </td>
				
				    <td class="auto-style1">
					
				    </td>
				
				    <td id="Budget" runat="server" class="auto-style1">
					    <b>Budget</b>
				    </td>
			    </tr>
			
			    <tr>
				    <td>
                        <asp:Literal ID="pProjectPercentage" runat="server"></asp:Literal>
				    </td>
				
				    <td width="20px"></td>
				
				    <td>
					    <asp:Literal ID="pBudget" runat="server"></asp:Literal>
				    </td>

                    <td width="20px"></td>
			    </tr>

                <tr>
				    <td colspan="4" style="height: 10px;"></td>
			    </tr>

                <tr id="ProfitExpense" runat="server">
                    <td class="auto-style3">
					    <b>Total Profit</b>
				    </td>
				
				    <td width="20px" class="auto-style1"></td>
				
				    <td class="auto-style1">
					    <b>Total Expenses</b>
				    </td>
                </tr>

                <tr id="ProfitExpenseLiteral" runat="server">
				    <td class="auto-style3">
                        <asp:Literal ID="pProfit" runat="server"></asp:Literal>
				    </td>
				
				    <td width="20px" class="auto-style1"></td>
				
				    <td class="auto-style1">
					    <asp:Literal ID="pCost" runat="server"></asp:Literal>
				    </td>

                    <td width="20px" class="auto-style1"></td>
			    </tr>
			
			    <tr>
				    <td colspan="4" style="height: 30px;"></td>
			    </tr>

                <tr>
                    <td colspan="4">
                        <h2 style="margin: 0;">Employees</h2><hr style="color: darkgray" />
                    </td>
                </tr>

                <tr>
                    <td colspan="4">
                        <asp:GridView ID="pEmployeesList" runat="server" AutoGenerateColumns="False" Width="100%" GridLines="None">
                            <Columns>
                                <asp:BoundField DataField="Name" HeaderText="Name" />
                                <asp:BoundField DataField="Position" HeaderText="Position" />
                                <asp:BoundField DataField="Type" HeaderText="Type" />
                                <asp:BoundField DataField="Weekly Hours" HeaderText="Weekly Hours" />
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>

                <tr>
				    <td colspan="4" style="height: 30px;"></td>
			    </tr>

                <tr>
                    <td colspan="4">
                        <h2 style="margin: 0;">Tasks</h2><hr style="color: darkgray" />
                    </td>
                </tr>
			
			    <tr>
				    <td colspan="4">
                        <asp:GridView ID="pTasks" runat="server" AutoGenerateColumns="False" Width="100%" GridLines="None">
                            <Columns>
                                <asp:BoundField DataField="Task" HeaderText="Task" >
                                <ItemStyle Width="60%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Owner" HeaderText="Owner" >
                                <ItemStyle Width="20%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Priority" HeaderText="Priority" >
                                <ItemStyle Width="10%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Status" HeaderText="Status" >
                                <ItemStyle Width="10%" />
                                </asp:BoundField>
                            </Columns>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:GridView>
				    </td>
			    </tr>

                <tr>
				    <td colspan="4" style="height: 10px;"></td>
			    </tr>

                <tr><td style="height: 20px;"></td></tr>
			
			    <tr>
				    <td colspan="4">
					    <h2 style="margin: 0;">Milestones</h2><hr style="color: darkgray" />
				    </td>
			    </tr>

                <tr>
                    <td colspan="4">
                        <asp:GridView ID="pMilestones" runat="server" AutoGenerateColumns="False" Width="100%" GridLines="None">
                            <Columns>
                                <asp:BoundField DataField="Milestone" HeaderText="Milestone">
                                <ItemStyle Width="50%" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Completed" DataField="Completed" DataFormatString="{0:0}%">
                                    <ItemStyle Width="10%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Start Date" HeaderText="Start Date">
                                <ItemStyle Width="20%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Complete Date" HeaderText="Complete Date">
                                <ItemStyle Width="20%" />
                                </asp:BoundField>
                            </Columns>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:GridView>
                    </td>
                </tr>

                <tr>
				    <td colspan="4" style="height: 10px;"></td>
			    </tr>

                <tr><td style="height: 20px;"></td></tr>
			
			    <tr>
				    <td colspan="4">
					    <h2 style="margin: 0;">Issue Log</h2><hr style="color: darkgray" />
				    </td>
			    </tr>

                <tr>
				    <td colspan="4">
                        <asp:GridView ID="pIssues" runat="server" AutoGenerateColumns="False" Width="100%" GridLines="None">
                            <Columns>
                                <asp:BoundField DataField="Issue" HeaderText="Issue">
                                <ItemStyle Width="40%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Date Identified" HeaderText="Date Identified" >
                                <ItemStyle Width="20%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Resolution" HeaderText="Resolution">
                                <ItemStyle Width="40%" />
                                </asp:BoundField>
                            </Columns>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:GridView>
				    </td>
			    </tr>

                <tr>
				    <td colspan="4" style="height: 10px;"></td>
			    </tr>

                <tr><td style="height: 20px;"></td></tr>
			
			    <tr>
				    <td colspan="4">
					    <h2 style="margin: 0;">Risk Log</h2><hr style="color: darkgray" />
				    </td>
			    </tr>

                <tr>
				    <td colspan="4">
                        <asp:GridView ID="pRisks" runat="server" AutoGenerateColumns="False" Width="100%" GridLines="None">
                            <Columns>
                                <asp:BoundField DataField="Risk" HeaderText="Risk">
                                <ItemStyle Width="25%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Date Identified" HeaderText="Date Identified">
                                <ItemStyle Width="15%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Risk Level" HeaderText="Risk Level">
                                <ItemStyle Width="15%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Owner" HeaderText="Owner">
                                <ItemStyle Width="15%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Response" HeaderText="Response">
                                <ItemStyle Width="30%" />
                                </asp:BoundField>
                            </Columns>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:GridView>
				    </td>
			    </tr>

                <tr>
				    <td colspan="4" style="height: 10px;"></td>
			    </tr>

                <tr><td style="height: 20px;"></td></tr>

                <tr id="pAdditionalDetails" runat="server">
                    <td colspan="4">
                        <h2 style="margin: 0;">Additional Details</h2><hr />
                    </td>
                </tr>

                <tr id="pProjectDetails" runat="server">
                    <td colspan="4"><b>Details</b></td>
                </tr>

                <tr id="pProjectDetailLit" runat="server">
                    <td colspan="4">
                        <asp:Literal ID="pDetails" runat="server"></asp:Literal>
                    </td>
                </tr>

                <tr>
				    <td colspan="4" style="height: 10px;"></td>
			    </tr>

                <tr id="pProjectConclusion" runat="server">
                    <td colspan="4"><b>Conclusion</b></td>
                </tr>

                <tr id="pProjectConclusionLit" runat="server">
                    <td colspan="4">
                        <asp:Literal ID="pConclusion" runat="server"></asp:Literal>
                    </td>
                </tr>
		    </tbody>
	    </table>
        </div>
    </form>
</body>
</html>
