﻿using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using System.Linq;
using System.Configuration;
using System.IO;
using System.Web.Hosting;

namespace Longview
{
    public enum FileType
    {
        FILETYPE_TIMESHEET,
        FILETYPE_PPQ,
        FILETYPE_PROGRESS,
        FILETYPE_SURVEY,
        FILETYPE_INVOICE,
        FILETYPE_STATUS
    }

    /*public enum EmployeeType
    {
        TYPE_PARTTIME,
        TYPE_FULLTIME,
    }*/

    public struct Employee
    {
        public string position;
        //public EmployeeType type;
        public decimal weeklyhours;

        public Employee(string pos, /*EmployeeType et,*/ decimal wh)
        {
            position = pos;
            //type = et;
            weeklyhours = wh;
        }
    }

    public struct Recruitment
    {
        public string position;
        public DateTime position_open;
        public decimal cost;
        public string causal_analysis;
        public List<Resume> resumes;

        public Recruitment(string s, DateTime open, decimal c, string ca, List<Resume> r)
        {
            position = s;
            position_open = open;
            cost = c;
            causal_analysis = ca;
            resumes = r;
        }

        public void AddResume(int project_id, string position, string name, string path, DateTime last_correspondence, int submitted)
        {
            using (MySqlConnection mysqlCon = new MySqlConnection(ConfigurationManager.ConnectionStrings["LocalMySqlServer"].ConnectionString))
            {
                mysqlCon.Open();
                using (MySqlCommand sqlCommand = new MySqlCommand("INSERT INTO project_recruiting_info (project_id, position, `name`," +
                    "resume_path, last_correspondence, submitted) VALUES (@param1, @param2, " +
                    "@param3, @param4, @param5, @param6);",
                    mysqlCon))
                {
                    sqlCommand.Parameters.AddWithValue("@param1", project_id);
                    sqlCommand.Parameters.AddWithValue("@param2", position);
                    sqlCommand.Parameters.AddWithValue("@param3", name);
                    sqlCommand.Parameters.AddWithValue("@param4", path);
                    sqlCommand.Parameters.AddWithValue("@param5", last_correspondence);
                    sqlCommand.Parameters.AddWithValue("@param6", submitted);
                    sqlCommand.ExecuteNonQuery();
                }
                mysqlCon.Close();
            }

            Resume r = new Resume(position, name, path, last_correspondence, submitted);
            resumes.Add(r);
        }

        public void AddResume(int project_id, Resume r)
        {
            using (MySqlConnection mysqlCon = new MySqlConnection(ConfigurationManager.ConnectionStrings["LocalMySqlServer"].ConnectionString))
            {
                mysqlCon.Open();
                using (MySqlCommand sqlCommand = new MySqlCommand("INSERT INTO project_recruiting_info (project_id, position, `name`," +
                    "resume_path, last_correspondence, submitted) VALUES (@param1, @param2, " +
                    "@param3, @param4, @param5, @param6);",
                    mysqlCon))
                {
                    sqlCommand.Parameters.AddWithValue("@param1", project_id);
                    sqlCommand.Parameters.AddWithValue("@param2", r.position);
                    sqlCommand.Parameters.AddWithValue("@param3", r.name);
                    sqlCommand.Parameters.AddWithValue("@param4", r.path);
                    sqlCommand.Parameters.AddWithValue("@param5", r.last_correspondence);
                    sqlCommand.Parameters.AddWithValue("@param6", r.submitted);
                    sqlCommand.ExecuteNonQuery();
                }
                mysqlCon.Close();
            }

            resumes.Add(r);
        }

        public void RemoveResume(int project_id, string position, string name, bool delete = true)
        {
            if (delete)
            {
                Resume r = resumes.FirstOrDefault(x => x.name == name);
                if (!r.Equals(default(Resume)))
                {
                    String nPath = HostingEnvironment.MapPath("~/" + r.path);
                    if (File.Exists(nPath))
                    {
                        File.Delete(nPath);
                    }
                }
            }

            resumes.RemoveAll(x => x.name == name);

            using (MySqlConnection mysqlCon = new MySqlConnection(ConfigurationManager.ConnectionStrings["LocalMySqlServer"].ConnectionString))
            {
                mysqlCon.Open();
                using (MySqlCommand sqlCommand = new MySqlCommand("DELETE FROM project_recruiting_info WHERE project_id = @param1 " +
                    "AND position = @param2 AND name = @param3;",
                    mysqlCon))
                {
                    sqlCommand.Parameters.AddWithValue("@param1", project_id);
                    sqlCommand.Parameters.AddWithValue("@param2", position);
                    sqlCommand.Parameters.AddWithValue("@param3", name);
                    sqlCommand.ExecuteNonQuery();
                }
                mysqlCon.Close();
            }
        }
    }

    public struct Resume
    {
        public string position;
        public string name;
        public string path;
        public DateTime last_correspondence;
        public int submitted;

        public Resume(string p, string n, string pa, DateTime lc, int sub)
        {
            position = p;
            name = n;
            path = pa;
            last_correspondence = lc;
            submitted = sub;
        }
    }

    public struct ProjectFile
    {
        public string path;
        public FileType type;

        public ProjectFile(string s, FileType ft)
        {
            path = s;
            type = ft;
        }
    }

    public class Project
    {
        private int pId;
        private int pProgressPercentage;
        private string pName;
        private string proj_contact_name;
        private string proj_contact_phone;
        private string proj_contact_email;
        private string pSummary;
        private string pProjectScope;
        private string pProductScope;
        private string pDetails;
        private string pConclusion;
        private string pAuthor;
        private decimal pBudget;
        private decimal pCost;
        private decimal pProfit;
        private decimal pMonthlyCost;
        private decimal pMonthlyProfit;
        private DateTime pProgressUpdated;
        private DateTime pStartDate;
        private DateTime pCompleteDate;
        private DateTime pStatusUpdated;
        private ProjectFile StatusReport;
        private List<Recruitment> recruiting;
        private Dictionary<int, string[]> actions = new Dictionary<int, string[]> { };
        private Dictionary<int, string[]> milestones = new Dictionary<int, string[]> { };
        private Dictionary<int, string[]> issues = new Dictionary<int, string[]> { };
        private Dictionary<int, string[]> risks = new Dictionary<int, string[]> { };
        private Dictionary<string, decimal[]> financial = new Dictionary<string, decimal[]> { };
        private Dictionary<string, Employee> employees = new Dictionary<string, Employee> { };
        private Dictionary<string, ProjectFile> files = new Dictionary<string, ProjectFile> { };

        public Project(int id, string name, string contact_name, string contact_phone, string contact_email,
            string summary, string start_date, string completeDate, string updated_date, string details = "",
            string conclusion = "", string project_scope = "", string product_scope = "", string author = "",
            decimal budget = 0, decimal profit = 0, int progress_percentage = 0)
        {
            pId = id;
            pName = name;
            proj_contact_name = contact_name;
            proj_contact_phone = contact_phone;
            proj_contact_email = contact_email;
            pSummary = summary;
            pProjectScope = project_scope;
            pProductScope = product_scope;
            pDetails = details;
            pConclusion = conclusion;
            pStartDate = DateTime.Parse(start_date);
            pCompleteDate = DateTime.Parse(completeDate);
            pProgressUpdated = DateTime.Parse(updated_date);
            pAuthor = author;
            pBudget = budget;
            pProfit = profit;
            pProgressPercentage = progress_percentage;
            pMonthlyCost = 0;
            pMonthlyProfit = 0;
            StatusReport = new ProjectFile("null", FileType.FILETYPE_STATUS);
            pStatusUpdated = DateTime.MinValue;

            LoadActions();
            LoadFinancial();
            LoadFiles();
            LoadEmployees();
        }

        public void UpdateInfo(string contact_name, string contact_phone, string contact_email,
            DateTime start_date, DateTime complete_date, decimal budget)
        {
            using (MySqlConnection mysqlCon = new MySqlConnection(ConfigurationManager.ConnectionStrings["LocalMySqlServer"].ConnectionString))
            {
                mysqlCon.Open();
                using (MySqlCommand sqlCommand = new MySqlCommand("UPDATE project SET project_manager = @param1, " +
                    "project_phone = @param2, project_email = @param3," +
                    "start_date = @param4, completion_date = @param5, budget = @param6 WHERE id = @param7;",
                    mysqlCon))
                {
                    sqlCommand.Parameters.AddWithValue("@param1", contact_name);
                    sqlCommand.Parameters.AddWithValue("@param2", contact_phone);
                    sqlCommand.Parameters.AddWithValue("@param3", contact_email);
                    sqlCommand.Parameters.AddWithValue("@param4", start_date);
                    sqlCommand.Parameters.AddWithValue("@param5", complete_date);
                    sqlCommand.Parameters.AddWithValue("@param6", budget);
                    sqlCommand.Parameters.AddWithValue("@param7", pId);

                    sqlCommand.ExecuteNonQuery();
                }
                mysqlCon.Close();
            }

            proj_contact_name = contact_name;
            proj_contact_email = contact_email;
            proj_contact_phone = contact_phone;
            pStartDate = start_date;
            pCompleteDate = complete_date;
            pBudget = budget;
        }

        public void UpdateInfo(string summary, string project_scope, string product_scope, string details,
            string conclusion, DateTime startDate, DateTime completeDate, DateTime updatedDate, string author, decimal budget,
            int progress_percentage,
            Dictionary<int, string[]> task_list = null,
            Dictionary<int, string[]> milestone_list = null, Dictionary<int, string[]> issue_list = null,
            Dictionary<int, string[]> risk_list = null, Dictionary<string, decimal[]> financial_list = null,
            Dictionary<string, Employee> employee_list = null, List<MySqlCommand> data = null)
        {
            pSummary = summary;
            pProjectScope = project_scope;
            pProductScope = product_scope;
            pDetails = details;
            pConclusion = conclusion;
            pStartDate = startDate;
            pCompleteDate = completeDate;
            pProgressUpdated = updatedDate;
            pAuthor = author;
            pBudget = budget;
            pProgressPercentage = progress_percentage;

            if (data == null)
                return;

            using (MySqlConnection mysqlCon = new MySqlConnection(ConfigurationManager.ConnectionStrings["LocalMySqlServer"].ConnectionString))
            {
                mysqlCon.Open();

                using (MySqlCommand command = new MySqlCommand("UPDATE project SET last_updated = @param1, progress_author = @param2 WHERE id = @param3;", mysqlCon))
                {
                    command.Parameters.AddWithValue("@param1", updatedDate.ToString("yyyy-MM-dd"));
                    command.Parameters.AddWithValue("@param2", author);
                    command.Parameters.AddWithValue("@param3", pId);
                    command.ExecuteNonQuery();
                }

                foreach (MySqlCommand cmd in data)
                {
                    try
                    {
                        cmd.Connection = mysqlCon;
                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                    }
                    catch (Exception ex)
                    {
                        continue;
                    }
                }

                actions = task_list != null ? task_list : actions;
                milestones = milestone_list != null ? milestone_list : milestones;
                issues = issue_list != null ? issue_list : issues;
                risks = risk_list != null ? risk_list : risks;
                financial = financial_list != null ? financial_list : financial;
                RecalculateFinances();
                employees = employee_list != null ? employee_list : employees;

                mysqlCon.Close();
            }
        }

        public void RecalculateFinances()
        {
            pCost = 0;
            pProfit = 0;
            Dictionary<string, decimal> monthlySpending = new Dictionary<string, decimal>();
            Dictionary<string, decimal> monthlyProfit = new Dictionary<string, decimal>();
            foreach (KeyValuePair<string, decimal[]> kvp in financial)
            {
                pCost += (kvp.Value[0] + kvp.Value[1] + kvp.Value[2] + kvp.Value[3] + kvp.Value[4] +
                            kvp.Value[5]);

                pProfit += (kvp.Value[6] + kvp.Value[7] + kvp.Value[8]);

                string[] month = kvp.Key.Split(' ');
                if (monthlySpending.Keys.Contains(month[1] + " " + month[2]))
                {
                    monthlySpending[month[1] + " " + month[2]] += (kvp.Value[0] + kvp.Value[1] + kvp.Value[2] +
                        kvp.Value[3] + kvp.Value[4] + kvp.Value[5]);
                }
                else
                {
                    monthlySpending.Add(month[1] + " " + month[2], (kvp.Value[0] + kvp.Value[1] + kvp.Value[2] +
                        kvp.Value[3] + kvp.Value[4] + kvp.Value[5]));
                }

                if (monthlyProfit.Keys.Contains(month[1] + " " + month[2]))
                {
                    monthlyProfit[month[1] + " " + month[2]] += (kvp.Value[6] + kvp.Value[7] + kvp.Value[8]);
                }
                else
                {
                    monthlyProfit.Add(month[1] + " " + month[2], (kvp.Value[6] + kvp.Value[7] + kvp.Value[8]));
                }
            }

            pMonthlyCost = Math.Round(monthlySpending.Values.Average(), 2, MidpointRounding.AwayFromZero);
            pMonthlyProfit = Math.Round(monthlyProfit.Values.Average(), 2, MidpointRounding.AwayFromZero);

            ProjectsManager.CalculateFinances();
        }

        private void LoadFinancial()
        {
            using (MySqlConnection mysqlCon = new MySqlConnection(ConfigurationManager.ConnectionStrings["LocalMySqlServer"].ConnectionString))
            {
                mysqlCon.Open();
                using (MySqlCommand sqlCommand = new MySqlCommand("SELECT year, month, day, expense_products, expense_labor, " +
                    "expense_operation, expense_marketing, expense_taxes, expense_other, profit_products, profit_services, " +
                    "profit_other FROM project_financial WHERE project_id = ?param1;", mysqlCon))
                {
                    sqlCommand.Parameters.AddWithValue("param1", pId);

                    using (MySqlDataReader dataReader = sqlCommand.ExecuteReader())
                    {
                        if (dataReader.HasRows)
                        {
                            while (dataReader.Read())
                            {
                                decimal[] fin = { dataReader.GetDecimal(3), dataReader.GetDecimal(4), dataReader.GetDecimal(5),
                            dataReader.GetDecimal(6),dataReader.GetDecimal(7),dataReader.GetDecimal(8),dataReader.GetDecimal(9),
                            dataReader.GetDecimal(10), dataReader.GetDecimal(11) };
                                financial.Add(dataReader.GetString(2) + " " + dataReader.GetString(1) + " " + dataReader.GetString(0),
                                    fin);
                                Company.AddFinancial(fin);
                            }
                            RecalculateFinances();
                        }
                    }
                }
                mysqlCon.Close();
            }
        }

        private void LoadActions()
        {
            using (MySqlConnection mysqlCon = new MySqlConnection(ConfigurationManager.ConnectionStrings["LocalMySqlServer"].ConnectionString))
            {
                mysqlCon.Open();
                using (MySqlCommand sqlCommand = new MySqlCommand("SELECT id, action, owner, priority, status FROM project_actions WHERE project_id = ?param1 LIMIT 10;", mysqlCon))
                {
                    sqlCommand.Parameters.AddWithValue("param1", pId);

                    using (MySqlDataReader dataReader = sqlCommand.ExecuteReader())
                    {
                        if (dataReader.HasRows)
                        {
                            while (dataReader.Read())
                            {
                                string[] s = { dataReader.IsDBNull(1) ? "" : dataReader.GetString(1), dataReader.IsDBNull(2) ? "" : dataReader.GetString(2), dataReader.GetString(3), dataReader.GetString(4) };
                                actions.Add(dataReader.GetInt32(0), s);
                            }
                        }
                    }
                }

                using (MySqlCommand sqlCommand = new MySqlCommand("SELECT id, issue, date_identified, resolution FROM project_issue WHERE project_id = ?param1 LIMIT 10;", mysqlCon))
                {
                    sqlCommand.Parameters.AddWithValue("param1", pId);

                    using (MySqlDataReader dataReader = sqlCommand.ExecuteReader())
                    {
                        if (dataReader.HasRows)
                        {
                            while (dataReader.Read())
                            {
                                string[] s = { dataReader.IsDBNull(1) ? "" : dataReader.GetString(1), DateTime.Parse(dataReader.GetString(2)).ToString("yyyy-MM-dd"), dataReader.IsDBNull(3) ? "" : dataReader.GetString(3) };
                                issues.Add(dataReader.GetInt32(0), s);
                            }
                        }
                    }
                }

                using (MySqlCommand sqlCommand = new MySqlCommand("SELECT id, milestone, startdate, completedate, percent_complete FROM project_milestone WHERE project_id = ?param1;", mysqlCon))
                {
                    sqlCommand.Parameters.AddWithValue("param1", pId);

                    using (MySqlDataReader dataReader = sqlCommand.ExecuteReader())
                    {
                        if (dataReader.HasRows)
                        {
                            while (dataReader.Read())
                            {
                                string[] s = { dataReader.IsDBNull(1) ? "" : dataReader.GetString(1), dataReader.GetString(4), DateTime.Parse(dataReader.GetString(2)).ToString("yyyy-MM-dd"), DateTime.Parse(dataReader.GetString(3)).ToString("yyyy/MM/dd") };
                                milestones.Add(dataReader.GetInt32(0), s);
                            }
                        }
                    }
                }

                using (MySqlCommand sqlCommand = new MySqlCommand("SELECT id, risk, date_identified, risk_level, owner, response FROM project_risk WHERE project_id = ?param1 LIMIT 10;", mysqlCon))
                {
                    sqlCommand.Parameters.AddWithValue("param1", pId);

                    using (MySqlDataReader dataReader = sqlCommand.ExecuteReader())
                    {
                        if (dataReader.HasRows)
                        {
                            while (dataReader.Read())
                            {
                                string[] s = { dataReader.IsDBNull(1) ? "" : dataReader.GetString(1), DateTime.Parse(dataReader.GetString(2)).ToString("yyyy-MM-dd"), dataReader.GetString(3), dataReader.IsDBNull(4) ? "" : dataReader.GetString(4), dataReader.IsDBNull(5) ? "" : dataReader.GetString(5) };
                                risks.Add(dataReader.GetInt32(0), s);
                            }
                        }
                    }
                }

                using (MySqlCommand sqlCommand = new MySqlCommand("SELECT position,position_open_since,cost,causal_analysis FROM project_recruiting WHERE project_id = ?param1 LIMIT 10;", mysqlCon))
                {
                    recruiting = new List<Recruitment>();
                    List<Resume> rec;
                    sqlCommand.Parameters.AddWithValue("param1", pId);

                    using (MySqlDataReader dataReader = sqlCommand.ExecuteReader())
                    {
                        if (dataReader.HasRows)
                        {
                            while (dataReader.Read())
                            {
                                using (MySqlConnection connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["LocalMySqlServer"].ConnectionString))
                                {
                                    connection.Open();
                                    rec = new List<Resume>();
                                    using (MySqlCommand cmd = new MySqlCommand("SELECT position, `name`, resume_path, last_correspondence, " +
                                        "submitted FROM project_recruiting_info WHERE project_id = ?param1 AND position = ?param2;", connection))
                                    {
                                        cmd.Parameters.AddWithValue("param1", pId);
                                        cmd.Parameters.AddWithValue("param2", dataReader.IsDBNull(0) ? "" : dataReader.GetString(0));
                                        using (MySqlDataReader dr = cmd.ExecuteReader())
                                        {
                                            while (dr.Read())
                                            {
                                                rec.Add(new Resume(dr.GetString(0), dr.GetString(1), dr.GetString(2), dr.GetDateTime(3), dr.GetInt32(4)));
                                            }
                                        }
                                    }
                                    connection.Close();
                                }

                                Recruitment r = new Recruitment(dataReader.IsDBNull(0) ? "" : dataReader.GetString(0), dataReader.GetDateTime(1), dataReader.GetDecimal(2),
                                    dataReader.IsDBNull(3) ? "" : dataReader.GetString(3), rec);
                                recruiting.Add(r);
                            }
                        }
                    }
                }
                mysqlCon.Close();
            }
        }

        public void LoadEmployees()
        {
            using (MySqlConnection mysqlCon = new MySqlConnection(ConfigurationManager.ConnectionStrings["LocalMySqlServer"].ConnectionString))
            {
                mysqlCon.Open();
                using (MySqlCommand sqlCommand = new MySqlCommand("SELECT `employee`, `position`, `hours` FROM project_employee WHERE project_id = ?param1;", mysqlCon))
                {
                    sqlCommand.Parameters.AddWithValue("?param1", pId);

                    using (MySqlDataReader dataReader = sqlCommand.ExecuteReader())
                    {
                        if (dataReader.HasRows)
                        {
                            while (dataReader.Read())
                            {
                                Employee e = new Employee(dataReader.GetString(1), dataReader.GetDecimal(2));
                                employees.Add(dataReader.GetString(0), e);
                            }
                        }
                    }

                    int affected = sqlCommand.ExecuteNonQuery();
                }
                mysqlCon.Close();
            }
        }

        private void LoadFiles()
        {
            using (MySqlConnection mysqlCon = new MySqlConnection(ConfigurationManager.ConnectionStrings["LocalMySqlServer"].ConnectionString))
            {
                mysqlCon.Open();
                using (MySqlCommand sqlCommand = new MySqlCommand("SELECT `name`, `path`, `type`, `date` FROM project_files WHERE project_id = ?param1;", mysqlCon))
                {
                    sqlCommand.Parameters.AddWithValue("?param1", pId);

                    using (MySqlDataReader dataReader = sqlCommand.ExecuteReader())
                    {
                        if (dataReader.HasRows)
                        {
                            while (dataReader.Read())
                            {
                                ProjectFile pf = new ProjectFile(dataReader.GetString(1), (FileType)dataReader.GetInt32(2));
                                if (pf.type == FileType.FILETYPE_STATUS)
                                {
                                    StatusReport = pf;
                                    pStatusUpdated = dataReader.GetDateTime(3);
                                }

                                files.Add(dataReader.GetString(0), pf);
                            }
                        }
                    }

                    int affected = sqlCommand.ExecuteNonQuery();
                }
                mysqlCon.Close();
            }
        }

        public void DeleteRecruiting(string name)
        {
            recruiting.Remove(recruiting.Where(x => x.position == name).First());
            using (MySqlConnection mysqlCon = new MySqlConnection(ConfigurationManager.ConnectionStrings["LocalMySqlServer"].ConnectionString))
            {
                mysqlCon.Open();
                using (MySqlCommand sqlCommand = new MySqlCommand("DELETE FROM project_recruiting WHERE position = @param1 AND project_id = @param2;", mysqlCon))
                {
                    sqlCommand.Parameters.AddWithValue("@param1", name);
                    sqlCommand.Parameters.AddWithValue("@param2", pId);

                    sqlCommand.ExecuteNonQuery();
                }

                using (MySqlCommand sqlCommand = new MySqlCommand("DELETE FROM project_recruiting_info WHERE position = @param1 AND project_id = @param2;", mysqlCon))
                {
                    sqlCommand.Parameters.AddWithValue("@param1", name);
                    sqlCommand.Parameters.AddWithValue("@param2", pId);

                    sqlCommand.ExecuteNonQuery();
                }
                mysqlCon.Close();
            }
        }

        public bool UpdateRecruiting(string position, DateTime open_since, decimal opportunity_cost,
            string causal_analysis, List<Resume> rec)
        {
            Recruitment r = new Recruitment(position, open_since, opportunity_cost, causal_analysis, rec);
            if (recruiting.Where(x => x.Equals(r)).Count() > 0)
                return false;

            if (recruiting.Where(x => x.position == position).Count() > 0)
            {
                recruiting.RemoveAll(x => x.position == position);
                recruiting.Add(r);
                using (MySqlConnection mysqlCon = new MySqlConnection(ConfigurationManager.ConnectionStrings["LocalMySqlServer"].ConnectionString))
                {
                    mysqlCon.Open();
                    using (MySqlCommand sqlCommand = new MySqlCommand("UPDATE project_recruiting SET " +
                        "position_open_since = @param1, cost = @param2, causal_analysis = @param3 WHERE project_id = @param4 AND " +
                        "position = @param5;",
                        mysqlCon))
                    {
                        sqlCommand.Parameters.AddWithValue("@param1", open_since);
                        sqlCommand.Parameters.AddWithValue("@param2", opportunity_cost);
                        sqlCommand.Parameters.AddWithValue("@param3", causal_analysis);
                        sqlCommand.Parameters.AddWithValue("@param4", pId);
                        sqlCommand.Parameters.AddWithValue("@param5", position);

                        try
                        {
                            sqlCommand.ExecuteNonQuery();
                        }
                        catch(Exception ex)
                        {
                            return false;
                        }
                    }
                    mysqlCon.Close();
                }
            }
            else
            {
                recruiting.Add(r);
                using (MySqlConnection mysqlCon = new MySqlConnection(ConfigurationManager.ConnectionStrings["LocalMySqlServer"].ConnectionString))
                {
                    mysqlCon.Open();
                    using (MySqlCommand sqlCommand = new MySqlCommand("INSERT INTO project_recruiting (`project_id`, `position`, `position_open_since`, `cost`, `causal_analysis`) VALUES (@param1, @param2, @param3, @param4, @param5);", mysqlCon))
                    {
                        sqlCommand.Parameters.AddWithValue("@param1", pId);
                        sqlCommand.Parameters.AddWithValue("@param2", position);
                        sqlCommand.Parameters.AddWithValue("@param3", open_since);
                        sqlCommand.Parameters.AddWithValue("@param4", opportunity_cost);
                        sqlCommand.Parameters.AddWithValue("@param5", causal_analysis);

                        try
                        {
                            sqlCommand.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            return false;
                        }
                    }
                    mysqlCon.Close();
                }
            }
            return true;
        }

        public void addFile(string name, ProjectFile file)
        {
            files.Add(name, file);
            using (MySqlConnection mysqlCon = new MySqlConnection(ConfigurationManager.ConnectionStrings["LocalMySqlServer"].ConnectionString))
            {
                mysqlCon.Open();
                using (MySqlCommand sqlCommand = new MySqlCommand("INSERT INTO project_files (`project_id`, `name`, `path`, `type`) VALUES (@param1, @param2, @param3, @param4);", mysqlCon))
                {
                    sqlCommand.Parameters.AddWithValue("@param1", pId);
                    sqlCommand.Parameters.AddWithValue("@param2", name);
                    sqlCommand.Parameters.AddWithValue("@param3", file.path);
                    sqlCommand.Parameters.AddWithValue("@param4", (int)file.type);

                    sqlCommand.ExecuteNonQuery();
                }
                mysqlCon.Close();
            }
        }

        public void removeFile(string name)
        {
            files.Remove(name);
            using (MySqlConnection mysqlCon = new MySqlConnection(ConfigurationManager.ConnectionStrings["LocalMySqlServer"].ConnectionString))
            {
                mysqlCon.Open();
                using (MySqlCommand sqlCommand = new MySqlCommand("DELETE FROM project_files WHERE name = @param1 AND project_id = @param2;", mysqlCon))
                {
                    sqlCommand.Parameters.AddWithValue("@param1", name);
                    sqlCommand.Parameters.AddWithValue("@param2", pId);
                    sqlCommand.ExecuteNonQuery();
                }
                mysqlCon.Close();
            }
        }

        public int getId() { return pId; }
        public int getProjectProgress() { return pProgressPercentage; }
        public string getSummary() { return pSummary; }
        public string getProjectScope() { return pProjectScope; }
        public string getProductScope() { return pProductScope; }
        public string getDetails() { return pDetails; }
        public string getConclusion() { return pConclusion; }
        public string getName() { return pName; }
        public string getContactName() { return proj_contact_name; }
        public string getContactPhone() { return proj_contact_phone; }
        public string getContactEmail() { return proj_contact_email; }
        public string getAuthor() { return pAuthor; }
        public decimal getBudget() { return pBudget; }
        public decimal getCost() { return pCost; }
        public decimal getProfit() { return pProfit; }
        public decimal getMonthlyCost() { return pMonthlyCost; }
        public decimal getMonthlyProfit() { return pMonthlyProfit; }
        public ProjectFile getStatusReport() { return StatusReport; }
        public DateTime getStartDate() { return pStartDate; }
        public DateTime getCompleteDate() { return pCompleteDate; }
        public DateTime getLastUpdated() { return pProgressUpdated; }
        public DateTime getStatusUpdated() { return pStatusUpdated; }
        public List<Recruitment> getRecruitment() { return recruiting; }
        public Dictionary<int, string[]> getActions() { return actions; }
        public Dictionary<int, string[]> getIssues() { return issues; }
        public Dictionary<int, string[]> getRisks() { return risks; }
        public Dictionary<int, string[]> getMilestones() { return milestones; }
        public Dictionary<string, decimal[]> getFinancial() { return financial; }
        public Dictionary<string, Employee> getEmployees() { return employees; }
        public Dictionary<string, ProjectFile> getFiles() { return files; }

        public void setContactInfo(string contact_name, string contact_phone, string contact_email)
        {
            using (MySqlConnection mysqlCon = new MySqlConnection(ConfigurationManager.ConnectionStrings["LocalMySqlServer"].ConnectionString))
            {
                mysqlCon.Open();
                try
                {
                    using (MySqlCommand sqlCommand = new MySqlCommand("UPDATE project SET project_manager = @param1, project_phone = @param2, project_email = @param3 WHERE id = @param4;", mysqlCon))
                    {
                        sqlCommand.Parameters.AddWithValue("@param1", contact_name);
                        sqlCommand.Parameters.AddWithValue("@param2", contact_phone);
                        sqlCommand.Parameters.AddWithValue("@param3", contact_email);
                        sqlCommand.Parameters.AddWithValue("@param4", pId);

                        int affected = sqlCommand.ExecuteNonQuery();
                        if (affected > 0)
                        {
                            proj_contact_name = contact_name;
                            proj_contact_phone = contact_phone;
                            proj_contact_email = contact_email;
                        }
                    }
                }
                finally
                {
                    mysqlCon.Close();
                }
            }
        }

        public void setStatusReport(string path)
        {
            using (MySqlConnection mysqlCon = new MySqlConnection(ConfigurationManager.ConnectionStrings["LocalMySqlServer"].ConnectionString))
            {
                mysqlCon.Open();
                using (MySqlCommand sqlCommand = new MySqlCommand("DELETE FROM project_files WHERE project_id = @param1 AND type = @param2;", mysqlCon))
                {
                    sqlCommand.Parameters.AddWithValue("@param1", pId);
                    sqlCommand.Parameters.AddWithValue("@param2", (int)FileType.FILETYPE_STATUS);
                    sqlCommand.ExecuteNonQuery();
                }

                using (MySqlCommand sqlCommand = new MySqlCommand("INSERT INTO project_files (`project_id`, `name`, `path`, `type`, `date`) VALUES (@param1, @param2, @param3, @param4, @param5);", mysqlCon))
                {
                    sqlCommand.Parameters.AddWithValue("@param1", pId);
                    sqlCommand.Parameters.AddWithValue("@param2", getName() + " Status Report");
                    sqlCommand.Parameters.AddWithValue("@param3", path);
                    sqlCommand.Parameters.AddWithValue("@param4", (int)FileType.FILETYPE_STATUS);
                    sqlCommand.Parameters.AddWithValue("@param5", DateTime.Now);
                    sqlCommand.ExecuteNonQuery();
                }
                mysqlCon.Close();
            }
            StatusReport = new ProjectFile(path, FileType.FILETYPE_STATUS);
            pStatusUpdated = DateTime.Now;
        }
    }
}
