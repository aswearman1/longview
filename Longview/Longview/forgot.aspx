﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Forgot.aspx.cs" Inherits="Longview.forgot" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="Content/bootstrap.css" rel="stylesheet" id="bootstrap-css" />
</head>
<body>
    <form id="form1" runat="server">
    <div style="font-family: Raleway;">
        <table cellpadding="0" style="width: 100%;">
            <tr>
                <td colspan="4">
                    <h2 style="text-align: center;">Forgot Password</h2>
                </td>
            </tr>
        
            <tr>
                <td colspan="4">
                    <p style="text-align: center;">Enter your User Name in the box below. We will send a link to the email associated with this account. Follow the directions in the email to reset your password.</p>
                </td>
            </tr>

            <tr>
                <td style="width: 25%"></td>

                <td align="center">
                    User Name: <asp:TextBox ID="unreq" runat="server" ValidateRequestMode="Enabled"></asp:TextBox><asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="unreq" ErrorMessage="User Name is required." ToolTip="User Name is required." ValidationGroup="emailSend" Font-Bold="True" Font-Size="Large" ValidateRequestMode="Enabled">*</asp:RequiredFieldValidator>
                </td>

                <td style="width: 25%"></td>
            </tr>

            <tr style="height: 10px;">
                <td align="center" colspan="4">
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="The username contains invalid characters." ControlToValidate="unreq" ValidationGroup="emailSend" ValidationExpression="^[0-9A-Za-z]*$" Display="None"></asp:RegularExpressionValidator>
                </td>
            </tr>

            <tr>
                <td align="center" colspan="4">
                    <asp:Button ID="send" runat="server" Text="Submit" causesvalidation="true" ValidationGroup="emailSend" CssClass="btn-default btn-sm" ForeColor="White" Height="30px" BackColor="#0D0435" OnClick="send_Click" />
                </td>
            </tr>

            <tr>
                <td align="center" colspan="4">
                    <asp:CustomValidator ID="CustomValidator1" runat="server" ValidationGroup="emailSend" ControlToValidate="unreq" Display="None"></asp:CustomValidator>
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="emailSend" DisplayMode="List" ForeColor="Red" />
                    <p id="completeText" runat="server"></p>
                </td>
            </tr>
        </table>

    </div>
    </form>
</body>
</html>
