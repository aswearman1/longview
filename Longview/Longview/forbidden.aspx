﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Forbidden.aspx.cs" Inherits="Longview.forbidden" %>

<!DOCTYPE html>
<html>
<head>
    <title>403 Forbidden</title>
	<meta charset="utf-8" />
</head>
<body>
    <h1>Forbidden</h1>
    <p>You don't have permission to access this page.</p>
</body>
</html>
