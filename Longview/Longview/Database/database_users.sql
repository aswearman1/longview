/*
Navicat MySQL Data Transfer

Source Server         : Longeviti
Source Server Version : 80011
Source Host           : localhost:3306
Source Database       : users

Target Server Type    : MYSQL
Target Server Version : 80011
File Encoding         : 65001

Date: 2019-03-12 14:42:08
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for actions
-- ----------------------------
DROP TABLE IF EXISTS `actions`;
CREATE TABLE `actions` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `action` varchar(255) DEFAULT NULL,
  `actor` varchar(255) DEFAULT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of actions
-- ----------------------------
INSERT INTO `actions` VALUES ('1', 'Do something', 'Gary Webb', '2018-09-03', '12:03:00');
INSERT INTO `actions` VALUES ('2', 'Do something else', 'Austin Swearman', '2018-09-03', '13:58:00');

-- ----------------------------
-- Table structure for company_employee
-- ----------------------------
DROP TABLE IF EXISTS `company_employee`;
CREATE TABLE `company_employee` (
  `EmployeeID` int(11) NOT NULL,
  `LastName` varchar(255) NOT NULL,
  `FirstName` varchar(255) NOT NULL,
  `Position` varchar(255) DEFAULT NULL,
  `StartDate` date DEFAULT NULL,
  `WeeklyHours` decimal(10,0) DEFAULT '0',
  `HourlyRate` decimal(10,0) DEFAULT '0',
  `Rewards` int(11) DEFAULT '0',
  `Recommendations` int(11) DEFAULT '0',
  `Reprimands` int(11) DEFAULT '0',
  `CorrectiveActions` int(11) DEFAULT '0',
  `H&W Rate` decimal(10,0) DEFAULT '0',
  `SCA` enum('no','yes') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'no',
  PRIMARY KEY (`EmployeeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of company_employee
-- ----------------------------
INSERT INTO `company_employee` VALUES ('3', 'Lewith', 'Torque', 'Blue', '2017-08-10', '25', '20', '0', '0', '0', '0', '0', 'no');
INSERT INTO `company_employee` VALUES ('987', 'T. Billings-Clyde', 'D\'Isiah', 'Coastal Carolina Univ.', '2018-06-06', '40', '25', '0', '0', '0', '0', '0', 'no');
INSERT INTO `company_employee` VALUES ('1080', 'Swearman', 'Austin', 'Programmer', '2018-07-29', '20', '25', '0', '0', '0', '0', '0', 'no');
INSERT INTO `company_employee` VALUES ('1234', 'Webb', 'Gary', 'Project Manager', '2016-05-30', '60', '100', '0', '0', '0', '0', '0', 'no');
INSERT INTO `company_employee` VALUES ('4321', 'Tacktheritrix', 'Jackmerius', 'Project Manager', '2016-08-15', '40', '55', '0', '0', '0', '0', '0', 'no');
INSERT INTO `company_employee` VALUES ('7654', 'Maxwell Jilliumz', 'Leoz', 'East Carolina Univ.', '2019-02-01', '75', '80', '0', '0', '0', '0', '0', 'no');
INSERT INTO `company_employee` VALUES ('9348', 'Jamar Javarison-Lamar', 'Javaris', 'University of Middle Tennessee', '2019-02-14', '39', '10', '1', '0', '1', '2', '0', 'no');

-- ----------------------------
-- Table structure for my_aspnet_applications
-- ----------------------------
DROP TABLE IF EXISTS `my_aspnet_applications`;
CREATE TABLE `my_aspnet_applications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) DEFAULT NULL,
  `description` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of my_aspnet_applications
-- ----------------------------
INSERT INTO `my_aspnet_applications` VALUES ('1', 'MySqlMembershipTest', 'MySQLdefaultapplication');
INSERT INTO `my_aspnet_applications` VALUES ('2', '/', 'MySQL Session State Store Provider');

-- ----------------------------
-- Table structure for my_aspnet_membership
-- ----------------------------
DROP TABLE IF EXISTS `my_aspnet_membership`;
CREATE TABLE `my_aspnet_membership` (
  `userId` int(11) NOT NULL,
  `Email` varchar(128) DEFAULT NULL,
  `Comment` varchar(255) DEFAULT NULL,
  `Password` varchar(128) NOT NULL,
  `PasswordKey` char(32) DEFAULT NULL,
  `PasswordFormat` tinyint(4) DEFAULT NULL,
  `PasswordQuestion` varchar(255) DEFAULT NULL,
  `PasswordAnswer` varchar(255) DEFAULT NULL,
  `IsApproved` tinyint(1) DEFAULT NULL,
  `LastActivityDate` datetime DEFAULT NULL,
  `LastLoginDate` datetime DEFAULT NULL,
  `LastPasswordChangedDate` datetime DEFAULT NULL,
  `CreationDate` datetime DEFAULT NULL,
  `IsLockedOut` tinyint(1) DEFAULT NULL,
  `LastLockedOutDate` datetime DEFAULT NULL,
  `FailedPasswordAttemptCount` int(10) unsigned DEFAULT NULL,
  `FailedPasswordAttemptWindowStart` datetime DEFAULT NULL,
  `FailedPasswordAnswerAttemptCount` int(10) unsigned DEFAULT NULL,
  `FailedPasswordAnswerAttemptWindowStart` datetime DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='2';

-- ----------------------------
-- Records of my_aspnet_membership
-- ----------------------------
INSERT INTO `my_aspnet_membership` VALUES ('1', 'austinswearman@msn.com', '', 'asdfqwer1234', '4plp7LeNqiVapYZeFaJZfA==', '0', null, null, '1', '2019-03-12 11:44:28', '2019-03-12 11:44:28', '2018-12-19 15:42:25', '2018-06-06 13:52:28', '0', '2018-12-19 15:43:56', '2', '2019-02-20 14:54:09', '0', '2018-06-06 13:52:28');
INSERT INTO `my_aspnet_membership` VALUES ('30', 'gwebb@longevitillc.com', '', 'niY+hjAlAvryH2Vj/askN+OTe4faxjV6sTDvTbv+FTY=', 'Pzlu2bxHmkWCg1T0E/29xw==', '1', null, null, '1', '2019-02-28 12:19:14', '2019-02-28 12:19:14', '2018-08-28 16:16:27', '2018-08-28 16:16:27', '0', '2018-08-28 16:16:27', '0', '2018-08-28 16:16:27', '0', '2018-08-28 16:16:27');
INSERT INTO `my_aspnet_membership` VALUES ('31', 'gwebb514@gmail.com', '', '9jA9mjtHEXZWwCKdoCDx7cicxx+1SdJ16Bmk3d14a88=', 'pH41RtHhqVWs0gqYb/5JeQ==', '1', null, null, '1', '2018-09-05 14:59:46', '2018-09-05 14:59:46', '2018-09-05 14:58:37', '2018-09-05 14:58:37', '0', '2018-09-05 14:58:37', '0', '2018-09-05 14:58:37', '0', '2018-09-05 14:58:37');
INSERT INTO `my_aspnet_membership` VALUES ('32', 'aelong@aelytics.com', 'fBbK/0XdJiF/pHwIyhxjDg==;;9/12/2018 9:05:41 PM', 'I0Bbya4R4zKT4Iz8APkgzr8QCjQxerBT+UsTbkDPJPo=', 'ltmO8zhUvUq3Xg3+G9B0kQ==', '1', null, null, '1', '2018-09-11 14:32:00', '2018-09-11 14:32:00', '2018-09-07 23:25:57', '2018-09-07 23:25:57', '0', '2019-01-03 16:18:07', '0', '2018-09-12 17:04:15', '0', '2018-09-07 23:25:57');
INSERT INTO `my_aspnet_membership` VALUES ('33', 'mjackson@longevitillc.com', '', 'SzuOOTYgLu8jvIGgv8HQD4mg5szztj0L9Ba2BcyAVv0=', 'efNrX0Gbp9FwV3lg47K9BA==', '1', null, null, '1', '2018-11-30 11:42:19', '2018-11-30 11:42:19', '2018-09-12 15:31:16', '2018-09-12 15:31:16', '0', '2018-09-12 15:31:16', '0', '2018-09-12 15:31:16', '0', '2018-09-12 15:31:16');
INSERT INTO `my_aspnet_membership` VALUES ('34', 'aelong@longevitillc.com', '', 'eE03MSW0SNIbpHfvLGlJxyMViB083mqobPuUvLxcCX4=', 'h/dFjxahxirtZrCXIUFKpQ==', '1', null, null, '1', '2018-09-18 11:23:50', '2018-09-18 11:23:50', '2018-09-12 17:07:23', '2018-09-12 17:07:23', '0', '2018-09-12 17:07:23', '0', '2018-09-12 17:07:23', '0', '2018-09-12 17:07:23');
INSERT INTO `my_aspnet_membership` VALUES ('35', 'bsmith@longevitillc.com', '', '+8RW9PGAFvg13pt4A6/auWAcsc7NcKzUoF0lRicNlx4=', 'Yn3nIYJGWsodkn2xturn0Q==', '1', null, null, '1', '2018-11-30 12:56:25', '2018-11-30 12:56:25', '2018-11-30 12:56:25', '2018-11-30 12:56:25', '0', '2018-11-30 12:56:25', '0', '2018-11-30 12:56:25', '0', '2018-11-30 12:56:25');

-- ----------------------------
-- Table structure for my_aspnet_paths
-- ----------------------------
DROP TABLE IF EXISTS `my_aspnet_paths`;
CREATE TABLE `my_aspnet_paths` (
  `applicationId` int(11) NOT NULL,
  `pathId` varchar(36) NOT NULL,
  `path` varchar(256) NOT NULL,
  `loweredPath` varchar(256) NOT NULL,
  PRIMARY KEY (`pathId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of my_aspnet_paths
-- ----------------------------

-- ----------------------------
-- Table structure for my_aspnet_personalizationallusers
-- ----------------------------
DROP TABLE IF EXISTS `my_aspnet_personalizationallusers`;
CREATE TABLE `my_aspnet_personalizationallusers` (
  `pathId` varchar(36) NOT NULL,
  `pageSettings` blob NOT NULL,
  `lastUpdatedDate` datetime NOT NULL,
  PRIMARY KEY (`pathId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of my_aspnet_personalizationallusers
-- ----------------------------

-- ----------------------------
-- Table structure for my_aspnet_personalizationperuser
-- ----------------------------
DROP TABLE IF EXISTS `my_aspnet_personalizationperuser`;
CREATE TABLE `my_aspnet_personalizationperuser` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicationId` int(11) NOT NULL,
  `pathId` varchar(36) DEFAULT NULL,
  `userId` int(11) DEFAULT NULL,
  `pageSettings` blob NOT NULL,
  `lastUpdatedDate` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of my_aspnet_personalizationperuser
-- ----------------------------

-- ----------------------------
-- Table structure for my_aspnet_profiles
-- ----------------------------
DROP TABLE IF EXISTS `my_aspnet_profiles`;
CREATE TABLE `my_aspnet_profiles` (
  `userId` int(11) NOT NULL,
  `valueindex` longtext,
  `stringdata` longtext,
  `binarydata` longblob,
  `lastUpdatedDate` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of my_aspnet_profiles
-- ----------------------------
INSERT INTO `my_aspnet_profiles` VALUES ('1', 'firstname/0/0/6:lastname/0/6/8:tfakey/0/14/1:', 'AustinSwearman0', '', null);
INSERT INTO `my_aspnet_profiles` VALUES ('30', 'firstname/0/0/4:lastname/0/4/4:', 'GaryWebb', '', null);
INSERT INTO `my_aspnet_profiles` VALUES ('31', 'firstname/0/0/3:lastname/0/3/7:', 'bobjohnson', '', null);
INSERT INTO `my_aspnet_profiles` VALUES ('32', 'firstname/0/0/7:lastname/0/7/4:', 'AnthonyLong', '', null);
INSERT INTO `my_aspnet_profiles` VALUES ('33', 'firstname/0/0/4:lastname/0/4/7:', 'MarvJackson', '', null);
INSERT INTO `my_aspnet_profiles` VALUES ('34', 'firstname/0/0/7:lastname/0/7/4:', 'AnthonyLong', '', null);
INSERT INTO `my_aspnet_profiles` VALUES ('35', 'firstname/0/0/3:lastname/0/3/5:', 'BobSmith', '', null);

-- ----------------------------
-- Table structure for my_aspnet_roles
-- ----------------------------
DROP TABLE IF EXISTS `my_aspnet_roles`;
CREATE TABLE `my_aspnet_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicationId` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of my_aspnet_roles
-- ----------------------------
INSERT INTO `my_aspnet_roles` VALUES ('1', '1', 'Customer');
INSERT INTO `my_aspnet_roles` VALUES ('2', '1', 'Employee');
INSERT INTO `my_aspnet_roles` VALUES ('3', '1', 'Manager');
INSERT INTO `my_aspnet_roles` VALUES ('4', '1', 'Administrators');
INSERT INTO `my_aspnet_roles` VALUES ('5', '1', 'LongView');
INSERT INTO `my_aspnet_roles` VALUES ('6', '1', 'Project');
INSERT INTO `my_aspnet_roles` VALUES ('7', '1', 'Project 2');
INSERT INTO `my_aspnet_roles` VALUES ('39', '1', 'Project 3');

-- ----------------------------
-- Table structure for my_aspnet_schemaversion
-- ----------------------------
DROP TABLE IF EXISTS `my_aspnet_schemaversion`;
CREATE TABLE `my_aspnet_schemaversion` (
  `version` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of my_aspnet_schemaversion
-- ----------------------------
INSERT INTO `my_aspnet_schemaversion` VALUES ('10');

-- ----------------------------
-- Table structure for my_aspnet_sessioncleanup
-- ----------------------------
DROP TABLE IF EXISTS `my_aspnet_sessioncleanup`;
CREATE TABLE `my_aspnet_sessioncleanup` (
  `LastRun` datetime NOT NULL,
  `IntervalMinutes` int(11) NOT NULL,
  `ApplicationId` int(11) NOT NULL,
  PRIMARY KEY (`ApplicationId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of my_aspnet_sessioncleanup
-- ----------------------------
INSERT INTO `my_aspnet_sessioncleanup` VALUES ('2018-06-06 14:28:26', '10', '2');

-- ----------------------------
-- Table structure for my_aspnet_sessions
-- ----------------------------
DROP TABLE IF EXISTS `my_aspnet_sessions`;
CREATE TABLE `my_aspnet_sessions` (
  `SessionId` varchar(191) NOT NULL,
  `ApplicationId` int(11) NOT NULL,
  `Created` datetime NOT NULL,
  `Expires` datetime NOT NULL,
  `LockDate` datetime NOT NULL,
  `LockId` int(11) NOT NULL,
  `Timeout` int(11) NOT NULL,
  `Locked` tinyint(1) NOT NULL,
  `SessionItems` longblob,
  `Flags` int(11) NOT NULL,
  PRIMARY KEY (`SessionId`,`ApplicationId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of my_aspnet_sessions
-- ----------------------------

-- ----------------------------
-- Table structure for my_aspnet_sitemap
-- ----------------------------
DROP TABLE IF EXISTS `my_aspnet_sitemap`;
CREATE TABLE `my_aspnet_sitemap` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Title` varchar(50) DEFAULT NULL,
  `Description` varchar(512) DEFAULT NULL,
  `Url` varchar(512) DEFAULT NULL,
  `Roles` varchar(1000) DEFAULT NULL,
  `ParentId` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of my_aspnet_sitemap
-- ----------------------------

-- ----------------------------
-- Table structure for my_aspnet_users
-- ----------------------------
DROP TABLE IF EXISTS `my_aspnet_users`;
CREATE TABLE `my_aspnet_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicationId` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `isAnonymous` tinyint(1) NOT NULL DEFAULT '1',
  `lastActivityDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of my_aspnet_users
-- ----------------------------
INSERT INTO `my_aspnet_users` VALUES ('1', '1', 'admin', '0', '2019-03-12 11:44:28');
INSERT INTO `my_aspnet_users` VALUES ('30', '1', 'gwebb', '0', '2019-02-28 12:19:14');
INSERT INTO `my_aspnet_users` VALUES ('31', '1', 'bjohnson', '0', '2018-09-05 14:59:46');
INSERT INTO `my_aspnet_users` VALUES ('32', '1', 'along', '0', '2018-09-11 14:32:00');
INSERT INTO `my_aspnet_users` VALUES ('33', '1', 'mjackson', '0', '2018-11-30 11:42:19');
INSERT INTO `my_aspnet_users` VALUES ('34', '1', 'aelong', '0', '2018-09-18 11:23:50');
INSERT INTO `my_aspnet_users` VALUES ('35', '1', 'bsmith', '0', '2018-11-30 12:56:24');

-- ----------------------------
-- Table structure for my_aspnet_usersinroles
-- ----------------------------
DROP TABLE IF EXISTS `my_aspnet_usersinroles`;
CREATE TABLE `my_aspnet_usersinroles` (
  `userId` int(11) NOT NULL,
  `roleId` int(11) NOT NULL,
  PRIMARY KEY (`userId`,`roleId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of my_aspnet_usersinroles
-- ----------------------------
INSERT INTO `my_aspnet_usersinroles` VALUES ('1', '4');
INSERT INTO `my_aspnet_usersinroles` VALUES ('1', '5');
INSERT INTO `my_aspnet_usersinroles` VALUES ('30', '3');
INSERT INTO `my_aspnet_usersinroles` VALUES ('30', '4');
INSERT INTO `my_aspnet_usersinroles` VALUES ('31', '2');
INSERT INTO `my_aspnet_usersinroles` VALUES ('32', '4');
INSERT INTO `my_aspnet_usersinroles` VALUES ('33', '4');
INSERT INTO `my_aspnet_usersinroles` VALUES ('34', '4');
INSERT INTO `my_aspnet_usersinroles` VALUES ('35', '2');

-- ----------------------------
-- Table structure for project
-- ----------------------------
DROP TABLE IF EXISTS `project`;
CREATE TABLE `project` (
  `id` int(11) NOT NULL,
  `project_manager` varchar(255) DEFAULT NULL,
  `project_phone` varchar(255) DEFAULT NULL,
  `project_email` varchar(255) DEFAULT NULL,
  `summary` longtext,
  `start_date` date NOT NULL DEFAULT '2018-01-01',
  `completion_date` date NOT NULL DEFAULT '2018-01-01',
  `last_updated` date NOT NULL DEFAULT '2018-01-01',
  `project_scope` longtext,
  `product_scope` longtext,
  `details` longtext,
  `conclusion` longtext,
  `progress_author` varchar(255) DEFAULT NULL,
  `budget` decimal(18,2) NOT NULL DEFAULT '0.00',
  `progress_percentage` int(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of project
-- ----------------------------
INSERT INTO `project` VALUES ('5', 'Gary Webb', '410-555-5457', 'gwebb@longevitillc.com', 'Longview is a software analytics dashboard for tracking projects, financial information, employee information, and company information for Longeviti LLC.', '2018-02-01', '2020-01-02', '2019-02-27', 'ASdf;lkjda;lkadfsjl; ;asldfjkdsf l;asdfkjl;sdf;lkwer werlkja fslkadf lk;asdf lw/ekrj;aslkjl;ksd fkls dfjal;sdkj werlksjaldk dklwejrl;kj sdfkldkl; warkl;asdflk;jasdfl;k ewrkljasdflkja we. asdf;lkjawer;laksdf dskl;awer lasdfjkl. ', 'ASdf;lkjda;lkadfsjl; ;asldfjkdsf l;asdfkjl;sdf;lkwer werlkja fslkadf lk;asdf lw/ekrj;aslkjl;ksd fkls dfjal;sdkj werlksjaldk dklwejrl;kj sdfkldkl; warkl;asdflk;jasdfl;k ewrkljasdflkja we. asdf;lkjawer;laksdf dskl;awer lasdfjkl. ', 'Test details', 'Test conclusion', 'Austin Swearman', '6000.00', '69');
INSERT INTO `project` VALUES ('6', 'John Smith', '410-555-5555', 'jsmith@longevitillc.com', 'Test', '2018-01-01', '2018-09-25', '2019-02-19', 'test', 'test', 'Test', '', 'Austin Swearman', '0.00', '0');
INSERT INTO `project` VALUES ('7', 'Jane Doe', '410-555-5555', 'jdoe@longevitillc.com', null, '2018-01-01', '2018-09-25', '2018-10-02', null, null, null, null, null, '0.00', '0');
INSERT INTO `project` VALUES ('39', 'John Smith', '410-555-5555', 'jsmith@longevitillc.com', null, '2018-12-21', '2018-12-28', '2018-01-01', null, null, null, null, null, '0.00', '0');

-- ----------------------------
-- Table structure for project_actions
-- ----------------------------
DROP TABLE IF EXISTS `project_actions`;
CREATE TABLE `project_actions` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `project_id` int(255) NOT NULL,
  `action` varchar(255) DEFAULT NULL,
  `owner` varchar(255) DEFAULT NULL,
  `status` enum('Pending','In Progress','Completed') NOT NULL DEFAULT 'Pending',
  `priority` enum('Low','Medium','High') NOT NULL DEFAULT 'Low',
  PRIMARY KEY (`id`,`project_id`)
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of project_actions
-- ----------------------------
INSERT INTO `project_actions` VALUES ('1', '5', 'Create task list', 'Swearman', 'Completed', 'Low');
INSERT INTO `project_actions` VALUES ('3', '5', 'Update training', 'Swearman', 'Completed', 'Low');
INSERT INTO `project_actions` VALUES ('4', '5', 'Pull data from external sources', 'Swearman', 'Pending', 'High');
INSERT INTO `project_actions` VALUES ('6', '5', 'Profit and loss', 'Swearman', 'Completed', 'Medium');
INSERT INTO `project_actions` VALUES ('7', '5', 'Opportunity loss for recruiting status', 'Swearman', 'Completed', 'Low');
INSERT INTO `project_actions` VALUES ('8', '5', 'Overview: Actions post for employees', 'Swearman', 'Completed', 'Medium');
INSERT INTO `project_actions` VALUES ('105', '5', 'Progress report rollup', 'Swearman', 'Completed', 'Medium');
INSERT INTO `project_actions` VALUES ('106', '5', 'Longview Business Development page design', 'Swearman', 'In Progress', 'High');

-- ----------------------------
-- Table structure for project_employee
-- ----------------------------
DROP TABLE IF EXISTS `project_employee`;
CREATE TABLE `project_employee` (
  `project_id` int(11) NOT NULL,
  `employee` varchar(255) NOT NULL,
  `position` varchar(255) NOT NULL,
  `hours` decimal(10,0) NOT NULL DEFAULT '0',
  PRIMARY KEY (`project_id`,`employee`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of project_employee
-- ----------------------------
INSERT INTO `project_employee` VALUES ('5', 'Andrew Ibrahim', 'Tester', '20');
INSERT INTO `project_employee` VALUES ('5', 'Austin Swearman', 'Programmer', '20');
INSERT INTO `project_employee` VALUES ('5', 'Gary Webb', 'Project Manager', '60');
INSERT INTO `project_employee` VALUES ('5', 'Hingle McCringleberry', 'Tester', '15');
INSERT INTO `project_employee` VALUES ('5', 'Ozamataz Buckshank', 'Debugger', '20');

-- ----------------------------
-- Table structure for project_files
-- ----------------------------
DROP TABLE IF EXISTS `project_files`;
CREATE TABLE `project_files` (
  `project_id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `path` longtext,
  `type` int(11) NOT NULL DEFAULT '0',
  `date` date NOT NULL DEFAULT '2000-01-01',
  PRIMARY KEY (`project_id`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of project_files
-- ----------------------------
INSERT INTO `project_files` VALUES ('0', 'Active Shooter Situation', 'https://www.dhs.gov/xlibrary/assets/active_shooter_booklet.pdf', '3', '2000-01-01');
INSERT INTO `project_files` VALUES ('0', 'CMMI', 'https://elsmar.com/pdf_files/cmmi-overview05.pdf', '3', '2000-01-01');
INSERT INTO `project_files` VALUES ('0', 'Code of Ethics', '/Documents/training/codeofethics.pdf', '0', '2000-01-01');
INSERT INTO `project_files` VALUES ('0', 'Fire Safety', 'https://www.osha.gov/dte/grant_materials/fy09/sh-18796-09/fireprotection.pdf', '3', '2000-01-01');
INSERT INTO `project_files` VALUES ('0', 'Foreign Travel Debrief', 'https://dbb.defense.gov/Portals/35/Documents/Misc_Documents/Travel_Info/Post%20Foreign%20Travel%20Debriefing%20Form.pdf?ver=2018-05-10-094114-993', '3', '2000-01-01');
INSERT INTO `project_files` VALUES ('0', 'ISO Training', 'https://www.iso.org/files/live/sites/isoorg/files/archive/pdf/en/pub100304.pdf', '3', '2000-01-01');
INSERT INTO `project_files` VALUES ('0', 'Security Clearance Refresher', 'https://www.cdse.edu/documents/cdse/Receive_and_Maint_Sct_Clnc.pdf', '3', '2000-01-01');
INSERT INTO `project_files` VALUES ('0', 'Timekeeping', '/Documents/training/timekeeping.pdf', '0', '2000-01-01');
INSERT INTO `project_files` VALUES ('5', 'LongView Status Report', 'Documents\\status_reports\\LongViewProgressReport.pdf', '5', '2019-03-04');

-- ----------------------------
-- Table structure for project_financial
-- ----------------------------
DROP TABLE IF EXISTS `project_financial`;
CREATE TABLE `project_financial` (
  `project_id` int(255) NOT NULL,
  `year` int(4) NOT NULL DEFAULT '2018',
  `month` enum('January','February','March','April','May','June','July','August','September','October','November','December') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'January',
  `day` int(2) NOT NULL,
  `expense_products` decimal(10,2) NOT NULL DEFAULT '0.00',
  `expense_labor` decimal(10,2) NOT NULL DEFAULT '0.00',
  `expense_operation` decimal(10,2) NOT NULL DEFAULT '0.00',
  `expense_marketing` decimal(10,2) NOT NULL DEFAULT '0.00',
  `expense_taxes` decimal(10,2) NOT NULL DEFAULT '0.00',
  `expense_other` decimal(10,2) NOT NULL DEFAULT '0.00',
  `profit_products` decimal(10,2) NOT NULL DEFAULT '0.00',
  `profit_services` decimal(10,2) NOT NULL DEFAULT '0.00',
  `profit_other` decimal(10,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`project_id`,`year`,`month`,`day`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of project_financial
-- ----------------------------
INSERT INTO `project_financial` VALUES ('5', '2018', 'January', '1', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00');
INSERT INTO `project_financial` VALUES ('5', '2018', 'January', '2', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00');
INSERT INTO `project_financial` VALUES ('5', '2018', 'January', '3', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00');
INSERT INTO `project_financial` VALUES ('5', '2018', 'January', '4', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00');
INSERT INTO `project_financial` VALUES ('5', '2018', 'February', '1', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00');
INSERT INTO `project_financial` VALUES ('5', '2018', 'August', '1', '0.00', '2000.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00');
INSERT INTO `project_financial` VALUES ('5', '2018', 'August', '14', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00');
INSERT INTO `project_financial` VALUES ('5', '2018', 'August', '21', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00');
INSERT INTO `project_financial` VALUES ('5', '2018', 'September', '1', '0.00', '2000.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00');
INSERT INTO `project_financial` VALUES ('5', '2018', 'September', '19', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00');
INSERT INTO `project_financial` VALUES ('5', '2018', 'September', '20', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00');
INSERT INTO `project_financial` VALUES ('5', '2018', 'October', '1', '0.00', '3000.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00');
INSERT INTO `project_financial` VALUES ('5', '2018', 'October', '2', '0.00', '500.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00');
INSERT INTO `project_financial` VALUES ('5', '2018', 'October', '3', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00');
INSERT INTO `project_financial` VALUES ('5', '2018', 'October', '4', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00');
INSERT INTO `project_financial` VALUES ('5', '2018', 'October', '5', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00');
INSERT INTO `project_financial` VALUES ('5', '2018', 'October', '9', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00');
INSERT INTO `project_financial` VALUES ('5', '2018', 'October', '10', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00');
INSERT INTO `project_financial` VALUES ('5', '2018', 'October', '11', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00');
INSERT INTO `project_financial` VALUES ('5', '2018', 'October', '14', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00');
INSERT INTO `project_financial` VALUES ('5', '2018', 'October', '15', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00');
INSERT INTO `project_financial` VALUES ('5', '2018', 'October', '16', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00');
INSERT INTO `project_financial` VALUES ('5', '2018', 'October', '17', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00');
INSERT INTO `project_financial` VALUES ('5', '2018', 'October', '18', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00');
INSERT INTO `project_financial` VALUES ('5', '2018', 'October', '22', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00');
INSERT INTO `project_financial` VALUES ('5', '2018', 'October', '23', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00');
INSERT INTO `project_financial` VALUES ('5', '2018', 'October', '25', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00');
INSERT INTO `project_financial` VALUES ('5', '2018', 'October', '31', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00');
INSERT INTO `project_financial` VALUES ('5', '2018', 'November', '1', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00');
INSERT INTO `project_financial` VALUES ('5', '2018', 'November', '2', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00');
INSERT INTO `project_financial` VALUES ('5', '2018', 'November', '12', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00');
INSERT INTO `project_financial` VALUES ('5', '2018', 'November', '21', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00');
INSERT INTO `project_financial` VALUES ('5', '2018', 'November', '23', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00');
INSERT INTO `project_financial` VALUES ('5', '2018', 'November', '27', '0.00', '0.00', '0.00', '1234.00', '0.00', '0.00', '0.00', '0.00', '0.00');
INSERT INTO `project_financial` VALUES ('5', '2018', 'November', '28', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00');
INSERT INTO `project_financial` VALUES ('5', '2018', 'November', '29', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00');
INSERT INTO `project_financial` VALUES ('5', '2018', 'November', '30', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00');
INSERT INTO `project_financial` VALUES ('5', '2018', 'December', '4', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00');
INSERT INTO `project_financial` VALUES ('5', '2018', 'December', '6', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00');
INSERT INTO `project_financial` VALUES ('5', '2018', 'December', '7', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00');
INSERT INTO `project_financial` VALUES ('5', '2018', 'December', '13', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00');
INSERT INTO `project_financial` VALUES ('5', '2018', 'December', '14', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00');
INSERT INTO `project_financial` VALUES ('5', '2018', 'December', '21', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00');
INSERT INTO `project_financial` VALUES ('5', '2018', 'December', '22', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00');
INSERT INTO `project_financial` VALUES ('5', '2018', 'December', '26', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00');
INSERT INTO `project_financial` VALUES ('5', '2018', 'December', '27', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '384.37', '31.49', '532.00');
INSERT INTO `project_financial` VALUES ('5', '2018', 'December', '28', '34.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00');
INSERT INTO `project_financial` VALUES ('5', '2018', 'December', '29', '36.00', '4.00', '5.00', '24.00', '30.00', '34.00', '0.00', '0.00', '0.00');
INSERT INTO `project_financial` VALUES ('5', '2019', 'January', '2', '1.00', '1.00', '1.00', '1.00', '1.00', '1.00', '1.00', '1.00', '1.00');
INSERT INTO `project_financial` VALUES ('5', '2019', 'January', '4', '2.00', '2.00', '2.00', '2.00', '2.00', '2.00', '2.00', '2.00', '2.00');
INSERT INTO `project_financial` VALUES ('5', '2019', 'January', '18', '4.00', '4.00', '4.00', '4.00', '4.00', '4.00', '4.00', '4.00', '4.00');
INSERT INTO `project_financial` VALUES ('5', '2019', 'February', '27', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00');
INSERT INTO `project_financial` VALUES ('6', '2018', 'October', '17', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00');
INSERT INTO `project_financial` VALUES ('6', '2019', 'February', '19', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00');

-- ----------------------------
-- Table structure for project_issue
-- ----------------------------
DROP TABLE IF EXISTS `project_issue`;
CREATE TABLE `project_issue` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `project_id` int(255) NOT NULL,
  `issue` longtext,
  `date_identified` date NOT NULL,
  `resolution` longtext,
  PRIMARY KEY (`id`,`project_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of project_issue
-- ----------------------------
INSERT INTO `project_issue` VALUES ('1', '5', 'Bug - Issue tracker not working properly', '2018-09-25', 'Correct database query');
INSERT INTO `project_issue` VALUES ('2', '5', 'Bug - Risk log not working properly', '2018-09-25', 'Correct database query');
INSERT INTO `project_issue` VALUES ('18', '5', 'Test', '2018-12-27', 'Test reso');

-- ----------------------------
-- Table structure for project_milestone
-- ----------------------------
DROP TABLE IF EXISTS `project_milestone`;
CREATE TABLE `project_milestone` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `project_id` int(255) NOT NULL,
  `milestone` longtext,
  `percent_complete` int(11) NOT NULL DEFAULT '0',
  `startdate` date NOT NULL,
  `completedate` date NOT NULL,
  PRIMARY KEY (`id`,`project_id`)
) ENGINE=InnoDB AUTO_INCREMENT=164 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of project_milestone
-- ----------------------------
INSERT INTO `project_milestone` VALUES ('161', '5', 'Next development', '44', '2019-01-02', '2019-01-31');
INSERT INTO `project_milestone` VALUES ('162', '5', 'Development', '80', '2018-07-27', '2018-12-31');
INSERT INTO `project_milestone` VALUES ('163', '5', 'Test', '2', '2018-12-27', '2018-12-31');

-- ----------------------------
-- Table structure for project_recruiting
-- ----------------------------
DROP TABLE IF EXISTS `project_recruiting`;
CREATE TABLE `project_recruiting` (
  `project_id` int(11) NOT NULL,
  `position` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `position_open_since` date NOT NULL DEFAULT '2018-01-01',
  `cost` decimal(10,2) NOT NULL DEFAULT '0.00',
  `causal_analysis` longtext,
  PRIMARY KEY (`project_id`,`position`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of project_recruiting
-- ----------------------------
INSERT INTO `project_recruiting` VALUES ('5', 'Test 10', '2019-02-13', '0.00', 'Test analysis');
INSERT INTO `project_recruiting` VALUES ('5', 'Test 11', '2019-02-12', '0.00', 'Test analysis');
INSERT INTO `project_recruiting` VALUES ('5', 'Test 6', '2018-01-10', '0.00', 'Test');
INSERT INTO `project_recruiting` VALUES ('5', 'Test 8', '2017-12-07', '0.00', 'Test');
INSERT INTO `project_recruiting` VALUES ('5', 'Test 9', '2018-07-04', '0.00', 'Test');

-- ----------------------------
-- Table structure for project_recruiting_info
-- ----------------------------
DROP TABLE IF EXISTS `project_recruiting_info`;
CREATE TABLE `project_recruiting_info` (
  `project_id` int(11) NOT NULL,
  `position` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `resume_path` varchar(255) DEFAULT NULL,
  `last_correspondence` date DEFAULT NULL,
  `submitted` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`position`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of project_recruiting_info
-- ----------------------------
INSERT INTO `project_recruiting_info` VALUES ('5', 'Test 10', 'Test', 'Documents\\resumes\\ProgressReportLongView (22).pdf', '2019-02-21', '1');

-- ----------------------------
-- Table structure for project_risk
-- ----------------------------
DROP TABLE IF EXISTS `project_risk`;
CREATE TABLE `project_risk` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `project_id` int(255) NOT NULL,
  `risk` longtext,
  `date_identified` date NOT NULL,
  `risk_level` enum('Low','Medium','High') NOT NULL DEFAULT 'Low',
  `owner` varchar(255) DEFAULT NULL,
  `response` longtext,
  PRIMARY KEY (`id`,`project_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of project_risk
-- ----------------------------
INSERT INTO `project_risk` VALUES ('1', '5', 'Opportunity loss', '2018-09-25', 'Medium', 'Swearman', 'Find database manager');
INSERT INTO `project_risk` VALUES ('4', '5', 'Test2', '2018-11-30', 'High', 'Smith', 'Test');
INSERT INTO `project_risk` VALUES ('5', '5', 'Test', '2018-12-26', 'Low', 'Austin', 'Test response');
INSERT INTO `project_risk` VALUES ('6', '5', 'Test3', '2018-12-28', 'Medium', 'Austin', 'Test response');

-- ----------------------------
-- Table structure for tab_data
-- ----------------------------
DROP TABLE IF EXISTS `tab_data`;
CREATE TABLE `tab_data` (
  `tab_name` varchar(255) NOT NULL,
  `notes` text,
  `data1` text,
  `data2` float DEFAULT NULL,
  `data3` float DEFAULT NULL,
  PRIMARY KEY (`tab_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tab_data
-- ----------------------------
INSERT INTO `tab_data` VALUES ('Customer', null, null, null, null);
INSERT INTO `tab_data` VALUES ('Employee', null, null, null, null);
INSERT INTO `tab_data` VALUES ('Overview', 'Notes to Gary', 'https://www.surveymonkey.com/r/T62HNHD', '1', '4570.63');
