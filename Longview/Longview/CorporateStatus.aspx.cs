﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Longview
{
    public partial class CorporateStatus : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!User.Identity.IsAuthenticated)
            {
                Response.Redirect("Default.aspx");
                return;
            }

            if (User.IsInRole("Manager") || User.IsInRole("Administrators"))
            {
                UploadButton.Visible = true;
            }
            /*List<Company.Employee> PartTime = Company.GetEmployees().Values.Where(x => x.WeeklyHours < 40).ToList();
            List<Company.Employee> FullTime = Company.GetEmployees().Values.Where(x => x.WeeklyHours >= 40).ToList();
            decimal PTE = 0;
            foreach (Company.Employee em in PartTime)
            {
                PTE += em.WeeklyHours;
            }
            FullTimeEquivalent.Text = String.Format("{0}", Math.Round((decimal)FullTime.Count + (PTE / 40), 1));
            EmployeeCount.Text = String.Format("{0}", Company.GetEmployees().Count);
            TotalRevenue.Text = String.Format("{0:C}", Company.GetRevenue());
            TotalExpenses.Text = String.Format("{0:C}", Company.GetExpenses());*/
        }

        protected decimal GetMonthlyFinances(int index)
        {
            decimal result = 0;
            foreach (KeyValuePair<string, decimal[]> kvp in ProjectsManager.GetYearlyFinances())
            {
                result += kvp.Value[index];
            }

            return result;
        }

        protected decimal GetFinancesByMonth(int month, bool profit)
        {
            decimal result = 0;

            string[] mo = { "January", "February", "March", "April", "May", "June", "July", "August",
            "September", "October", "November", "December" };

            string selected = mo[month - 1] + " " + DateTime.Now.Year;

            if (ProjectsManager.GetYearlyFinances().ContainsKey(selected))
            {
                if (!profit)
                {
                    result += ProjectsManager.GetYearlyFinances()[selected][0];
                    result += ProjectsManager.GetYearlyFinances()[selected][1];
                    result += ProjectsManager.GetYearlyFinances()[selected][2];
                    result += ProjectsManager.GetYearlyFinances()[selected][3];
                    result += ProjectsManager.GetYearlyFinances()[selected][4];
                    result += ProjectsManager.GetYearlyFinances()[selected][5];
                }
                else
                {
                    result += ProjectsManager.GetYearlyFinances()[selected][6];
                    result += ProjectsManager.GetYearlyFinances()[selected][7];
                    result += ProjectsManager.GetYearlyFinances()[selected][8];
                }
            }

            return result;
        }

        protected string GetDayFinances(int month, bool profit = false)
        {
            string result = "";
            decimal decResult = 0;
            DateTime value = new DateTime(DateTime.Now.Year, month,
                1);
            decimal[] d = ProjectsManager.GetMonthlyFinances().ContainsKey(value.ToString("d MMMM yyyy")) ?
                ProjectsManager.GetMonthlyFinances()[value.ToString("d MMMM yyyy")] : null;

            if (d != null)
                if (!profit)
                    decResult = d[0] + d[1] + d[2] + d[3] + d[4] + d[5];
                else
                    decResult = d[6] + d[7] + d[8];
            else
                decResult = 0;

            result += decResult;

            for (int i = 2; i <= DateTime.DaysInMonth(value.Year, value.Month); ++i)
            {
                value = value.AddDays(1);

                d = ProjectsManager.GetMonthlyFinances().ContainsKey(value.ToString("d MMMM yyyy")) ?
                ProjectsManager.GetMonthlyFinances()[value.ToString("d MMMM yyyy")] : null;

                if (d != null)
                    if (!profit)
                        decResult = d[0] + d[1] + d[2] + d[3] + d[4] + d[5];
                    else
                        decResult = d[6] + d[7] + d[8];
                else
                    decResult = 0;

                result += "," + decResult;
            }

            return result;
        }

        protected string GetDayFinancialArray(int month, bool profit)
        {
            string result = "";
            decimal[] d = { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            DateTime dt = new DateTime(DateTime.Now.Year, month, 1);
            if (profit)
            {
                foreach (KeyValuePair<string, decimal[]> kvp in
                    ProjectsManager.GetYearlyFinances().Where(x => x.Key.Contains(dt.ToString("MMMM yyyy"))))
                {
                    d[6] += kvp.Value[6];
                    d[7] += kvp.Value[7];
                    d[8] += kvp.Value[8];
                }
            }
            else
            {
                foreach (KeyValuePair<string, decimal[]> kvp in
                    ProjectsManager.GetYearlyFinances().Where(x => x.Key.Contains(dt.ToString("MMMM yyyy"))))
                {
                    d[0] += kvp.Value[0];
                    d[1] += kvp.Value[1];
                    d[2] += kvp.Value[2];
                    d[3] += kvp.Value[3];
                    d[4] += kvp.Value[4];
                    d[5] += kvp.Value[5];
                }
            }

            result += d[0] + "," + d[1] + "," + d[2] + "," + d[3] + "," + d[4] + "," + d[5] + "," +
                d[6] + "," + d[7] + "," + d[8];

            return result;
        }

        protected void GridView1_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadGridView1();
            }
        }

        protected void LoadGridView1()
        {
            DataTable dt = new DataTable();
            if (dt.Columns.Count == 0)
            {
                dt.Columns.Add("Project", typeof(string));
                dt.Columns.Add("Open Position", typeof(string));
                dt.Columns.Add("Days Open", typeof(int));
                dt.Columns.Add("Resumes Found", typeof(int));
                dt.Columns.Add("Resumes Submitted", typeof(int));
            }

            DataRow NewRow;
            foreach (Project pr in ProjectsManager.GetAllRoles().Values)
            {
                foreach (Recruitment r in pr.getRecruitment())
                {
                    NewRow = dt.NewRow();
                    NewRow[0] = pr.getName();
                    NewRow[1] = r.position;
                    NewRow[2] = DateTime.Now.Subtract(r.position_open).TotalDays;
                    NewRow[3] = r.resumes.Where(x => x.submitted == 0).Count();
                    NewRow[4] = r.resumes.Where(x => x.submitted == 1).Count();
                    dt.Rows.Add(NewRow);
                }
            }

            GridView1.DataSource = dt;
            GridView1.DataBind();
            ViewState["dattab"] = dt;
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(GridView1, "Select$" + e.Row.RowIndex);
                e.Row.Attributes["style"] = "cursor:pointer";
                e.Row.Attributes["onmouseover"] = "this.style.backgroundColor = '#061c45'; this.style.color = '#fff'; this.style.textShadow = '2px 2px black'";
                e.Row.Attributes["onmouseout"] = "this.style.backgroundColor = ''; this.style.color = '#181727'; this.style.textShadow = ''";
            }
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Project proj = ProjectsManager.GetRoleByName(GridView1.SelectedRow.Cells[0].Text);
            if (proj == null)
                return;

            string id = GridView1.SelectedRow.Cells[1].Text;
            string days = GridView1.SelectedRow.Cells[2].Text;
            Recruitment selected = proj.getRecruitment().FirstOrDefault(x => x.position == id);
            GridView1.Visible = false;
            Literal1.Visible = true;
            LinkButton1.Visible = true;
            Resumes.Visible = true;

            Literal1.Text = "<b>Project:</b> " + proj.getName() + "<br>";
            TextProject.Text = proj.getName();
            TextPosition.Text = id;
            Literal1.Text += "<b>Position:</b> " + id + "<br>";
            Literal1.Text += "<b>Position Open Since:</b> " + selected.position_open.ToLongDateString() + "<br>";
            Literal1.Text += "<b>Estimated Opportunity Loss:</b> " + String.Format("{0:C}", selected.cost) + "<br>";
            Literal1.Text += "<b>Causal Analysis:</b> " + selected.causal_analysis + "<br><br>";

            if (LoadResumes(id, proj) > 0)
                Literal1.Text += "<b>Resumes</b><br>";

            UpdatePanel1.Update();
        }

        protected int LoadResumes(string position, Project proj)
        {
            Recruitment pos = proj.getRecruitment().FirstOrDefault(x => x.position == position);
            if (pos.Equals(default(Recruitment)))
                return 0;

            int count = 0;
            DataTable dt = new DataTable();
            if (dt.Columns.Count == 0)
            {
                dt.Columns.Add("Name", typeof(string));
                dt.Columns.Add("Last Correspondence", typeof(string));
                dt.Columns.Add("Submitted", typeof(string));
            }

            DataRow NewRow;
            foreach (Resume r in pos.resumes)
            {
                NewRow = dt.NewRow();
                NewRow[0] = r.name;
                NewRow[1] = r.last_correspondence.ToString("yyyy-MM-dd");
                NewRow[2] = r.submitted == 0 ? "No" : "Yes";
                dt.Rows.Add(NewRow);
                ++count;
            }

            Resumes.DataSource = dt;
            Resumes.DataBind();

            return count;
        }

        protected void Resumes_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            Project proj = ProjectsManager.GetRoleByName(TextProject.Text);
            if (proj == null)
                return;

            HyperLink hl = (HyperLink)e.Row.FindControl("HyperLink1");
            string s = TextPosition.Text;
            Recruitment r = proj.getRecruitment().FirstOrDefault(x => x.position == s);
            if (!r.Equals(default(Recruitment)))
            {
                Resume res = r.resumes.FirstOrDefault(x => x.name == e.Row.Cells[0].Text);
                if (!res.Equals(default(Resume)))
                    hl.NavigateUrl = res.path;
            }
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            GridView1.Visible = true;
            LoadGridView1();
            Literal1.Visible = false;
            LinkButton1.Visible = false;
            Resumes.DataSource = null;
            Resumes.DataBind();
            Resumes.Visible = false;
            UpdatePanel1.Update();
        }

        protected void ProgressReports_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            Project proj = ProjectsManager.GetRoleByName(ProgressReports.Rows[e.NewSelectedIndex].Cells[0].Text);
            if (proj == null)
                return;

            if (proj.getStatusReport().path == "null")
                return;

            ClientScript.RegisterStartupScript(this.GetType(), "scriptid", "window.parent.location.href='" + proj.getStatusReport().path.Replace(@"\", @"\\") + "'", true);
        }

        protected void ProgressReports_Load(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            if (dt.Columns.Count == 0)
            {
                dt.Columns.Add("Project", typeof(string));
                dt.Columns.Add("Date Submitted", typeof(string));
            }

            DataRow NewRow;
            foreach (Project p in ProjectsManager.GetAllRoles().Values)
            {
                if (p.getId() == 0)
                    continue;

                if(p.getStatusReport().path == "null")
                    continue;

                NewRow = dt.NewRow();
                NewRow[0] = p.getName();
                NewRow[1] = p.getStatusUpdated().ToString("yyyy-MM-dd");
                dt.Rows.Add(NewRow);
            }

            ProgressReports.DataSource = dt;
            ProgressReports.DataBind();
        }

        protected void UploadButton_Click(object sender, EventArgs e)
        {
            UploadButton.Visible = false;
            PR.Style.Add("display", "none");
            UploadBox.Style.Add("display", "block");
        }

        protected void DropDownList1_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
                return;

            foreach (Project p in ProjectsManager.GetAllRoles().Values)
            {
                if (p.getId() == 0)
                    continue;

                if(User.IsInRole(p.getName()))
                    DropDownList1.Items.Add(p.getName());
            }
        }

        protected void BackButton_Click(object sender, EventArgs e)
        {
            UploadButton.Visible = true;
            PR.Style.Add("display", "block");
            UploadBox.Style.Add("display", "none");
        }

        protected void Upload_Click(object sender, EventArgs e)
        {
            if (!User.IsInRole("Manager") && !User.IsInRole("Administrators"))
                return;

            if (DropDownList1.SelectedValue == null)
            {
                errortext.InnerText = "A valid project has not been selected.";
                return;
            }

            Project proj = ProjectsManager.GetRoleByName(DropDownList1.SelectedValue);
            if(proj == null)
            {
                errortext.InnerText = "A valid project has not been selected.";
                return;
            }

            String path = Server.MapPath("~/Documents/status_reports/");

            Boolean fileOK = false;
            if (!FileUpload1.HasFile)
            {
                errortext.InnerText = "Please select a file to upload.";
                return;
            }

            if (FileUpload1.PostedFile.ContentLength > 4194304)
            {
                errortext.InnerText = "File cannot be larger than 4MB.";
                return;
            }

            String fileExtension =
                    System.IO.Path.GetExtension(FileUpload1.FileName).ToLower();
            String allowedExtension = ".pdf";
            if (fileExtension == allowedExtension)
                fileOK = true;

            if (!fileOK)
            {
                errortext.InnerText = "Cannot accept files of this type.";
                return;
            }

            try
            {
                FileUpload1.PostedFile.SaveAs(path
                    + proj.getName() + "ProgressReport" + fileExtension);
                path = path.Replace(Request.ServerVariables["APPL_PHYSICAL_PATH"], String.Empty);
                proj.setStatusReport(path + proj.getName() + "ProgressReport" + fileExtension);
                Server.TransferRequest(Request.Url.AbsolutePath, false);
                errortext.InnerText = "File uploaded.";
            }
            catch (Exception ex)
            {
                errortext.InnerText = "File could not be uploaded.";
            }
        }
    }
}