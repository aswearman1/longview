﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Web.Security;
using MySql.Data.MySqlClient;
using System.Web.Profile;
using System.Text.RegularExpressions;
using System.Configuration;

namespace Longview
{
    public partial class Admin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!User.IsInRole("Administrators") || !User.Identity.IsAuthenticated)
            {
                Response.StatusCode = 403;
                Response.End();
                return;
            }

            if (!IsPostBack)
            {
                foreach (KeyValuePair<string, Project> kvp in ProjectsManager.GetAllRoles())
                {
                    if (kvp.Key == "Overview")
                        continue;

                    pProjDropdown.Items.Add(kvp.Key);
                }
                pProjDropdown_SelectedIndexChanged(null, EventArgs.Empty);
            }
        }

        protected void SearchButton(object obj, EventArgs e)
        {
            ListBox1.Items.Clear();
            foreach(string s in queryUsers(sBox.Text))
            {
                ListBox1.Items.Add(s);
            }
        }

        protected void CreateUserWizard1_ContinueButtonClick(object sender, EventArgs e)
        {
            CreateUserWizard1.MoveTo(wizCreate);
        }

        protected string[] queryUsers(string search)
        {
            List<string> rtn = new List<string>();
            using (MySqlConnection mysqlCon = new MySqlConnection(ConfigurationManager.ConnectionStrings["LocalMySqlServer"].ConnectionString))
            {
                mysqlCon.Open();
                using (MySqlCommand sqlCommand = new MySqlCommand("SELECT name FROM my_aspnet_users WHERE name LIKE ?param1 ORDER BY name LIMIT 10;", mysqlCon))
                {
                    sqlCommand.Parameters.AddWithValue("param1", "%" + search + "%");
                    using (MySqlDataReader dataReader = sqlCommand.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            rtn.Add(dataReader.GetString(0));
                        }
                    }
                }
                mysqlCon.Close();
            }

            return rtn.ToArray();
        }

        protected void Wizard1_FinishButtonClick(object sender, WizardNavigationEventArgs e)
        {
            errortext.InnerHtml = "";
            if (ListBox1 != null)
            {
                MembershipUser user = Membership.GetUser(roUsername.Text);
                if (user == null)
                    return;

                foreach (ListItem listi in CheckBoxList1.Items)
                {
                    if(listi.Selected == true)
                    { 
                        if(!Roles.IsUserInRole(roUsername.Text, listi.Value))
                        {
                            try
                            {
                                Roles.AddUserToRole(roUsername.Text, listi.Value);
                            }
                            catch (Exception ex)
                            {
                                errortext.InnerHtml = "Error: " + ex.Message;
                            }
                        }
                    }
                    else
                    {
                        if(Roles.IsUserInRole(roUsername.Text, listi.Value))
                        {
                            try
                            {
                                Roles.RemoveUserFromRole(roUsername.Text, listi.Value);
                            }
                            catch (Exception ex)
                            {
                                errortext.InnerHtml = "Error: " + ex.Message;
                            }
                        }
                    }
                }

                if (user.IsLockedOut)
                {
                    if (!Locked.Checked)
                    {
                        user.UnlockUser();
                        Membership.UpdateUser(user);
                    }
                }
            }

            errortext.InnerHtml = "Action completed successfully.";
            Wizard1.MoveTo(Step1);
            ClientScript.RegisterStartupScript(this.GetType(), "scriptid", "window.parent.location.href='/dashboard.aspx'", true);
        }

        protected void Wizard1_NextButtonClick(object sender, WizardNavigationEventArgs e)
        {
            errortext.InnerHtml = "";
            if (ListBox1.SelectedItem == null)
            {
                e.Cancel = true;
                return;
            }

            MembershipUser user = Membership.GetUser(ListBox1.SelectedItem.Value);
            if (user != null)
            {
                roUID.Text = user.ProviderUserKey.ToString();
                roUsername.Text = user.UserName;
                CheckBoxList1.ClearSelection();
                foreach (string r in Roles.GetRolesForUser(ListBox1.SelectedItem.Value))
                {
                    CheckBoxList1.Items.FindByValue(r).Selected = true;
                }

            if (user.IsLockedOut) { 
                Locked.Checked = true;
                Locked.Enabled = true;
            }
            else
                Locked.Enabled = false;
            }
        }

        protected void CheckBoxList1_Load(object sender, EventArgs e)
        {
            if (CheckBoxList1.Items.Count == 0 && Roles.GetAllRoles().Length > 0)
            {
                foreach (string r in Roles.GetAllRoles())
                {
                    CheckBoxList1.Items.Add(r);
                }
            }
        }

        protected void Wizard1_PreviousButtonClick(object sender, WizardNavigationEventArgs e)
        {
            errortext.InnerHtml = "";
        }

        protected void CreateUserWizard1_CreatedUser(object sender, EventArgs e)
        {
            ProfileBase prf = ProfileBase.Create(((CreateUserWizard)sender).UserName);
            prf["firstname"] = ((TextBox)wizCreate.ContentTemplateContainer.FindControl("FirstName")).Text;
            prf["lastname"] = ((TextBox)wizCreate.ContentTemplateContainer.FindControl("LastName")).Text;
            prf.Save();
        }

        protected void ListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ListBox1.SelectedItem == null)
                ListBox1.ClearSelection();

            ((Button)Wizard1.FindControl("StartNavigationTemplateContainerID").FindControl("StartNextButton")).Enabled = true;
        }

        protected void pAddProject_Click(object sender, EventArgs e)
        {
            if (pProjectName.Text == "" || !Regex.IsMatch(pProjectName.Text, @"^([A-Za-z0-9\s-_]+)$") 
                || pProjectName.Text == "New Project")
            {
                errortext2.InnerText = "Project name is invalid.";
                return;
            }

            if (pProjectManager.Text == "")
            {
                errortext2.InnerText = "Project manager field cannot be blank.";
                return;
            }

            if (!Regex.IsMatch(pProjectManager.Text, @"^([A-Za-z\s]+)$"))
            {
                errortext2.InnerText = "Project manager field is invalid.";
                return;
            }

            if(pProjectPhone.Text == "")
            {
                errortext2.InnerText = "Project phone field cannot be blank.";
                return;
            }

            if (pProjectEmail.Text == "")
            {
                errortext2.InnerText = "Project email field cannot be blank.";
                return;
            }

            DateTime start;
            if(!DateTime.TryParse(pStartDate.Text, out start) || pStartDate.Text == "")
            {
                errortext2.InnerText = "Project start date field is invalid.";
                return;
            }

            DateTime end;
            if(!DateTime.TryParse(pEndDate.Text, out end) || pEndDate.Text == "")
            {
                errortext2.InnerText = "Project end date field is invalid.";
                return;
            }

            decimal budget;
            if(!decimal.TryParse(pBudget.Text, out budget) || pBudget.Text == "")
            {
                errortext2.InnerText = "Project budget field is invalid.";
                return;
            }

            if(pProjDropdown.Text == "New Project")
            {
                ProjectsManager.AddRole(pProjectName.Text, pProjectManager.Text, pProjectPhone.Text,
                    pProjectEmail.Text, start, end, budget);
                UpdatePanel1.Update();
                pProjDropdown.SelectedItem.Text = pProjectName.Text;
                pProjDropdown.SelectedItem.Value = pProjectName.Text;
                errortext2.InnerText = "Project creation successful.";
            }
            else
            {
                if(ProjectsManager.GetAllRoles().ContainsKey(pProjDropdown.Text))
                {
                    Project p = ProjectsManager.GetAllRoles()[pProjDropdown.Text];
                    p.UpdateInfo(pProjectManager.Text, pProjectPhone.Text,
                    pProjectEmail.Text, start, end, budget);
                    UpdatePanel1.Update();
                    errortext2.InnerText = "Project update successful.";
                }
            }
        }

        protected void pProjDropdown_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ProjectsManager.GetAllRoles().ContainsKey(pProjDropdown.SelectedValue))
            {
                Project p = ProjectsManager.GetAllRoles()[pProjDropdown.SelectedValue];
                pProjectName.Text = p.getName();
                pProjectManager.Text = p.getContactName();
                pProjectPhone.Text = p.getContactPhone();
                pProjectEmail.Text = p.getContactEmail();
                pStartDate.Text = p.getStartDate().ToString("yyyy-MM-dd");
                pEndDate.Text = p.getCompleteDate().ToString("yyyy-MM-dd");
                pBudget.Text = String.Format("{0}", p.getBudget());
            }

            if(pProjDropdown.SelectedValue == "New Project")
            {
                pProjectName.Text = "New Project";
                pProjectName.ReadOnly = false;
                pProjectManager.Text = "";
                pProjectPhone.Text = "";
                pProjectEmail.Text = "";
                pStartDate.Text = "";
                pEndDate.Text = "";
                pBudget.Text = "";
            }
            else
            {
                pProjectName.ReadOnly = true;
            }

            UpdatePanel1.Update();
        }

        protected void pNewProject_Click(object sender, EventArgs e)
        {
            if (!pProjDropdown.Items.Contains(new ListItem("New Project")))
            {
                pProjDropdown.Items.Add("New Project");
                pProjDropdown.SelectedValue = "New Project";
                pProjDropdown_SelectedIndexChanged(null, EventArgs.Empty);
            }
        }

        protected void pDeleteProject_Click(object sender, EventArgs e)
        {
            if (pProjDropdown.SelectedValue == "New Project")
                return;

            ProjectsManager.RemoveRole(pProjDropdown.SelectedValue);
            UpdatePanel1.Update();
            pProjDropdown.Items.Remove(pProjDropdown.SelectedValue);
            errortext2.InnerText = "Project has been deleted successfully.";
            pProjDropdown_SelectedIndexChanged(null, EventArgs.Empty);
        }
    }
}