﻿using System;
using System.Web.UI;
using System.Web.Security;
using System.Web.Profile;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Threading;
using System.Net.Mail;
using System.Net;

namespace Longview
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated)
                Response.Redirect("dashboard.aspx#top");
        }

        protected void Login1_Authenticate(object sender, System.Web.UI.WebControls.AuthenticateEventArgs e)
        {
            System.Web.UI.WebControls.Login log = (System.Web.UI.WebControls.Login)sender;
            try
            {
                if (Membership.ValidateUser(log.UserName, log.Password))
                {
                    FormsAuthentication.SetAuthCookie(log.UserName, log.RememberMeSet);
                    Response.Redirect("dashboard.aspx#top");
                }
                else
                    log.FailureText = "Login failed. Please check your user name and password and try again.";
            }
            catch (Exception ex)
            {
                log.FailureText = "Login failed. Please check your user name and password and try again.";
            }
        }

        protected void Login1_LoggingIn(object sender, System.Web.UI.WebControls.LoginCancelEventArgs e)
        {
            Login l = (Login)lv.FindControl("Login1");
            if (l == null)
                return;

            HtmlGenericControl tfaPanel = (HtmlGenericControl)lv.FindControl("TFAPanel");
            if (tfaPanel == null)
                return;

            if(!Membership.ValidateUser(l.UserName, l.Password))
                return;

            ProfileBase pb = ProfileBase.Create(l.UserName, true);
            if (pb == null)
                return;

            string s = (string)pb.GetPropertyValue("tfakey");
            if (s != "0" && s != "")
            {
                string key = GenerateTFAKey();
                pb["tfakey"] = key;
                pb.Save();
                sendEmail(Membership.GetUser(l.UserName, false).Email, key);
                e.Cancel = true;
            }

            CheckBox remember = (CheckBox)l.FindControl("RememberMe");
            if(remember != null)
                Session["remember"] = remember.Checked == true ? "true" : "false";

            Session["username"] = l.UserName;
            Session["password"] = l.Password;
            

            l.Visible = false;
            tfaPanel.Visible = true;
        }

        protected void sendEmail(string emailAddress, string key)
        {
            ThreadPool.QueueUserWorkItem(t =>
            {
                using (MailMessage mm = new MailMessage("noreply.longevitillc@gmail.com", emailAddress))
                {
                    mm.Subject = "LongView confirmation code";
                    mm.Body = string.Format("<p>Your LongView 6-digit confirmation code is {0}<p>", key);
                    mm.IsBodyHtml = true;
                    using (SmtpClient smtp = new SmtpClient())
                    {
                        smtp.Host = "smtp.gmail.com";
                        smtp.EnableSsl = true;
                        NetworkCredential NetworkCred = new NetworkCredential("noreply.longevitillc@gmail.com", "longeviti1234");
                        smtp.UseDefaultCredentials = false;
                        smtp.Credentials = NetworkCred;
                        smtp.Port = 587;
                        smtp.Send(mm);
                    }
                }
            });
        }

        protected string GenerateTFAKey()
        {
            string key = "";
            Random rand = new Random();
            string values = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            for (int i = 0; i < 6; ++i)
            {
                int index = rand.Next(values.Length);
                key += values[index];
            }

            return key;
        }

        protected void TFASubmit_Click(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)lv.FindControl("TFACode");
            if (tb == null)
                return;

            string username = Session["username"] as string;
            string password = Session["password"] as string;

            ProfileBase pb = ProfileBase.Create(username, true);
            if (pb == null)
                return;

            string key = (string)pb.GetPropertyValue("tfakey");
            if (key == "0" || key == "1" || key == "")
                return;

            if(tb.Text == key)
            {
                string s = Session["remember"] as string;
                bool persist = s != null && s == "true" ? true : false;
                if (Membership.ValidateUser(username, password))
                {
                    pb["tfakey"] = "1";
                    pb.Save();
                    FormsAuthentication.SetAuthCookie(username, persist);
                    Response.Redirect("~/dashboard");
                }
            }
            else
            {
                Literal l = (Literal)lv.FindControl("Login1").FindControl("FailureText");
                if(l != null)
                {
                    l.Text = "The confirmation code was invalid. Please try again.";
                }
            }
        }
    }
}