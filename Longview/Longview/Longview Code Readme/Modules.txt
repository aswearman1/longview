Admin.aspx.cs
	Page_Load - Module for when page loads
	SearchButton - Search for users
	pAddProject_Click - Add a new project with information input into each TextBox
	pNewProject_Click - Add a blank project to the dropdown menu for data input
	pDeleteProject_Click - Delete the selected project (in the dropdown menu) and delete all the data for selected project
	
	string[] queryUsers(string search) - Query the database for users that meet the search box criteria
	
	CreateUserWizard1_ContinueButtonClick - Next page for CreateUserWizard1
	CreateUserWizard1_CreatedUser - Create user and set user's First and Last name field
		
	Wizard1_FinishButtonClick - Confirm permission changes to user and save
	Wizard1_NextButtonClick - Go to next Wizard1 page and change permission settings for selected user
	Wizard1_PreviousButtonClick - Go back to the beginning of Wizard1
	
	CheckBoxList1_Load - Load the list of checkboxes for each user permission
	
	ListBox1_SelectedIndexChanged - Select user in the ListBox
	
	pProjDropdown_SelectedIndexChanged - Select a project to modify in the dropdown menu
	
Company.cs - Class for tracking company data such as employees and finances
	struct Employee - Employee data type for employee tracker (only used on Employee.aspx page)
	AddFinancial(decimal[] d) - Accumulator for overall Expenses and Profits
		- decimal[] d - A 9 element array
			0					1				2					3					4					5				6					7					8
			product expenses	labor expenses	operation expenses	marketing expenses	tax expenditures	other expenses	product profits		service profits		other profits
	GetRevenue - Get the revenue variable
	GetExpenses - Get the expenses variable
	PopulateEmployees - Get the list of employees from the database and store in memory in the form of Dictionary<employee id, Employee datatype>
	EditEmployee - Edit an employee in the employees dictionary and in the database. Parameters should be self explanatory.
	AddEmployee - Add a new employee to the database and then to the dictionary
	RemoveEmployee - Delete an employee from the database and then from the dictionary
	
CorporateStatus.aspx.cs
	Page_Load
	GetMonthlyFinances - Get overall finances for the company (uses deprecated method ProjectsManager.GetYearlyFinances())
	GetFinancesByMonth - Get finances by specified month
		-bool profit - Get the profit or get the expenses    true-profit   false-expenses
	GetDayFinances - Get an array of finances for each day of the specified month. Used for the javascript drilldown chart.
	GetDayFinancialArray - Get sum of finances for selected month . Used for the javascript drilldown table.
	GridView1_Load - Populate recruiting table with open positions
	LoadGridView1 - Same as above
	GridView1_RowDataBound - Makes each position in the recruiting table selectable
	GridView1_SelectedIndexChanged - Show information and resumes for the selected position in recruiting table
	LoadResumes - Load table of resumes for the selected position
	Resumes_RowDataBound - Creates a link for each data row in resume table to view each resume
	LinkButton1_Click - Back button to go back to recruiting table
	ProgressReports_Load - Load data table of status reports
	ProgressReports_SelectedIndexChanging - Select a status report and navigate to status report pdf
	UploadButton_Click - Navigate to control to upload new status report
	DropDownList1_Load - Load a list of projects that are accessable to the user
	BackButton_Click - Hide the upload control and return to the status report list
	Upload_Click - Uploads a status report
	
Customer.aspx.cs
	Page_Load - Populate all tables with files
	Upload_Click - Upload file based on data input
	GetCurrentProject - Get the Project data for the project selected in dropdown menu at the top of the page
	IsStrValid - Regex check for various strings
	DocumentList_SelectedIndexChanged - Change file upload control into a url textbox when Survey radiobutton is selected
	*_RowDataBound - Load list of uploaded files
	*_RowDeleting - Delete file from uploaded files
	
Default.aspx.cs - Login page
	Page_Load
	Login1_Authenticate - Authenticate the user
	Login1_LoggingIn - Check username and password input for validation. If user has 2 factor authentication enabled, create a tfa key and send to user's email.
						Allow user to input tfa key for validation if applicable.
	sendEmail - Send tfa key in email
	GenerateTFAKey - Generate a two factor authentication key
	TFASubmit_Click - Validate input tfa key
	
Employee.aspx.cs
	Page_Load
	GridView1_RowDataBound - Makes each document in the training table selectable
	GridView1_SelectedIndexChanged - Navigate user to selected pdf document
	GridView1_Load - Populate document table for mandatory training
	GridView1_RowDeleting - Delete document from mandatory training table
	UploadButton_Click - Upload new document for mandatory training
	IsStrValid - Regex check for various strings
	RadioButtonList1_SelectedIndexChanged - Change file upload control into a url textbox when link radiobutton is selected
	BindTable - Populate the employee table
	GridView2_Load - Same as above
	GridView2_RowDeleting - Delete employee from the table
	GridView2_RowEditing - Edit data for selected employee
	GridView2_Sorting - Sort employee table
	GridView2_SelectedIndexChanging - Show employee information
	SortGridView - Same as above
	SaveEmployee_Click - Save data for employee after editing
	CancelEmployee_Click - Cancel data changes for employee while editing
	NextButton - Add or edit employee
	SelectButton - Show information for selected employee
	BackButton - Go back to employee table
	LinkButton2_Click - Same as above
	AddEmployee_Click - Show control for adding a new employee
	AddNewEmployee_Click - Add a new employee to the database and to Company.employees dictionary
	SearchButton_Click - Search for a specific employee
	
Forgot.aspx.cs - Forgot password page
	Page_Load
	computeSalt - Create a salt key for encryption
	sendEmail - Send email with encryption key to reset password
	send_Click - Compute an encryption key with a salt and current day and send email with link to reset password
	
Global.asax.cs
	Application_Start - Initialize ProjectsManager and Company class
	
News.aspx.cs
	Page_Load - Pull html for latest new page from longevitillc.com/news
	
Overview.aspx.cs
	Page_Load
	getData - Get notes and survey url from database
	Button1_Click - Edit or save notes
	SetNotes - Set notes in the database to the specified string
	SetSurvey - Set survey url in the database to the specified string
	Button2_Click - Edit or save survey url
	
Progress.aspx.cs
	Page_Load
	DisplayData - Populate all Literal controls and GridView controls for the page
	Setup* - Populate data for specified GridView
	Button1_OnClick - Log out button
	Button1_Click - Download button to download pdf of page
	
ProgressOverview.aspx.cs
	Page_Load - Populate all data on the page
	Button1_OnClick - Log out button
	
ProgressReport.aspx.cs
	Page_Load
	DisplayData - Populate all Literal controls and GridView controls for the page
	Setup* - Populate data for specified GridView
	*_RowCommand - Execute the specified command for the table
	add*_Click - Add an entry to the specified table
	pSubmitButton_Click - Save all modified data on the page to the database and in the memory
	pCancelButton_Click - Discard all modified data from the page
	getDictionary - Get a dictionary representation of all the data in the specified datatable
	getEmployeeDictionary - Get a dictionary representation of all the data in the employee datatable
	Calendar1_Load - Set up the calendar control
	Calendar1_SelectionChanged - Set financial text boxes to reflect financial data for the selected date
	SaveFinancial - Save any changes to financial data into the database
	Year_SelectedIndexChanged - Set the calendar to the selected year in the dropdown box
	Year_Load - Load a list of the most recent years to the current date     current year +- 5 years
	Month_SelectedIndexChanged - Set the calendar to the selected month in the dropdown box
	Month_Load - Load a list of the 12 months
	Button1_OnClick - Log out button
	GetCurrentProject - Gets the currently selected project
	PrepareData - Update data in the database
	IsManager - Is the user a manager and does the user have permissions for the current project?
	
Project.aspx.cs
	Page_Load
	GetCurrentProject - Gets the currently selected project
	DisplayData - Populate all Literal controls and GridView controls for the page
	GetPriority - Return an html representation of the priority with stylization
	GridView1_Load - Populate recruiting table
	LoadGridView - Populate the specified gridview with data
	Button1_Click - Edit button for project manager contact information
	Button2_Click - Save button for project manager contact information
	GridView1_RowDataBound - Makes each position in the recruiting table selectable
	GridView1_SelectedIndexChanged - Show information and resumes for the selected position in recruiting table
	GetFinancesByMonth - Get finances by specified month
		-bool profit - Get the profit or get the expenses    true-profit   false-expenses
	GetDayFinances - Get an array of finances for each day of the specified month. Used for the javascript drilldown chart.
	GetDayFinancialArray - Get sum of finances for selected month . Used for the javascript drilldown table.
	Button4_Click - Edit progress report page for current project
	LoadResumes - Populate resumes table
	GridView1_RowDeleting - Delete position from the recruiting table
	Button5_Click - Add new recruiting position
	Button6_Click - Save new recruiting position
	GridView1_RowEditing - Edit selected recruiting position
	LinkButton1_Click - Back button
	IsManager - Is the user a manager and does the user have permissions for the current project?
	pSubmit_Click - Upload new resume button
	Resumes_RowCommand - Command on Resumes table
	Resumes_RowDataBound - Creates a link for each data row in resume table to view each resume
	GridView1_Sorting - Sort recruiting table
	SortGridView - Same as above
	ProjectDropdown_SelectedIndexChanged - Change project page to reflect selected project
	Button3_Click - Show progress report page for current project
	GridView2_SelectedIndexChanged - Show information for selected recruiting position (page overlay)
	GridView2_Sorting - Sort recruiting table (page overlay)
	GridView2_RowDataBound - Makes each position in the recruiting table selectable (page overlay)
	GridView2_Load - Populate the specified gridview with data
	Resumes2_RowDataBound - Creates a link for each data row in resume table to view each resume (page overlay)
	LinkButton2_Click - Back button (page overlay)
	
Project.cs
	enum FileType - FileType for ProjectFile struct
	struct Employee - DataType for project employee tracker
	struct Recruitment - DataType to track open positions for each project
	struct Resume - DataType to track resumes for each positions
	struct ProjectFile - DataType to track files for customer page and status reports for companystatus page
	class Project
		Project - Initializer
		UpdateInfo - Update the information for a specific instance of Project
		RecalculateFinances - Accumulate financial information for overall company
		LoadFinancial - Load financial information from database
		LoadActions - Load actions, issues, risks, milestones, recruiting info from database
		LoadEmployees - Load employees from database
		LoadFiles - Load files from database
		DeleteRecruiting - Delete an entry from the recruiting table in the database
		UpdateRecruiting - Add a position to the recruiting table in the database
		addFile - Add a file entry in the database
		removeFile - Remove a file entry from the database
		get* - Get specified variable
		setContactInfo - Set the project manager contact info in the database
		setStatusReport - Set the status report for the specified project
		
ProjectsManager.cs
	PopulateRoles - Get all roles (projects) from the database
	CalculateFinances - Accumulate all the profits and expenses for each month and each year
	GetYearlyFinances, GetMonthlyFinances - Return variable by specified name
	GetRoleByName - Get a project by its name
	GetRoleNameById - Get a project by its id
	AddRole - Add a new project to the project tables in the database
	RemoveRole - Delete a project and all its data from the database
	GetAllRoles - Gets a dictionary of all the roles   <string name, Project datatype>
	
Reset.aspx.cs
	Page_Load
	IsValidToken - Checks if the Query string matches the hash for the user in the database
	Button1_Click - Confirm password change if specified password meets all criteria
	
Settings.aspx.cs
	Page_Load
	Button1_OnClick - Sign out button
	pEditAccountInfo_Click - Edit or save first name, last name, and email for the user
	pEditPassword_Click - Edit or save password for the user
	pToggleTFA_Click - Toggle two factor authentication for the user
	
Site.Master.cs
	Page_Load
	Button1_OnClick - Sign out button
	
Startup.cs
	Configuration - Set up authentication for site
	
