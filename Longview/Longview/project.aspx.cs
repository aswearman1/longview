﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Longview
{
    public partial class project : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!User.Identity.IsAuthenticated)
            {
                Response.Redirect("/Default.aspx");
                return;
            }

            if(!IsPostBack)
            {
                foreach (Project p in ProjectsManager.GetAllRoles().Values)
                {
                    if (p.getId() == 0)
                        continue;

                    if (User.IsInRole(p.getName()) || User.IsInRole("Manager") || User.IsInRole("Administrators"))
                    {
                        ProjectDropdown.Items.Add(p.getName());
                    }
                }
            }

            if(ProjectDropdown.SelectedItem == null)
            {
                Response.StatusCode = 403;
                Response.End();
                return;
            }

            Project proj = ProjectsManager.GetRoleByName(ProjectDropdown.SelectedValue);
            if(proj == null)
            {
                Response.StatusCode = 404;
                Response.End();
                return;
            }

            int ID = proj.getId();
            if (!User.IsInRole(ProjectsManager.GetRoleNameById(ID)) &&
                !User.IsInRole("Administrators") &&
                !User.IsInRole("Manager"))
            {
                Response.StatusCode = 403;
                Response.End();
                return;
            }

            DisplayData(ID);
            Page.Form.Attributes.Add("enctype", "multipart/form-data");
        }

        protected Project GetCurrentProject()
        {
            if (ProjectDropdown.SelectedItem == null)
                return null;

            Project proj = ProjectsManager.GetRoleByName(ProjectDropdown.SelectedValue);

            return proj;
        }

        protected void DisplayData(int s)
        {
            string name = ProjectsManager.GetRoleNameById(s);
            if (name == null)
            {
                Response.StatusCode = 404;
                Response.End();
                return;
            }

            Project proj = ProjectsManager.GetRoleByName(name);
            if (proj == null)
            {
                Response.StatusCode = 404;
                Response.End();
                return;
            }

            budget.Text = String.Format("{0:C}", proj.getBudget());
            pOverlayBudget.Text = String.Format("{0:C}", proj.getBudget());
            spent.Text = String.Format("{0:C}", proj.getCost());
            pOverlayExpenses.Text = String.Format("{0:C}", proj.getCost());
            totalprofit.Text = String.Format("{0:C}", proj.getProfit());
            pOverlayProfit.Text = String.Format("{0:C}", proj.getProfit());
            netprofit.Text = String.Format("{0:C}", proj.getProfit() - proj.getCost());
            pOverlayNetProfit.Text = String.Format("{0:C}", proj.getProfit() - proj.getCost());
            progressbar.InnerHtml = HttpUtility.HtmlEncode(proj.getProjectProgress() + "%");
            progressbar.Style.Add("width", proj.getProjectProgress() + "%");
            progressbar2.InnerHtml = HttpUtility.HtmlEncode(proj.getProjectProgress() + "%");
            progressbar2.Style.Add("width", proj.getProjectProgress() + "%");
            startdate.Text = proj.getStartDate().ToLongDateString();
            pOverlayStartDate.Text = proj.getStartDate().ToLongDateString();
            completedate.Text = proj.getCompleteDate().ToLongDateString();
            pOverlayCompleteDate.Text = proj.getCompleteDate().ToLongDateString();
            employeesbill.Text = String.Format("{0}", proj.getEmployees().Count);
            pOverlayBillable.Text = String.Format("{0}", proj.getEmployees().Count);
            pProjectName.Text = proj.getName();
            pOverlayProjectName.Text = proj.getName();
            pProjectSummary.Text = proj.getSummary();
            pOverlaySummary.Text = proj.getSummary();
            pOverlayProjectScope.Text = proj.getProjectScope();
            pOverlayProductScope.Text = proj.getProductScope();

            contactName.Text = HttpUtility.HtmlEncode(proj.getContactName());
            pOverlayName.Text = HttpUtility.HtmlEncode(proj.getContactName());
            contactPhone.Text = HttpUtility.HtmlEncode(proj.getContactPhone());
            pOverlayPhone.Text = HttpUtility.HtmlEncode(proj.getContactPhone());
            contactEmail.Text = HttpUtility.HtmlEncode(proj.getContactEmail());
            pOverlayEmail.Text = HttpUtility.HtmlEncode(proj.getContactEmail());

            Literal2.Text = "";
            Literal3.Text = "";
            ganttChart.Text = "";

            if (IsManager() || User.IsInRole("Administrators"))
            {
                if (!IsPostBack)
                {
                    CommandField field = new CommandField();
                    field.UpdateText = "Edit";
                    field.ShowEditButton = true;
                    GridView1.Columns.Add(field);

                    field = new CommandField();
                    field.DeleteText = "Delete";
                    field.ShowDeleteButton = true;
                    GridView1.Columns.Add(field);
                }
            }

            foreach (KeyValuePair<int, string[]> kvp in proj.getActions())
            {
                Literal2.Text += @"<tr style='height: 50px;'>";

                Literal2.Text += "<td>";
                Literal2.Text += "<b>" + kvp.Value[0] + "</b><br>Priority: " + GetPriority(kvp.Value[2]);
                Literal2.Text += "</td>";

                Literal2.Text += "<td style='width: 20%;'>";
                if (kvp.Value[3] == "Completed")
                {
                    Literal2.Text += "<div style='background-color: green; color: white; text-align: center; border-radius: 3px;'>";
                    Literal2.Text += "Completed";
                    Literal2.Text += "</div>";
                }
                else if (kvp.Value[3] == "In Progress")
                {
                    Literal2.Text += "<div style='background-color: blue; color: white; text-align: center; border-radius: 3px;'>";
                    Literal2.Text += "In Progress";
                    Literal2.Text += "</div>";
                }
                else
                {
                    Literal2.Text += "<div style='background-color: red; color: white; text-align: center; border-radius: 3px;'>";
                    Literal2.Text += "Pending";
                    Literal2.Text += "</div>";
                }
                Literal2.Text += "</td></tr>";
            }

            decimal hours = 0;
            foreach (KeyValuePair<string, Employee> kvp in proj.getEmployees())
            {
                Literal3.Text += @"<tr style='height: 50px;'>";

                Literal3.Text += "<td>";
                Literal3.Text += kvp.Key;
                Literal3.Text += "</td>";

                Literal3.Text += "<td style='width: 25%;'>";
                Literal3.Text += kvp.Value.position;
                Literal3.Text += "</td>";

                Literal3.Text += "<td style='width: 20%;'>";
                Literal3.Text += (kvp.Value.weeklyhours < 40 ? "Part-time" : "Full-time");
                Literal3.Text += "</td>";
                hours += (kvp.Value.weeklyhours * 4);

                Literal3.Text += "</tr>";
            }
            hourscompleted.Text = String.Format("{0}", hours);
            pOverlayHours.Text = String.Format("{0}", hours);

            if (proj.getMilestones().Count > 0)
            {
                string chartScript =
                    @"<script type=""text/javascript"">
                function drawChart()";

                string chartScriptAlt =
                    @"<script type=""text/javascript"">
                    function drawChartS()";

                string chartScript1 = @"{

                    var data = new google.visualization.DataTable();
                    data.addColumn('string', 'Task ID');
                    data.addColumn('string', 'Task Name');
                    data.addColumn('date', 'Start Date');
                    data.addColumn('date', 'End Date');
                    data.addColumn('number', 'Duration');
                    data.addColumn('number', 'Percent Complete');
                    data.addColumn('string', 'Dependencies');" + Environment.NewLine + Environment.NewLine +

                        @"data.addRows([";

                string chartScript2 =
                        @"]);

                    var options = {
                        height: 275,
                        width: '100%',
                    };

                    var chart = new google.visualization.Gantt(document.getElementById('chart_div'));

                    chart.draw(data, options);
                }
                </script>";

                string chartScript3 =
                        @"]);

                    var options = {
                        height: 275,
                        width: '100%',
                    };

                    var chart = new google.visualization.Gantt(document.getElementById('chart_divS'));

                    chart.draw(data, options);
                }
                </script>";

                int count = 1;
                foreach (string[] str in proj.getMilestones().Values)
                {
                    DateTime dtStart = DateTime.Parse(str[2]);
                    DateTime dtEnd = DateTime.Parse(str[3]);
                    chartScript1 += "['" + count + "', '" + str[0] + "', new Date(" + dtStart.Year + ", " + (dtStart.Month - 1) +
                        ", " + dtStart.Day + "), new Date(" + dtEnd.Year + ", " + (dtEnd.Month - 1) + ", " + dtEnd.Day + "), " +
                        "null, " + str[1] + ", null]," + Environment.NewLine;
                    ++count;
                }

                ganttChart.Text = chartScript + chartScript1 + chartScript2 + chartScriptAlt + chartScript1 + chartScript3;
            }
        }

        protected string GetPriority(string i)
        {
            string priority = "<span style='margin: 0;'>" + i + "</span>";

            switch(i)
            {
                case "Low":
                    priority = "<span style='margin: 0; color: blue;'>" + i + "</span>";
                    break;

                case "Medium":
                    priority = "<span style='margin: 0; color: green;'>" + i + "</span>";
                    break;

                case "High":
                    priority = "<span style='margin: 0; color: red;'>" + i + "</span>";
                    break;
            }

            return priority;
        }

        protected void GridView1_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadGridView(GridView1);
            }
        }

        protected void LoadGridView(GridView gv)
        {
            Project proj = GetCurrentProject();
            if (proj == null)
                return;

            DataTable dt = new DataTable();

            if (dt.Columns.Count == 0)
            {
                dt.Columns.Add("Open Position", typeof(string));
                dt.Columns.Add("Days Open", typeof(int));
                dt.Columns.Add("Resumes Found", typeof(int));
                dt.Columns.Add("Resumes Submitted", typeof(int));
            }

            DataRow NewRow;
            foreach (Recruitment r in proj.getRecruitment())
            {
                NewRow = dt.NewRow();
                NewRow[0] = r.position;
                NewRow[1] = DateTime.Now.Subtract(r.position_open).TotalDays;
                NewRow[2] = r.resumes.Where(x => x.submitted == 0).Count();
                NewRow[3] = r.resumes.Where(x => x.submitted == 1).Count();
                dt.Rows.Add(NewRow);
            }

            gv.DataSource = dt;
            gv.DataBind();
            ViewState["dattab"] = dt;
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if(IsManager() || User.IsInRole("Administrators"))
            {
                contactName.Visible = false;
                TextBox1.Visible = true;
                TextBox1.Text = contactName.Text;
                contactPhone.Visible = false;
                TextBox2.Visible = true;
                TextBox2.Text = contactPhone.Text;
                contactEmail.Visible = false;
                TextBox3.Visible = true;
                TextBox3.Text = contactEmail.Text;
                Button1.Visible = false;
                Button2.Visible = true;
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            if (!IsManager() && !User.IsInRole("Administrators"))
                return;

            string qs = Request.QueryString["project"];
            if (String.IsNullOrEmpty(qs))
                return;

            int ID;
            if (!int.TryParse(qs, out ID))
                return;

            string projId = ProjectsManager.GetRoleNameById(ID);
            if (projId == null)
                return;

            if (TextBox1.Text == contactName.Text &&
                TextBox2.Text == contactPhone.Text &&
                TextBox3.Text == contactEmail.Text)
            {
                contactName.Visible = true;
                TextBox1.Visible = false;
                contactName.Text = TextBox1.Text;

                contactPhone.Visible = true;
                TextBox2.Visible = false;
                contactPhone.Text = TextBox2.Text;

                contactEmail.Visible = true;
                TextBox3.Visible = false;
                contactEmail.Text = TextBox3.Text;

                Button1.Visible = true;
                Button2.Visible = false;
                return;
            }

            Project prj = ProjectsManager.GetRoleByName(projId);
            try
            {
                prj.setContactInfo(TextBox1.Text, TextBox2.Text, TextBox3.Text);
            }
            finally
            {
                contactName.Visible = true;
                TextBox1.Visible = false;
                contactName.Text = TextBox1.Text;

                contactPhone.Visible = true;
                TextBox2.Visible = false;
                contactPhone.Text = TextBox2.Text;

                contactEmail.Visible = true;
                TextBox3.Visible = false;
                contactEmail.Text = TextBox3.Text;

                Button1.Visible = true;
                Button2.Visible = false;
            }
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(GridView1, "Select$" + e.Row.RowIndex);
                e.Row.Attributes["style"] = "cursor:pointer";
                e.Row.Attributes["onmouseover"] = "this.style.backgroundColor = '#061c45'; this.style.color = '#fff'; this.style.textShadow = '2px 2px black'";
                e.Row.Attributes["onmouseout"] = "this.style.backgroundColor = ''; this.style.color = '#181727'; this.style.textShadow = ''";
            }
        }

        //Click row for GridView1
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Project proj = GetCurrentProject();
            if (proj == null)
                return;

            string id = GridView1.SelectedRow.Cells[0].Text;
            string days = GridView1.SelectedRow.Cells[1].Text;
            Recruitment selected = proj.getRecruitment().FirstOrDefault(x => x.position == id);
            GridView1.Visible = false;
            Literal1.Visible = true;
            LinkButton1.Visible = true;
            if (Button5 != null)
                Button5.Visible = false;
            Resumes.Visible = true;

            Literal1.Text = "<b>Position:</b> " + id + "<br>";
            TextPosition.Text = id;  //Used for checking the position id for the View button
            Literal1.Text += "<b>Position Open Since:</b> " + selected.position_open.ToLongDateString() + "<br>";
            Literal1.Text += "<b>Estimated Opportunity Loss:</b> " + String.Format("{0:C}", selected.cost) + "<br>";
            Literal1.Text += "<b>Causal Analysis:</b> " + selected.causal_analysis + "<br><br>";

            if(LoadResumes(Resumes, id) > 0)
                Literal1.Text += "<b>Resumes</b><br>";

            UpdatePanel1.Update();
        }

        protected decimal GetFinances(int i)
        {
            Project proj = GetCurrentProject();
            if (proj == null)
            {
                Response.StatusCode = 404;
                Response.End();
                return 0;
            }

            decimal result = 0;

            foreach(KeyValuePair<string,decimal[]> kvp in proj.getFinancial())
            {
                result += kvp.Value[i];
            }

            return result;
        }

        protected decimal GetMonthlyFinances(int month, bool profit)
        {
            Project proj = GetCurrentProject();
            if (proj == null)
            {
                Response.StatusCode = 404;
                Response.End();
                return 0;
            }

            decimal result = 0;
            DateTime monthYear = new DateTime(DateTime.Now.Year, month, 1);

            foreach(KeyValuePair<string, decimal[]> kvp in proj.getFinancial().Where(x => x.Key.Contains(monthYear.ToString("MMMM yyyy"))))
            {
                if (!profit)
                {
                    result += kvp.Value[0] + kvp.Value[1] + kvp.Value[2] + kvp.Value[3] + kvp.Value[4] +
                    kvp.Value[5];
                }
                else
                {
                    result += kvp.Value[6] + kvp.Value[7] + kvp.Value[8];
                }
            }

            return result;
        }

        protected string GetDayFinances(int month, bool profit = false)
        {
            Project proj = GetCurrentProject();
            if (proj == null)
            {
                Response.StatusCode = 404;
                Response.End();
                return "";
            }

            string result = "";
            decimal decResult = 0;
            DateTime value = new DateTime(DateTime.Now.Year, month,
                1);
            decimal[] d = proj.getFinancial().ContainsKey(value.ToString("d MMMM yyyy")) ?
                proj.getFinancial()[value.ToString("d MMMM yyyy")] : null;

            if (d != null)
                if (!profit)
                    decResult = d[0] + d[1] + d[2] + d[3] + d[4] + d[5];
                else
                    decResult = d[6] + d[7] + d[8];
            else
                decResult = 0;

            result += decResult;

            for (int i = 2; i <= DateTime.DaysInMonth(value.Year, value.Month); ++i)
            {
                value = value.AddDays(1);

                d = proj.getFinancial().ContainsKey(value.ToString("d MMMM yyyy")) ?
                proj.getFinancial()[value.ToString("d MMMM yyyy")] : null;

                if (d != null)
                    if (!profit)
                        decResult = d[0] + d[1] + d[2] + d[3] + d[4] + d[5];
                    else
                        decResult = d[6] + d[7] + d[8];
                else
                    decResult = 0;

                result += "," + decResult;
            }

            return result;
        }

        protected string GetDayFinancialArray(int month, bool profit)
        {
            Project proj = GetCurrentProject();
            if (proj == null)
            {
                Response.StatusCode = 404;
                Response.End();
                return "";
            }

            string result = "";
            decimal[] d = { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            DateTime dt = new DateTime(DateTime.Now.Year, month, 1);
            if(profit)
            {
                foreach (KeyValuePair<string, decimal[]> kvp in 
                    proj.getFinancial().Where(x => x.Key.Contains(dt.ToString("MMMM yyyy"))))
                {
                    d[6] += kvp.Value[6];
                    d[7] += kvp.Value[7];
                    d[8] += kvp.Value[8];
                }
            }
            else
            {
                foreach (KeyValuePair<string, decimal[]> kvp in
                    proj.getFinancial().Where(x => x.Key.Contains(dt.ToString("MMMM yyyy"))))
                {
                    d[0] += kvp.Value[0];
                    d[1] += kvp.Value[1];
                    d[2] += kvp.Value[2];
                    d[3] += kvp.Value[3];
                    d[4] += kvp.Value[4];
                    d[5] += kvp.Value[5];
                }
            }

            result += d[0] + "," + d[1] + "," + d[2] + "," + d[3] + "," + d[4] + "," + d[5] + "," +
                d[6] + "," + d[7] + "," + d[8];

            return result;
        }

        protected void Button4_Click(object sender, EventArgs e)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "scriptid", "window.parent.location.href='/progressreport?project=" + GetCurrentProject().getId() + "'", true);
        }

        protected int LoadResumes(GridView gv, string position)
        {
            Project proj = GetCurrentProject();
            Recruitment pos = proj.getRecruitment().FirstOrDefault(x => x.position == position);
            if (pos.Equals(default(Recruitment)))
                return 0;

            int count = 0;
            DataTable dt = new DataTable();
            if (dt.Columns.Count == 0)
            {
                dt.Columns.Add("Name", typeof(string));
                dt.Columns.Add("Last Correspondence", typeof(string));
                dt.Columns.Add("Submitted", typeof(string));
            }

            DataRow NewRow;
            foreach (Resume r in pos.resumes)
            {
                NewRow = dt.NewRow();
                NewRow[0] = r.name;
                NewRow[1] = r.last_correspondence.ToString("yyyy-MM-dd");
                NewRow[2] = r.submitted == 0 ? "No" : "Yes";
                dt.Rows.Add(NewRow);
                ++count;
            }

            gv.DataSource = dt;
            gv.DataBind();

            return count;
        }

        //Delete gridview1 row
        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            if (!IsManager() && !User.IsInRole("Administrators"))
                return;

            Project proj = GetCurrentProject();
            proj.DeleteRecruiting(GridView1.Rows[e.RowIndex].Cells[0].Text);

            DataTable dt = new DataTable();

            if (dt.Columns.Count == 0)
            {
                dt.Columns.Add("Open Position", typeof(string));
                dt.Columns.Add("Days Open", typeof(int));
                dt.Columns.Add("Resumes Found", typeof(int));
                dt.Columns.Add("Resumes Submitted", typeof(int));
            }

            DataRow NewRow;
            foreach (Recruitment r in proj.getRecruitment())
            {
                NewRow = dt.NewRow();
                NewRow[0] = r.position;
                NewRow[1] = DateTime.Now.Subtract(r.position_open).TotalDays;
                NewRow[2] = r.resumes.Where(x => x.submitted == 0).Count();
                NewRow[3] = r.resumes.Where(x => x.submitted == 1).Count();
                dt.Rows.Add(NewRow);
            }

            GridView1.DataSource = dt;
            GridView1.DataBind();
            UpdatePanel1.Update();
        }

        //Add new position button
        protected void Button5_Click(object sender, EventArgs e)
        {
            if (!IsManager() && !User.IsInRole("Administrators"))
                return;

            GridView1.Visible = false;
            RecruitForm.Visible = true;
            LinkButton1.Visible = true;
            TextPosition.Enabled = true;
            if (Button5 != null)
                Button5.Visible = false;

            TextPosition.Text = "";
            TextOpenDate.Text = "";
            TextOpportunityCost.Text = "";
            TextCausalAnalysis.Text = "";

            UpdatePanel1.Update();
        }

        //Save new position
        protected void Button6_Click(object sender, EventArgs e)
        {
            if (!IsManager() && !User.IsInRole("Administrators"))
                return;

            Project proj = GetCurrentProject();
            if (proj == null)
                return;

            errortext.InnerText = "";

            List<Recruitment> recruitment = proj.getRecruitment().Where(x => x.position == TextPosition.Text).ToList();
            List<Resume> resume = recruitment.Count() > 0 ? recruitment.First().resumes : new List<Resume>();

            if (proj.UpdateRecruiting(TextPosition.Text, DateTime.Parse(TextOpenDate.Text), decimal.Parse(TextOpportunityCost.Text),
                    TextCausalAnalysis.Text, resume))
            {
                Literal1.Visible = true;
                RecruitForm.Visible = true;
                Resumes.Visible = true;
                Resumes.Columns[5].Visible = true;
                Resumes.Columns[6].Visible = true;
                UploadBox.Visible = true;
                Literal1.Text = "<b>Resumes</b><br>";
                TextPosition.Enabled = false;
            }
            else
            {
                errortext.InnerText = "One or more field(s) are invalid.";
            }

            UpdatePanel1.Update();
        }

        //Edit GridView1 position
        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            e.Cancel = true;
            if (!IsManager() && !User.IsInRole("Administrators"))
                return;

            Project proj = GetCurrentProject();
            if (proj == null)
                return;

            List<Recruitment> r = proj.getRecruitment().Where(x => x.position == GridView1.Rows[e.NewEditIndex].Cells[0].Text).ToList();
            if (r.Count == 0)
                return;

            GridView1.Visible = false;
            RecruitForm.Visible = true;
            LinkButton1.Visible = true;
            if (Button5 != null)
                Button5.Visible = false;

            TextPosition.Text = r.FirstOrDefault().position;
            TextOpenDate.Text = r.FirstOrDefault().position_open.ToString("yyyy-MM-dd");
            TextOpportunityCost.Text = String.Format("{0}", r.First().cost);
            TextCausalAnalysis.Text = r.FirstOrDefault().causal_analysis;
            UploadBox.Visible = true;
            Resumes.Visible = true;

            if (LoadResumes(Resumes, r.FirstOrDefault().position) > 0)
            {
                Literal1.Visible = true;
                Literal1.Text = "<b>Resumes</b><br>";
                Resumes.Columns[5].Visible = true;
                Resumes.Columns[6].Visible = true;
            }

            UpdatePanel1.Update();
        }

        //<Back button
        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            GridView1.Visible = true;
            LoadGridView(GridView1);
            Literal1.Visible = false;
            RecruitForm.Visible = false;
            LinkButton1.Visible = false;
            errortext.InnerText = "";
            TextPosition.Enabled = false;
            if (Button5 != null)
                Button5.Visible = true;
            Resumes.DataSource = null;
            Resumes.DataBind();
            Resumes.Visible = false;
            Resumes.Columns[5].Visible = false;
            Resumes.Columns[6].Visible = false;
            UploadBox.Visible = false;
            UpdatePanel1.Update();
        }

        protected bool IsManager()
        {
            Project proj = GetCurrentProject();

            if (User.IsInRole("Manager") && User.IsInRole(ProjectsManager.GetRoleNameById(proj.getId())))
                return true;

            return false;
        }

        //Resume upload submit button
        protected void pSubmit_Click(object sender, EventArgs e)
        {
            Project proj = GetCurrentProject();
            if(proj == null)
            {
                recruiterrortext.InnerText = "An unknown error has occurred.";
                return;
            }

            if (pResumeName.Text == "")
            {
                recruiterrortext.InnerText = "Resume name field is invalid.";
                return;
            }

            DateTime dt;
            if (!DateTime.TryParse(pLastCorrespondence.Text, out dt))
            {
                recruiterrortext.InnerText = "Last Correspondence field is invalid.";
                return;
            }

            if (!FileUpload1.HasFile)
            {
                recruiterrortext.InnerText = "Please select a file to upload.";
                return;
            }

            if (FileUpload1.PostedFile.ContentLength > 4194304)
            {
                recruiterrortext.InnerText = "File cannot be larger than 4MB.";
                return;
            }

            String fileName = Path.GetFileNameWithoutExtension(FileUpload1.PostedFile.FileName);
            String extension = Path.GetExtension(FileUpload1.PostedFile.FileName);
            String path = Server.MapPath("~/Documents/resumes/");
            int count = 1;
            while (File.Exists(path + fileName + extension))
            {
                fileName = Path.GetFileNameWithoutExtension(FileUpload1.PostedFile.FileName) + "(" + count + ")";
                ++count;

                if (count > 10)
                { 
                    recruiterrortext.InnerText = "File with that name already exists. Please rename the file before re-uploading.";
                    return;
                }
            }

            bool fileOK = false;
            String fileExtension =
                    System.IO.Path.GetExtension(FileUpload1.FileName).ToLower();
            String[] allowedExtension = { ".pdf", ".doc", ".docx" };
            if (allowedExtension.Contains(fileExtension))
                fileOK = true;

            if (!fileOK)
            {
                recruiterrortext.InnerText = "Cannot accept files of this type.";
                return;
            }

            Recruitment recr = proj.getRecruitment().FirstOrDefault(x => x.position == TextPosition.Text);
            if(recr.Equals(default(Recruitment)))
            {
                recruiterrortext.InnerText = "The position with name " + TextPosition.Text + " could not be found.";
                return;
            }

            Resume r = recr.resumes.FirstOrDefault(x => x.name == pResumeName.Text);
            if (!r.Equals(default(Resume)))
            {
                recruiterrortext.InnerText = "A resume with that name already exists.";
                return;
            }

            try
            {
                FileUpload1.PostedFile.SaveAs(path
                    + fileName + extension);
                path = path.Replace(Request.ServerVariables["APPL_PHYSICAL_PATH"], String.Empty);
                recr.AddResume(proj.getId(), TextPosition.Text, pResumeName.Text, path + fileName + extension,
                    dt, pSubmitted.SelectedValue == "Yes" ? 1 : 0);
                recruiterrortext.InnerText = "File uploaded.";
                pResumeName.Text = "";
                pLastCorrespondence.Text = "";
                LoadResumes(Resumes, TextPosition.Text);
            }
            catch (Exception ex)
            {
                recruiterrortext.InnerText = "File could not be uploaded.";
            }
        }

        protected void Resumes_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            e.Cancel = true;
        }

        protected void Resumes_RowEditing(object sender, GridViewEditEventArgs e)
        {
            e.Cancel = true;          
        }

        protected void Resumes_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            e.Cancel = true;
        }

        protected void Resumes_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            if (e.CommandName == "Delete")
            {
                Project proj = GetCurrentProject();
                if (proj == null)
                    return;

                List<Recruitment> r = proj.getRecruitment().Where(x => x.position == TextPosition.Text).ToList();
                if (r.Count() <= 0)
                    return;

                r.FirstOrDefault().RemoveResume(proj.getId(), TextPosition.Text, Resumes.Rows[index].Cells[0].Text);
                LoadResumes(Resumes, TextPosition.Text);
            }

            if(e.CommandName == "Edit")
            {
                TextBox tb = Resumes.Rows[index].FindControl("TextBox1") as TextBox;
                if (tb != null)
                {
                    tb.Text = Resumes.Rows[index].Cells[1].Text;
                    tb.Visible = true;
                }

                LinkButton lb = ((LinkButton)Resumes.Rows[index].Cells[5].Controls[0]);
                if (lb != null)
                {
                    lb.Text = "Update";
                    lb.CommandName = "Update";
                }
            }

            if (e.CommandName == "Update")
            {
                TextBox tb = Resumes.Rows[index].FindControl("TextBox1") as TextBox;
                if (tb != null)
                {
                    Project proj = GetCurrentProject();
                    if (proj == null)
                        return;

                    int r = proj.getRecruitment().FindIndex(x => x.position == TextPosition.Text);
                    if (r < 0)
                        return;

                    int res = proj.getRecruitment()[r].resumes.FindIndex(x => x.name == Resumes.Rows[index].Cells[0].Text);
                    if (res < 0)
                        return;

                    Resume newResume = proj.getRecruitment()[r].resumes[res];
                    newResume.last_correspondence = DateTime.Parse(tb.Text);
                    proj.getRecruitment()[r].RemoveResume(proj.getId(), TextPosition.Text, Resumes.Rows[index].Cells[0].Text, false);
                    proj.getRecruitment()[r].AddResume(proj.getId(), newResume);
                    LoadResumes(Resumes, TextPosition.Text);
                }
            }
        }

        protected void Resumes_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            Project proj = GetCurrentProject();
            if (proj == null)
                return;

            HyperLink hl = (HyperLink)e.Row.FindControl("HyperLink1");
            string s = TextPosition.Text;
            Recruitment r = proj.getRecruitment().FirstOrDefault(x => x.position == s);
            if (!r.Equals(default(Recruitment)))
            {
                Resume res = r.resumes.FirstOrDefault(x => x.name == e.Row.Cells[0].Text);
                if (!res.Equals(default(Resume)))
                    hl.NavigateUrl = res.path;
            }
        }

        private const string ASCENDING = " ASC";
        private const string DESCENDING = " DESC";

        private SortDirection GridViewSortDirection
        {
            get
            {
                if (ViewState["sortDirection"] == null)
                    ViewState["sortDirection"] = SortDirection.Ascending;

                return (SortDirection)ViewState["sortDirection"];
            }
            set { ViewState["sortDirection"] = value; }
        }

        protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
        {
            string sortExpression = e.SortExpression;

            if (GridViewSortDirection == SortDirection.Ascending)
            {
                GridViewSortDirection = SortDirection.Descending;
                SortGridView(GridView1, sortExpression, DESCENDING);
            }
            else
            {
                GridViewSortDirection = SortDirection.Ascending;
                SortGridView(GridView1, sortExpression, ASCENDING);
            }
        }

        private void SortGridView(GridView gv, string sortExpression, string direction)
        {
            //  You can cache the DataTable for improving performance
            DataTable dt = ViewState["dattab"] as DataTable;

            DataView dv = new DataView(dt);
            dv.Sort = sortExpression + direction;

            gv.DataSource = dv;
            gv.DataBind();
        }

        protected void ProjectDropdown_Load(object sender, EventArgs e)
        {
        }

        protected void ProjectDropdown_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadGridView(GridView1);
            LoadGridView(GridView2);
            GridView1.Visible = true;
            GridView2.Visible = true;
            Literal1.Visible = false;
            Literal6.Visible = false;
            RecruitForm.Visible = false;
            LinkButton1.Visible = false;
            LinkButton2.Visible = false;
            errortext.InnerText = "";
            TextPosition.Enabled = false;
            TextPosition2.Enabled = false;
            if (Button5 != null)
                Button5.Visible = true;
            Resumes.DataSource = null;
            Resumes.DataBind();
            Resumes.Visible = false;
            Resumes2.DataSource = null;
            Resumes2.DataBind();
            Resumes2.Visible = false;
            Resumes.Columns[5].Visible = false;
            Resumes.Columns[6].Visible = false;
            UploadBox.Visible = false;
            UpdatePanel1.Update();
            UpdatePanel2.Update();
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "scriptid", "window.parent.location.href='/progress?project=" + GetCurrentProject().getId() + "'", true);
        }

        protected void GridView2_SelectedIndexChanged(object sender, EventArgs e)
        {
            Project proj = GetCurrentProject();
            if (proj == null)
                return;

            string id = GridView2.SelectedRow.Cells[0].Text;
            string days = GridView2.SelectedRow.Cells[1].Text;
            Recruitment selected = proj.getRecruitment().FirstOrDefault(x => x.position == id);
            GridView2.Visible = false;
            Literal6.Visible = true;
            LinkButton2.Visible = true;
            Resumes2.Visible = true;

            Literal6.Text = "<b>Position:</b> " + id + "<br>";
            TextPosition2.Text = id;  //Used for checking the position id for the View button
            Literal6.Text += "<b>Position Open Since:</b> " + selected.position_open.ToLongDateString() + "<br>";
            Literal6.Text += "<b>Estimated Opportunity Loss:</b> " + String.Format("{0:C}", selected.cost) + "<br>";
            Literal6.Text += "<b>Causal Analysis:</b> " + selected.causal_analysis + "<br><br>";

            if (LoadResumes(Resumes2, id) > 0)
                Literal6.Text += "<b>Resumes</b><br>";

            UpdatePanel2.Update();
        }

        protected void GridView2_Sorting(object sender, GridViewSortEventArgs e)
        {
            string sortExpression = e.SortExpression;

            if (GridViewSortDirection == SortDirection.Ascending)
            {
                GridViewSortDirection = SortDirection.Descending;
                SortGridView(GridView2, sortExpression, DESCENDING);
            }
            else
            {
                GridViewSortDirection = SortDirection.Ascending;
                SortGridView(GridView2, sortExpression, ASCENDING);
            }
        }

        protected void GridView2_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(GridView2, "Select$" + e.Row.RowIndex);
                e.Row.Attributes["style"] = "cursor:pointer";
                e.Row.Attributes["onmouseover"] = "this.style.backgroundColor = '#061c45'; this.style.color = '#fff'; this.style.textShadow = '2px 2px black'";
                e.Row.Attributes["onmouseout"] = "this.style.backgroundColor = ''; this.style.color = '#181727'; this.style.textShadow = ''";
            }
        }

        protected void GridView2_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadGridView(GridView2);
            }
        }

        protected void Resumes2_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            Project proj = GetCurrentProject();
            if (proj == null)
                return;

            HyperLink hl = (HyperLink)e.Row.FindControl("HyperLink1");
            string s = TextPosition2.Text;
            Recruitment r = proj.getRecruitment().FirstOrDefault(x => x.position == s);
            if (!r.Equals(default(Recruitment)))
            {
                Resume res = r.resumes.FirstOrDefault(x => x.name == e.Row.Cells[0].Text);
                if (!res.Equals(default(Resume)))
                    hl.NavigateUrl = res.path;
            }
        }

        protected void LinkButton2_Click(object sender, EventArgs e)
        {
            GridView2.Visible = true;
            LoadGridView(GridView2);
            Literal6.Visible = false;
            LinkButton2.Visible = false;
            TextPosition2.Enabled = false;
            Resumes2.DataSource = null;
            Resumes2.DataBind();
            Resumes2.Visible = false;
            UpdatePanel2.Update();
        }
    }
}