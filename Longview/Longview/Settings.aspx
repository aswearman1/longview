﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Settings.aspx.cs" Inherits="Longview.Settings" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="Content/bootstrap.css" rel="stylesheet" id="bootstrap-css">
    <style type="text/css">
            
            @font-face { font-family: "Raleway"; src: url('/fonts/Raleway-Regular.ttf'); }
            @font-face { font-family: "Raleway"; src: url('/fonts/Raleway-SemiBold.ttf'); font-weight: 500; }
            @font-face { font-family: "Raleway"; src: url('/fonts/Raleway-Bold.ttf'); font-weight: bold; }

            .dropdown {
                position: relative;
                display: inline-block;
            }

            .dropdown-content {
                visibility: hidden;
                position: absolute;
                background-color: #f9f9f9;
                min-width: 160px;
                box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
                z-index: 1;
                right: 0;
                border-color: gray;

                transition: visibility 0.5s;
            }

            .dropdown-item img
            {
                float: left;
            }

            .dropdown-item
            {
                padding: 10px;
            }

            .dropdown-item:hover
            {
                background-color: lightgray;
            }

            .dropdown:focus {
                pointer-events: none;
                background-color: lightgray;
            }

            .dropdown-content:focus
            {
                display: block;
            }

            .dropdown:focus .dropdown-content {
                visibility: visible;
                outline: none;
                pointer-events: auto;

                transition: visibility 0.5s;
            }

            .navItem
            {
                height: 100%;
                font-size: 19px;
                padding: 16px 25px;
                display: block;
                text-align: center;
                color: #2f415f;
            }

            .navItem:hover
            {
                background: rgba(0,0,0,0.3);
                color: #FFF;
                text-align: center;
            }
        </style>
</head>
<body>
    <form id="form1" runat="server">
        <header> 
            <table width="100%" style="font-family: Raleway;">
		        <tr>
			        <td width="5%"></td>
			        <td width="10%" valign="middle" align="left"><a href="/"><img src="/images/longview.png" style="height: 67px;" alt=""/></a></td>
			        <td align="right">
                        <div style="width: auto; height: 100%; display: inline-block; border-right: 1px solid black;">
                            <nav style="height: 100%;">
                                <ul style="list-style: none; margin: 0; padding: 0;">
                                    <li>
                                        <a class="navItem" href="dashboard.aspx" style="text-decoration: none;">
                                            Dashboard
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                        <% if (Page.User.Identity.IsAuthenticated)
                            { %>
                        <div class="dropdown" tabindex="0" style="outline: none; padding: 0 5px;" onclick="this.focus();">
                            <div style="height: 50px; cursor: pointer;">
                                <asp:Label ID="Username" runat="server" Text="Label" Font-Size="14pt"></asp:Label><img src="Images/user.png" width="50px" height="50px" />
                            </div>
                            
                            <div class="dropdown-content" aria-labelledby="dropdownMenuButton">
                                 <a class="dropdown-item" href="Settings" runat="server" style="width: 200px;">&nbsp;&nbsp;&nbsp;<img src="Images/settings.png" />Account Settings</a>
                                <a class="dropdown-item" href="#" runat="server" onserverclick="Button1_OnClick"><img src="Images/logout.png" />Log Out</a>
                            </div>
                        </div>
                        <% } %>
			        </td>
		            <td width="5%"></td>
		        </tr>
	        </table>
        </header>
        <div style="width: 100%; height: 100%; padding: 40px 25px 40px 25px;">
            <h2>Account Settings</h2><br />
            <div style="width: 100%;">
                <b>Account Info</b>&nbsp;&nbsp;&nbsp;<asp:LinkButton ID="pEditAccountInfo" runat="server" OnClick="pEditAccountInfo_Click">Edit</asp:LinkButton>
                <hr style="margin-top: 0px;" />
                <table style="width: 100%;">
                    <tr>
                        <td style="width: 30%;">Username</td>
                        <td style="width: 20px;"></td>
                        <td style="width: 30%;">Email</td>
                    </tr>

                    <tr>
                        <td>
                            <asp:TextBox ID="pUsername" runat="server" Width="100%" Enabled="false"></asp:TextBox>
                        </td>

                        <td>

                        </td>

                        <td>
                            <asp:TextBox ID="pEmail" runat="server" Width="100%" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                        <td style="height: 10px;"></td>
                    </tr>

                    <tr>
                        <td>First Name</td>
                        <td></td>
                        <td>Last Name</td>
                    </tr>

                    <tr>
                        <td>
                            <asp:TextBox ID="pFirstName" runat="server" Width="100%" Enabled="false"></asp:TextBox>
                        </td>

                        <td>

                        </td>

                        <td>
                            <asp:TextBox ID="pLastName" runat="server" Width="100%" Enabled="false"></asp:TextBox>
                        </td>

                        <td>

                        </td>
                    </tr>

                    <tr>
                        <td colspan="4" style="height: 20px;">
                            <p id="aierrortext" runat="server" style="color: red;"></p>
                        </td>
                    </tr>
                </table>

                <b>Password</b>&nbsp;&nbsp;&nbsp;<asp:LinkButton ID="pEditPassword" runat="server" OnClick="pEditPassword_Click">Edit</asp:LinkButton>
                <hr style="margin-top: 0px;" />
                <table style="width: 100%;">
                    <tr>
                        <td style="width: 30%;">
                            Current Password
                        </td>

                        <td>

                        </td>
                    </tr>

                    <tr>
                        <td>
                            <asp:TextBox ID="pPassword" runat="server" Width="100%" Enabled="false" TextMode="Password"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                        <td style="height: 10px;"></td>
                    </tr>

                    <tr>
                        <td style="width: 30%;">
                            New Password
                        </td>

                        <td>

                        </td>
                    </tr>

                    <tr>
                        <td>
                            <asp:TextBox ID="pNewPassword" runat="server" Width="100%" Enabled="false" TextMode="Password"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                        <td style="height: 10px;"></td>
                    </tr>

                    <tr>
                        <td style="width: 30%;">
                            Confirm Password
                        </td>

                        <td>

                        </td>
                    </tr>

                    <tr>
                        <td>
                            <asp:TextBox ID="pConfirmPassword" runat="server" Width="100%" Enabled="false" TextMode="Password"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="4" style="height: 20px;">
                            <p id="passerrortext" runat="server" style="color: red;">&nbsp;</p>
                        </td>
                    </tr>
                </table>

                <b>Security</b><hr style="margin-top: 0px;" />
                <table style="width: 100%;">
                    <tr>
                        <td style="width: 30%;">
                            Two-factor Authentication
                        </td>

                        <td>

                        </td>
                    </tr>

                    <tr>
                        <td colspan="2" style="font-size: 14px;">
                            Two-factor authentication is an added measure of security for your account. Each time you log in, you will receive an email with a confirmation code which can be used to verify your identity.<br />
                            Before enabling two-factor authentication, please ensure the email address associated with this account is correct.
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <asp:Label ID="pTFA" runat="server" Text="Disabled" ForeColor="Red"></asp:Label>
                        </td>

                        <td>
                            <asp:LinkButton ID="pToggleTFA" runat="server" OnClick="pToggleTFA_Click">Enable two-factor authentication</asp:LinkButton>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </form>
</body>
</html>
