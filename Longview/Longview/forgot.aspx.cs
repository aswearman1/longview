﻿using System;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Security;

namespace Longview
{
    public partial class forgot : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(User.Identity.IsAuthenticated)
            {
                Response.StatusCode = 404;
                Response.End();
                return;
            }
        }

        protected string computeSalt(int size)
        {
            RNGCryptoServiceProvider csp = new RNGCryptoServiceProvider();
            byte[] saltByte = new byte[size];
            csp.GetBytes(saltByte);
            string salt = Convert.ToBase64String(saltByte);

            return salt;
        }

        protected void sendEmail(string emailAddress, string link)
        {
            ThreadPool.QueueUserWorkItem(t =>
            {
                using (MailMessage mm = new MailMessage("noreply.longevitillc@gmail.com", emailAddress))
                {
                    mm.Subject = "LongView Password Recovery";
                    mm.Body = string.Format("<p>Click <a href=\"{0}\">here</a> to reset your password. <br>This link is only valid for {1}.<p>", link, DateTime.Today.ToString("D"));
                    mm.IsBodyHtml = true;
                    using (SmtpClient smtp = new SmtpClient())
                    {
                        smtp.Host = "smtp.gmail.com";
                        smtp.EnableSsl = true;
                        NetworkCredential NetworkCred = new NetworkCredential("noreply.longevitillc@gmail.com", "longeviti1234");
                        smtp.UseDefaultCredentials = false;
                        smtp.Credentials = NetworkCred;
                        smtp.Port = 587;
                        smtp.Send(mm);
                    }
                }
            });
        }

        protected void send_Click(object sender, EventArgs e)
        {
            MembershipUser user = Membership.GetUser(unreq.Text, false);

            if (user == null)
            {
                CustomValidator1.IsValid = false;
                CustomValidator1.ErrorMessage = "The user could not be found.";
                return;
            }

            if (user.Email == "")
            {
                CustomValidator1.IsValid = false;
                CustomValidator1.ErrorMessage = "There is no email address tied to this account. Please contact an administrator.";
                return;
            }

            if (user.Comment != "")
            {
                string[] splitter = { ";;" };
                string[] saltOptions = user.Comment.Split(splitter, StringSplitOptions.None);
                if (saltOptions.Length >= 2)
                {
                    DateTime oldTime = DateTime.Parse(saltOptions[1]);
                    if (DateTime.UtcNow.Subtract(oldTime) < TimeSpan.FromMinutes(60))
                    {
                        CustomValidator1.IsValid = false;
                        CustomValidator1.ErrorMessage = "You can only reset your password once an hour.";
                        return;
                    }
                }
            }

            try
            {
                string accessToken = "";
                string salt = computeSalt(16);
                using (var sha256 = System.Security.Cryptography.SHA256Managed.Create())
                {
                    byte[] buf = Encoding.UTF8.GetBytes(string.Concat(DateTime.Today, user.UserName, salt));
                    accessToken = Convert.ToBase64String(sha256.ComputeHash(buf));
                }

                user.Comment = salt + ";;" + DateTime.UtcNow;
                Membership.UpdateUser(user);
                sendEmail(user.Email, "http://localhost:59491/reset.aspx?token=" + HttpUtility.UrlEncode(accessToken) + "&user=" + HttpUtility.UrlEncode(user.UserName));
            }
            catch (Exception ex)
            {
                user.Comment = "";
                Membership.UpdateUser(user);

                CustomValidator1.IsValid = false;
                CustomValidator1.ErrorMessage = "An unknown error has occurred.";
                return;
            }

            completeText.InnerHtml = "We have sent password reset instructions to your email address.";
        }
    }
}