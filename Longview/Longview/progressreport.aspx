﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProgressReport.aspx.cs" Inherits="Longview.ProgressReport" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="Content/bootstrap.css" rel="stylesheet" id="bootstrap-css" />
    <link href="Content/longview.css" rel="stylesheet" id="longview-css" />
    <style type="text/css">
        .auto-style1 {
            height: 26px;
        }
        .auto-style2 {
            width: 300px;
        }
        .auto-style3 {
            height: 26px;
            width: 300px;
        }
        .auto-style4 {
            height: 20px;
            width: 300px;
        }
        </style>

    <style>
            @font-face { font-family: "Raleway"; src: url('/fonts/Raleway-Regular.ttf'); }
            @font-face { font-family: "Raleway"; src: url('/fonts/Raleway-SemiBold.ttf'); font-weight: 500; }
            @font-face { font-family: "Raleway"; src: url('/fonts/Raleway-Bold.ttf'); font-weight: bold; }

            .dropdown {
                position: relative;
                display: inline-block;
            }

            .dropdown-content {
                visibility: hidden;
                position: absolute;
                background-color: #f9f9f9;
                min-width: 160px;
                box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
                z-index: 1;
                right: 0;
                border-color: gray;

                transition: visibility 0.5s;
            }

            .dropdown-item img
            {
                float: left;
            }

            .dropdown-item
            {
                padding: 10px;
            }

            .dropdown-item:hover
            {
                background-color: lightgray;
            }

            .dropdown:focus {
                pointer-events: none;
                background-color: lightgray;
            }

            .dropdown-content:focus
            {
                display: block;
            }

            .dropdown:focus .dropdown-content {
                visibility: visible;
                outline: none;
                pointer-events: auto;

                transition: visibility 0.5s;
            }

            .navItem
            {
                height: 100%;
                font-size: 19px;
                padding: 16px 25px;
                display: block;
                text-align: center;
                color: #2f415f;
            }

            .navItem:hover
            {
                background: rgba(0,0,0,0.3);
                color: #FFF;
                text-align: center;
            }
        .auto-style5 {
            height: 20px;
        }
        .auto-style6 {
            height: 31px;
        }
        </style>
</head>
<body style="font-family: Raleway; padding: 8px;">
    <header> 
        <table width="100%" style="font-family: Raleway;">
		    <tr>
			    <td width="5%"></td>
			    <td width="10%" valign="middle" align="left"><a href="/"><img src="/images/longview.png" style="height: 67px;" alt=""/></a></td>
			    <td align="right">
                    <div style="width: auto; height: 100%; display: inline-block; border-right: 1px solid black;">
                        <nav style="height: 100%;">
                            <ul style="list-style: none; margin: 0; padding: 0;">
                                <li>
                                    <a class="navItem" href="dashboard.aspx" style="text-decoration: none;">
                                        Dashboard
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    <% if (Page.User.Identity.IsAuthenticated)
                        { %>
                    <div class="dropdown" tabindex="0" style="outline: none; padding: 0 5px;" onclick="this.focus();">
                        <div style="height: 50px; cursor: pointer;">
                            <asp:Label ID="Username" runat="server" Text="Label" Font-Size="14pt"></asp:Label><img src="Images/user.png" width="50px" height="50px" />
                        </div>
                            
                        <div class="dropdown-content" aria-labelledby="dropdownMenuButton">
                             <a class="dropdown-item" href="Settings" runat="server" style="width: 200px;">&nbsp;&nbsp;&nbsp;<img src="Images/settings.png" />Account Settings</a>
                            <a class="dropdown-item" href="#" runat="server" onserverclick="Button1_OnClick"><img src="Images/logout.png" />Log Out</a>
                        </div>
                    </div>
                    <% } %>
			    </td>
		        <td width="5%"></td>
		    </tr>
	    </table>
    </header>
    <form id="form1" runat="server">
        <div style="padding: 0 50px 50px 50px; height: 100%; color: #383838;">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <table style="width: 100%;">	
		    <tbody>
			    <tr>
				    <td colspan="4" align="center">
					    <h1>Progress Report</h1>
				    </td>
			    </tr>

                <tr>
                    <td class="auto-style2">
                        
                    </td>
                </tr>
		
			    <tr>
				    <td colspan="4">
					    <h2 style="margin: 0;">Project Info</h2><hr />
				    </td>
			    </tr>
		
			    <tr>
				    <td class="auto-style3">
					    <b>Project name</b>
				    </td>

				    <td width="20px" class="auto-style1">

				    </td>

				    <td class="auto-style1">
					    <b>Project Manager</b>
				    </td>

                    <td width="20px" class="auto-style1"></td>
			    </tr>
		
			    <tr>
				    <td class="auto-style2">
                        <asp:Literal ID="pName" runat="server"></asp:Literal>
				    </td>
				
				    <td>
					
				    </td>
				
				    <td>
					    <asp:Literal ID="pManager" runat="server"></asp:Literal>
				    </td>
			    </tr>
			
			    <tr>
				    <td colspan="4" style="height: 5px;"></td>
			    </tr>
			
			    <tr>
				    <td class="auto-style2">
					    <b>Author</b>
				    </td>
				
				    <td width="20px"></td>
				
				    <td>
					    <b>Last Updated</b>
				    </td>

                    <td width="20px"></td>
			    </tr>
			
			    <tr>
				    <td class="auto-style2">
                        <asp:Literal ID="pAuthor" runat="server"></asp:Literal>
				    </td>
				
				    <td width="20px"></td>
				
				    <td>
                        <asp:Literal ID="pDate" runat="server"></asp:Literal>
				    </td>

                    <td width="20px"></td>
			    </tr>
			
			    <tr>
				    <td colspan="4" style="height: 20px;"></td>
			    </tr>

                <tr>
                    <td class="auto-style1">
                        <b>Project Started</b></td>

                    <td>

                    </td>

                    <td>
                        &nbsp;<b>Estimated Completion Date</b>
				    </td>
                </tr>

                <tr>
                    <td>
                        <asp:Literal ID="pProjectStart" runat="server"></asp:Literal>
                    </td>

                    <td>

                    </td>

                    <td>
                        <asp:TextBox ID="pCompletion" runat="server" Width="300px" TextMode="Date"></asp:TextBox>
                    </td>
                </tr>

                <tr>
				    <td colspan="4" style="height: 20px;"></td>
			    </tr>
			
			    <tr>
				    <td colspan="4">
					    <h2 style="margin: 0;">Project Summary</h2><hr />
				    </td>
			    </tr>

                <tr>
                    <td colspan="4"><b>Summary</b></td>
                </tr>

                <tr>
                    <td colspan="4">
                        <asp:TextBox ID="pSummary" runat="server" Height="200px" TextMode="MultiLine" Width="100%"></asp:TextBox>
                    </td>
                </tr>

                <tr>
                    <td colspan="4"><b>Project Scope</b></td>
                </tr>

                <tr>
                    <td colspan="4">
                        <asp:TextBox ID="pProjectScope" runat="server" Height="100px" TextMode="MultiLine" Width="100%"></asp:TextBox>
                    </td>
                </tr>

                <tr>
                    <td colspan="4"><b>Product Scope</b></td>
                </tr>

                <tr>
                    <td colspan="4">
                        <asp:TextBox ID="pProductScope" runat="server" Height="100px" TextMode="MultiLine" Width="100%"></asp:TextBox>
                    </td>
                </tr>
			
			    <tr>
				    <td class="auto-style2">
                        <b>Project Progress</b>
				    </td>
				
				    <td>
					
				    </td>
				
				    <td>
					    <b>Budget</b>
				    </td>
			    </tr>
			
			    <tr>
				    <td class="auto-style2">
                        <asp:TextBox ID="pProjectPercentage" runat="server" TextMode="Number" min="0" max="100"></asp:TextBox>%</td>
				
				    <td width="20px"></td>
				
				    <td>
					    $<asp:TextBox ID="pBudget" runat="server" Width="300px"></asp:TextBox>
				    </td>

                    <td width="20px"></td>
			    </tr>

                <tr>
				    <td colspan="4" style="height: 10px;"></td>
			    </tr>

                <tr>
                    <td class="auto-style3">
					    <b>Gross Profit</b>
				    </td>
				
				    <td width="20px" class="auto-style1"></td>
				
				    <td class="auto-style1">
					    <b>Total Expenses</b>
				    </td>
                </tr>

                <tr>
				    <td class="auto-style3">
                        <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:Literal ID="pProfit" runat="server"></asp:Literal>
                            </ContentTemplate>
                        </asp:UpdatePanel>
				    </td>
				
				    <td width="20px" class="auto-style1"></td>
				
				    <td class="auto-style1">
                        <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
					            <asp:Literal ID="pCost" runat="server"></asp:Literal>
                            </ContentTemplate>
                        </asp:UpdatePanel>
				    </td>

                    <td width="20px" class="auto-style1"></td>
			    </tr>

                <tr>
				    <td colspan="4" style="height: 10px;"></td>
			    </tr>

                <tr>
                    <td colspan="4">
                        Choose a date and input profits and expenses for that day.<br />
                        <b>Important: To commit all changes after they have been saved you must press the submit button at the bottom of the page.</b>
                    </td>
			    </tr>

                <tr>
				    <td id="tableCell" runat="server" style="vertical-align: top;" class="auto-style2">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" ChildrenAsTriggers="true">
                            <ContentTemplate>
                                <asp:DropDownList ID="Month" runat="server" AutoPostBack="True" OnLoad="Month_Load" OnSelectedIndexChanged="Month_SelectedIndexChanged">
                                    <asp:ListItem>January</asp:ListItem>
                                    <asp:ListItem>February</asp:ListItem>
                                    <asp:ListItem>March</asp:ListItem>
                                    <asp:ListItem>April</asp:ListItem>
                                    <asp:ListItem>May</asp:ListItem>
                                    <asp:ListItem>June</asp:ListItem>
                                    <asp:ListItem>July</asp:ListItem>
                                    <asp:ListItem>August</asp:ListItem>
                                    <asp:ListItem>September</asp:ListItem>
                                    <asp:ListItem>October</asp:ListItem>
                                    <asp:ListItem>November</asp:ListItem>
                                    <asp:ListItem>December</asp:ListItem>
                                </asp:DropDownList>&nbsp;&nbsp;&nbsp;<asp:DropDownList ID="Year" runat="server" AutoPostBack="True" OnLoad="Year_Load" OnSelectedIndexChanged="Year_SelectedIndexChanged"></asp:DropDownList>
                                <asp:Calendar ID="Calendar1" runat="server" OnLoad="Calendar1_Load" OnSelectionChanged="Calendar1_SelectionChanged" BackColor="White" BorderColor="White" Font-Names="Verdana" Font-Size="9pt" ForeColor="Black" Height="190px" NextPrevFormat="FullMonth" Width="300px" BorderWidth="1px">
                                    <DayHeaderStyle Font-Bold="True" Font-Size="8pt" />
                                    <NextPrevStyle Font-Bold="True" Font-Size="8pt" ForeColor="#333333" VerticalAlign="Bottom" />
                                    <OtherMonthDayStyle ForeColor="#999999" />
                                    <SelectedDayStyle BackColor="#333399" ForeColor="White" />
                                    <TitleStyle BackColor="White" Font-Bold="True" Font-Size="12pt" ForeColor="#333399" />
                                    <TodayDayStyle BackColor="#CCCCCC" />
                                </asp:Calendar>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>

                    <td colspan="3">
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div style="width: 30%; display: inline-block; padding-left: 20px;">
                                    <h3>Profit</h3>
                                    <b>Products</b><br />
                                    <asp:TextBox ID="ProfitProducts" runat="server"></asp:TextBox><br />
                                    <b>Services</b><br />
                                    <asp:TextBox ID="ProfitServices" runat="server"></asp:TextBox><br />
                                    <b>Other</b><br />
                                    <asp:TextBox ID="ProfitOther" runat="server"></asp:TextBox><br />
                                    <p id="profittext" runat="server"></p>
                                </div>
                                <div style="width: 70%; float: right; display: inline-block; padding-left: 20px; border-left: 1px solid darkgray">
                                    <h3>Expenses</h3>
                                    <b>Products</b><br />
                                    <asp:TextBox ID="ExpenseProducts" runat="server"></asp:TextBox><br />
                                    <b>Labor</b><br />
                                    <asp:TextBox ID="ExpenseServices" runat="server"></asp:TextBox><br />
                                    <b>Operating Costs</b><br />
                                    <asp:TextBox ID="ExpenseOperation" runat="server"></asp:TextBox><br />
                                    <b>Marketing</b><br />
                                    <asp:TextBox ID="ExpenseMarketing" runat="server"></asp:TextBox><br />
                                    <b>Taxes</b><br />
                                    <asp:TextBox ID="ExpenseTaxes" runat="server"></asp:TextBox><br />
                                    <b>Other</b><br />
                                    <asp:TextBox ID="ExpenseOther" runat="server"></asp:TextBox>
                                    <p id="expensetext" runat="server"></p>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
			    </tr>
			
			    <tr>
				    <td colspan="4" class="auto-style5"></td>
			    </tr>

                <tr>
                    <td colspan="4">
                        <h2 style="margin: 0;">Employees</h2><hr/>
                    </td>
                </tr>

                <tr>
                    <td colspan="4">
                        <asp:GridView ID="pEmployeesList" runat="server" AutoGenerateColumns="False" Width="100%" GridLines="None" OnRowCommand="pEmployeesList_RowCommand" OnRowDeleting="pEmployeesList_RowDeleting">
                            <Columns>
                                <asp:BoundField DataField="Name" HeaderText="Name" />
                                <asp:BoundField DataField="Position" HeaderText="Position" />
                                <asp:BoundField DataField="Type" HeaderText="Type" />
                                <asp:BoundField DataField="Weekly Hours" HeaderText="Weekly Hours" />
                                <asp:CommandField ButtonType="Button" DeleteText="-" ShowDeleteButton="True">
                                <ItemStyle Width="10px" />
                                </asp:CommandField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>

                <tr>
				    <td colspan="4" class="auto-style5"></td>
			    </tr>

                <tr>
                    <td colspan="4">
                        <table style="width: 100%">
						    <thead>
							    <tr>
								    <td class="auto-style1"><b>Name</b></td>
								    <td class="auto-style1"><b>Position</b></td>
                                    <td class="auto-style1"><b>Weekly Hours</b></td>
							    </tr>
						    </thead>
						    <tbody>
                                <tr>
							        <td width="20%" class="auto-style6">
                                        <asp:TextBox ID="pEmployeeName" runat="server" Width="90%"></asp:TextBox>
							        </td>
							
							        <td width="25%" class="auto-style6">
                                        <asp:TextBox ID="pEmployeePosition" runat="server" Width="90%"></asp:TextBox>
							        </td>
							
                                    <td width="20%" class="auto-style6">
                                        <asp:TextBox ID="pEmployeeHours" runat="server" Width="90%" TextMode="Number"></asp:TextBox>
                                    </td>

                                    <td align="right" class="auto-style6">
                                        <asp:Button ID="addEmployee" runat="server" Text="+" OnClick="addEmployee_Click" />
                                    </td>
                                </tr>
						    </tbody>
					    </table>
                    </td>
                </tr>

                <tr>
				    <td colspan="4" class="auto-style5"><p id="pEmployeeError" style="color: red" runat="server"></p></td>
			    </tr>
                    
                <tr>
                    <td colspan="4">
                        <h2 style="margin: 0;">Tasks</h2><hr/>
                    </td>
                </tr>
			
			    <tr>
				    <td colspan="4">
                        <asp:GridView ID="pTasks" runat="server" AutoGenerateColumns="False" Width="100%" OnRowCommand="pTasks_RowCommand" OnRowDeleting="pTasks_RowDeleting" GridLines="None" OnRowEditing="pTasks_RowEditing" OnRowUpdating="pTasks_RowUpdating">
                            <Columns>
                                <asp:BoundField DataField="Task" HeaderText="Task" >
                                <ItemStyle Width="70%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Owner" HeaderText="Owner" >
                                <ItemStyle Width="10%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Priority" HeaderText="Priority" >
                                <ItemStyle Width="10%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Status" HeaderText="Status" >
                                <ItemStyle Width="10%" />
                                </asp:BoundField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:DropDownList ID="DropDownList1" runat="server" Visible="false">
                                            <asp:ListItem Text="Pending" Value="Pending"></asp:ListItem>
                                            <asp:ListItem Text="In Progress" Value="In Progress"></asp:ListItem>
                                            <asp:ListItem Text="Completed" Value="Completed"></asp:ListItem>
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:CommandField HeaderText="Edit" ShowEditButton="True" />
                                <asp:ButtonField ButtonType="Button" Text="-" CommandName="Delete" />
                            </Columns>
                        </asp:GridView>
				    </td>
			    </tr>

                <tr>
				    <td colspan="4" style="height: 10px;"></td>
			    </tr>

                <tr>
                    <td colspan="4">
                        <table style="width: 100%">
						    <thead>
							    <tr>
								    <td width="50%"><b>Task</b></td>
								    <td width="20%"><b>Owner</b></td>
								    <td width="10%"><b>Priority</b></td>
                                    <td width="20%"><b>Status</b></td>
							    </tr>
						    </thead>
						    <tbody>
                                <tr>
							        <td>
                                        <asp:TextBox ID="pTask" runat="server" Width="95%"></asp:TextBox>
							        </td>
							
							        <td>
                                        <asp:TextBox ID="pOwner" runat="server" Width="90%"></asp:TextBox>
							        </td>
							
							        <td>
                                        <asp:DropDownList ID="pPriority" runat="server" Width="90%">
                                            <asp:ListItem>Low</asp:ListItem>
                                            <asp:ListItem>Medium</asp:ListItem>
                                            <asp:ListItem>High</asp:ListItem>
                                        </asp:DropDownList> 
							        </td>

                                    <td>
                                        <asp:DropDownList ID="pStatus" runat="server" Width="90%">
                                            <asp:ListItem>Pending</asp:ListItem>
                                            <asp:ListItem>In Progress</asp:ListItem>
                                            <asp:ListItem>Completed</asp:ListItem>
                                        </asp:DropDownList> 
                                    </td>

                                    <td>
                                        <asp:Button ID="addTask" runat="server" Text="+" OnClick="addTask_Click" />
                                    </td>
                                </tr>
						    </tbody>
					    </table>
                    </td>
                </tr>

                <tr><td class="auto-style4"><p id="pTaskError" style="color: red" runat="server"></p></td></tr>
			
			    <tr>
				    <td colspan="4">
					    <h2 style="margin: 0;">Milestones</h2><hr />
				    </td>
			    </tr>

                <tr>
                    <td colspan="4">
                        <asp:GridView ID="pMilestones" runat="server" AutoGenerateColumns="False" Width="100%" OnRowCommand="pMilestones_RowCommand" OnRowDeleting="pMilestones_RowDeleting" GridLines="None" OnRowEditing="pMilestones_RowEditing" OnRowUpdating="pMilestones_RowUpdating">
                            <Columns>
                                <asp:BoundField DataField="Milestone" HeaderText="Milestone">
                                <ItemStyle Width="50%" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Completed" DataField="Completed" DataFormatString="{0:0}%">
                                    <ItemStyle Width="10%" />
                                </asp:BoundField>
                                <asp:TemplateField ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:TextBox ID="TextBox1" runat="server" Visible="false" TextMode="Number" Width="50px"></asp:TextBox>
                                    </ItemTemplate>
                                    <ItemStyle Width="8%" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="Start Date" HeaderText="Start Date">
                                    <HeaderStyle Width="15%" />
                                <ItemStyle Width="20%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Complete Date" HeaderText="Complete Date">
                                <ItemStyle Width="20%" />
                                </asp:BoundField>
                                <asp:CommandField HeaderText="Edit" ShowEditButton="True" />
                                <asp:ButtonField ButtonType="Button" CommandName="Delete" Text="-" />
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>

                <tr>
				    <td colspan="4" style="height: 10px;"></td>
			    </tr>
			
			    <tr>
				    <td colspan="4">
					    <table style="width: 100%;">
						    <thead>
							    <tr>
								    <td width="50%" class="auto-style1"><b>Milestone</b></td>
                                    <td width="10%" class="auto-style1"><b>Completed</b></td>
								    <td width="20%" class="auto-style1"><b>Start Date</b></td>
								    <td width="20%" class="auto-style1"><b>Complete Date</b></td>
							    </tr>
						    </thead>
						    <tbody>
                                <tr>
							        <td>
                                        <asp:TextBox ID="pMilestone" runat="server" Width="95%"></asp:TextBox>
							        </td>

                                    <td>
                                        <asp:TextBox ID="pPercentComplete" runat="server" Width="80%" TextMode="Number"></asp:TextBox>%
                                    </td>
							
							        <td>
                                        <asp:TextBox ID="pStartDate" runat="server" Width="90%" TextMode="Date"></asp:TextBox>
							        </td>
							
							        <td>
                                        <asp:TextBox ID="pEndDate" runat="server" Width="90%" TextMode="Date"></asp:TextBox>
							        </td>

                                    <td>
                                        <asp:Button ID="addMilestone" runat="server" Text="+" OnClick="addMilestone_Click" />
                                    </td>
                                </tr>
						    </tbody>
					    </table>
				    </td>
			    </tr>

                <tr><td class="auto-style4"><p id="pMilestoneError" style="color: red" runat="server"></p></td></tr>
			
			    <tr>
				    <td colspan="4">
					    <h2 style="margin: 0;">Issue Log</h2><hr />
				    </td>
			    </tr>

                <tr>
				    <td colspan="4">
                        <asp:GridView ID="pIssues" runat="server" AutoGenerateColumns="False" Width="100%" OnRowCommand="pIssues_RowCommand" OnRowDeleting="pIssues_RowDeleting" GridLines="None">
                            <Columns>
                                <asp:BoundField DataField="Issue" HeaderText="Issue">
                                <ItemStyle Width="40%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Date Identified" HeaderText="Date Identified" >
                                <ItemStyle Width="20%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Resolution" HeaderText="Resolution">
                                <ItemStyle Width="40%" />
                                </asp:BoundField>
                                <asp:ButtonField ButtonType="Button" Text="-" CommandName="Delete" />
                            </Columns>
                        </asp:GridView>
				    </td>
			    </tr>

                <tr>
				    <td colspan="4" style="height: 10px;"></td>
			    </tr>
			
			    <tr>
				    <td colspan="4">
					    <table style="width: 100%">
						    <thead>
							    <tr>
								    <td width="40%"><b>Issue</b></td>
								    <td width="20%"><b>Date Identified</b></td>
								    <td width="40%"><b>Resolution</b></td>
							    </tr>
						    </thead>
						    <tbody>
                                <tr>
							        <td>
                                        <asp:TextBox ID="pIssue" runat="server" Width="95%"></asp:TextBox>
							        </td>
							
							        <td>
                                        <asp:TextBox ID="pDateId" runat="server" Width="90%" TextMode="Date"></asp:TextBox>
							        </td>
							
							        <td>
                                        <asp:TextBox ID="pResolution" runat="server" Width="90%"></asp:TextBox>
							        </td>

                                    <td>
                                        <asp:Button ID="addIssue" runat="server" Text="+" OnClick="addIssue_Click" />
                                    </td>
                                </tr>
						    </tbody>
					    </table>
				    </td>
			    </tr>

                <tr><td class="auto-style4"><p id="pIssueError" style="color: red" runat="server"></p></td></tr>
			
			    <tr>
				    <td colspan="4">
					    <h2 style="margin: 0;">Risk Log</h2><hr />
				    </td>
			    </tr>

                <tr>
				    <td colspan="4">
                        <asp:GridView ID="pRisks" runat="server" AutoGenerateColumns="False" Width="100%" OnRowCommand="pRisks_RowCommand" OnRowDeleting="pRisks_RowDeleting" GridLines="None">
                            <Columns>
                                <asp:BoundField DataField="Risk" HeaderText="Risk">
                                <ItemStyle Width="30%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Date Identified" HeaderText="Date Identified">
                                <ItemStyle Width="10%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Risk Level" HeaderText="Risk Level">
                                <ItemStyle Width="10%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Owner" HeaderText="Owner">
                                <ItemStyle Width="20%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Response" HeaderText="Response">
                                <ItemStyle Width="30%" />
                                </asp:BoundField>
                                <asp:ButtonField ButtonType="Button" Text="-" CommandName="Delete" />
                            </Columns>
                        </asp:GridView>
				    </td>
			    </tr>

                <tr>
				    <td colspan="4" style="height: 10px;"></td>
			    </tr>
			
			    <tr>
				    <td colspan="4">
					    <table style="width: 100%">
						    <thead>
							    <tr>
								    <td width="30%"><b>Risk</b></td>
								    <td width="10%"><b>Date Identified</b></td>
								    <td width="10%"><b>Risk Level</b></td>
								    <td width="20%"><b>Owner</b></td>
								    <td width="30%"><b>Response</b></td>
							    </tr>
						    </thead>
						    <tbody>
                                <tr>
							        <td>
                                        <asp:TextBox ID="pRisk" runat="server" Width="95%"></asp:TextBox>
							        </td>
							
							        <td>
                                        <asp:TextBox ID="pRiskDateId" runat="server" Width="90%" TextMode="Date"></asp:TextBox>
							        </td>
							
							        <td>
                                        <asp:DropDownList ID="pRiskLevel" runat="server">
                                            <asp:ListItem>Low</asp:ListItem>
                                            <asp:ListItem>Medium</asp:ListItem>
                                            <asp:ListItem>High</asp:ListItem>
                                        </asp:DropDownList>
							        </td>
							
							        <td>
                                        <asp:TextBox ID="pRiskOwner" runat="server" Width="90%"></asp:TextBox>
							        </td>
							
							        <td>
                                        <asp:TextBox ID="pRiskMitig" runat="server" Width="90%"></asp:TextBox>
							        </td>

                                    <td>
                                        <asp:Button ID="addRisk" runat="server" Text="+" OnClick="addRisk_Click" />
                                    </td>
                                </tr>
						    </tbody>
					    </table>
				    </td>
			    </tr>

                <tr><td class="auto-style4"><p id="pRiskError" style="color: red" runat="server"></p></td></tr>

                <tr>
                    <td colspan="4">
                        <h2 style="margin: 0;">Additional Details</h2><hr />
                    </td>
                </tr>

                <tr>
                    <td colspan="2"><b>Details</b></td>

                    <td colspan="2" align="right">Field not required</td>
                </tr>

                <tr>
                    <td colspan="4">
                        <asp:TextBox ID="pDetails" runat="server" Height="100px" TextMode="MultiLine" Width="100%"></asp:TextBox>
                    </td>
                </tr>

                <tr>
                    <td colspan="2"><b>Conclusion</b></td>

                    <td colspan="2" align="right">Field not required</td>
                </tr>

                <tr>
                    <td colspan="4">
                        <asp:TextBox ID="pConclusion" runat="server" Height="100px" TextMode="MultiLine" Width="100%"></asp:TextBox>
                    </td>
                </tr>

                <tr>
                    <td class="auto-style4">
                        <a name="errors" id="errortext" style="color: red;" runat="server"></a>
                    </td>
                </tr>

                <tr>
                    <td class="auto-style2">
                        <asp:Button ID="pSubmitButton" runat="server" Text="Submit" CssClass="btn-default btn-sm" Width="80px" BackColor="#0d0435" ForeColor="White" style="font-weight: 500;" OnClick="pSubmitButton_Click" />&nbsp;
                        <asp:Button ID="pCancelButton" runat="server" Text="Cancel" CssClass="btn-default btn-sm" Width="80px" BackColor="#0d0435" ForeColor="White" style="font-weight: 500;" OnClick="pCancelButton_Click" OnClientClick="window.onbeforeunload = function() { return 'You have attempted to leave this page. If you have made any changes to the fields without clicking the Submit button, your changes will be lost. Are you sure you want to exit this page?'; }" />
                    </td>
                </tr>
		    </tbody>
	    </table>
            </div>
    </form>
</body>
</html>
