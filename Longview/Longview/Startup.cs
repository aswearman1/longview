﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Longview.Startup))]
namespace Longview
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
