﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Longview
{
    public partial class customer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!User.Identity.IsAuthenticated)
            {
                Response.Redirect("/Default.aspx");
                return;
            }

            if (!IsPostBack)
            {
                foreach (Project p in ProjectsManager.GetAllRoles().Values)
                {
                    if (p.getId() == 0)
                        continue;

                    if (User.IsInRole(p.getName()) || User.IsInRole("Manager") || User.IsInRole("Administrators"))
                    {
                        ProjectDropdown.Items.Add(p.getName());
                    }
                }

                if (User.IsInRole("Manager") || User.IsInRole("Administrators"))
                {
                    CommandField field = new CommandField();
                    field.DeleteText = "Delete";
                    field.ShowDeleteButton = true;
                    Timesheets.Columns.Add(field);
                    Progress.Columns.Add(field);
                    Surveys.Columns.Add(field);
                    PPQ.Columns.Add(field);
                    Invoices.Columns.Add(field);
                }
            }

            if (ProjectDropdown.SelectedItem == null)
            {
                Response.StatusCode = 403;
                Response.End();
                return;
            }

            Project proj = ProjectsManager.GetRoleByName(ProjectDropdown.SelectedValue);
            if (proj == null)
            {
                Response.StatusCode = 404;
                Response.End();
                return;
            }

            int ID = proj.getId();
            if (!User.IsInRole(ProjectsManager.GetRoleNameById(ID)) &&
                !User.IsInRole("Administrators") &&
                !User.IsInRole("Manager"))
            {
                Response.StatusCode = 403;
                Response.End();
                return;
            }

            if (!User.IsInRole("Manager") && !User.IsInRole("Administrators"))
            {
                UploadBox.InnerHtml = "";
                UploadBox.Style.Add(HtmlTextWriterStyle.Display, "none");
            }

            DataTable dt = new DataTable();

            // Add columns to table
            dt.Columns.Add("Document", typeof(string));

            // Add row to table
            foreach (KeyValuePair<string, ProjectFile> kvp in proj.getFiles())
            {
                if (kvp.Value.type == FileType.FILETYPE_TIMESHEET)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = kvp.Key + ";" + kvp.Value.path;
                    dt.Rows.Add(dr);
                }
            }

            if (dt.Rows.Count == 0)
                NoTimesheets.Visible = true;
            else
                NoTimesheets.Visible = false;

            // Bind table
            Timesheets.DataSource = dt;
            Timesheets.DataBind();

            dt = new DataTable();

            // Add columns to table
            dt.Columns.Add("Document", typeof(string));

            // Add row to table
            foreach (KeyValuePair<string, ProjectFile> kvp in proj.getFiles())
            {
                if (kvp.Value.type == FileType.FILETYPE_PROGRESS)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = kvp.Key + ";" + kvp.Value.path;
                    dt.Rows.Add(dr);
                }
            }

            if (dt.Rows.Count == 0)
                NoProgress.Visible = true;
            else
                NoProgress.Visible = false;

            // Bind table
            Progress.DataSource = dt;
            Progress.DataBind();

            dt = new DataTable();

            // Add columns to table
            dt.Columns.Add("Document", typeof(string));

            // Add row to table
            foreach (KeyValuePair<string, ProjectFile> kvp in proj.getFiles())
            {
                if (kvp.Value.type == FileType.FILETYPE_SURVEY)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = kvp.Key + ";" + kvp.Value.path;
                    dt.Rows.Add(dr);
                }
            }

            if (dt.Rows.Count == 0)
                NoSurvey.Visible = true;
            else
                NoSurvey.Visible = false;

            // Bind table
            Surveys.DataSource = dt;
            Surveys.DataBind();

            dt = new DataTable();

            // Add columns to table
            dt.Columns.Add("Document", typeof(string));

            // Add row to table
            foreach (KeyValuePair<string, ProjectFile> kvp in proj.getFiles())
            {
                if (kvp.Value.type == FileType.FILETYPE_PPQ)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = kvp.Key + ";" + kvp.Value.path;
                    dt.Rows.Add(dr);
                }
            }

            if (dt.Rows.Count == 0)
                NoPPQ.Visible = true;
            else
                NoPPQ.Visible = false;

            // Bind table
            PPQ.DataSource = dt;
            PPQ.DataBind();

            dt = new DataTable();

            // Add columns to table
            dt.Columns.Add("Document", typeof(string));

            // Add row to table
            foreach (KeyValuePair<string, ProjectFile> kvp in proj.getFiles())
            {
                if (kvp.Value.type == FileType.FILETYPE_INVOICE)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = kvp.Key + ";" + kvp.Value.path;
                    dt.Rows.Add(dr);
                }
            }

            if (dt.Rows.Count == 0)
                NoInvoice.Visible = true;
            else
                NoInvoice.Visible = false;

            // Bind table
            Invoices.DataSource = dt;
            Invoices.DataBind();
        }

        protected void Upload_Click(object sender, EventArgs e)
        {
            if (!User.IsInRole("Manager") && !User.IsInRole("Administrators"))
                return;

            if (!IsStrValid(TextBox1.Text))
            {
                errortext.InnerText = "File name field is invalid. Allowable characters: \"A-Z a-z 0-9 - _\". Field must also be at least 4 characters.";
                return;
            }

            Project proj = GetCurrentProject();
            FileType ft = FileType.FILETYPE_TIMESHEET;
            string file = DocumentList.SelectedItem.Text;
            switch (file)
            {
                case "Timesheet":
                    file = "timesheets";
                    ft = FileType.FILETYPE_TIMESHEET;
                    break;

                case "PPQ Request":
                    file = "ppq_requests";
                    ft = FileType.FILETYPE_PPQ;
                    break;

                case "Survey":
                        Uri url;
                        if (Uri.TryCreate(TextBox2.Text, UriKind.Absolute, out url))
                        {
                            if (proj.getFiles().ContainsKey(TextBox1.Text))
                            {
                                errortext.InnerText = "A survey with that name already exists.";
                                return;
                            }

                            ProjectFile fl = new ProjectFile(url.AbsoluteUri, FileType.FILETYPE_SURVEY);
                            proj.addFile(TextBox1.Text, fl);
                            Server.TransferRequest(Request.Url.AbsolutePath, false);
                            errortext.InnerText = "Survey added.";
                        }
                        else
                            errortext.InnerText = "Provided URL is invalid.";
                    return;

                case "Progress Report":
                    file = "progress_reports";
                    ft = FileType.FILETYPE_PROGRESS;
                    break;

                case "Invoice":
                    file = "invoices";
                    ft = FileType.FILETYPE_INVOICE;
                    break;
            }

            String path = Server.MapPath("~/Documents/" + file + "/");

            Boolean fileOK = false;
            if (!FileUpload1.HasFile)
            {
                errortext.InnerText = "Please select a file to upload.";
                return;
            }

            if (FileUpload1.PostedFile.ContentLength > 4194304)
            {
                errortext.InnerText = "File cannot be larger than 4MB.";
                return;
            }

            if (File.Exists(path + FileUpload1.PostedFile.FileName))
            {
                errortext.InnerText = "File with that name already exists. Please rename the file before re-uploading.";
                return;
            }

            String fileExtension =
                    System.IO.Path.GetExtension(FileUpload1.FileName).ToLower();
            String allowedExtension = ".pdf";
            if (fileExtension == allowedExtension)
                fileOK = true;

            if (!fileOK)
            {
                errortext.InnerText = "Cannot accept files of this type.";
                return;
            }

            if(proj.getFiles().ContainsKey(TextBox1.Text))
            {
                errortext.InnerText = "A " + file + " with that name already exists.";
                return;
            }

            try
            {
                FileUpload1.PostedFile.SaveAs(path
                    + FileUpload1.FileName);
                path = path.Replace(Request.ServerVariables["APPL_PHYSICAL_PATH"], String.Empty);
                ProjectFile fl = new ProjectFile(path + FileUpload1.FileName, ft);
                proj.addFile(TextBox1.Text, fl);
                Server.TransferRequest(Request.Url.AbsolutePath, false);
                errortext.InnerText = "File uploaded.";
            }
            catch (Exception ex)
            {
                errortext.InnerText = "File could not be uploaded.";
            }
        }

        protected Project GetCurrentProject()
        {
            if (ProjectDropdown.SelectedItem == null)
                return null;

            Project proj = ProjectsManager.GetRoleByName(ProjectDropdown.SelectedValue);

            return proj;
        }

        protected bool IsStrValid(string s)
        {
            if (s.Length <= 3)
                return false;

            if (!Regex.IsMatch(s, @"^([A-Za-z0-9\s-_]+)$"))
                return false;

            return true;
        }

        protected void DocumentList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DocumentList.Text == "Survey")
            {
                FileUpload1.Visible = false;
                TextBox2.Visible = true;
                Label2.Text = "URL";
                Upload.Text = "Add";
            }
            else
            {
                FileUpload1.Visible = true;
                TextBox2.Visible = false;
                Label2.Text = "File Path";
                Upload.Text = "Upload";
            }
        }

        protected void PPQ_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HyperLink hl = new HyperLink();
                string[] s = e.Row.Cells[0].Text.Split(';');
                hl.NavigateUrl = s[1];
                hl.Text = s[0];
                e.Row.Cells[0].Controls.Add(hl);
            }
        }

        protected void Progress_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HyperLink hl = new HyperLink();
                string[] s = e.Row.Cells[0].Text.Split(';');
                hl.NavigateUrl = s[1];
                hl.Text = s[0];
                e.Row.Cells[0].Controls.Add(hl);
            }
        }

        protected void Surveys_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HyperLink hl = new HyperLink();
                string[] s = e.Row.Cells[0].Text.Split(';');
                hl.NavigateUrl = s[1];
                hl.Text = s[0];
                e.Row.Cells[0].Controls.Add(hl);
            }
        }

        protected void Invoices_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HyperLink hl = new HyperLink();
                string[] s = e.Row.Cells[0].Text.Split(';');
                hl.NavigateUrl = s[1];
                hl.Text = s[0];
                e.Row.Cells[0].Controls.Add(hl);
            }
        }

        protected void Timesheets_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HyperLink hl = new HyperLink();
                string[] s = e.Row.Cells[0].Text.Split(';');
                hl.NavigateUrl = s[1];
                hl.Text = s[0];
                e.Row.Cells[0].Controls.Add(hl);
            }
        }

        protected void Timesheets_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            if (!User.IsInRole("Manager") && !User.IsInRole("Administrators"))
                return;

            Project proj = GetCurrentProject();
            string[] values = Timesheets.Rows[e.RowIndex].Cells[0].Text.Split(';');
            proj.removeFile(values[0]);
            if (File.Exists(Server.MapPath(values[1])))
                File.Delete(Server.MapPath(values[1]));

            Server.TransferRequest(Request.Url.AbsolutePath, false);
        }

        protected void PPQ_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            if (!User.IsInRole("Manager") && !User.IsInRole("Administrators"))
                return;

            Project proj = GetCurrentProject();
            string[] values = PPQ.Rows[e.RowIndex].Cells[0].Text.Split(';');
            proj.removeFile(values[0]);
            if (File.Exists(Server.MapPath(values[1])))
                File.Delete(Server.MapPath(values[1]));

            Server.TransferRequest(Request.Url.AbsolutePath, false);
        }

        protected void Progress_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            if (!User.IsInRole("Manager") && !User.IsInRole("Administrators"))
                return;

            Project proj = GetCurrentProject();
            string[] values = Progress.Rows[e.RowIndex].Cells[0].Text.Split(';');
            proj.removeFile(values[0]);
            if (File.Exists(Server.MapPath(values[1])))
                File.Delete(Server.MapPath(values[1]));

            Server.TransferRequest(Request.Url.AbsolutePath, false);
        }

        protected void Surveys_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            if (!User.IsInRole("Manager") && !User.IsInRole("Administrators"))
                return;

            Project proj = GetCurrentProject();
            string[] values = Surveys.Rows[e.RowIndex].Cells[0].Text.Split(';');
            proj.removeFile(values[0]);

            Server.TransferRequest(Request.Url.AbsolutePath, false);
        }

        protected void Invoices_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            if (!User.IsInRole("Manager") && !User.IsInRole("Administrators"))
                return;

            Project proj = GetCurrentProject();
            string[] values = Invoices.Rows[e.RowIndex].Cells[0].Text.Split(';');
            proj.removeFile(values[0]);
            if (File.Exists(Server.MapPath(values[1])))
                File.Delete(Server.MapPath(values[1]));

            Server.TransferRequest(Request.Url.AbsolutePath, false);
        }

        protected void ProjectDropdown_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void ProjectDropdown_Load(object sender, EventArgs e)
        {

        }
    }
}