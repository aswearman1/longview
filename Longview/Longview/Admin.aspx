﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Admin.aspx.cs" Inherits="Longview.Admin" %>

<!DOCTYPE html>

<html>
    <head>
        <title></title>
        <link href="Content/bootstrap.css" rel="stylesheet" id="bootstrap-css" />
        <link href="Content/longview.css" rel="stylesheet" id="longview-css" />
        <script type="text/javascript"> 

            function confirm_delete()
            {
              if (confirm("Are you sure you want to delete this Project?")==true)
                return true;
              else
                return false;
            }
        </script> 
        <style type="text/css">
            .auto-style1 {
                height: 27px;
            }
            .auto-style2 {
                width: 10px;
                height: 27px;
            }
            .auto-style4 {
                height: 10px;
            }
            .auto-style5 {
                width: 209px;
            }
        </style>
        </head>
    <body style="padding: 8px; background: linear-gradient(to bottom, rgba(191,191,191,1) 0%,rgba(232,232,232,1) 13%,rgba(244,244,244,1) 100%) no-repeat;">
        <form name="form1" runat="server">
            <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
            <table style="width: 100%">
                <tr>
                    <td style="min-width: 10px; width: 10px;"></td>

                    <td style="width: 50%;">
                        <div class="contentBox" style="width: 100%; height: 450px; min-width: 450px;">
                            <img src="Images/accountcreate.png" style="margin-left: 10px; margin-top: 5px; float: left;" /><h1 style="margin-left: 10px; display: inline-block; font-size: 30px; margin-top: 10px;">Account Creation</h1><br />
                                <div style="padding-left: 20px; padding-right: 20px; width: 100%;">
                                <asp:CreateUserWizard ID="CreateUserWizard1" runat="server" CompleteSuccessText="The account has been successfully created." LoginCreatedUser="False" UnknownErrorMessage="The account was not created. Please try again." Width="100%" OnContinueButtonClick="CreateUserWizard1_ContinueButtonClick" OnCreatedUser="CreateUserWizard1_CreatedUser">
                                    <ContinueButtonStyle BackColor="#0D0435" CssClass="btn-default btn-lg" ForeColor="White" Height="50px" BorderStyle="None" />
                                    <CreateUserButtonStyle BackColor="#0D0435" CssClass="btn-default btn-lg" ForeColor="White" Height="50px" />
                                    <WizardSteps>
                                        <asp:CreateUserWizardStep runat="server" Title="Create a new user" ID="wizCreate" >
                                            <ContentTemplate>
                                                <table style="font-size:100%;width:100%;">
                                                    <tr>
                                                        <td align="left" colspan="2">Create a new user</td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" class="auto-style5">
                                                            <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">User Name:</asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="UserName" runat="server"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName" ErrorMessage="User Name is required." ToolTip="User Name is required." ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" class="auto-style5">
                                                            <asp:Label ID="FirstNameLabel" runat="server" AssociatedControlID="FirstName">First Name:</asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="FirstName" runat="server"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="FirstNameRequired" runat="server" ControlToValidate="FirstName" ErrorMessage="First Name is required." ToolTip="First Name is required." ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" class="auto-style5">
                                                            <asp:Label ID="LastNameLabel" runat="server" AssociatedControlID="LastName">Last Name:</asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="LastName" runat="server"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="LastNameRequired" runat="server" ControlToValidate="LastName" ErrorMessage="Last Name is required." ToolTip="Last Name is required." ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" class="auto-style5">
                                                            <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password">Password:</asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="Password" runat="server" TextMode="Password"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password" ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" class="auto-style5">
                                                            <asp:Label ID="ConfirmPasswordLabel" runat="server" AssociatedControlID="ConfirmPassword">Confirm Password:</asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="ConfirmPassword" runat="server" TextMode="Password"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="ConfirmPasswordRequired" runat="server" ControlToValidate="ConfirmPassword" ErrorMessage="Confirm Password is required." ToolTip="Confirm Password is required." ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" class="auto-style5">
                                                            <asp:Label ID="EmailLabel" runat="server" AssociatedControlID="Email">E-mail:</asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="Email" runat="server"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="EmailRequired" runat="server" ControlToValidate="Email" ErrorMessage="E-mail is required." ToolTip="E-mail is required." ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" colspan="2">
                                                            <asp:CompareValidator ID="PasswordCompare" runat="server" ControlToCompare="Password" ControlToValidate="ConfirmPassword" Display="Dynamic" ErrorMessage="The Password and Confirmation Password must match." ValidationGroup="CreateUserWizard1"></asp:CompareValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" colspan="2" style="color:Red;">
                                                            <asp:Literal ID="ErrorMessage" runat="server" EnableViewState="False"></asp:Literal>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                            <CustomNavigationTemplate>
                                                <table border="0" cellspacing="5" style="width:100%;height:100%; margin-left: 20px;">
                                                    <tr>
                                                        <td align="left" colspan="0">
                                                            <asp:Button ID="StepNextButton" runat="server" BackColor="#0D0435" CommandName="MoveNext" CssClass="btn-default btn-lg" ForeColor="White" Height="50px" Text="Create User" ValidationGroup="CreateUserWizard1" BorderStyle="None" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </CustomNavigationTemplate>
                                        </asp:CreateUserWizardStep>
                                        <asp:CompleteWizardStep runat="server" />
                                    </WizardSteps>
                                    <FinishCompleteButtonStyle CssClass="btn-default btn-lg" BorderStyle="None" />
                                    <FinishPreviousButtonStyle BorderStyle="None" />
                                    <NavigationButtonStyle BorderStyle="None" />
                                    <StartNextButtonStyle BorderStyle="None" />
                                    <StepNextButtonStyle BorderStyle="None" />
                                    <StepPreviousButtonStyle BorderStyle="None" />
                                    <SideBarButtonStyle BorderStyle="None" />
                                </asp:CreateUserWizard>
                            </div>
                        </div>
                    </td>

                    <td style="min-width: 10px; width: 10px;"></td>

                    <td style="width: 50%;">
                        <div class="contentBox" style="width: 100%; height: 450px;">
                            <img src="Images/recruiting.png" style="margin-left: 10px; margin-top: 5px; float: left;" /><h1 style="margin-left: 10px; display: inline-block; font-size: 30px; margin-top: 10px;">Edit Users</h1><br />
                            <div style="margin: 0 20px 20px 20px; font-family: Raleway;">
                                <asp:Wizard ID="Wizard1" runat="server" DisplaySideBar="False" Width="100%" ActiveStepIndex="0" OnFinishButtonClick="Wizard1_FinishButtonClick" StartNextButtonText="Edit User" OnNextButtonClick="Wizard1_NextButtonClick" OnPreviousButtonClick="Wizard1_PreviousButtonClick" FinishCompleteButtonText="Save Changes" FinishPreviousButtonText="&lt;Back" StepPreviousButtonText="&lt;Back" ValidateRequestMode="Enabled" BorderStyle="None">
                                    <FinishCompleteButtonStyle BackColor="#0D0435" CssClass="btn-default btn-lg" ForeColor="White" BorderStyle="None" />
                                    <FinishPreviousButtonStyle BackColor="#0D0435" CssClass="btn-default btn-lg" ForeColor="White" BorderStyle="None" />
                                    <NavigationButtonStyle BorderStyle="None" />
                                    <StartNextButtonStyle BackColor="#0D0435" CssClass="btn-default btn-lg" ForeColor="White" />
                                    <StepNextButtonStyle BorderStyle="None" />
                                    <StepPreviousButtonStyle BackColor="#0D0435" CssClass="btn-default btn-lg" ForeColor="White" BorderStyle="None" />
                                    <SideBarButtonStyle BorderStyle="None" />
                                    <StartNavigationTemplate>
                                        <asp:Button ID="StartNextButton" runat="server" BackColor="#0D0435" CommandName="MoveNext" CssClass="btn-default btn-lg" ForeColor="White" Text="Edit User" Enabled="false" BorderStyle="None" />
                                    </StartNavigationTemplate>
                                    <WizardSteps>
                                        <asp:WizardStep runat="server" title="Step 1" ID="Step1">
                                            <p>Search by username:</p>
                                            <asp:TextBox ID="sBox" runat="server"></asp:TextBox>
                                            <input class="btn-default btn-sm" type="button" value="Search" runat="server" style="color: white; background-color: #0D0435; border: none;" onserverclick="SearchButton" />

                                        &nbsp;&nbsp;</input></input><br /><br /><asp:ListBox ID="ListBox1" runat="server" Width="100%" Height="80px" OnSelectedIndexChanged="ListBox1_SelectedIndexChanged" AutoPostBack="True" ValidateRequestMode="Enabled"></asp:ListBox>
                                        </asp:WizardStep>
                                        <asp:WizardStep runat="server" title="Step 2">
                                            <table style="width: 100%">
                                                <tr>
                                                    <td>
                                                        User ID: 
                                                    </td>

                                                    <td>
                                                        <asp:TextBox ID="roUID" runat="server" ReadOnly="true" Enabled="false"></asp:TextBox>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        Username: 
                                                    </td>

                                                    <td>
                                                        <asp:TextBox ID="roUsername" runat="server" ReadOnly="true" Enabled="false"></asp:TextBox>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        Roles:
                                                    </td>

                                                    <td>
                                                        <div style="width: 100%; height: 100px; overflow-y: auto;">
                                                            <asp:CheckBoxList ID="CheckBoxList1" runat="server" Height="100px" OnLoad="CheckBoxList1_Load" Width="100%"></asp:CheckBoxList>
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        Account Locked:
                                                    </td>

                                                    <td>
                                                        <asp:CheckBox ID="Locked" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:WizardStep>
                                    </WizardSteps>
                                </asp:Wizard>

                                <p runat="server" id="errortext"></p>
                            </div>
                        </div>
                    </td>

                    <td style="min-width: 10px; width: 10px;"></td>
                </tr>

                <tr>
                    <td style="height: 20px;">

                    </td>
                </tr>

                <tr>
                    <td>

                    </td>

                    <td colspan="3" align="center">
                        <div class="contentBox" style="width: 500px; height: 450px; text-align: left;">
                            <img src="Images/project.png" style="margin-left: 10px; margin-top: 5px; float: left;" /><h1 style="margin-left: 10px; display: inline-block; font-size: 30px; margin-top: 10px;">Projects</h1><br />
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                            <div style="width: 100%; padding-left: 20px; padding-right: 20px; height: 300px;">
                                <asp:DropDownList ID="pProjDropdown" runat="server" AutoPostBack="True" OnSelectedIndexChanged="pProjDropdown_SelectedIndexChanged"></asp:DropDownList>&nbsp;
                                <asp:Button ID="pNewProject" runat="server" Text="New Project" OnClick="pNewProject_Click" />&nbsp;
                                <asp:Button ID="pDeleteProject" runat="server" Text="Delete Project" OnClick="pDeleteProject_Click" OnClientClick="return confirm_delete();" />
                                <table style="width: 100%;">
                                    <tr>
                                        <td style="width: 250px;">
                                            <p id="errortext2" runat="server" style="color: red;"></p>
                                        </td>

                                        <td class="auto-style4"></td>

                                        <td>
                                            
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                           Project Name:
                                        </td>

                                        <td style="width: 10px;"></td>

                                        <td>
                                            <asp:TextBox ID="pProjectName" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                           Project Manager:
                                        </td>

                                        <td style="width: 10px;"></td>

                                        <td>
                                            <asp:TextBox ID="pProjectManager" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                           Manager Phone:
                                        </td>

                                        <td></td>

                                        <td>
                                            <asp:TextBox ID="pProjectPhone" runat="server" TextMode="Phone"></asp:TextBox>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                           Manager Email:
                                        </td>

                                        <td style="width: 10px;"></td>

                                        <td>
                                            <asp:TextBox ID="pProjectEmail" runat="server" TextMode="Email"></asp:TextBox>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                           Start Date:
                                        </td>

                                        <td style="width: 10px;"></td>

                                        <td>
                                            <asp:TextBox ID="pStartDate" runat="server" TextMode="Date"></asp:TextBox>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                           Estimated Completion Date:
                                        </td>

                                        <td class="auto-style2"></td>

                                        <td class="auto-style1">
                                            <asp:TextBox ID="pEndDate" runat="server" TextMode="Date"></asp:TextBox>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                           Budget:
                                        </td>

                                        <td>$</td>

                                        <td>
                                            <asp:TextBox ID="pBudget" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <asp:Button ID="pAddProject" runat="server" Text="Save" OnClick="pAddProject_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </td>

                    <td>

                    </td>
                </tr>
            </table>
        </form>
    </body>
</html>
