﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Longview._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

        <table style="width: 100%; background-image: url(images/customer.png); background-size: cover; height: 350px; overflow: hidden;">
            <tbody>
                <tr>
                    <td valign="middle" align="center">
                        <p style="font-size: 60px; font-family: raleway; color: white; text-shadow: 2px 2px black;"><strong>Customer Portal</strong></p>
                    </td>
                </tr>
            </tbody>
        </table>

        <table style="width: 100%;" cellpadding="0" cellspacing="0">
		    <tbody>
		        <tr>
		            <td colspan="3" height="30px"></td>
			    </tr>

			    <tr>
				    <td style="width: 10%;">&nbsp;</td>

                    <asp:LoginView ID="lv" runat="server" ViewStateMode="Disabled">
                        <AnonymousTemplate>
				            <td align="center">
                                <asp:Login ID="Login1" runat="server" OnAuthenticate="Login1_Authenticate" Font-Names="Raleway" VisibleWhenLoggedIn="False" Font-Bold="False" Font-Size="Medium" Height="174px" Width="282px" ValidateRequestMode="Enabled" DestinationPageUrl="~/dashboard.aspx" OnLoggingIn="Login1_LoggingIn">
                                    <InstructionTextStyle Font-Bold="False" Font-Strikeout="False" />
                                    <LabelStyle Font-Bold="False" Font-Italic="False" Font-Size="Medium" Font-Strikeout="False" />
                                    <LayoutTemplate>
                                        <table cellpadding="1" cellspacing="0" style="border-collapse:collapse;">
                                            <tr>
                                                <td>
                                                    <table cellpadding="0" style="height:174px;width:282px;">
                                                        <tr>
                                                            <td align="center" colspan="2">Log In</td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right" style="font-size:Medium;font-weight:normal;font-style:normal;text-decoration:none;">
                                                                <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">User Name:</asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="UserName" runat="server"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName" ErrorMessage="User Name is required." ToolTip="User Name is required." ValidationGroup="ctl01$Login1">*</asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right" style="font-size:Medium;font-weight:normal;font-style:normal;text-decoration:none;">
                                                                <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password">Password:</asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="Password" runat="server" TextMode="Password"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password" ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="ctl01$Login1">*</asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <asp:CheckBox ID="RememberMe" runat="server" Text="Keep me signed in" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <a href="/forgot.aspx">Forgot your password?</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center" colspan="2" style="color:Red;">
                                                                <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right" colspan="2">
                                                                <asp:Button ID="LoginButton" runat="server" CommandName="Login" Text="Log In" ValidationGroup="ctl01$Login1" />
                                                            </td>
                                                        </tr> 
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </LayoutTemplate>
                                </asp:Login>
                                <span id="TFAPanel" runat="server" visible="false">
                                    <b>Two-factor Authentication</b><br />
                                    Two-factor authentication is enabled for this account. We have sent a 6-digit code to your email address. Please input the 6-digit code in the box below and click submit.<br />
                                    <asp:TextBox ID="TFACode" runat="server"></asp:TextBox>
                                    <asp:Button ID="TFASubmit" runat="server" Text="Submit" OnClick="TFASubmit_Click" />
                                </span>
                            </td>
                        </AnonymousTemplate>
                    </asp:LoginView> 
				    <td style="width: 10%;">&nbsp;</td>
			    </tr>

                <tr>
                    <td colspan="3" height="30px"></td>
                </tr>
		    </tbody>
	    </table>
</asp:Content>
