﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Overview.aspx.cs" Inherits="Longview.Overview2" MaintainScrollPositionOnPostback="true" %>

<!DOCTYPE html>

<html style="overflow-y: hidden;">
<head>
    <title></title>
    <link href="Content/bootstrap.css" rel="stylesheet" id="bootstrap-css" />
    <link href="Content/longview.css" rel="stylesheet" id="longview-css" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript" src="Scripts/smoothscroll.js"></script>

    <script>
        function selectActive(e) {
            var active = document.getElementsByClassName("iconLink active");
            active[0].classList.remove("active");
            e.classList.add("active");
            sessionStorage.setItem("activeScroll", e.id);
        }

        function scrollElement(e) {
            var rect = e.getBoundingClientRect();
            window.scrollTo({ top: rect.top + window.pageYOffset, left: 0, behavior: 'smooth' });
        }
    </script>

    <style>
        svg > g > g:last-child { pointer-events: none }

        .headerStyle
        {
            height: 30px;
            background: linear-gradient(to bottom, #242e3a 0%,#061a33 2%,#001e44 4%,#05224e 6%,#061c45 7%,#0a1c40 9%,#05224a 13%,#082044 15%,#09254d 28%,#0d2450 30%,#112f63 52%,#102f65 57%,#0b2c61 59%,#0b2c61 100%);
            color: white;
            text-shadow: 2px 2px black;
        }

        .iconLink
        {
            color: rgb(216, 216, 216); 
            text-decoration: none; 
            font-weight: 600; 
            display: block; 
            margin-bottom: 20px;
        }

        .iconLink:hover
        {
            color: darkblue;
            text-decoration: none;
        }

        .active
        {
            color: darkblue;
        }

        html {
          scroll-behavior: smooth;
        }
    </style>
</head>
<body style="background: linear-gradient(to bottom, rgba(191,191,191,1) 0%,rgba(232,232,232,1) 13%,rgba(244,244,244,1) 100%) no-repeat;">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <table style="width: 100%">
        <tr>
            <td rowspan="5" style="min-width: 100px; width: 100px; vertical-align: top;">
                <div style="background-color: white; height: 100%; width: 100px; position: fixed; text-align: center; padding-top: 10px; line-height: 0.5;">
                    <a id="NewsButton" class="iconLink active" href="javascript:void(0);" onclick="window.scrollTo({ top: 0, left: 0, behavior: 'smooth' }); selectActive(this);"><i class="material-icons" style="font-size: 48px;">chrome_reader_mode</i><br />News</a>
                    <a id="SurveyButton" class="iconLink" href="javascript:scrollElement(document.getElementById('surveybox'));" onclick="selectActive(this);"><i class="material-icons" style="font-size: 48px;">assignment_turned_in</i><br />Survey</a>
                    <a id="NotesButton" class="iconLink" href="javascript:scrollElement(document.getElementById('notes'));" onclick="selectActive(this);"><i class="material-icons" style="font-size: 48px;">notes</i><br />Notes</a>
                </div>
            </td>

            <td style="width: 100%">
                <div id="newsbox" class="contentBox" style="width: 100%; height: 800px;">
                    <img src="Images/news.png" style="margin-left: 10px; margin-top: 5px; float: left;" /><h1 style="margin-left: 10px; display: inline-block; font-size: 30px; margin-top: 10px;">Latest News</h1>
                    <div id="news" style="width: auto; height: 700px; margin-top: 10px; overflow-y: hidden; margin-left: 20px; margin-right: 20px;">
                        <iframe src="news.aspx" style="width: 100%; height: 100%; overflow: hidden;" scrolling="no" frameborder="0">
                        </iframe>
                    </div>
                </div>
            </td>
        </tr>

        <tr>
            <td style="width: 100%;">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="Button2" />
                    </Triggers>
                    <ContentTemplate>
                        <div id="surveybox" class="contentBox" style="width: 100%; height: 800px;">
                            <img src="Images/survey.png" style="margin-left: 10px; margin-top: 5px; float: left;" /><h1 style="margin-left: 10px; font-size: 30px; display: inline-block; margin-top: 10px;">Weekly Survey</h1>
                            <asp:Button ID="Button2" runat="server" Text="Edit" CssClass="btn-default btn-sm" Width="80px" BackColor="#0d0435" ForeColor="White" Visible="false" style="float: right; margin-top: 10px; margin-right: 20px; font-weight: 500;" BorderStyle="None" OnClick="Button2_Click" />
                            <br />
                            <div style="margin: 0 20px 20px 20px; font-size: 16pt; height: 700px;">
                                <iframe id="survey" runat="server" width="100%" height="100%" frameborder="0" allowtransparency="true" style="margin-top: 10px;" src="https://www.surveymonkey.com/r/T62HNHD"></iframe>
                                <span id="SurveyInput" runat="server" Visible="false">Survey URL: <asp:TextBox ID="SurveyURL" runat="server" TextMode="Url"></asp:TextBox></span>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>

        <tr>
            <td>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <Triggers>
                <asp:AsyncPostBackTrigger ControlID="Button1" />
                </Triggers>
                <ContentTemplate>
                    <div id="notes" class="contentBox" style="width: 100%; height: auto; overflow-wrap: break-word; word-wrap: break-word;">
                        <img src="Images/notes.png" style="margin-left: 10px; margin-top: 5px; float: left;" /><h1 style="margin-left: 10px; display: inline-block; font-size: 30px; margin-top: 10px;">Notes</h1>
                        <asp:Button ID="Button1" runat="server" Text="Edit" CssClass="btn-default btn-sm" Width="80px" BackColor="#0d0435" ForeColor="White" Visible="false" OnClick="Button1_Click" style="float: right; margin-top: 10px; margin-right: 20px; font-weight: 500;" BorderStyle="None" />
                        <br />
                        <div style="margin-left: 20px; margin-right: 20px; height: auto; max-height: 800px; overflow-y: auto;">
                            <p id="notesText" runat="server" style="font-size: 16pt; overflow-y: auto; word-break: break-word;">
                                (No current notes)
                            </p>
                            <asp:TextBox ID="TextBox1" runat="server" Visible="false" Height="100%" TextMode="MultiLine" Width="100%" style="resize:none;" ></asp:TextBox>
                        </div>
                    </div>
                </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>

        <tr>
            <td style="height: 700px;">

            </td>
        </tr>
    </table>
    </form>
    <script>
        if (sessionStorage.getItem("activeScroll") != null) {
            var el = document.getElementById(sessionStorage.getItem("activeScroll"));
            if (el != null) {
                el.click();
            }
        }
    </script>
</body>
</html>
