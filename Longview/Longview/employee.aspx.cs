﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Longview
{
    public partial class employee : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!User.Identity.IsAuthenticated)
            {
                Response.Redirect("Default.aspx");
                return;
            }
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(GridView1, "Select$" + e.Row.RowIndex);
                e.Row.Attributes["style"] = "cursor:pointer";
                e.Row.Attributes["onmouseover"] = "this.style.backgroundColor = '#061c45'; this.style.color = '#fff'; this.style.textShadow = '2px 2px black'";
                e.Row.Attributes["onmouseout"] = "this.style.backgroundColor = ''; this.style.color = '#181727'; this.style.textShadow = ''";
            }
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Project p = ProjectsManager.GetRoleByName("Overview");
            if (p.getFiles().ContainsKey(GridView1.SelectedRow.Cells[0].Text))
            {
                Uri path;
                if(Uri.TryCreate(p.getFiles()[GridView1.SelectedRow.Cells[0].Text].path, UriKind.Relative, out path))
                    Response.Redirect(path.OriginalString);
                else
                    ClientScript.RegisterStartupScript(this.GetType(), "scriptid", "window.parent.location.href='" + p.getFiles()[GridView1.SelectedRow.Cells[0].Text].path + "'", true);
            }
        }

        protected void GridView1_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (User.IsInRole("Administrators"))
                {
                    CommandField field = new CommandField();
                    field.DeleteText = "Delete";
                    field.ShowDeleteButton = true;
                    field.ItemStyle.Width = new Unit("10px");
                    GridView1.Columns.Add(field);
                }
            }

            Project p = ProjectsManager.GetRoleByName("Overview");
            DataTable dt = new DataTable();

            if (dt.Columns.Count == 0)
            {
                dt.Columns.Add("Document", typeof(string));
            }

            DataRow dr;
            foreach (KeyValuePair<string, ProjectFile> kvp in p.getFiles())
            {
                dr = dt.NewRow();
                dr[0] = kvp.Key;
                dt.Rows.Add(dr);
            }

            GridView1.DataSource = dt;
            GridView1.DataBind();
        }

        protected void UploadButton_Click(object sender, EventArgs e)
        {
            if (!User.IsInRole("Administrators"))
                return;

            if (!IsStrValid(TextBox1.Text))
            {
                errortext.InnerText = "File name field is invalid. Allowable characters: \"A-Z a-z 0-9 - _\". Field must also be at least 4 characters.";
                return;
            }

            Project proj = ProjectsManager.GetRoleByName("Overview");
            FileType ft = FileType.FILETYPE_TIMESHEET;

            if (proj.getFiles().ContainsKey(TextBox1.Text))
            {
                errortext.InnerText = "A file with that name already exists.";
                return;
            }

            if (RadioButtonList1.SelectedValue == "Link")
            {
                Uri url;
                if (Uri.TryCreate(TextBox2.Text, UriKind.Absolute, out url))
                {
                    ProjectFile fl = new ProjectFile(url.AbsoluteUri, FileType.FILETYPE_SURVEY);
                    proj.addFile(TextBox1.Text, fl);
                    Server.TransferRequest(Request.Url.AbsolutePath, false);
                    errortext.InnerText = "Link added.";
                }
                return;
            }

            String path = Server.MapPath("~/Documents/");

            Boolean fileOK = false;
            if (!FileUpload1.HasFile)
            {
                errortext.InnerText = "Please select a file to upload.";
                return;
            }

            if (FileUpload1.PostedFile.ContentLength > 4194304)
            {
                errortext.InnerText = "File cannot be larger than 4MB.";
                return;
            }

            if (File.Exists(path + FileUpload1.PostedFile.FileName))
            {
                errortext.InnerText = "File with that name already exists. Please rename the file before re-uploading.";
                return;
            }

            String fileExtension =
                    System.IO.Path.GetExtension(FileUpload1.FileName).ToLower();
            String allowedExtension = ".pdf";
            if (fileExtension == allowedExtension)
                fileOK = true;

            if (!fileOK)
            {
                errortext.InnerText = "Cannot accept files of this type.";
                return;
            }            

            try
            {
                FileUpload1.PostedFile.SaveAs(path
                    + FileUpload1.FileName);
                path = path.Replace(Request.ServerVariables["APPL_PHYSICAL_PATH"], String.Empty);
                ProjectFile fl = new ProjectFile(path + FileUpload1.FileName, ft);
                proj.addFile(TextBox1.Text, fl);
                Server.TransferRequest(Request.Url.AbsolutePath, false);
                errortext.InnerText = "File uploaded.";
            }
            catch (Exception ex)
            {
                errortext.InnerText = "File could not be uploaded.";
            }
        }

        protected bool IsStrValid(string s)
        {
            if (s.Length <= 3)
                return false;

            if (!Regex.IsMatch(s, @"^([A-Za-z0-9\s-_]+)$"))
                return false;

            return true;
        }

        protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(RadioButtonList1.SelectedValue == "Link")
            {
                FileUpload1.Visible = false;
                TextBox2.Visible = true;
                Upload.InnerText = "URL";
                UploadButton.Text = "Add";
            }
            else
            {
                FileUpload1.Visible = true;
                TextBox2.Visible = false;
                Upload.InnerText = "Upload";
                UploadButton.Text = "Upload";
            }
        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            if(!User.IsInRole("Administrators"))
                return;

            Project proj = ProjectsManager.GetRoleByName("Overview");
            if (proj.getFiles().ContainsKey(GridView1.Rows[e.RowIndex].Cells[0].Text))
            {
                if(proj.getFiles()[GridView1.Rows[e.RowIndex].Cells[0].Text].type != FileType.FILETYPE_SURVEY)
                {
                    string path = Server.MapPath(proj.getFiles()[GridView1.Rows[e.RowIndex].Cells[0].Text].path);
                    if (File.Exists(path))
                        File.Delete(path);
                }
                proj.removeFile(GridView1.Rows[e.RowIndex].Cells[0].Text);
                Server.TransferRequest(Request.Url.AbsolutePath, false);
            }
        }

        protected void BindTable()
        {
            DataTable dt = new DataTable();
            if (dt.Columns.Count == 0)
            {
                dt.Columns.Add("ID", typeof(string));
                dt.Columns.Add("Last Name", typeof(string));
                dt.Columns.Add("First Name", typeof(string));
                dt.Columns.Add("Position", typeof(string));
                dt.Columns.Add("Start Date", typeof(string));
                dt.Columns.Add("Weekly Hours", typeof(string));
                dt.Columns.Add("Hourly Rate", typeof(string));
            }

            DataRow dr;
            foreach (KeyValuePair<int, Company.Employee> kvp in Company.GetEmployees())
            {
                dr = dt.NewRow();
                dr[0] = kvp.Key;
                dr[1] = kvp.Value.LastName;
                dr[2] = kvp.Value.FirstName;
                dr[3] = kvp.Value.Position;
                dr[4] = kvp.Value.StartDate.ToString("yyyy-MM-dd");
                dr[5] = kvp.Value.WeeklyHours;
                dr[6] = kvp.Value.HourlyRate;
                dt.Rows.Add(dr);
            }

            GridView2.DataSource = dt;
            GridView2.DataBind();
            ViewState["dtbl"] = dt;
        }

        protected void GridView2_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
                BindTable();
        }

        protected void GridView2_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            e.Cancel = true;
            int index = int.Parse(GridView2.Rows[e.RowIndex].Cells[0].Text);
            Company.RemoveEmployee(index);
            BindTable();
        }

        protected void GridView2_RowEditing(object sender, GridViewEditEventArgs e)
        {
            e.Cancel = true;
            NextButton(true);
            int id = int.Parse(GridView2.Rows[e.NewEditIndex].Cells[0].Text);
            EmployeeID.Text = GridView2.Rows[e.NewEditIndex].Cells[0].Text;
            LastName.Text = HttpUtility.HtmlDecode(GridView2.Rows[e.NewEditIndex].Cells[1].Text);
            FirstName.Text = HttpUtility.HtmlDecode(GridView2.Rows[e.NewEditIndex].Cells[2].Text);
            Position.Text = HttpUtility.HtmlDecode(GridView2.Rows[e.NewEditIndex].Cells[3].Text);
            StartDate.Text = GridView2.Rows[e.NewEditIndex].Cells[4].Text;
            WeeklyHours.Text = GridView2.Rows[e.NewEditIndex].Cells[5].Text;
            HourlyRate.Text = GridView2.Rows[e.NewEditIndex].Cells[6].Text;
            Rewards.Text = String.Format("{0}", Company.GetEmployeeById(id).Rewards);
            Recommendations.Text = String.Format("{0}", Company.GetEmployeeById(id).Recommendations);
            Reprimands.Text = String.Format("{0}", Company.GetEmployeeById(id).Reprimands);
            CorrectiveActions.Text = String.Format("{0}", Company.GetEmployeeById(id).CorrectiveActions);
            HWRate.Text = String.Format("{0}", Company.GetEmployeeById(id).HWRate);
            errortextemp.InnerText = "";
        }

        protected void SaveEmployee_Click(object sender, EventArgs e)
        {
            int index;
            if(!int.TryParse(EmployeeID.Text, out index))
            {
                errortextemp.InnerText = "The employee ID is invalid.";
                return;
            }

            if(FirstName.Text == "")
            {
                errortextemp.InnerText = "First name field cannot be blank.";
                return;
            }

            if (LastName.Text == "")
            {
                errortextemp.InnerText = "First name field cannot be blank.";
                return;
            }

            DateTime startdate;
            if(!DateTime.TryParse(StartDate.Text, out startdate))
            {
                errortextemp.InnerText = "The employee start date is invalid.";
                return;
            }

            decimal weeklyhours;
            if(!decimal.TryParse(WeeklyHours.Text, out weeklyhours))
            {
                errortextemp.InnerText = "Weekly hours is invalid.";
                return;
            }

            decimal hourlyrate;
            if(!decimal.TryParse(HourlyRate.Text, out hourlyrate))
            {
                errortextemp.InnerText = "Hourly rate is invalid.";
                return;
            }

            int rewards;
            if(!int.TryParse(Rewards.Text, out rewards))
            {
                errortextemp.InnerText = "The Rewards field is invalid.";
                return;
            }

            int recommendations;
            if(!int.TryParse(Recommendations.Text, out recommendations))
            {
                errortextemp.InnerText = "The Recommendations field is invalid.";
                return;
            }

            int reprimands;
            if(!int.TryParse(Reprimands.Text, out reprimands))
            {
                errortextemp.InnerText = "The Reprimands field is invalid.";
                return;
            }

            int correctiveactions;
            if (!int.TryParse(CorrectiveActions.Text, out correctiveactions))
            {
                errortextemp.InnerText = "The Corrective Actions field is invalid.";
                return;
            }

            decimal hwrate;
            if(!decimal.TryParse(HWRate.Text, out hwrate))
            {
                errortextemp.InnerText = "The HW Rate field is invalid.";
                return;
            }

            BackButton();

            Company.EditEmployee(index, LastName.Text, FirstName.Text, Position.Text, startdate, weeklyhours, hourlyrate, rewards, recommendations, reprimands, correctiveactions, hwrate);
            BindTable();
        }

        protected void CancelEmployee_Click(object sender, EventArgs e)
        {
            BackButton();
        }

        protected void NextButton(bool save = true)
        {
            GridView2.Visible = false;
            EmployeeEditor.Style.Add("display", "block");
            EmployeeID.Enabled = true;
            SaveEmployee.Visible = save;
            AddNewEmployee.Visible = !save;
            SearchBox.Visible = false;
            SearchButton.Visible = false;
            AddEmployee.Visible = false;
        }

        protected void SelectButton()
        {
            GridView2.Visible = false;
            EmployeeView.Style.Add("display", "block");
            SearchBox.Visible = false;
            SearchButton.Visible = false;
            AddEmployee.Visible = false;
        }

        protected void BackButton()
        {
            GridView2.Visible = true;
            SearchBox.Visible = true;
            SearchButton.Visible = true;
            AddEmployee.Visible = true;
            EmployeeEditor.Style.Add("display", "none");
            EmployeeView.Style.Add("display", "none");
        }

        protected void AddEmployee_Click(object sender, EventArgs e)
        {
            EmployeeID.Text = "";
            LastName.Text = "";
            FirstName.Text = "";
            Position.Text = "";
            StartDate.Text = "";
            WeeklyHours.Text = "";
            HourlyRate.Text = "";
            Rewards.Text = "";
            Recommendations.Text = "";
            Reprimands.Text = "";
            CorrectiveActions.Text = "";
            HWRate.Text = "";
            errortextemp.InnerText = "";
            NextButton(false);
        }

        protected void AddNewEmployee_Click(object sender, EventArgs e)
        {
            int index;
            if (!int.TryParse(EmployeeID.Text, out index))
            {
                errortextemp.InnerText = "The employee ID is invalid.";
                return;
            }

            if (Company.GetEmployees().ContainsKey(index))
            {
                errortextemp.InnerText = "The employee ID already exists.";
                return;
            }

            if (FirstName.Text == "")
            {
                errortextemp.InnerText = "First name field cannot be blank.";
                return;
            }

            if (LastName.Text == "")
            {
                errortextemp.InnerText = "First name field cannot be blank.";
                return;
            }

            DateTime startdate;
            if (!DateTime.TryParse(StartDate.Text, out startdate))
            {
                errortextemp.InnerText = "The employee start date is invalid.";
                return;
            }

            decimal weeklyhours;
            if (!decimal.TryParse(WeeklyHours.Text, out weeklyhours))
            {
                errortextemp.InnerText = "Weekly hours is invalid.";
                return;
            }

            decimal hourlyrate;
            if (!decimal.TryParse(HourlyRate.Text, out hourlyrate))
            {
                errortextemp.InnerText = "Hourly rate is invalid.";
                return;
            }

            int rewards;
            if (!int.TryParse(Rewards.Text, out rewards))
            {
                errortextemp.InnerText = "The Rewards field is invalid.";
                return;
            }

            int recommendations;
            if (!int.TryParse(Recommendations.Text, out recommendations))
            {
                errortextemp.InnerText = "The Recommendations field is invalid.";
                return;
            }

            int reprimands;
            if (!int.TryParse(Reprimands.Text, out reprimands))
            {
                errortextemp.InnerText = "The Reprimands field is invalid.";
                return;
            }

            int correctiveactions;
            if (!int.TryParse(CorrectiveActions.Text, out correctiveactions))
            {
                errortextemp.InnerText = "The Corrective Actions field is invalid.";
                return;
            }

            decimal hwrate;
            if (!decimal.TryParse(HWRate.Text, out hwrate))
            {
                errortextemp.InnerText = "The HW Rate field is invalid.";
                return;
            }

            BackButton();

            Company.Employee emp = new Company.Employee(LastName.Text, FirstName.Text, Position.Text, startdate, weeklyhours, hourlyrate, rewards,
                recommendations, reprimands, correctiveactions, hwrate, false);
            Company.AddEmployee(index, emp);
            BindTable();
        }

        protected void SearchButton_Click(object sender, EventArgs e)
        {
            if(SearchBox.Text == "")
            {
                BindTable();
                return;
            }

            DataTable dt = new DataTable();
            if (dt.Columns.Count == 0)
            {
                dt.Columns.Add("ID", typeof(string));
                dt.Columns.Add("Last Name", typeof(string));
                dt.Columns.Add("First Name", typeof(string));
                dt.Columns.Add("Position", typeof(string));
                dt.Columns.Add("Start Date", typeof(string));
                dt.Columns.Add("Weekly Hours", typeof(string));
                dt.Columns.Add("Hourly Rate", typeof(string));
            }

            DataRow dr;
            foreach (KeyValuePair<int, Company.Employee> kvp in Company.GetEmployees().Where(x => x.Value.LastName.ToUpper().Contains(SearchBox.Text.ToUpper())))
            {
                dr = dt.NewRow();
                dr[0] = kvp.Key;
                dr[1] = kvp.Value.LastName;
                dr[2] = kvp.Value.FirstName;
                dr[3] = kvp.Value.Position;
                dr[4] = kvp.Value.StartDate.ToString("yyyy-MM-dd");
                dr[5] = kvp.Value.WeeklyHours;
                dr[6] = kvp.Value.HourlyRate;
                dt.Rows.Add(dr);
            }

            foreach (KeyValuePair<int, Company.Employee> kvp in Company.GetEmployees().Where(x => x.Value.FirstName.ToUpper().Contains(SearchBox.Text.ToUpper()) && !x.Value.LastName.ToUpper().Contains(SearchBox.Text.ToUpper())))
            {
                dr = dt.NewRow();
                dr[0] = kvp.Key;
                dr[1] = kvp.Value.LastName;
                dr[2] = kvp.Value.FirstName;
                dr[3] = kvp.Value.Position;
                dr[4] = kvp.Value.StartDate.ToString("yyyy-MM-dd");
                dr[5] = kvp.Value.WeeklyHours;
                dr[6] = kvp.Value.HourlyRate;
                dt.Rows.Add(dr);
            }

            GridView2.DataSource = dt;
            GridView2.DataBind();
            ViewState["dtbl"] = dt;
        }

        private const string ASCENDING = " ASC";
        private const string DESCENDING = " DESC";

        private SortDirection GridViewSortDirection
        {
            get
            {
                if (ViewState["sortDirection"] == null)
                    ViewState["sortDirection"] = SortDirection.Ascending;

                return (SortDirection)ViewState["sortDirection"];
            }
            set { ViewState["sortDirection"] = value; }
        }

        protected void GridView2_Sorting(object sender, GridViewSortEventArgs e)
        {
            string sortExpression = e.SortExpression;

            if (GridViewSortDirection == SortDirection.Ascending)
            {
                GridViewSortDirection = SortDirection.Descending;
                SortGridView(sortExpression, DESCENDING);
            }
            else
            {
                GridViewSortDirection = SortDirection.Ascending;
                SortGridView(sortExpression, ASCENDING);
            }
        }

        private void SortGridView(string sortExpression, string direction)
        {
            //  You can cache the DataTable for improving performance
            DataTable dt = ViewState["dtbl"] as DataTable;

            DataView dv = new DataView(dt);
            dv.Sort = sortExpression + direction;

            GridView2.DataSource = dv;
            GridView2.DataBind();
        }

        protected void GridView2_Sorted(object sender, EventArgs e)
        {

        }

        protected void GridView2_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            int id = int.Parse(GridView2.Rows[e.NewSelectedIndex].Cells[0].Text);
            LitEmpID.Text = GridView2.Rows[e.NewSelectedIndex].Cells[0].Text;
            LitLastName.Text = GridView2.Rows[e.NewSelectedIndex].Cells[1].Text;
            LitFirstName.Text = GridView2.Rows[e.NewSelectedIndex].Cells[2].Text;
            LitPosition.Text = GridView2.Rows[e.NewSelectedIndex].Cells[3].Text;
            LitStartDate.Text = GridView2.Rows[e.NewSelectedIndex].Cells[4].Text;
            LitWeeklyHours.Text = GridView2.Rows[e.NewSelectedIndex].Cells[5].Text;
            LitHourlyRate.Text = GridView2.Rows[e.NewSelectedIndex].Cells[6].Text;
            LitRew.Text = String.Format("{0}", Company.GetEmployeeById(id).Rewards);
            LitRecom.Text = String.Format("{0}", Company.GetEmployeeById(id).Recommendations);
            LitReprimands.Text = String.Format("{0}", Company.GetEmployeeById(id).Reprimands);
            LitCorrectiveActions.Text = String.Format("{0}", Company.GetEmployeeById(id).CorrectiveActions);
            LitHWRate.Text = String.Format("{0}", Company.GetEmployeeById(id).HWRate);
            SelectButton();
        }

        protected void LinkButton2_Click(object sender, EventArgs e)
        {
            BackButton();
        }
    }
}