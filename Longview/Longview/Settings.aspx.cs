﻿using Microsoft.AspNet.Identity;
using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;

namespace Longview
{
    public partial class Settings : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Page.User.Identity.IsAuthenticated)
                {
                    string fullName = (string)HttpContext.Current.Profile.GetPropertyValue("firstname") == "" ? Page.User.Identity.GetUserName() : (string)HttpContext.Current.Profile.GetPropertyValue("firstname");
                    Username.Text = "Hello, " + fullName + "&nbsp;&nbsp;&nbsp;";

                    pUsername.Text = User.Identity.GetUserName();
                    pEmail.Text = Membership.GetUser(User.Identity.GetUserName()).Email;
                    pFirstName.Text = (string)HttpContext.Current.Profile.GetPropertyValue("firstname");
                    pLastName.Text = (string)HttpContext.Current.Profile.GetPropertyValue("lastname");

                    if ((string)HttpContext.Current.Profile.GetPropertyValue("tfakey") == "1")
                    {
                        pTFA.Text = "Enabled";
                        pTFA.ForeColor = System.Drawing.Color.Green;
                        pToggleTFA.Text = "Disable two-factor authentication";
                    }
                }
                else
                {
                    Response.Redirect("~/Default");
                }
            }
        }

        protected void Button1_OnClick(object sender, EventArgs e)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated == true)
            {
                FormsAuthentication.SignOut();
                Response.Cookies[FormsAuthentication.FormsCookieName].Expires = DateTime.Now.AddYears(-1);
                FormsAuthentication.RedirectToLoginPage();
            }
        }

        protected void pEditAccountInfo_Click(object sender, EventArgs e)
        {
            if (pEditAccountInfo.Text == "Edit")
            {
                pEditAccountInfo.Text = "Save";
                pEmail.Enabled = true;
                pFirstName.Enabled = true;
                pLastName.Enabled = true;
                aierrortext.InnerText = "";
            }
            else
            {
                bool pass = true;
                aierrortext.InnerText = "";
                if (pEmail.Text != Membership.GetUser(User.Identity.Name).Email)
                {
                    if (Regex.IsMatch(pEmail.Text, @"\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}\b") &&
                        Membership.FindUsersByEmail(pEmail.Text).Count == 0)
                    {
                        MembershipUser m = Membership.GetUser(User.Identity.Name);
                        m.Email = pEmail.Text;
                        Membership.UpdateUser(m);
                        aierrortext.InnerText = "Email has been changed successfully. ";
                    }
                    else
                    {
                        aierrortext.InnerText = "Email is not valid or is already in use.";
                        pass = false;
                    }
                }

                if (pFirstName.Text != (string)HttpContext.Current.Profile.GetPropertyValue("firstname"))
                {
                    if (Regex.IsMatch(pFirstName.Text, @"^([A-Za-z-]+)$") && pFirstName.Text.Count() > 3)
                    {
                        HttpContext.Current.Profile.SetPropertyValue("firstname", pFirstName.Text);
                        HttpContext.Current.Profile.Save();
                        aierrortext.InnerText += "First name has been changed successfully. ";
                    }
                    else
                    {
                        aierrortext.InnerText += "First name must countain at least 4 characters and cannot contain numbers, symbols, or spaces. ";
                        pass = false;
                    }
                }

                if (pLastName.Text != (string)HttpContext.Current.Profile.GetPropertyValue("lastname"))
                {
                    if (Regex.IsMatch(pLastName.Text, @"^([A-Za-z-]+)$") && pLastName.Text.Count() > 3)
                    {
                        HttpContext.Current.Profile.SetPropertyValue("lastname", pLastName.Text);
                        HttpContext.Current.Profile.Save();
                        aierrortext.InnerText += "Last name has been changed successfully.";
                    }
                    else
                    {
                        aierrortext.InnerText += "Last name must countain at least 4 characters and cannot contain numbers, symbols, or spaces.";
                        pass = false;
                    }
                }

                if (!pass)
                    return;

                pEditAccountInfo.Text = "Edit";
                pEmail.Enabled = false;
                pFirstName.Enabled = false;
                pLastName.Enabled = false;
            }
        }

        protected void pEditPassword_Click(object sender, EventArgs e)
        {
            if (pEditPassword.Text == "Edit")
            {
                pEditPassword.Text = "Save";
                pPassword.Enabled = true;
                pNewPassword.Enabled = true;
                pConfirmPassword.Enabled = true;
                passerrortext.InnerText = "";
            }
            else
            {
                if (pPassword.Text == "")
                {
                    passerrortext.InnerText = "Password cannot be blank.";
                    return;
                }

                if (pNewPassword.Text == "")
                {
                    passerrortext.InnerText = "New password cannot be blank.";
                    return;
                }

                if (pConfirmPassword.Text == "")
                {
                    passerrortext.InnerText = "Confirm password cannot be blank.";
                    return;
                }

                if (!Membership.ValidateUser(User.Identity.Name, pPassword.Text))
                {
                    passerrortext.InnerText = "Password is incorrect.";
                    return;
                }

                if (pNewPassword.Text != pConfirmPassword.Text)
                {
                    passerrortext.InnerText = "Password confirmation does not match.";
                    return;
                }

                if (Membership.ValidateUser(User.Identity.Name, pNewPassword.Text))
                {
                    passerrortext.InnerText = "New password must be different from the old password.";
                    if (Membership.GetUser(User.Identity.Name).IsLockedOut)
                        Membership.GetUser(User.Identity.Name).UnlockUser();
                    return;
                }

                if (pNewPassword.Text.Count() < 6)
                {
                    passerrortext.InnerText = "Password must be at least 6 characters.";
                    return;
                }

                if (!Regex.IsMatch(pNewPassword.Text, @"^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$"))
                {
                    passerrortext.InnerText = "Password must contain at least one letter and one number.";
                    return;
                }

                if (!Membership.GetUser(User.Identity.Name).ChangePassword(pPassword.Text, pNewPassword.Text))
                {
                    passerrortext.InnerText = "Your password could not be changed due to an unknown error.";
                    return;
                }

                passerrortext.InnerText = "Your password has been changed successfully.";
                pEditPassword.Text = "Edit";
                pPassword.Enabled = false;
                pNewPassword.Enabled = false;
                pConfirmPassword.Enabled = false;
            }
        }

        protected void pToggleTFA_Click(object sender, EventArgs e)
        {
            if (pTFA.Text == "Disabled")
            {
                pToggleTFA.Text = "Disable two-factor authentication";
                pTFA.Text = "Enabled";
                pTFA.ForeColor = System.Drawing.Color.Green;
                HttpContext.Current.Profile.SetPropertyValue("tfakey", "1");
                HttpContext.Current.Profile.Save();
            }
            else
            {
                pToggleTFA.Text = "Enable two-factor authentication";
                pTFA.Text = "Disabled";
                pTFA.ForeColor = System.Drawing.Color.Red;
                HttpContext.Current.Profile.SetPropertyValue("tfakey", "0");
                HttpContext.Current.Profile.Save();
            }
        }
    }
}