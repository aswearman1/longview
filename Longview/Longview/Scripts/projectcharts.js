﻿
google.charts.load('current', { packages: [['corechart'], ['table'], ['gantt']] });
//google.charts.setOnLoadCallback(drawChart);

function daysToMilliseconds(days) {
    return days * 24 * 60 * 60 * 1000;
}

function drawChart() {

    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Task ID');
    data.addColumn('string', 'Task Name');
    data.addColumn('date', 'Start Date');
    data.addColumn('date', 'End Date');
    data.addColumn('number', 'Duration');
    data.addColumn('number', 'Percent Complete');
    data.addColumn('string', 'Dependencies');

    data.addRows([
      ['Research', 'Find sources',
       new Date(2015, 0, 1), new Date(2015, 0, 5), null,  100,  null],
      ['Write', 'Write paper',
       null, new Date(2015, 0, 9), daysToMilliseconds(3), 25, 'Research,Outline'],
      ['Cite', 'Create bibliography',
       null, new Date(2015, 0, 7), daysToMilliseconds(1), 20, 'Research'],
      ['Complete', 'Hand in paper',
       null, new Date(2015, 0, 10), daysToMilliseconds(1), 0, 'Cite,Write'],
      ['Outline', 'Outline paper',
       null, new Date(2015, 0, 6), daysToMilliseconds(1), 100, 'Research']
    ]);

    var options = {
        height: 275,
        width: '100%',
    };

    var chart = new google.visualization.Gantt(document.getElementById('chart_div'));

    chart.draw(data, options);
}

function drawBasic() {
    //Start profit chart
    var tableData = new google.visualization.DataTable();
    tableData.addColumn('string', 'Profit');
    tableData.addColumn('number', 'Amount');
    tableData.addRows([
      ['Service', 7000],
      ['Products', 10000],
      ['Other', 2500],
    ]);

    var total = 0;
    for (var i = 0; i < tableData.getNumberOfRows() ; ++i) {
        total = total + tableData.getValue(i, 1);
    }

    tableData.addRow(['TOTAL', total]);

    var formatter = new google.visualization.NumberFormat({
        prefix: '$',
        negativeParens: true
    });

    formatter.format(tableData, 1);

    var table = new google.visualization.Table(document.getElementById('profit'));

    table.draw(tableData, { showRowNumber: false, width: '100%', height: '100%', vAxis: { format: '$###,###,###.00' } });

    //Start loss chart
    var tableData2 = new google.visualization.DataTable();
    tableData2.addColumn('string', 'Loss');
    tableData2.addColumn('number', 'Amount');
    tableData2.addRows([
      ['Labor', 2483.26],
      ['Marketing', 928.58],
      ['Operating Costs', 10820.82],
      ['Taxes', 302.43],
      ['Other', 0],
    ]);

    total = 0;
    for (var i = 0; i < tableData2.getNumberOfRows() ; ++i) {
        total = total + tableData2.getValue(i, 1);
    }

    tableData2.addRow(['TOTAL', total]);
    formatter.format(tableData2, 1);

    var table2 = new google.visualization.Table(document.getElementById('losses'));

    table2.draw(tableData2, { showRowNumber: false, width: '100%', height: '100%', vAxis: { format: '$###,###,###.00' }});

    //Start bar graph
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'X');
    data.addColumn('number', 'Expenses');

    data.addRows([
      ['Jan', 8321], ['Feb', 305],
      ['Mar', 3383], ['Apr', 5390],
      ['May', 2340], ['Jun', 3824],
      ['Jul', 2340], ['Aug', 9674],
      ['Sept', 5032], ['Oct', 4067],
      ['Nov', 9350], ['Dec', 2907],
    ]);

    var topLevel = 0;

    var options = {
        hAxis: {
            title: 'Month'
        },
        vAxis: {
            title: 'Spent'
        },
        title: 'Monthly Spending',
        legend: { position: 'none' },
        chartArea: { width: '100%', },
        animation: {
            duration: 1000,
            easing: 'out'
        },
    };

    var chart = new google.visualization.ColumnChart(document.getElementById('columnChart'));

    chart.draw(data, options);
    google.visualization.events.addListener(chart, 'select', function selectHandler(e) {
        var selectedItem = chart.getSelection()[0];
        switch (topLevel) {
            case 0:
                if (selectedItem) {
                    var mo = data.getValue(selectedItem.row, 0);
                    var val = data.getValue(selectedItem.row, 1);

                    options = {
                        hAxis: {
                            title: 'Day'
                        },
                        vAxis: {
                            title: 'Spent'
                        },
                        title: mo + ' Spending',
                        legend: { position: 'none' },
                        chartArea: { width: '100%', },
                        animation: {
                            duration: 1000,
                            easing: 'out'
                        },
                    };

                    chart.draw(getData(mo, val), options);
                    table.draw(getTableData(val * 1.07, true), { showRowNumber: false, width: '100%', height: '100%', vAxis: { format: '$###,###,###.00' } });
                    table2.draw(getTableData(val, false), { showRowNumber: false, width: '100%', height: '100%', vAxis: { format: '$###,###,###.00' } });
                    topLevel += 1;
                }
                break;

            case 1:
                options = {
                    hAxis: {
                        title: 'Month'
                    },
                    vAxis: {
                        title: 'Spent'
                    },
                    title: 'Monthly Spending',
                    legend: { position: 'none' },
                    chartArea: { width: '100%', },
                    animation: {
                        duration: 1000,
                        easing: 'out'
                    },
                };

                chart.draw(data, options);
                table.draw(tableData, { showRowNumber: false, width: '100%', height: '100%', vAxis: { format: '$###,###,###.00' } });
                table2.draw(tableData2, { showRowNumber: false, width: '100%', height: '100%', vAxis: { format: '$###,###,###.00' } });
                topLevel = 0;
                break;
        }
    });
}

function getData(row, value)
{
    var data = new google.visualization.DataTable();
    data.addColumn('number', 'X');
    data.addColumn('number', 'Expenses');

    var c = [ [1, value * .03], [2, value * .02],
            [3, value * .09], [4, value * .06],
            [5, value * .01], [6, value * .02],
            [7, value * .02],
            [8, value * .04], [9, value * .05],
            [10, value * .02], [11, value * .01],
            [12, value * .01], [13, value * .02],
            [14, value * .0],
            [15, value * .04], [16, value * .01],
            [17, value * .07], [18, value * .04],
            [19, value * .02], [20, value * .08],
            [21, value * .0],
            [22, value * .09], [23, value * .04],
            [24, value * .02], [25, value * .07],
            [26, value * .0], [27, value * .02],
            [28, value * .0],
            [29, value * .05], [30, value * .02] ];

    data.addRows(c);

    switch(row)
    {
        case "Jan":
        case "Mar":
        case "May":
        case "Jul":
        case "Aug":
        case "Oct":
        case "Dec":
            data.addRows([
                [31, value * .03],
            ]);
            break;

        case "Feb":
            data.removeRow(29);
            break;
    }

    return data;
}

function getTableData(value, profit)
{
    var data = new google.visualization.DataTable();
    if(profit)
    {
        data.addColumn('string', 'Profit');
        data.addColumn('number', 'Amount');

        data.addRows([
        ['Service', value * .54],
        ['Products', value * .32],
        ['Other', value * .14],
        ]);
    }
    else
    {
        data.addColumn('string', 'Loss');
        data.addColumn('number', 'Amount');

        data.addRows([
          ['Labor', value * .12],
          ['Marketing', value * .08],
          ['Operating Costs', value * .73],
          ['Taxes', value * .07],
          ['Other', 0],
        ]);
    }

    var total = 0;
    for (var i = 0; i < data.getNumberOfRows(); ++i) {
        total = total + data.getValue(i, 1);
    }

    data.addRow(['TOTAL', total]);

    var formatter = new google.visualization.NumberFormat({
        prefix: '$',
        negativeParens: true
    });

    formatter.format(data, 1);

    return data;
}

google.charts.setOnLoadCallback(drawProfit);

function drawProfit() {

    var data = google.visualization.arrayToDataTable([
      ['Source', 'Amount'],
      ['Service', 7000],
      ['Products', 10000],
      ['Other', 2500],
    ]);

    var options = {
        title: 'Profits',
        legend: 'left',
        chartArea: { width: '100%', height: '80%'},
    };

    var formatter = new google.visualization.NumberFormat({
        prefix: '$',
        negativeParens: true
    });

    formatter.format(data, 1);

    var chart = new google.visualization.PieChart(document.getElementById('profit_chart'));

    chart.draw(data, options);
}

google.charts.setOnLoadCallback(drawLosses);

function drawLosses() {

    var data = google.visualization.arrayToDataTable([
          ['Source', 'Amount'],
          ['Labor', 2483.26],
          ['Marketing', 928.58],
          ['Operating Costs', 10820.82],
          ['Taxes', 302.43],
          ['Other', 0],
    ]);

    var options = {
        title: 'Losses',
        legend: 'left',
        chartArea: { width: '100%', height: '80%' },
    };

    var formatter = new google.visualization.NumberFormat({
        prefix: '$',
        negativeParens: true
    });

    formatter.format(data, 1);

    var chart = new google.visualization.PieChart(document.getElementById('losses_chart'));

    chart.draw(data, options);
}
