﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.draw;
using System.Web.Security;
using Microsoft.AspNet.Identity;

namespace Longview
{
    public partial class progress : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string qs = Request.QueryString["project"];
                if (String.IsNullOrEmpty(qs))
                {
                    Response.StatusCode = 404;
                    Response.End();
                    return;
                }

                int ID;
                if (!int.TryParse(qs, out ID))
                {
                    Response.StatusCode = 404;
                    Response.End();
                    return;
                }

                if (ID == 0)
                {
                    Response.StatusCode = 404;
                    Response.End();
                    return;
                }

                if (!User.IsInRole(ProjectsManager.GetRoleNameById(ID)) &&
                    !User.IsInRole("Administrators") &&
                    !User.IsInRole("Manager"))
                {
                    Response.StatusCode = 403;
                    Response.End();
                    return;
                }

                DisplayData(ID);

                if (Page.User.Identity.IsAuthenticated)
                {
                    string fullName = (string)HttpContext.Current.Profile.GetPropertyValue("firstname") == "" ? Page.User.Identity.GetUserName() : (string)HttpContext.Current.Profile.GetPropertyValue("firstname");
                    Username.Text = "Hello, " + fullName + "&nbsp;&nbsp;&nbsp;";
                }
            }
        }

        protected void DisplayData(int s)
        {
            string name = ProjectsManager.GetRoleNameById(s);
            if (name == null)
            {
                Response.StatusCode = 404;
                Response.End();
                return;
            }

            Project proj = ProjectsManager.GetRoleByName(name);
            if (proj == null)
            {
                Response.StatusCode = 404;
                Response.End();
                return;
            }

            pName.Text = proj.getName();
            pManager.Text = proj.getContactName();

            pDate.Text = proj.getLastUpdated().ToLongDateString();
            pSummary.Text = proj.getSummary();
            pAuthor.Text = proj.getAuthor();
            if (!User.IsInRole("Customer"))
            {
                pBudget.Text = String.Format("{0:C}", proj.getBudget());
                pCost.Text = String.Format("{0:C}", proj.getCost());
                pProfit.Text = String.Format("{0:C}", proj.getProfit());
            }
            else
            {
                Budget.Visible = false;
                ProfitExpense.Visible = false;
                ProfitExpenseLiteral.Visible = false;
            }

            pProjectPercentage.Text = String.Format("{0}%", proj.getProjectProgress());
            pCompletion.Text = proj.getCompleteDate().ToLongDateString();
            pProjectScope.Text = proj.getProjectScope();
            pProductScope.Text = proj.getProductScope();

            pProjectStart.Text = proj.getStartDate().ToLongDateString();

            if (proj.getDetails() != "")
                pDetails.Text = proj.getDetails();
            else
            {
                pProjectDetails.Visible = false;
                pProjectDetailLit.Visible = false;
            }

            if(proj.getConclusion() != "")
                pConclusion.Text = proj.getConclusion();
            else
            {
                pProjectConclusion.Visible = false;
                pProjectConclusionLit.Visible = false;
            }

            if (proj.getDetails() == "" && proj.getConclusion() == "")
                pAdditionalDetails.Visible = false;

            SetupTasks(proj);
            SetupMilestones(proj);
            SetupIssues(proj);
            SetupRisks(proj);
            SetupEmployees(proj);
        }

        protected void SetupEmployees(Project proj)
        {
            DataTable dt = new DataTable();
            if (dt.Columns.Count == 0)
            {
                dt.Columns.Add("Name", typeof(string));
                dt.Columns.Add("Position", typeof(string));
                dt.Columns.Add("Type", typeof(string));
                dt.Columns.Add("Weekly Hours", typeof(string));
            }

            DataRow NewRow;
            foreach (KeyValuePair<string, Employee> kvp in proj.getEmployees())
            {
                string employeeType = kvp.Value.weeklyhours < 40 ? "Part-time" : "Full-time";
                NewRow = dt.NewRow();
                NewRow[0] = kvp.Key;
                NewRow[1] = kvp.Value.position;
                NewRow[2] = employeeType;
                NewRow[3] = kvp.Value.weeklyhours;
                dt.Rows.Add(NewRow);
            }

            pEmployeesList.DataSource = dt;
            pEmployeesList.DataBind();
        }

        protected void SetupIssues(Project proj)
        {
            DataTable dt = new DataTable();
            if (dt.Columns.Count == 0)
            {
                dt.Columns.Add("Issue", typeof(string));
                dt.Columns.Add("Date Identified", typeof(string));
                dt.Columns.Add("Resolution", typeof(string));
            }

            DataRow NewRow;
            foreach (string[] str in proj.getIssues().Values)
            {
                NewRow = dt.NewRow();
                NewRow[0] = str[0];
                NewRow[1] = DateTime.Parse(str[1]).ToString("yyyy-MM-dd");
                NewRow[2] = str[2];
                dt.Rows.Add(NewRow);
            }

            pIssues.DataSource = dt;
            pIssues.DataBind();
        }

        protected void SetupRisks(Project proj)
        {
            DataTable dt = new DataTable();
            if (dt.Columns.Count == 0)
            {
                dt.Columns.Add("Risk", typeof(string));
                dt.Columns.Add("Date Identified", typeof(string));
                dt.Columns.Add("Risk Level", typeof(string));
                dt.Columns.Add("Owner", typeof(string));
                dt.Columns.Add("Response", typeof(string));
            }

            DataRow NewRow;
            foreach (string[] str in proj.getRisks().Values)
            {
                NewRow = dt.NewRow();
                NewRow[0] = str[0];
                NewRow[1] = DateTime.Parse(str[1]).ToString("yyyy-MM-dd");
                NewRow[2] = str[2];
                NewRow[3] = str[3];
                NewRow[4] = str[4];
                dt.Rows.Add(NewRow);
            }

            pRisks.DataSource = dt;
            pRisks.DataBind();
        }

        protected void SetupMilestones(Project proj)
        {
            DataTable dt = new DataTable();
            if (dt.Columns.Count == 0)
            {
                dt.Columns.Add("Milestone", typeof(string));
                dt.Columns.Add("Completed", typeof(string));
                dt.Columns.Add("Start Date", typeof(string));
                dt.Columns.Add("Complete Date", typeof(string));
            }

            DataRow NewRow;
            foreach (string[] str in proj.getMilestones().Values)
            {
                NewRow = dt.NewRow();
                NewRow[0] = str[0];
                NewRow[1] = str[1];
                NewRow[2] = DateTime.Parse(str[2]).ToString("yyyy-MM-dd");
                NewRow[3] = DateTime.Parse(str[3]).ToString("yyyy-MM-dd"); ;
                dt.Rows.Add(NewRow);
            }

            pMilestones.DataSource = dt;
            pMilestones.DataBind();
        }

        protected void SetupTasks(Project proj)
        {
            DataTable dt = new DataTable();
            if (dt.Columns.Count == 0)
            {
                dt.Columns.Add("Task", typeof(string));
                dt.Columns.Add("Owner", typeof(string));
                dt.Columns.Add("Priority", typeof(string));
                dt.Columns.Add("Status", typeof(string));
            }

            DataRow NewRow;
            foreach (string[] str in proj.getActions().Values)
            {
                NewRow = dt.NewRow();
                NewRow[0] = str[0];
                NewRow[1] = str[1];
                NewRow[2] = str[2];
                NewRow[3] = str[3];
                dt.Rows.Add(NewRow);
            }

            pTasks.DataSource = dt;
            pTasks.DataBind();
        }

        protected void Button1_OnClick(object sender, EventArgs e)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated == true)
            {
                FormsAuthentication.SignOut();
                Response.Cookies[FormsAuthentication.FormsCookieName].Expires = DateTime.Now.AddYears(-1);
                FormsAuthentication.RedirectToLoginPage();
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                Document document = new Document(PageSize.A4, 25, 25, 0, 25);
                PdfWriter writer = PdfWriter.GetInstance(document, ms);
                writer.PageEvent = new PDFHeader();
                document.AddAuthor(pAuthor.Text);
                document.AddCreator("Longeviti LLC");
                document.AddSubject("Progress Report");
                document.AddTitle("Progress Report");

                Font f = new Font(Font.FontFamily.TIMES_ROMAN, 18.0f, Font.BOLD);
                Paragraph heading = new Paragraph("Progress Report",f);
                heading.Alignment = Element.ALIGN_CENTER;

                Font h2 = new Font(Font.FontFamily.TIMES_ROMAN, 16.0f, Font.BOLD);
                Paragraph section = new Paragraph("Project Info", h2);
                LineSeparator line = new LineSeparator(1f, 100f, BaseColor.BLACK, Element.ALIGN_LEFT, -6f);

                Font h3 = new Font(Font.FontFamily.TIMES_ROMAN, 14.0f, Font.BOLD);
                PdfPTable table = new PdfPTable(2);
                table.HorizontalAlignment = 0;
                PdfPCell cell = new PdfPCell(new Phrase(" "));
                cell.Border = Rectangle.NO_BORDER;
                table.AddCell(cell);
                table.AddCell(cell);
                cell.Phrase = new Phrase("Project Name", h3);
                table.AddCell(cell);
                cell.Phrase = new Phrase("Project Manager", h3);
                table.AddCell(cell);
                Font rf = new Font(Font.FontFamily.TIMES_ROMAN, 12.0f, Font.NORMAL);
                cell.Phrase = new Phrase(pName.Text, rf);
                table.AddCell(cell);
                cell.Phrase = new Phrase(pManager.Text, rf);
                table.AddCell(cell);
                cell.Phrase = new Phrase("Author", h3);
                table.AddCell(cell);
                cell.Phrase = new Phrase("Last Updated", h3);
                table.AddCell(cell);
                cell.Phrase = new Phrase(pAuthor.Text, rf);
                table.AddCell(cell);
                cell.Phrase = new Phrase(pDate.Text, rf);
                table.AddCell(cell);
                cell.Phrase = new Phrase("Project Started", h3);
                table.AddCell(cell);
                cell.Phrase = new Phrase("Estimated Completion Date", h3);
                table.AddCell(cell);
                cell.Phrase = new Phrase(pDate.Text, rf);
                table.AddCell(cell);
                cell.Phrase = new Phrase(pCompletion.Text, rf);
                table.AddCell(cell);
                cell.Phrase = new Phrase(" ");
                table.AddCell(cell);
                table.AddCell(cell);

                document.Open();
                document.Add(heading);
                document.Add(new Chunk("\n"));
                document.Add(section);
                document.Add(line);
                document.Add(table);
                section = new Paragraph("Project Summary", h2);
                document.Add(section);
                document.Add(line);

                table = new PdfPTable(2);
                table.HorizontalAlignment = 0;
                cell = new PdfPCell(new Phrase(" "));
                cell.Border = Rectangle.NO_BORDER;
                table.AddCell(cell);
                table.AddCell(cell);
                cell.Colspan = 2;
                cell.Phrase = new Phrase("Summary", h3);
                table.AddCell(cell);
                cell.Phrase = new Phrase(pSummary.Text, rf);
                table.AddCell(cell);
                cell.Phrase = new Phrase(" ");
                table.AddCell(cell);
                cell.Phrase = new Phrase("Project Scope", h3);
                table.AddCell(cell);
                cell.Phrase = new Phrase(pProjectScope.Text, rf);
                table.AddCell(cell);
                cell.Phrase = new Phrase(" ");
                table.AddCell(cell);
                cell.Phrase = new Phrase("Product Scope", h3);
                table.AddCell(cell);
                cell.Phrase = new Phrase(pProductScope.Text, rf);
                table.AddCell(cell);
                cell.Colspan = 1;
                cell.Phrase = new Phrase(" ");
                table.AddCell(cell);
                table.AddCell(cell);
                cell.Phrase = new Phrase("Project Progress", h3);
                table.AddCell(cell);
                cell.Phrase = User.IsInRole("Customer") ? new Phrase(" ") : new Phrase("Budget", h3);
                table.AddCell(cell);
                cell.Phrase = new Phrase(pProjectPercentage.Text, rf);
                table.AddCell(cell);
                cell.Phrase = User.IsInRole("Customer") ? new Phrase(" ") : new Phrase(pBudget.Text, rf);
                table.AddCell(cell);
                if (!User.IsInRole("Customer"))
                {
                    cell.Phrase = new Phrase("Total Profit", h3);
                    table.AddCell(cell);
                    cell.Phrase = new Phrase("Total Expenses", h3);
                    table.AddCell(cell);
                    cell.Phrase = new Phrase(pProfit.Text, rf);
                    table.AddCell(cell);
                    cell.Phrase = new Phrase(pCost.Text, rf);
                    table.AddCell(cell);
                }
                cell.Phrase = new Phrase(" ");
                table.AddCell(cell);
                table.AddCell(cell);

                document.Add(table);

                section = new Paragraph("Employees", h2);
                document.Add(section);
                document.Add(line);

                Font rb = new Font(Font.FontFamily.TIMES_ROMAN, 12.0f, Font.BOLD);
                table = new PdfPTable(new float[] { 40, 25, 20, 15 });
                table.WidthPercentage = 100f;
                table.HorizontalAlignment = 0;
                cell.Phrase = new Phrase(" ");
                cell.Colspan = 4;
                table.AddCell(cell);
                cell.Phrase = new Phrase("Name", rb);
                cell.Colspan = 1;
                table.AddCell(cell);
                cell.Phrase = new Phrase("Position", rb);
                table.AddCell(cell);
                cell.Phrase = new Phrase("Type", rb);
                table.AddCell(cell);
                cell.Phrase = new Phrase("Weekly Hours", rb);
                table.AddCell(cell);

                foreach (GridViewRow row in pEmployeesList.Rows)
                {
                    cell.Phrase = new Phrase(row.Cells[0].Text, rf);
                    table.AddCell(cell);
                    cell.Phrase = new Phrase(row.Cells[1].Text, rf);
                    table.AddCell(cell);
                    cell.Phrase = new Phrase(row.Cells[2].Text, rf);
                    table.AddCell(cell);
                    cell.Phrase = new Phrase(row.Cells[3].Text, rf);
                    table.AddCell(cell);
                }

                document.Add(table);

                section = new Paragraph("Tasks", h2);
                document.Add(section);
                document.Add(line);

                table = new PdfPTable(new float[] { 50, 20, 15,  15});
                table.WidthPercentage = 100f;
                table.HorizontalAlignment = 0;
                cell.Phrase = new Phrase(" ");
                cell.Colspan = 4;
                table.AddCell(cell);
                cell.Phrase = new Phrase("Task" , rb);
                cell.Colspan = 1;
                table.AddCell(cell);
                cell.Phrase = new Phrase("Owner", rb);
                table.AddCell(cell);
                cell.Phrase = new Phrase("Priority", rb);
                table.AddCell(cell);
                cell.Phrase = new Phrase("Status", rb);
                table.AddCell(cell);

                foreach (GridViewRow row in pTasks.Rows)
                {
                    cell.Phrase = new Phrase(row.Cells[0].Text, rf);
                    table.AddCell(cell);
                    cell.Phrase = new Phrase(row.Cells[1].Text, rf);
                    table.AddCell(cell);
                    cell.Phrase = new Phrase(row.Cells[2].Text, rf);
                    table.AddCell(cell);
                    cell.Phrase = new Phrase(row.Cells[3].Text, rf);
                    table.AddCell(cell);
                }

                document.Add(table);

                section = new Paragraph("Milestones", h2);
                document.Add(section);
                document.Add(line);

                table = new PdfPTable(new float[] { 45, 15, 20, 20 });
                table.WidthPercentage = 100f;
                table.HorizontalAlignment = 0;
                cell.Phrase = new Phrase(" ");
                cell.Colspan = 4;
                table.AddCell(cell);
                cell.Phrase = new Phrase("Milestone", rb);
                cell.Colspan = 1;
                table.AddCell(cell);
                cell.Phrase = new Phrase("Completion", rb);
                table.AddCell(cell);
                cell.Phrase = new Phrase("Start Date", rb);
                table.AddCell(cell);
                cell.Phrase = new Phrase("Complete Date", rb);
                table.AddCell(cell);

                foreach (GridViewRow row in pMilestones.Rows)
                {
                    cell.Phrase = new Phrase(row.Cells[0].Text, rf);
                    table.AddCell(cell);
                    cell.Phrase = new Phrase(row.Cells[1].Text, rf);
                    table.AddCell(cell);
                    cell.Phrase = new Phrase(row.Cells[2].Text, rf);
                    table.AddCell(cell);
                    cell.Phrase = new Phrase(row.Cells[3].Text, rf);
                    table.AddCell(cell);
                }

                document.Add(table);

                section = new Paragraph("Issue Log", h2);
                document.Add(section);
                document.Add(line);

                table = new PdfPTable(new float[] { 40, 20, 40 });
                table.WidthPercentage = 100f;
                table.HorizontalAlignment = 0;
                cell.Phrase = new Phrase(" ");
                cell.Colspan = 3;
                table.AddCell(cell);
                cell.Phrase = new Phrase("Issue", rb);
                cell.Colspan = 1;
                table.AddCell(cell);
                cell.Phrase = new Phrase("Date Identified", rb);
                table.AddCell(cell);
                cell.Phrase = new Phrase("Resolution", rb);
                table.AddCell(cell);

                foreach (GridViewRow row in pIssues.Rows)
                {
                    cell.Phrase = new Phrase(row.Cells[0].Text, rf);
                    table.AddCell(cell);
                    cell.Phrase = new Phrase(row.Cells[1].Text, rf);
                    table.AddCell(cell);
                    cell.Phrase = new Phrase(row.Cells[2].Text, rf);
                    table.AddCell(cell);
                }

                document.Add(table);

                section = new Paragraph("Risk Log", h2);
                document.Add(section);
                document.Add(line);

                table = new PdfPTable(new float[] { 25, 20, 15, 15, 25 });
                table.WidthPercentage = 100f;
                table.HorizontalAlignment = 0;
                cell.Phrase = new Phrase(" ");
                cell.Colspan = 5;
                table.AddCell(cell);
                cell.Phrase = new Phrase("Risk", rb);
                cell.Colspan = 1;
                table.AddCell(cell);
                cell.Phrase = new Phrase("Date Identified", rb);
                table.AddCell(cell);
                cell.Phrase = new Phrase("Risk Level", rb);
                table.AddCell(cell);
                cell.Phrase = new Phrase("Owner", rb);
                table.AddCell(cell);
                cell.Phrase = new Phrase("Response", rb);
                table.AddCell(cell);

                foreach (GridViewRow row in pRisks.Rows)
                {
                    cell.Phrase = new Phrase(row.Cells[0].Text, rf);
                    table.AddCell(cell);
                    cell.Phrase = new Phrase(row.Cells[1].Text, rf);
                    table.AddCell(cell);
                    cell.Phrase = new Phrase(row.Cells[2].Text, rf);
                    table.AddCell(cell);
                    cell.Phrase = new Phrase(row.Cells[3].Text, rf);
                    table.AddCell(cell);
                    cell.Phrase = new Phrase(row.Cells[4].Text, rf);
                    table.AddCell(cell);
                }

                document.Add(table);

                section = new Paragraph("Additional Details", h2);
                document.Add(section);
                document.Add(line);
                //document.Add(new Chunk("\n"));

                document.Add(new Paragraph("Details", rb));
                document.Add(new Paragraph(pDetails.Text, rf));
                document.Add(new Chunk("\n"));
                document.Add(new Paragraph("Conclusion", rb));
                document.Add(new Paragraph(pConclusion.Text, rf));
                document.Add(new Chunk("\n"));

                document.Close();
                writer.Close();

                Response.ContentType = "pdf/application";
                Response.AddHeader("content-disposition", "attachment;filename=" + ("ProgressReport" + pName.Text + ".pdf").Replace(" ", ""));
                Response.OutputStream.Write(ms.GetBuffer(), 0, ms.GetBuffer().Length);
            }
        }
    }

    public class PDFHeader : PdfPageEventHelper
    {
        // write on top of document
        public override void OnOpenDocument(PdfWriter writer, Document document)
        {
            base.OnOpenDocument(writer, document);
        }

        // write on start of each page
        public override void OnStartPage(PdfWriter writer, Document document)
        {
            base.OnStartPage(writer, document);
            iTextSharp.text.Image banner = iTextSharp.text.Image.GetInstance(HttpContext.Current.Server.MapPath("Images/banner.png"));
            banner.ScaleToFit(document.PageSize.Width, document.PageSize.Height);
            banner.SetAbsolutePosition(0, banner.AbsoluteY);
            document.Add(banner);
        }

        // write on end of each page
        public override void OnEndPage(PdfWriter writer, Document document)
        {
            base.OnEndPage(writer, document);
        }

        //write on close of document
        public override void OnCloseDocument(PdfWriter writer, Document document)
        {
            base.OnCloseDocument(writer, document);
        }
    }
}