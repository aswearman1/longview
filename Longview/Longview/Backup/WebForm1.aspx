﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="Longview.WebForm1" %>

<%@ Register assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Longview</title>
    <style type="text/css">
        .auto-style1 {
            height: 20px;
        }
    </style>
</head>

<body style="margin: 0 0 0 0">
		<form id="form1" runat="server">
		<header> 
		<table width="100%">
		<tr>
			<td width="5%"></td>
			<td width="10%" valign="middle" align="left"><a href="#"><img src="images/longview.png" style="height: 67px;" alt=""/></a></td>
			<td align="right">
				<nav>
                    <asp:LoginView runat="server" ViewStateMode="Disabled">
                    <AnonymousTemplate>
				<ul style="display: inline-table; text-align: center; list-style: none;">
					<li style="text-align: center; float: left;"><a href="#" style="font-size: 24px; font-family: raleway; text-decoration: none; color: #2f415f; padding: 16px 25px; display: block;" onMouseOver="this.style.backgroundColor='#7F7F7F'; this.style.color='#FFF';" onMouseOut="this.style.backgroundColor=''; this.style.color='#2f415f';">Home</a></li>
					</ul>
                       </AnonymousTemplate>

                        <LoggedInTemplate>
                            <ul style="display: inline-table; text-align: center; list-style: none;">
                                <li style="text-align: center; float: left;"><a href="#" style="font-size: 24px; font-family: raleway; text-decoration: none; color: #2f415f; padding: 16px 25px; display: block;" onMouseOver="this.style.backgroundColor='#7F7F7F'; this.style.color='#FFF';" onMouseOut="this.style.backgroundColor=''; this.style.color='#2f415f';">Home</a></li>
                                <li style="text-align: center; float: left;"><a href="#" style="font-size: 24px; font-family: raleway; text-decoration: none; color: #2f415f; padding: 16px 25px; display: block;" onMouseOver="this.style.backgroundColor='#7F7F7F'; this.style.color='#FFF';" onMouseOut="this.style.backgroundColor=''; this.style.color='#2f415f';">Dashboard</a></li>
                                <% if (Roles.Provider.IsUserInRole(User.Identity.Name, "Administrators"))
                                    {%>
                                <li style="text-align: center; float: left;"><a href="#" style="font-size: 24px; font-family: raleway; text-decoration: none; color: #2f415f; padding: 16px 25px; display: block;" onMouseOver="this.style.backgroundColor='#7F7F7F'; this.style.color='#FFF';" onMouseOut="this.style.backgroundColor=''; this.style.color='#2f415f';">Admin Panel</a></li>
                                <% } %>
                                <li style="text-align: center; float: left;"><a href="#" style="font-size: 24px; font-family: raleway; text-decoration: none; color: #2f415f; padding: 16px 25px; display: block;" OnServerClick="Button1_OnClick" runat="server" onMouseOver="this.style.backgroundColor='#7F7F7F'; this.style.color='#FFF';" onMouseOut="this.style.backgroundColor=''; this.style.color='#2f415f';">Logout</a></li>
                                </ul>
                        </LoggedInTemplate>
                        </asp:LoginView>
				</nav>
			</td>
		<td width="5%"></td>
		</tr>
		</table>
	</header>
	
	<table style="width: 100%; background-image: url(images/customer.png); background-size: cover; height:350px; overflow: hidden;">
		<tbody>
		<tr>
			<td valign="middle" align="center"><p style="font-size: 60px; font-family: raleway; color: white;  text-shadow: 2px 2px black;"><strong>Customer Portal</strong></p></td>
		  </tr>
		</tbody>
	</table>
	
	<table style="width: 100%;" cellpadding="0" cellspacing="0">
		<tbody>
		<tr>
		<td colspan="5" height="30px"></td>
			</tr>

			<tr>
				<td style="width: 10%;">&nbsp;</td>
                <td style="width: 80%;" valign="middle" align="center">
                    <asp:Login ID="Login1" runat="server" OnAuthenticate="Login1_Authenticate" Font-Names="Raleway" VisibleWhenLoggedIn="False">
                    </asp:Login>
                    </td>
				<td style="width: 10%">&nbsp;</td>
			</tr>

            <tr>
                <td colspan="5" height="30px"></td>
            </tr>
		</tbody>
	</table>
	
	
	
<!--Longeviti signature-->
<table style="min-width:960px; background-color:#181727" bgcolor="#181727" height="250px" width="100%" align="center">
<tr>
<td colspan="9" height="40px"></td>
</tr>
<tr>
<td width="10%"></td>
<td valign="top">
<p style="font-family:'Raleway'; color:#8e909e; font-size:16px; font-weight:bold">LONGEVITI LLC.</p><br />
<p style="font-family:'Raleway'; color:#8e909e; font-size:16px"><i>As Dynamic As Our Customers</i></p>
</td>
<td width="10%"></td>
<td valign="top"><p style="font-family:'Raleway'; color:#8e909e; font-size:16px; font-weight:bold">HEADQUARTERS</p><br />
<p style="font-family:'Raleway'; color:#8e909e; font-size:16px">45240 Business Court <br />Suite #410<br />
Sterling, VA 20166<br />
(703) 848-7965</p></td>
<td width="10%"></td>
<td valign="top"><p style="font-family:'Raleway'; color:#8e909e; font-size:16px; font-weight:bold">QUICK LINKS</p><br />
<a href="http://www.vscyberhosting3.com/longeviti/" style="text-decoration:none"><p style="font-family:'Raleway'; color:#8e909e; font-size:16px; text-decoration:none">> Careers</p></a><br /><br />
<a href="services.html" style="text-decoration:none"><p style="font-family:'Raleway'; color:#8e909e; font-size:16px; text-decoration:none">> Services</p></a></td>
<td width="10%"></td>
<td valign="top"><p style="font-family:'Raleway'; color:#8e909e; font-size:16px; font-weight:bold">FOLLOW US</p><br />
<a href="https://www.linkedin.com/company/longeviti-llc" style="border:0;"><img src="images/linkedin.png" width="35px" /></a></td>
<td width="10%"></td>
</tr>
<tr>
<td colspan="9" height="40px"></td>
</tr>
</table>
<table style="min-width:960px" bgcolor="#4e5866" height="50px" width="100%" align="center">
<tr>
<td width="10%"></td>
<td width="40%"><p style="font-family:'Raleway'; font-size:14px; color:#828b98">Copyright 2016 &copy; Website designed by <em style="color:#42b880; font-style:normal">Longeviti LLC.</em></p></td>
<td width="40%" align="right"><table border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><a href="http://www.longevitillc.com/news/" style="text-decoration:none"><p style="font-family:'Raleway'; font-size:14px; color:#828b98">News</p></a></td>
    <td><p style="font-family:'Raleway'; font-size:14px; color:#828b98">&nbsp;&nbsp;&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;</p></td>
    <td><a href="contact.html" style="text-decoration:none"><p style="font-family:'Raleway'; font-size:14px; color:#828b98">Contact The Team</p></a></td>
  </tr>
</table></td>
<td width="10%"></td>
</tr>
</table>
        </form>
</body>
</html>
