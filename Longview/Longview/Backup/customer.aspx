﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="customer.aspx.cs" Inherits="Longview.customer" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="Content/bootstrap.css" rel="stylesheet" id="bootstrap-css" />
    <link href="Content/longview.css" rel="stylesheet" id="longview-css" />
</head>
<body style="padding: 8px; background: linear-gradient(to bottom, rgba(191,191,191,1) 0%,rgba(232,232,232,1) 13%,rgba(244,244,244,1) 100%) no-repeat;">
    <form id="form1" runat="server">
        <table style="width: 100%">
            <tr>
                <td style="min-width: 10px; width: 10px;"></td>

                <td style="width: 33%;">
                    <div class="contentBox" style="width: 100%; height: 450px; min-width: 300px;">
                        <h1 style="margin-left: 20px;">Employee Timesheets</h1>
                        <div style="margin: 0 20px 20px 20px; clear: both;">
                            <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                            <asp:ListBox ID="ListBox1" runat="server" Height="200px" Width="100%" ValidateRequestMode="Enabled" OnLoad="ListBox1_Load">
                            </asp:ListBox><br />
                            <asp:FileUpload ID="FileUpload1" runat="server" Width="100%" Visible="false" />
                            <button id="timesheetbut" class="btn-default btn-sm" style="width: 80px; background-color: #0d0435; color: white; cursor: pointer; float: right; font-weight: 500;" runat="server" onserverclick="btnClick">View ></button>
                            <asp:Button ID="UploadTimesheet" runat="server" Text="Upload" CssClass="btn-default btn-sm" style="width: 80px; background-color: #0d0435; color: white; cursor: pointer; font-weight: 500;" OnClick="UploadTimesheet_Click" Visible="false" />
                            <asp:Button ID="DeleteTimesheet" runat="server" Text="Remove" CssClass="btn-default btn-sm" style="width: 80px; background-color: #0d0435; color: white; cursor: pointer; font-weight: 500;" OnClick="DeleteTimesheet_Click" Visible="false" />
                        </div>
                    </div>
                </td>

                <td style="min-width: 10px; width: 10px;"></td>

                <td style="width: 33%;">
                    <div class="contentBox" style="width: 100%; height: 450px; min-width: 300px;">
                        <h1 style="margin-left: 20px;">PPQ Requests</h1><br />
                        <div style="margin: 0 20px 20px 20px">
                            <asp:Label ID="Label5" runat="server" Text=""></asp:Label>
                            <asp:ListBox ID="ListBox5" runat="server" Height="200px" Width="100%" OnLoad="ListBox5_Load">
                            </asp:ListBox><br />
                            <asp:FileUpload ID="FileUpload5" runat="server" Width="100%" Visible="false" />
                            <button id="ppqbut" class="btn-default btn-sm" style="width: 80px; background-color: #0d0435; color: white; cursor: pointer; float: right; font-weight: 500;" runat="server" onserverclick="btnClick">View ></button>
                            <asp:Button ID="UploadPPQ" runat="server" Text="Upload" CssClass="btn-default btn-sm" style="width: 80px; background-color: #0d0435; color: white; cursor: pointer; font-weight: 500;" OnClick="UploadTimesheet_Click" Visible="false" />
                            <asp:Button ID="DeletePPQ" runat="server" Text="Remove" CssClass="btn-default btn-sm" style="width: 80px; background-color: #0d0435; color: white; cursor: pointer; font-weight: 500;" OnClick="DeleteTimesheet_Click" Visible="false" />
                        </div>
                    </div>
                </td>

                <td style="min-width: 10px; width: 10px;"></td>

                <td style="width: 33%;">
                    <div class="contentBox" style="width: 100%; height: 450px; min-width: 300px;">
                        <h1 style="margin-left: 20px;">Progress Reports</h1><br />
                        <div style="margin: 0 20px 20px 20px">
                            <asp:Label ID="Label3" runat="server" Text=""></asp:Label>
                            <asp:ListBox ID="ListBox3" runat="server" Height="200px" Width="100%" OnLoad="ListBox3_Load">
                            </asp:ListBox><br />
                            <asp:FileUpload ID="FileUpload3" runat="server" Width="100%" Visible="false" />
                            <button id="reportbut" class="btn-default btn-sm" style="width: 80px; background-color: #0d0435; color: white; cursor: pointer; float: right; font-weight: 500;" runat="server" onserverclick="btnClick">View ></button>
                            <asp:Button ID="UploadProgress" runat="server" Text="Upload" CssClass="btn-default btn-sm" style="width: 80px; background-color: #0d0435; color: white; cursor: pointer; font-weight: 500;" OnClick="UploadTimesheet_Click" Visible="false" />
                            <asp:Button ID="DeleteProgress" runat="server" Text="Remove" CssClass="btn-default btn-sm" style="width: 80px; background-color: #0d0435; color: white; cursor: pointer; font-weight: 500;" OnClick="DeleteTimesheet_Click" Visible="false" />
                        </div>
                    </div>
                </td>

                <td style="min-width: 10px; width: 10px;"></td>
            </tr>

            <tr style="height: 20px;"></tr>
            </table>

        <table>
            <tr>
                <td style="min-width: 10px; width: 10px;"></td>

                <td style="width: 50%;">
                    <div class="contentBox" style="width: 100%; height: 450px;">
                        <h1 style="margin-left: 20px;">Surveys</h1><br />
                        <div style="margin: 0 20px 20px 20px">
                            <asp:ListBox ID="ListBox4" runat="server" Height="200px" Width="100%" OnLoad="ListBox4_Load">
                            </asp:ListBox><br />
                            <button class="btn-default btn-sm" style="width: 80px; background-color: #0d0435; color: white; cursor: pointer; float: right; font-weight: 500;" runat="server">View ></button>
                        </div>
                    </div>
                </td>

                <td style="min-width: 10px; width: 10px;"></td>

                <td style="width: 50%;">
                    <div class="contentBox" style="width: 100%; height: 450px; margin: 0 auto;">
                        <h1 style="margin-left: 20px;">Invoices</h1><br />
                        <div style="margin: 0 20px 20px 20px">
                            <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
                            <asp:ListBox ID="ListBox2" runat="server" Height="200px" Width="100%" OnLoad="ListBox2_Load">
                            </asp:ListBox><br />
                            <asp:FileUpload ID="FileUpload2" runat="server" Width="100%" Visible="false" />
                            <button id="invoicesbut" class="btn-default btn-sm" style="width: 80px; background-color: #0d0435; color: white; cursor: pointer; float: right; font-weight: 500;" runat="server" onserverclick="btnClick">View ></button>
                            <asp:Button ID="UploadInvoice" runat="server" Text="Upload" CssClass="btn-default btn-sm" style="width: 80px; background-color: #0d0435; color: white; cursor: pointer; font-weight: 500;" OnClick="UploadTimesheet_Click" Visible="false" />
                            <asp:Button ID="DeleteInvoice" runat="server" Text="Remove" CssClass="btn-default btn-sm" style="width: 80px; background-color: #0d0435; color: white; cursor: pointer; font-weight: 500;" OnClick="DeleteTimesheet_Click" Visible="false" />
                        </div>
                    </div>
                </td>

                <td style="min-width: 10px; width: 10px;"></td>
            </tr>

            </table>
    </form>
</body>
</html>
