﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Project - Copy.aspx.cs" Inherits="Longview.project2" EnableEventValidation="false" %>

<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta charset="utf-8" />
    <link href="Content/bootstrap.css" rel="stylesheet" id="bootstrap-css">
    <link href="Content/longview.css" rel="stylesheet" id="longview-css" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    <script type="text/javascript">
        google.charts.load('current', { packages: [['corechart'], ['table'], ['gantt']] });

        google.charts.setOnLoadCallback(drawProfit);

        function drawProfit() {
            var services = parseFloat("<%=GetFinances(7)%>");
            var products = parseFloat("<%=GetFinances(6)%>");
            var other = parseFloat("<%=GetFinances(8)%>");

            var data = google.visualization.arrayToDataTable([
              ['Source', 'Amount'],
              ['Service', services],
              ['Products', products],
              ['Other', other],
            ]);

            var options = {
                title: 'Profits',
                legend: 'left',
                chartArea: { width: '100%', height: '80%' },
                sliceVisibilityThreshold:0,
            };

            var formatter = new google.visualization.NumberFormat({
                prefix: '$',
                negativeParens: true
            });

            formatter.format(data, 1);

            var chart = new google.visualization.PieChart(document.getElementById('profit_chart'));

            chart.draw(data, options);
        }

        google.charts.setOnLoadCallback(drawLosses);

        function drawLosses() {
            var labor = parseFloat("<%=GetFinances(1)%>");
            var products = parseFloat("<%=GetFinances(0)%>");
            var operation = parseFloat("<%=GetFinances(2)%>");
            var marketing = parseFloat("<%=GetFinances(3)%>");
            var taxes = parseFloat("<%=GetFinances(4)%>");
            var other = parseFloat("<%=GetFinances(5)%>");

            var data = google.visualization.arrayToDataTable([
                  ['Source', 'Amount'],
                  ['Products', products],
                  ['Labor', labor],
                  ['Operating Costs', operation],
                  ['Marketing', marketing],
                  ['Taxes', taxes],
                  ['Other', other],
            ]);

            var options = {
                title: 'Losses',
                legend: 'left',
                chartArea: { width: '100%', height: '80%' },
                sliceVisibilityThreshold:0,
            };

            var formatter = new google.visualization.NumberFormat({
                prefix: '$',
                negativeParens: true
            });

            formatter.format(data, 1);

            var chart = new google.visualization.PieChart(document.getElementById('losses_chart'));

            chart.draw(data, options);
        }

        function daysToMilliseconds(days) {
            return days * 24 * 60 * 60 * 1000;
        }

        function drawBasic() {

            var ProfitService = parseFloat("<%=GetFinances(7)%>");
            var ProfitProducts = parseFloat("<%=GetFinances(6)%>");
            var ProfitOther = parseFloat("<%=GetFinances(8)%>");

            //Start profit chart
            var tableData = new google.visualization.DataTable();
            tableData.addColumn('string', 'Profit');
            tableData.addColumn('number', 'Amount');
            tableData.addRows([
              ['Service', ProfitService],
              ['Products', ProfitProducts],
              ['Other', ProfitOther],
            ]);

            var total = 0;
            for (var i = 0; i < tableData.getNumberOfRows() ; ++i) {
                total = total + tableData.getValue(i, 1);
            }

            tableData.addRow(['TOTAL', total]);

            var formatter = new google.visualization.NumberFormat({
                prefix: '$',
                negativeParens: true
            });

            formatter.format(tableData, 1);

            var table = new google.visualization.Table(document.getElementById('profit'));

            table.draw(tableData, { showRowNumber: false, width: '100%', height: '100%', vAxis: { format: '$###,###,###.00' } });

            var labor = parseFloat("<%=GetFinances(1)%>");
            var products = parseFloat("<%=GetFinances(0)%>");
            var operation = parseFloat("<%=GetFinances(2)%>");
            var marketing = parseFloat("<%=GetFinances(3)%>");
            var taxes = parseFloat("<%=GetFinances(4)%>");
            var other = parseFloat("<%=GetFinances(5)%>");

            //Start loss chart
            var tableData2 = new google.visualization.DataTable();
            tableData2.addColumn('string', 'Loss');
            tableData2.addColumn('number', 'Amount');
            tableData2.addRows([
              ['Products', products],
              ['Labor', labor],
              ['Operating Costs', operation],
              ['Marketing', marketing],
              ['Taxes', taxes],
              ['Other', other],
            ]);

            total = 0;
            for (var i = 0; i < tableData2.getNumberOfRows() ; ++i) {
                total = total + tableData2.getValue(i, 1);
            }

            tableData2.addRow(['TOTAL', total]);
            formatter.format(tableData2, 1);

            var table2 = new google.visualization.Table(document.getElementById('losses'));

            table2.draw(tableData2, { showRowNumber: false, width: '100%', height: '100%', vAxis: { format: '$###,###,###.00' }});

            //Start bar graph
            var data = new google.visualization.DataTable();

            var pJan = <%=GetMonthlyFinances(1, true)%>;
            var pFeb = <%=GetMonthlyFinances(2, true)%>;
            var pMar = <%=GetMonthlyFinances(3, true)%>;
            var pApr = <%=GetMonthlyFinances(4, true)%>;
            var pMay = <%=GetMonthlyFinances(5, true)%>;
            var pJun = <%=GetMonthlyFinances(6, true)%>;
            var pJul = <%=GetMonthlyFinances(7, true)%>;
            var pAug = <%=GetMonthlyFinances(8, true)%>;
            var pSep = <%=GetMonthlyFinances(9, true)%>;
            var pOct = <%=GetMonthlyFinances(10, true)%>;
            var pNov = <%=GetMonthlyFinances(11, true)%>;
            var pDec = <%=GetMonthlyFinances(12, true)%>;
            var eJan = <%=GetMonthlyFinances(1, false)%>;
            var eFeb = <%=GetMonthlyFinances(2, false)%>;
            var eMar = <%=GetMonthlyFinances(3, false)%>;
            var eApr = <%=GetMonthlyFinances(4, false)%>;
            var eMay = <%=GetMonthlyFinances(5, false)%>;
            var eJun = <%=GetMonthlyFinances(6, false)%>;
            var eJul = <%=GetMonthlyFinances(7, false)%>;
            var eAug = <%=GetMonthlyFinances(8, false)%>;
            var eSep = <%=GetMonthlyFinances(9, false)%>;
            var eOct = <%=GetMonthlyFinances(10, false)%>;
            var eNov = <%=GetMonthlyFinances(11, false)%>;
            var eDec = <%=GetMonthlyFinances(12, false)%>;
            var Year = <%=DateTime.Now.Year%>;

            data.addColumn('string', 'X');
            data.addColumn('number', 'Profit');
            data.addColumn('number', 'Expenses');
            data.addRows([
              ['Jan', pJan, eJan], ['Feb', pFeb, eFeb],
              ['Mar', pMar, eMar], ['Apr', pApr, eApr],
              ['May', pMay, eMay], ['Jun', pJun, eJun],
              ['Jul', pJul, eJul], ['Aug', pAug, eAug],
              ['Sept', pSep, eSep], ['Oct', pOct, eOct],
              ['Nov', pNov, eNov], ['Dec', pDec, eDec],
            ]);

            var topLevel = 0;

            var options = {
                hAxis: {
                    title: 'Month'
                },
                vAxis: {
                    title: 'Spent'
                },
                title: 'Monthly Finances ' + Year,
                legend: { position: 'none' },
                chartArea: { width: '100%', },
                animation: {
                    duration: 1000,
                    easing: 'out'
                },
            };

            var chart = new google.visualization.ColumnChart(document.getElementById('columnChart'));

            chart.draw(data, options);
            google.visualization.events.addListener(chart, 'select', function selectHandler(e) {
                var selectedItem = chart.getSelection()[0];
                switch (topLevel) {
                    case 0:
                        if (selectedItem) {
                            var mo = data.getValue(selectedItem.row, 0);

                            options = {
                                hAxis: {
                                    title: 'Day',
                                    textStyle : { fontSize : 9 },
                                },
                                vAxis: {
                                    title: 'Spent'
                                },
                                title: mo + ' Finances',
                                legend: { position: 'none' },
                                chartArea: { width: '100%', },
                                animation: {
                                    duration: 1000,
                                    easing: 'out'
                                },
                            };

                            chart.draw(getData(mo), options);
                            table.draw(getTableData(mo, true), { showRowNumber: false, width: '100%', height: '100%', vAxis: { format: '$###,###,###.00' } });
                            table2.draw(getTableData(mo, false), { showRowNumber: false, width: '100%', height: '100%', vAxis: { format: '$###,###,###.00' } });
                            topLevel += 1;
                        }
                        break;

                    case 1:
                        options = {
                            hAxis: {
                                title: 'Month'
                            },
                            vAxis: {
                                title: 'Spent'
                            },
                            title: 'Monthly Finances ' + Year,
                            legend: { position: 'none' },
                            chartArea: { width: '100%', },
                            animation: {
                                duration: 1000,
                                easing: 'out'
                            },
                        };

                        chart.draw(data, options);
                        table.draw(tableData, { showRowNumber: false, width: '100%', height: '100%', vAxis: { format: '$###,###,###.00' } });
                        table2.draw(tableData2, { showRowNumber: false, width: '100%', height: '100%', vAxis: { format: '$###,###,###.00' } });
                        topLevel = 0;
                        break;
                }
            });
        }

        function getData(row)
        {
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'X');
            data.addColumn('number', 'Profit');
            data.addColumn('number', 'Expenses');
            var str = "";
            var str2 = "";

            switch(row)
            {
                case "Jan":
                    str = [ <%=GetDayFinances(1)%> ];
                    str2 = [ <%=GetDayFinances(1, true)%> ];
                    break;
                case "Feb":
                    str = [ <%=GetDayFinances(2)%> ];
                    str2 = [ <%=GetDayFinances(2, true)%> ];
                    break;
                case "Mar":
                    str = [ <%=GetDayFinances(3)%> ];
                    str2 = [ <%=GetDayFinances(3, true)%> ];
                    break;
                case "Apr":
                    str = [ <%=GetDayFinances(4)%> ];
                    str2 = [ <%=GetDayFinances(4, true)%> ];
                    break;
                case "May":
                    str = [ <%=GetDayFinances(5)%> ];
                    str2 = [ <%=GetDayFinances(5, true)%> ];
                    break;
                case "Jun":
                    str = [ <%=GetDayFinances(6)%> ];
                    str2 = [ <%=GetDayFinances(6, true)%> ];
                    break;
                case "Jul":
                    str = [ <%=GetDayFinances(7)%> ];
                    str2 = [ <%=GetDayFinances(7, true)%> ];
                    break;
                case "Aug":
                    str = [ <%=GetDayFinances(8)%> ];
                    str2 = [ <%=GetDayFinances(8, true)%> ];
                    break;
                case "Sept":
                    str = [ <%=GetDayFinances(9)%> ];
                    str2 = [ <%=GetDayFinances(9, true)%> ];
                    break;
                case "Oct":
                    str = [ <%=GetDayFinances(10)%> ];
                    str2 = [ <%=GetDayFinances(10, true)%> ];
                    break;
                case "Nov":
                    str = [ <%=GetDayFinances(11)%> ];
                    str2 = [ <%=GetDayFinances(11, true)%> ];
                    break;
                case "Dec":
                    str = [ <%=GetDayFinances(12)%> ];
                    str2 = [ <%=GetDayFinances(12, true)%> ];
                    break;
            }

            for (var i = 0; i < str.length; ++i)
            {
                data.addRow([(i + 1).toString(), parseFloat(str2[i]), parseFloat(str[i])]);
            }

            return data;
        }

        function getTableData(row, profit)
        {
            var data = new google.visualization.DataTable();
            var str = "";
            if(profit)
            {
                data.addColumn('string', 'Profit');
                data.addColumn('number', 'Amount');

                switch(row)
                {
                    case "Jan":
                        str = [ <%=GetDayFinancialArray(1, true)%> ];
                        break;
                    case "Feb":
                        str = [ <%=GetDayFinancialArray(2, true)%> ];
                        break;
                    case "Mar":
                        str = [ <%=GetDayFinancialArray(3, true)%> ];
                        break;
                    case "Apr":
                        str = [ <%=GetDayFinancialArray(4, true)%> ];
                        break;
                    case "May":
                        str = [ <%=GetDayFinancialArray(5, true)%> ];
                        break;
                    case "Jun":
                        str = [ <%=GetDayFinancialArray(6, true)%> ];
                        break;
                    case "Jul":
                        str = [ <%=GetDayFinancialArray(7, true)%> ];
                        break;
                    case "Aug":
                        str = [ <%=GetDayFinancialArray(8, true)%> ];
                        break;
                    case "Sept":
                        str = [ <%=GetDayFinancialArray(9, true)%> ];
                        break;
                    case "Oct":
                        str = [ <%=GetDayFinancialArray(10, true)%> ];
                        break;
                    case "Nov":
                        str = [ <%=GetDayFinancialArray(11, true)%> ];
                        break;
                    case "Dec":
                        str = [ <%=GetDayFinancialArray(12, true)%> ];
                        break;
                }

                data.addRows([
                ['Service', str[7]],
                ['Products', str[6]],
                ['Other', str[8]],
                ]);
            }
            else
            {
                data.addColumn('string', 'Loss');
                data.addColumn('number', 'Amount');

                switch(row)
                {
                    case "Jan":
                        str = [ <%=GetDayFinancialArray(1, false)%> ];
                        break;
                    case "Feb":
                        str = [ <%=GetDayFinancialArray(2, false)%> ];
                        break;
                    case "Mar":
                        str = [ <%=GetDayFinancialArray(3, false)%> ];
                        break;
                    case "Apr":
                        str = [ <%=GetDayFinancialArray(4, false)%> ];
                        break;
                    case "May":
                        str = [ <%=GetDayFinancialArray(5, false)%> ];
                        break;
                    case "Jun":
                        str = [ <%=GetDayFinancialArray(6, false)%> ];
                        break;
                    case "Jul":
                        str = [ <%=GetDayFinancialArray(7, false)%> ];
                        break;
                    case "Aug":
                        str = [ <%=GetDayFinancialArray(8, false)%> ];
                        break;
                    case "Sept":
                        str = [ <%=GetDayFinancialArray(9, false)%> ];
                        break;
                    case "Oct":
                        str = [ <%=GetDayFinancialArray(10, false)%> ];
                        break;
                    case "Nov":
                        str = [ <%=GetDayFinancialArray(11, false)%> ];
                        break;
                    case "Dec":
                        str = [ <%=GetDayFinancialArray(12, false)%> ];
                        break;
                }

                data.addRows([
                  ['Products', str[0]],
                  ['Labor', str[1]],
                  ['Operating Costs', str[2]],
                  ['Marketing', str[3]],
                  ['Taxes', str[4]],
                  ['Other', str[5]],
                ]);
            }

            var total = 0;
            for (var i = 0; i < data.getNumberOfRows(); ++i) {
                total = total + data.getValue(i, 1);
            }

            data.addRow(['TOTAL', total]);

            var formatter = new google.visualization.NumberFormat({
                prefix: '$',
                negativeParens: true
            });

            formatter.format(data, 1);

            return data;
        }
    </script>

    <script type="text/javascript">
        function activate(e)
        {
            var active = document.getElementsByClassName("btn btn-primary active")[1];
            active.classList.remove("active");

            e.classList.add("active");

            switch(e.value)
            {
                case "1":
                    projectprogress.style.display = "";
                    chart_div.style.display = "none";
                    actions_div.style.display = "none";
                    employees_div.style.display = "none";
                    break;

                case "2":
                    projectprogress.style.display = "none";
                    chart_div.style.display = "";
                    actions_div.style.display = "none";
                    employees_div.style.display = "none";
                    drawChart();

                    //Google charts bug hackfix
                    chart_div.children[0].style.width = "100%";
                    break;

                case "3":
                    projectprogress.style.display = "none";
                    chart_div.style.display = "none";
                    actions_div.style.display = "";
                    employees_div.style.display = "none";
                    break;

                case "4":
                    projectprogress.style.display = "none";
                    chart_div.style.display = "none";
                    actions_div.style.display = "none";
                    employees_div.style.display = "";
                    break;
            }
        }

        function financialSwitch(i, e)
        {
            var active = document.getElementsByClassName("btn btn-primary active")[0];
            active.classList.remove("active");

            e.classList.add("active");

            if (i == 0) {
                finBox.style.display = "none";
                finBox2.style.display = "";

                drawBasic();
            }
            else {
                finBox.style.display = "";
                finBox2.style.display = "none";
            }
        }
    </script>

    <script>
        function selectActive(e) {
            var active = document.getElementsByClassName("iconLink active");
            active[0].classList.remove("active");
            e.classList.add("active");
        }

        function scrollElement(e) {
            var elem = document.documentElement;
            var rect = e.getBoundingClientRect();
            elem.scrollTop = rect.top + window.scrollY;
        }
    </script>

    <asp:Literal ID="ganttChart" runat="server"></asp:Literal>

    <style>
        .chartHeader
        {
            background: linear-gradient(to bottom, #242e3a 0%,#061a33 2%,#001e44 4%,#05224e 6%,#061c45 7%,#0a1c40 9%,#05224a 13%,#082044 15%,#09254d 28%,#0d2450 30%,#112f63 52%,#102f65 57%,#0b2c61 59%,#0b2c61 100%);
            color: white; 
            text-shadow: 2px 2px black;
        }

        .iconLink
        {
            color: rgb(216, 216, 216); 
            text-decoration: none; 
            font-weight: 600; 
            display: block; 
            margin-bottom: 20px;
        }

        .iconLink:hover
        {
            color: darkblue;
            text-decoration: none;
        }

        .active
        {
            color: darkblue;
        }

        html {
          scroll-behavior: smooth;
        }

        svg > g > g:last-child { pointer-events: none }
    </style>
</head>
<body style="padding: 8px; background: linear-gradient(to bottom, rgba(191,191,191,1) 0%,rgba(232,232,232,1) 13%,rgba(244,244,244,1) 100%) no-repeat; overflow-y: hidden;">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>
    <table style="width: 100%">
        <tr>
            <td rowspan="5" style="min-width: 100px; width: 100px; vertical-align: top;">
                <div style="background-color: white; height: 100%; width: 100px; position: fixed; text-align: center; padding-top: 10px; line-height: 0.5;">
                    <a class="iconLink active" href="javascript:void(0);" onclick="document.documentElement.scrollTop = 0; selectActive(this);"><i class="material-icons" style="font-size: 48px;">folder</i><br />Project</a>
                    <a class="iconLink" href="javascript:scrollElement(document.getElementById('financial'));" onclick="selectActive(this);"><i class="material-icons" style="font-size: 48px;">monetization_on</i><br />Financial</a>
                    <a class="iconLink" href="javascript:scrollElement(document.getElementById('progress'));" onclick="selectActive(this);"><i class="material-icons" style="font-size: 48px;">assessment</i><br />Progress</a>
                    <a class="iconLink" href="javascript:scrollElement(document.getElementById('recruiting'));" onclick="selectActive(this);"><i class="material-icons" style="font-size: 48px;">assignment_ind</i><br />Recruiting</a>
                </div>
            </td>

            <td>
                <div id="project" class="contentBox" style="width: 100%; height: auto; word-wrap: break-word;">
                    <img src="Images/project.png" style="margin-left: 10px; margin-top: 5px; float: left;" /><h1 style="font-family: Raleway; margin-left: 10px; display: inline-block; font-size: 30px; margin-top: 10px;">Project</h1>
                    <div style="margin: 0 20px 20px 20px; max-height: 900px;">
                        <table width="100%" style="font-size: 14pt;">
                            <thead>
                                <tr>
                                    <td colspan="2" style="font-size: 18pt;"><b>Information</b></td>
                                    <td style="width: 30px;"></td>
                                    <td colspan="3" style="font-size: 18pt; width: 50%;"><b>Project Manager</b></td>
                                </tr>
                            </thead>

                            <tr>
                                <td>
                                    <b>Name:</b>
                                </td>

                                <td>
                                    <asp:Literal runat="server" ID="pProjectName"></asp:Literal>
                                </td>

                                <td>

                                </td>

                                <td>
                                    <b>Name:</b>
                                </td>

                                <td></td>

                                <td>
                                    <asp:Literal runat="server" ID="contactName"></asp:Literal><asp:TextBox ID="TextBox1" runat="server" Visible="false" Width="100%"></asp:TextBox>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <b>Summary:</b>
                                </td>

                                <td rowspan="5" style="vertical-align: top;">
                                    <asp:Literal runat="server" ID="pProjectSummary"></asp:Literal>
                                </td>

                                <td>

                                </td>

                                <td>
                                    <b>Phone:</b>
                                </td>

                                <td></td>

                                <td>
                                    <asp:Literal runat="server" ID="contactPhone"></asp:Literal><asp:TextBox ID="TextBox2" runat="server" Visible="false" Width="100%"></asp:TextBox>
                                </td>
                            </tr>

                            <tr>
                                <td></td>

                                <td>

                                </td>

                                <td>
                                    <b>Email:</b>
                                </td>

                                <td></td>

                                <td>
                                    <asp:Literal runat="server" ID="contactEmail"></asp:Literal><asp:TextBox ID="TextBox3" runat="server" Visible="false" Width="100%"></asp:TextBox>
                                </td>
                            </tr>

                            <tr>
                                <td>

                                </td>

                                <td>

                                </td>

                                <td colspan="4">
                                    <% if (User.IsInRole("Manager") || User.IsInRole("Administrators")) { %>
                                    <asp:Button ID="Button1" CssClass="btn-default btn-sm" runat="server" Text="Edit" Width="80px" BackColor="#0d0435" ForeColor="White" OnClick="Button1_Click" style="font-weight: 500; border: none;" />
                                    <% } %>
                                    <asp:Button ID="Button2" CssClass="btn-default btn-sm" runat="server" Text="Save" Width="80px" BackColor="#0d0435" ForeColor="White" Visible="false" OnClick="Button2_Click" style="font-weight: 500; border: none;" />
                                </td>
                            </tr>

                            <tr>
                                <td style="height: 20px;"></td>
                            </tr>

                            <tr>
                                <td></td>

                                <td>

                                </td>

                                <td  colspan="3">
                                    <% if (User.IsInRole("Manager") || User.IsInRole("Administrators"))
                                        { %>
                                    <span style="font-size: 18pt; font-weight: bold;">Progress Report</span><br />
                                    <asp:Button ID="Button4" CssClass="btn-default btn-sm" runat="server" Text="Edit" Width="80px" BackColor="#0d0435" ForeColor="White" OnClick="Button4_Click" style="font-weight: 500; border: none;" />
                                    <% } %>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </td>

            <td style="min-width: 10px; width: 10px;"></td>
        </tr>  

        <tr>
            <td id="fin" style="width: 100%;">
                <div id="financial" class="contentBox" style="width: auto; height: 500px; overflow-y: auto;" runat="server">
                    <img src="Images/financial.png" style="margin-left: 10px; margin-top: 5px; float: left;" /><h1 style="margin-left: 10px; display: inline-block; font-size: 30px; margin-top: 10px;">Financial</h1><br />
                    <div style="text-align: center;">
                        <div class="btn-group">
                            <button type="button" value="1" class="btn btn-primary active" onclick="financialSwitch(1, this)">Total Finances</button>
                            <button type="button" value="2" class="btn btn-primary" onclick="financialSwitch(0, this)">Monthly Finances</button>
                        </div> 
                    </div>
                    <div id="finBox" runat="server" style="height: 350px; margin: 0 20px 20px 20px;">
                        <p style="font-size: 14pt; margin: 0;"><b>Budget:</b> <asp:Literal runat="server" ID="budget"></asp:Literal></p>
                        <p style="font-size: 14pt; margin: 0;"><b>Gross Profit:</b> <asp:Literal runat="server" ID="totalprofit"></asp:Literal></p>
                        <p style="font-size: 14pt; margin: 0;"><b>Total Expenses:</b> <asp:Literal runat="server" ID="spent"></asp:Literal></p>
                        <p style="font-size: 14pt; margin: 0;"><b>Net Profit:</b> <asp:Literal runat="server" ID="netprofit"></asp:Literal></p>
                        <hr />
                        <div id="profit_chart" style="width: 50%; display: inline; float: left;"></div>
                        <div id="losses_chart" style="width: 50%; display: inline; float: left;"></div>
                    </div>

                    <div id="finBox2" runat="server" style="margin: 0 20px; height: 400px; display: none;">
                        <asp:HiddenField ID="javascriptDay" Value="1" runat="server" />
                        <asp:HiddenField ID="javascriptMonth" Value="1" runat="server" />
                        <div id="columnChart" style="height: auto;"></div>
                        <div id="profit" style="width: 50%; display: inline; float: left;"></div>
                        <div id="losses" style="width: 50%; display: inline; float: left;"></div>
                    </div>
                </div>
            </td>

            <td style="min-width: 10px; width: 10px;"></td>
        </tr>

        <tr>
            <td style="width: 100%;">
                <div id="progress" class="contentBox" style="width: 100%; height: auto; max-height: 950px;">
                    <img src="Images/progress.png" style="margin-left: 10px; margin-top: 5px; float: left;" /><h1 style="font-family: Raleway; margin-left: 10px; display: inline-block; font-size: 30px; margin-top: 10px;">Progress</h1>
                    <div style="padding-left: 20px; padding-right: 20px;">
                        <div style="text-align: center;">
                             <div class="btn-group">
                                <button type="button" value="1" class="btn btn-primary active" onclick="activate(this)">Overview</button>
                                <button type="button" value="2" class="btn btn-primary" onclick="activate(this)">Timeline</button>
                                <button type="button" value="3" class="btn btn-primary" onclick="activate(this)">Tasks</button>
                                <button type="button" value="4" class="btn btn-primary" onclick="activate(this)">Employees</button>
                             </div> 
                        </div><br />
                        <div id="projectprogress" runat="server" style="height: auto; max-height: 900px; overflow-y: auto; margin-bottom: 20px;">
                            <p style="font-size: 14pt; margin: 0; text-align: center;"><b>Project Completion</b></p>
                            <div class="progress" style="height: 50px;">
                                <div id="progressbar" class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="background-color: #1b377e; width:72%; font-family: Verdana,sans-serif; font-weight: 400; font-size: 18pt; text-shadow: 2px 2px black;" runat="server">
                                            72%
                                            <asp:Chart ID="Chart1" runat="server">
                                                <Series>
                                                    <asp:Series Name="Series1">
                                                    </asp:Series>
                                                </Series>
                                                <ChartAreas>
                                                    <asp:ChartArea Name="ChartArea1">
                                                    </asp:ChartArea>
                                                </ChartAreas>
                                            </asp:Chart>
                                </div>
                            </div>
                            <hr />
                            <p style="font-size: 14pt; margin: 0;"><b>Project Start Date:</b> <asp:Literal runat="server" ID="startdate"></asp:Literal></p>
                            <p style="font-size: 14pt; margin: 0;"><b>Target Completion Date:</b> <asp:Literal runat="server" ID="completedate"></asp:Literal></p>
                            <p style="font-size: 14pt; margin: 0;"><b>Monthly Hours (Estimated):</b> <asp:Literal runat="server" ID="hourscompleted"></asp:Literal></p>
                            <p style="font-size: 14pt; margin: 0;"><b>Billable Employees:</b> <asp:Literal runat="server" ID="employeesbill"></asp:Literal></p>
                        </div>

                        <div id="chart_div" style="width: 100%; height: auto; max-height: 900px; overflow-y: auto; display: none; margin-bottom: 20px;">

                        </div>

                        <div id="actions_div" style="width: 100%; height: auto; max-height: 900px; overflow-y: auto; display: none; margin-bottom: 20px;">
                            <table style="width: 100%;">
                                <thead style="border-bottom: 1px solid lightgray;">
                                    <tr>
                                        <td>
                                            <b>Task Name</b>
                                        </td>

                                        <td>
                                            <b>Status</b>
                                        </td>
                                    </tr>
                                </thead>
                                <asp:Literal ID="Literal2" runat="server"></asp:Literal>
                            </table>
                        </div>

                        <div id="employees_div" style="width: 100%; height: auto; max-height: 900px; overflow-y: auto; display: none; margin-bottom: 20px;">
                            <table style="width: 100%;">
                                <thead style="border-bottom: 1px solid lightgray;">
                                    <tr>
                                        <td>
                                            <b>Name</b>
                                        </td>

                                        <td>
                                            <b>Position</b>
                                        </td>

                                        <td>
                                            <b>Type</b>
                                        </td>
                                    </tr>
                                </thead>
                                <asp:Literal ID="Literal3" runat="server"></asp:Literal>
                            </table>
                        </div>
                    </div>
                </div>
            </td>

            <td style="min-width: 10px; width: 10px;"></td>
        </tr>

        <tr>
            <td style="width: 100%;">
                <div id="recruiting" class="contentBox" style="width: 100%; height: auto; max-height: 950px;">
                    <img src="Images/recruiting.png" style="margin-left: 10px; margin-top: 5px; float: left;" /><h1 style="font-family: Raleway; margin-left: 10px; display: inline-block; font-size: 30px; margin-top: 10px;">Recruiting Status</h1>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div id="grid" style="margin: 20px; height: auto; max-height: 900px; margin-bottom: 20px; overflow-y: auto;" runat="server">
                                <% if (User.IsInRole("Manager") || User.IsInRole("Administrators"))
                                { %>
                                <asp:Button ID="Button5" CssClass="btn-default btn-sm" runat="server" Text="New Entry" Width="100px" BackColor="#0d0435" ForeColor="White" style="font-weight: 500; border: none;" OnClick="Button5_Click" />
                                <% } %>
                                <p id="errortext" runat="server" style="color: red;"></p>
                                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" OnLoad="GridView1_Load" Width="100%" OnRowDataBound="GridView1_RowDataBound" OnSelectedIndexChanged="GridView1_SelectedIndexChanged" OnRowDeleting="GridView1_RowDeleting" OnRowEditing="GridView1_RowEditing" GridLines="None">
                                    <Columns>
                                        <asp:BoundField DataField="Open Position" HeaderText="Open Position" >
                                        <ItemStyle Width="30%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Days Open" HeaderText="Days Open" />
                                        <asp:BoundField DataField="Resumes Found" HeaderText="Resumes Found" />
                                        <asp:BoundField DataField="Resumes Submitted" HeaderText="Resumes Submitted" />
                                    </Columns>
                                    <HeaderStyle CssClass="chartHeader" />
                                </asp:GridView>
                                <div id="gridContent" runat="server">
                                    <div id="RecruitForm" runat="server" visible="false">
                                        <table style="width: 100%">
                                            <tr>
                                                <td>
                                                    <b>Position:</b>
                                                </td>

                                                <td>
                                                    <asp:TextBox ID="TextPosition" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <b>Resumes Found:</b>
                                                </td>

                                                <td>
                                                    <asp:TextBox ID="TextResumeFound" runat="server" TextMode="Number"></asp:TextBox>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <b>Resumes Submitted:</b>
                                                </td>

                                                <td>
                                                    <asp:TextBox ID="TextResumeSubmitted" runat="server" TextMode="Number"></asp:TextBox>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <b>Position Open Since:</b>
                                                </td>

                                                <td>
                                                    <asp:TextBox ID="TextOpenDate" runat="server" TextMode="Date"></asp:TextBox>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <asp:Label ID="LabelOpportunityCost" runat="server" Text="Opportunity Loss:" Font-Bold="True" ToolTip="The estimated total cost (in USD) of the position being open for x days"></asp:Label>
                                                </td>

                                                <td>
                                                    <asp:TextBox ID="TextOpportunityCost" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <asp:Label ID="LabelCausalAnalysis" runat="server" Text="Causal Analysis:" Font-Bold="True" ToolTip="Reason for position being open for x days"></asp:Label>
                                                </td>

                                                <td>
                                                    <asp:TextBox ID="TextCausalAnalysis" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:Button ID="Button6" CssClass="btn-default btn-sm" runat="server" Text="Save" Width="100px" BackColor="#0d0435" ForeColor="White" style="font-weight: 500; border: none;" OnClick="Button6_Click" />
                                    </div>
                                    <asp:Literal ID="Literal1" runat="server" Visible="false"></asp:Literal>
                                    <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click" Visible="false" >← Back</asp:LinkButton>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel> 
                </div>
            </td>

            <td style="min-width: 10px; width: 10px;"></td>
        </tr>

        <tr>
            <td style="height: 800px;"></td>
        </tr>
    </table>
    </form>
</body>
</html>
