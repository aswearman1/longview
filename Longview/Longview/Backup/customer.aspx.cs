﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace Longview
{
    public partial class customer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {  
            if(User.IsInRole("Administrators"))
            {
                FileUpload1.Visible = true;
                UploadTimesheet.Visible = true;
                DeleteTimesheet.Visible = true;

                FileUpload2.Visible = true;
                UploadInvoice.Visible = true;
                DeleteInvoice.Visible = true;

                FileUpload3.Visible = true;
                UploadProgress.Visible = true;
                DeleteProgress.Visible = true;

                FileUpload5.Visible = true;
                UploadPPQ.Visible = true;
                DeletePPQ.Visible = true;
            }
        }

        protected string[] getDocuments(string directory)
        {
            string[] files = { };
            try
            {
                files = Directory.GetFiles(Server.MapPath("~/Documents/" + directory), "*.pdf");
            }
            catch (Exception ex)
            {

            }

            return files;
        }

        protected void btnClick(object sender, EventArgs args)
        {
            var htmlBut = (HtmlButton)sender;
            string path = "~/Documents/";
            switch (htmlBut.ID)
            {
                case "timesheetbut":
                    if (ListBox1.SelectedItem == null)
                        return;

                    path += "timesheets/" + ListBox1.SelectedItem.Value;
                    break;

                case "invoicesbut":
                    if (ListBox2.SelectedItem == null)
                        return;

                    path += "invoices/" + ListBox2.SelectedItem.Value;
                    break;

                case "reportbut":
                    if (ListBox3.SelectedItem == null)
                        return;

                    path += "progress_reports/" + ListBox3.SelectedItem.Value;
                    break;

                case "ppqbut":
                    if (ListBox5.SelectedItem == null)
                        return;

                    path += "ppq_requests/" + ListBox5.SelectedItem.Value;
                    break;
            }

            Response.Redirect(path + ".pdf");
        }

        protected void UploadTimesheet_Click(object sender, EventArgs e)
        {
            if (!User.IsInRole("Administrators"))
                return;

            if (IsPostBack)
            {
                Label lab = null;
                FileUpload fup = null;
                ListBox lb = null;
                String file = "";
                switch(((Button)sender).ID)
                {
                    case "UploadTimesheet":
                        file = "timesheets";
                        lb = ListBox1;
                        lab = Label1;
                        fup = FileUpload1;
                        break;

                    case "UploadPPQ":
                        file = "ppq_requests";
                        lb = ListBox5;
                        lab = Label5;
                        fup = FileUpload5;
                        break;

                    case "UploadProgress":
                        file = "progress_reports";
                        lb = ListBox3;
                        lab = Label3;
                        fup = FileUpload3;
                        break;

                    case "UploadInvoice":
                        file = "invoices";
                        lb = ListBox2;
                        lab = Label2;
                        fup = FileUpload2;
                        break;
                }

                if (lab == null)
                    return;

                if(lb == null || fup == null)
                {
                    lab.Text = "An error has occurred.";
                    return;
                }

                Boolean fileOK = false;
                String path = Server.MapPath("~/Documents/" + file + "/");
                if (!fup.HasFile)
                {
                    lab.Text = "Please select a file to upload.";
                    return;
                }

                if(fup.PostedFile.ContentLength > 4194304)
                {
                    lab.Text = "File cannot be larger than 4MB.";
                    return;
                }

                if(File.Exists(path + fup.PostedFile.FileName))
                {
                    lab.Text = "File with that name already exists. Please rename the file before re-uploading.";
                    return;
                }

                String fileExtension =
                    System.IO.Path.GetExtension(fup.FileName).ToLower();
                String allowedExtension = ".pdf";
                if (fileExtension == allowedExtension)
                    fileOK = true;

                if (!fileOK)
                {
                    lab.Text = "Cannot accept files of this type.";
                    return;
                }

                try
                {
                    fup.PostedFile.SaveAs(path
                        + fup.FileName);
                    lab.Text = "File uploaded!";
                    lb.Items.Clear();
                    refresh(lb, file);
                }
                catch (Exception ex)
                {
                    lab.Text = "File could not be uploaded.";
                }
            }
        }

        protected void ListBox1_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                refresh(ListBox1, "timesheets");
            }
        }

        protected void ListBox5_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                refresh(ListBox5, "ppq_requests");
            }
        }

        protected void ListBox3_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                refresh(ListBox3, "progress_reports");
            }
        }

        protected void ListBox4_Load(object sender, EventArgs e)
        {

        }

        protected void ListBox2_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                refresh(ListBox2, "invoices");
            }
        }

        protected void refresh(ListBox lb, string folder)
        {
            string[] populate = getDocuments(folder);
            if (populate.Count() > 0)
            {
                foreach (string s in populate)
                {
                    lb.Items.Add(Path.GetFileNameWithoutExtension(s));
                }
            }
        }

        protected void DeleteTimesheet_Click(object sender, EventArgs e)
        {
            if (!User.IsInRole("Administrators"))
                return;

            ListBox lb = null;
            Label lab = null;
            String file = "";
            switch (((Button)sender).ID)
            {
                case "DeleteTimesheet":
                    file = "timesheets";
                    lb = ListBox1;
                    lab = Label1;
                    break;

                case "DeletePPQ":
                    file = "ppq_requests";
                    lb = ListBox5;
                    lab = Label5;
                    break;

                case "DeleteProgress":
                    file = "progress_reports";
                    lb = ListBox3;
                    lab = Label3;
                    break;

                case "DeleteInvoice":
                    file = "invoices";
                    lb = ListBox2;
                    lab = Label2;
                    break;
            }

            if(lab == null)
                return;

            if (lb == null)
            {
                lab.Text = "An error has occurred.";
                return;
            }

            if (lb.SelectedItem == null)
            {
                lab.Text = "Please make a selection first.";
                return;
            }

            string path = Server.MapPath("~/Documents/" + file + "/" + lb.SelectedItem.Value + ".pdf");
            if(!File.Exists(path))
            {
                lab.Text = "The file does not exist.";
                return;
            }

            try
            {
                File.Delete(path);
                lb.Items.Clear();
                refresh(lb, file);
                lab.Text = "The file has been removed.";
            }
            catch (Exception ex)
            {
                lab.Text = "An error has occurred.";
            }
        }
    }
}