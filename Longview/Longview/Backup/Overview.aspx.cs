﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Longview
{
    public partial class Overview : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!User.Identity.IsAuthenticated)
            { 
                Response.Redirect("Default.aspx");
                return;
            }

            string notes = "";
            string surveyt = "";
            if(getData(out notes, out surveyt))
            {
                notesText.InnerHtml = notes;
                survey.Src = surveyt;
            }

            if(User.IsInRole("Administrators"))
            {
                Button1.Visible = true;
                Button2.Visible = true;
            }
        }

        protected bool getData(out string notes, out string survey)
        {
            string nt = "";
            string sv = "";

            using (MySqlConnection mysqlCon = new MySqlConnection(ConfigurationManager.ConnectionStrings["LocalMySqlServer"].ConnectionString))
            {
                try
                {
                    mysqlCon.Open();
                    using (MySqlCommand sqlCommand = new MySqlCommand("SELECT notes, data1 FROM tab_data WHERE tab_name = ?param1;", mysqlCon))
                    {
                        sqlCommand.Parameters.AddWithValue("param1", "Overview");
                        using (MySqlDataReader dataReader = sqlCommand.ExecuteReader())
                        {
                            if (dataReader != null)
                            {
                                while (dataReader.Read())
                                {
                                    nt = dataReader.GetString(0);
                                    sv = dataReader.GetString(1);
                                }
                            }
                        }
                    }
                    mysqlCon.Close();
                }
                catch (Exception ex)
                {
                    notes = "";
                    survey = "";
                    mysqlCon.Close();
                    return false;
                }
            }

            notes = nt;
            survey = sv;

            return true;
        }



        protected void Button1_Click(object sender, EventArgs e)
        {
            if (User.IsInRole("Administrators"))
            {
                if (notesText.Visible)
                {
                    Button1.Text = "Save";
                    notesText.Visible = false;
                    TextBox1.Text = notesText.InnerText;
                    TextBox1.Visible = true;
                }
                else
                {
                    Button1.Text = "Edit";
                    notesText.Visible = true;
                    SetNotes(TextBox1.Text);
                    TextBox1.Visible = false;
                }
            }
        }

        protected void SetNotes(string s)
        {
            using (MySqlConnection mysqlCon = new MySqlConnection(ConfigurationManager.ConnectionStrings["LocalMySqlServer"].ConnectionString))
            {
                mysqlCon.Open();
                try
                {
                    using (MySqlCommand sqlCommand = new MySqlCommand("UPDATE tab_data SET notes = @param1 WHERE tab_name = @param2;", mysqlCon))
                    {
                        sqlCommand.Parameters.AddWithValue("@param1", s);
                        sqlCommand.Parameters.AddWithValue("@param2", "Overview");

                        int affected = sqlCommand.ExecuteNonQuery();
                        if (affected > 0)
                        {
                            notesText.InnerText = TextBox1.Text;
                        }
                    }
                }
                finally
                {
                    mysqlCon.Close();
                }
            }
        }

        protected void SetSurvey(string s)
        {
            if (s == survey.Src)
                return;

            WebRequest webRequest = WebRequest.Create(s);
            if (webRequest.RequestUri.Host != "www.surveymonkey.com")
                return;

            using (MySqlConnection mysqlCon = new MySqlConnection(ConfigurationManager.ConnectionStrings["LocalMySqlServer"].ConnectionString))
            {
                mysqlCon.Open();
                try
                {
                    using (MySqlCommand sqlCommand = new MySqlCommand("UPDATE tab_data SET data1 = @param1 WHERE tab_name = @param2;", mysqlCon))
                    {
                        sqlCommand.Parameters.AddWithValue("@param1", s);
                        sqlCommand.Parameters.AddWithValue("@param2", "Overview");

                        int affected = sqlCommand.ExecuteNonQuery();
                        if (affected > 0)
                        {
                            survey.Src = s;
                        }
                    }
                }
                finally
                {
                    mysqlCon.Close();
                }
            }
        }

        protected decimal GetMonthlyFinances(int index)
        {
            decimal result = 0;
            foreach(KeyValuePair<string, decimal[]> kvp in ProjectsManager.GetYearlyFinances())
            {
                result += kvp.Value[index];
            }

            return result;
        }

        protected decimal GetFinancesByMonth(int month, bool profit)
        {
            decimal result = 0;

            string[] mo = { "January", "February", "March", "April", "May", "June", "July", "August",
            "September", "October", "November", "December" };

            string selected = mo[month - 1] + " " + DateTime.Now.Year;

            if (ProjectsManager.GetYearlyFinances().ContainsKey(selected))
            {
                if (!profit)
                {
                    result += ProjectsManager.GetYearlyFinances()[selected][0];
                    result += ProjectsManager.GetYearlyFinances()[selected][1];
                    result += ProjectsManager.GetYearlyFinances()[selected][2];
                    result += ProjectsManager.GetYearlyFinances()[selected][3];
                    result += ProjectsManager.GetYearlyFinances()[selected][4];
                    result += ProjectsManager.GetYearlyFinances()[selected][5];
                }
                else
                {
                    result += ProjectsManager.GetYearlyFinances()[selected][6];
                    result += ProjectsManager.GetYearlyFinances()[selected][7];
                    result += ProjectsManager.GetYearlyFinances()[selected][8];
                }
            }

            return result;
        }

        protected string GetDayFinances(int month, bool profit = false)
        {
            string result = "";
            decimal decResult = 0;
            DateTime value = new DateTime(DateTime.Now.Year, month,
                1);
            decimal[] d = ProjectsManager.GetMonthlyFinances().ContainsKey(value.ToString("d MMMM yyyy")) ?
                ProjectsManager.GetMonthlyFinances()[value.ToString("d MMMM yyyy")] : null;

            if (d != null)
                if (!profit)
                    decResult = d[0] + d[1] + d[2] + d[3] + d[4] + d[5];
                else
                    decResult = d[6] + d[7] + d[8];
            else
                decResult = 0;

            result += decResult;

            for (int i = 2; i <= DateTime.DaysInMonth(value.Year, value.Month); ++i)
            {
                value = value.AddDays(1);

                d = ProjectsManager.GetMonthlyFinances().ContainsKey(value.ToString("d MMMM yyyy")) ?
                ProjectsManager.GetMonthlyFinances()[value.ToString("d MMMM yyyy")] : null;

                if (d != null)
                    if (!profit)
                        decResult = d[0] + d[1] + d[2] + d[3] + d[4] + d[5];
                    else
                        decResult = d[6] + d[7] + d[8];
                else
                    decResult = 0;

                result += "," + decResult;
            }

            return result;
        }

        protected string GetDayFinancialArray(int month, bool profit)
        {
            string result = "";
            decimal[] d = { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            DateTime dt = new DateTime(DateTime.Now.Year, month, 1);
            if (profit)
            {
                foreach (KeyValuePair<string, decimal[]> kvp in
                    ProjectsManager.GetYearlyFinances().Where(x => x.Key.Contains(dt.ToString("MMMM yyyy"))))
                {
                    d[6] += kvp.Value[6];
                    d[7] += kvp.Value[7];
                    d[8] += kvp.Value[8];
                }
            }
            else
            {
                foreach (KeyValuePair<string, decimal[]> kvp in
                    ProjectsManager.GetYearlyFinances().Where(x => x.Key.Contains(dt.ToString("MMMM yyyy"))))
                {
                    d[0] += kvp.Value[0];
                    d[1] += kvp.Value[1];
                    d[2] += kvp.Value[2];
                    d[3] += kvp.Value[3];
                    d[4] += kvp.Value[4];
                    d[5] += kvp.Value[5];
                }
            }

            result += d[0] + "," + d[1] + "," + d[2] + "," + d[3] + "," + d[4] + "," + d[5] + "," +
                d[6] + "," + d[7] + "," + d[8];

            return result;
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            if (!User.IsInRole("Administrators"))
                return;

            if (Button2.Text == "Edit")
            {
                survey.Visible = false;
                SurveyInput.Visible = true;
                Button2.Text = "Save";
                SurveyURL.Text = survey.Src;
            }
            else
            {
                survey.Visible = true;
                SurveyInput.Visible = false;
                Button2.Text = "Edit";
                SetSurvey(SurveyURL.Text);
            }
        }
    }
}