﻿using Microsoft.AspNet.Identity;
using MySql.Web.Profile;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Longview
{
    public partial class project2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string qs = Request.QueryString["project"];
            if (String.IsNullOrEmpty(qs))
            {
                Response.StatusCode = 404;
                Response.End();
                return;
            }

            int ID;
            if (!int.TryParse(qs, out ID))
            {
                Response.StatusCode = 404;
                Response.End();
                return;
            }

            if (ID == 0)
            {
                Response.StatusCode = 404;
                Response.End();
                return;
            }

            if (!User.IsInRole(ProjectsManager.GetRoleNameById(ID)) && !User.IsInRole("Administrators"))
                {
                    Response.StatusCode = 403;
                    Response.End();
                    return;
                }

                DisplayData(ID);
        }

        protected Project GetCurrentProject()
        {
            string qs = Request.QueryString["project"];
            if (String.IsNullOrEmpty(qs))
                return null;

            int ID;
            if (!int.TryParse(qs, out ID))
                return null;

            string name = ProjectsManager.GetRoleNameById(ID);
            if (name == null)
                return null;

            Project proj = ProjectsManager.GetRoleByName(name);

            return proj;
        }

        protected void DisplayData(int s)
        {
            string name = ProjectsManager.GetRoleNameById(s);
            if (name == null)
            {
                Response.StatusCode = 404;
                Response.End();
                return;
            }

            Project proj = ProjectsManager.GetRoleByName(name);
            if (proj == null)
            {
                Response.StatusCode = 404;
                Response.End();
                return;
            }

            budget.Text = String.Format("{0:C}", proj.getBudget());
            spent.Text = String.Format("{0:C}", proj.getCost());
            totalprofit.Text = String.Format("{0:C}", proj.getProfit());
            netprofit.Text = String.Format("{0:C}", proj.getProfit() - proj.getCost());
            progressbar.InnerHtml = HttpUtility.HtmlEncode(proj.getProjectProgress() + "%");
            progressbar.Style.Add("width", proj.getProjectProgress() + "%");
            startdate.Text = proj.getStartDate().ToLongDateString();
            completedate.Text = proj.getCompleteDate().ToLongDateString();
            employeesbill.Text = String.Format("{0}", proj.getEmployees().Count);
            pProjectName.Text = proj.getName();
            pProjectSummary.Text = proj.getSummary();

            contactName.Text = HttpUtility.HtmlEncode(proj.getContactName());
            contactPhone.Text = HttpUtility.HtmlEncode(proj.getContactPhone());
            contactEmail.Text = HttpUtility.HtmlEncode(proj.getContactEmail());

            if (!IsPostBack)
            {
                if (User.IsInRole("Manager") || User.IsInRole("Administrators"))
                {
                    CommandField field = new CommandField();
                    field.UpdateText = "Edit";
                    field.ShowEditButton = true;
                    GridView1.Columns.Add(field);

                    field = new CommandField();
                    field.DeleteText = "Delete";
                    field.ShowDeleteButton = true;
                    GridView1.Columns.Add(field);
                }

                foreach (KeyValuePair<int, string[]> kvp in proj.getActions())
                {
                    Literal2.Text += @"<tr style='height: 50px;'>";

                    Literal2.Text += "<td>";
                    Literal2.Text += "<b>" + kvp.Value[0] + "</b><br>Priority: " + GetPriority(kvp.Value[2]);
                    Literal2.Text += "</td>";

                    Literal2.Text += "<td style='width: 20%;'>";
                    if (kvp.Value[3] == "Completed")
                    {
                        Literal2.Text += "<div style='background-color: green; color: white; text-align: center; border-radius: 3px;'>";
                        Literal2.Text += "Completed";
                        Literal2.Text += "</div>";
                    }
                    else if(kvp.Value[3] == "In Progress")
                    {
                        Literal2.Text += "<div style='background-color: blue; color: white; text-align: center; border-radius: 3px;'>";
                        Literal2.Text += "In Progress";
                        Literal2.Text += "</div>";
                    }
                    else
                    {
                        Literal2.Text += "<div style='background-color: red; color: white; text-align: center; border-radius: 3px;'>";
                        Literal2.Text += "Pending";
                        Literal2.Text += "</div>";
                    }
                    Literal2.Text += "</td>";
                }

                decimal hours = 0;
                foreach (KeyValuePair<string, Employee> kvp in proj.getEmployees())
                {
                    Literal3.Text += @"<tr style='height: 50px;'>";

                    Literal3.Text += "<td>";
                    Literal3.Text += kvp.Key;
                    Literal3.Text += "</td>";

                    Literal3.Text += "<td style='width: 25%;'>";
                    Literal3.Text += kvp.Value.position;
                    Literal3.Text += "</td>";

                    Literal3.Text += "<td style='width: 20%;'>";
                    Literal3.Text += (kvp.Value.type == EmployeeType.TYPE_PARTTIME ? "Part-time" : "Full-time");
                    Literal3.Text += "</td>";
                    hours += (kvp.Value.weeklyhours * 4);
                }
                hourscompleted.Text = String.Format("{0}", hours);

                if (proj.getMilestones().Count > 0)
                {
                    string chartScript =
                        @"<script type=""text/javascript"">
                    function drawChart() {

                        var data = new google.visualization.DataTable();
                        data.addColumn('string', 'Task ID');
                        data.addColumn('string', 'Task Name');
                        data.addColumn('date', 'Start Date');
                        data.addColumn('date', 'End Date');
                        data.addColumn('number', 'Duration');
                        data.addColumn('number', 'Percent Complete');
                        data.addColumn('string', 'Dependencies');" + Environment.NewLine + Environment.NewLine +

                            @"data.addRows([
                          ";

                    string chartScript2 =
                            @"]);

                        var options = {
                            height: 275,
                            width: '100%',
                        };

                        var chart = new google.visualization.Gantt(document.getElementById('chart_div'));

                        chart.draw(data, options);
                    }
                    </script>";

                    int count = 1;
                    foreach (string[] str in proj.getMilestones().Values)
                    {
                        DateTime dtStart = DateTime.Parse(str[2]);
                        DateTime dtEnd = DateTime.Parse(str[3]);
                        chartScript += "['" + count + "', '" + str[0] + "', new Date(" + dtStart.Year + ", " + (dtStart.Month - 1) +
                            ", " + dtStart.Day + "), new Date(" + dtEnd.Year + ", " + (dtEnd.Month - 1) + ", " + dtEnd.Day + "), " +
                            "null, " + str[1] + ", null]," + Environment.NewLine;
                        ++count;
                    }

                    ganttChart.Text = chartScript + chartScript2;
                }
            }
        }

        protected string GetPriority(string i)
        {
            string priority = "<span style='margin: 0;'>" + i + "</span>";

            switch(i)
            {
                case "Low":
                    priority = "<span style='margin: 0; color: blue;'>" + i + "</span>";
                    break;

                case "Medium":
                    priority = "<span style='margin: 0; color: green;'>" + i + "</span>";
                    break;

                case "High":
                    priority = "<span style='margin: 0; color: red;'>" + i + "</span>";
                    break;
            }

            return priority;
        }

        protected void GridView1_Load(object sender, EventArgs e)
        {
            Project proj = GetCurrentProject();
            if (proj == null)
            {
                Response.StatusCode = 404;
                Response.End();
                return;
            }

            if (!IsPostBack)
            {
                DataTable dt = new DataTable();

                if (dt.Columns.Count == 0)
                {
                    dt.Columns.Add("Open Position", typeof(string));
                    dt.Columns.Add("Days Open", typeof(int));
                    dt.Columns.Add("Resumes Found", typeof(int));
                    dt.Columns.Add("Resumes Submitted", typeof(int));
                }

                DataRow NewRow;
                foreach (Recruitment r in proj.getRecruitment())
                {
                    NewRow = dt.NewRow();
                    NewRow[0] = r.position;
                    NewRow[1] = DateTime.Now.Subtract(r.position_open).TotalDays;
                    NewRow[2] = r.resumes_found;
                    NewRow[3] = r.resumes_submitted;
                    dt.Rows.Add(NewRow);
                }
                
                GridView1.DataSource = dt;
                GridView1.DataBind();
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if(User.IsInRole("Manager") || User.IsInRole("Administrators"))
            {
                contactName.Visible = false;
                TextBox1.Visible = true;
                TextBox1.Text = contactName.Text;
                contactPhone.Visible = false;
                TextBox2.Visible = true;
                TextBox2.Text = contactPhone.Text;
                contactEmail.Visible = false;
                TextBox3.Visible = true;
                TextBox3.Text = contactEmail.Text;
                Button1.Visible = false;
                Button2.Visible = true;
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            if (!User.IsInRole("Manager") && !User.IsInRole("Administrators"))
                return;

            string qs = Request.QueryString["project"];
            if (String.IsNullOrEmpty(qs))
                return;

            int ID;
            if (!int.TryParse(qs, out ID))
                return;

            string projId = ProjectsManager.GetRoleNameById(ID);
            if (projId == null)
                return;

            if (TextBox1.Text == contactName.Text &&
                TextBox2.Text == contactPhone.Text &&
                TextBox3.Text == contactEmail.Text)
            {
                contactName.Visible = true;
                TextBox1.Visible = false;
                contactName.Text = TextBox1.Text;

                contactPhone.Visible = true;
                TextBox2.Visible = false;
                contactPhone.Text = TextBox2.Text;

                contactEmail.Visible = true;
                TextBox3.Visible = false;
                contactEmail.Text = TextBox3.Text;

                Button1.Visible = true;
                Button2.Visible = false;
                return;
            }

            Project prj = ProjectsManager.GetRoleByName(projId);
            try
            {
                prj.setContactInfo(TextBox1.Text, TextBox2.Text, TextBox3.Text);
            }
            finally
            {
                contactName.Visible = true;
                TextBox1.Visible = false;
                contactName.Text = TextBox1.Text;

                contactPhone.Visible = true;
                TextBox2.Visible = false;
                contactPhone.Text = TextBox2.Text;

                contactEmail.Visible = true;
                TextBox3.Visible = false;
                contactEmail.Text = TextBox3.Text;

                Button1.Visible = true;
                Button2.Visible = false;
            }
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(GridView1, "Select$" + e.Row.RowIndex);
                e.Row.Attributes["style"] = "cursor:pointer";
                e.Row.Attributes["onmouseover"] = "this.style.backgroundColor = '#061c45'; this.style.color = '#fff'; this.style.textShadow = '2px 2px black'";
                e.Row.Attributes["onmouseout"] = "this.style.backgroundColor = ''; this.style.color = '#181727'; this.style.textShadow = ''";
            }
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Project proj = GetCurrentProject();
            if (proj == null)
                return;

            string id = proj.getRecruitment()[GridView1.SelectedIndex].position;
            string days = GridView1.SelectedRow.Cells[1].Text;
            GridView1.Visible = false;
            Literal1.Visible = true;
            LinkButton1.Visible = true;
            if (Button5 != null)
                Button5.Visible = false;

            Literal1.Text = "<b>Position:</b> " + id + "<br>";
            Literal1.Text += "<b>Position Open Since:</b> " + proj.getRecruitment()[GridView1.SelectedIndex].position_open.ToLongDateString() + "<br>";
            Literal1.Text += "<b>Estimated Opportunity Loss:</b> " + String.Format("{0:C}", proj.getRecruitment()[GridView1.SelectedIndex].cost) + "<br>";
            Literal1.Text += "<b>Causal Analysis:</b> " + proj.getRecruitment()[GridView1.SelectedIndex].causal_analysis + "<br>";

            UpdatePanel1.Update();
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            GridView1.Visible = true;
            Literal1.Visible = false;
            RecruitForm.Visible = false;
            LinkButton1.Visible = false;
            errortext.InnerText = "";
            if (Button5 != null)
                Button5.Visible = true;
            UpdatePanel1.Update();
        }

        protected decimal GetFinances(int i)
        {
            Project proj = GetCurrentProject();
            if (proj == null)
            {
                Response.StatusCode = 404;
                Response.End();
                return 0;
            }

            decimal result = 0;

            foreach(KeyValuePair<string,decimal[]> kvp in proj.getFinancial())
            {
                result += kvp.Value[i];
            }

            return result;
        }

        protected decimal GetMonthlyFinances(int month, bool profit)
        {
            Project proj = GetCurrentProject();
            if (proj == null)
            {
                Response.StatusCode = 404;
                Response.End();
                return 0;
            }

            decimal result = 0;
            DateTime monthYear = new DateTime(DateTime.Now.Year, month, 1);

            foreach(KeyValuePair<string, decimal[]> kvp in proj.getFinancial().Where(x => x.Key.Contains(monthYear.ToString("MMMM yyyy"))))
            {
                if (!profit)
                {
                    result += kvp.Value[0] + kvp.Value[1] + kvp.Value[2] + kvp.Value[3] + kvp.Value[4] +
                    kvp.Value[5];
                }
                else
                {
                    result += kvp.Value[6] + kvp.Value[7] + kvp.Value[8];
                }
            }

            return result;
        }

        protected string GetDayFinances(int month, bool profit = false)
        {
            Project proj = GetCurrentProject();
            if (proj == null)
            {
                Response.StatusCode = 404;
                Response.End();
                return "";
            }

            string result = "";
            decimal decResult = 0;
            DateTime value = new DateTime(DateTime.Now.Year, month,
                1);
            decimal[] d = proj.getFinancial().ContainsKey(value.ToString("d MMMM yyyy")) ?
                proj.getFinancial()[value.ToString("d MMMM yyyy")] : null;

            if (d != null)
                if (!profit)
                    decResult = d[0] + d[1] + d[2] + d[3] + d[4] + d[5];
                else
                    decResult = d[6] + d[7] + d[8];
            else
                decResult = 0;

            result += decResult;

            for (int i = 2; i <= DateTime.DaysInMonth(value.Year, value.Month); ++i)
            {
                value = value.AddDays(1);

                d = proj.getFinancial().ContainsKey(value.ToString("d MMMM yyyy")) ?
                proj.getFinancial()[value.ToString("d MMMM yyyy")] : null;

                if (d != null)
                    if (!profit)
                        decResult = d[0] + d[1] + d[2] + d[3] + d[4] + d[5];
                    else
                        decResult = d[6] + d[7] + d[8];
                else
                    decResult = 0;

                result += "," + decResult;
            }

            return result;
        }

        protected string GetDayFinancialArray(int month, bool profit)
        {
            Project proj = GetCurrentProject();
            if (proj == null)
            {
                Response.StatusCode = 404;
                Response.End();
                return "";
            }

            string result = "";
            decimal[] d = { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            DateTime dt = new DateTime(DateTime.Now.Year, month, 1);
            if(profit)
            {
                foreach (KeyValuePair<string, decimal[]> kvp in 
                    proj.getFinancial().Where(x => x.Key.Contains(dt.ToString("MMMM yyyy"))))
                {
                    d[6] += kvp.Value[6];
                    d[7] += kvp.Value[7];
                    d[8] += kvp.Value[8];
                }
            }
            else
            {
                foreach (KeyValuePair<string, decimal[]> kvp in
                    proj.getFinancial().Where(x => x.Key.Contains(dt.ToString("MMMM yyyy"))))
                {
                    d[0] += kvp.Value[0];
                    d[1] += kvp.Value[1];
                    d[2] += kvp.Value[2];
                    d[3] += kvp.Value[3];
                    d[4] += kvp.Value[4];
                    d[5] += kvp.Value[5];
                }
            }

            result += d[0] + "," + d[1] + "," + d[2] + "," + d[3] + "," + d[4] + "," + d[5] + "," +
                d[6] + "," + d[7] + "," + d[8];

            return result;
        }

        protected void Button4_Click(object sender, EventArgs e)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "scriptid", "window.parent.location.href='/progressreport?project=" + GetCurrentProject().getId() + "'", true);
        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            if (!User.IsInRole("Manager") && !User.IsInRole("Administrators"))
                return;

            Project proj = GetCurrentProject();
            proj.DeleteRecruiting(GridView1.Rows[e.RowIndex].Cells[0].Text);

            DataTable dt = new DataTable();

            if (dt.Columns.Count == 0)
            {
                dt.Columns.Add("Open Position", typeof(string));
                dt.Columns.Add("Days Open", typeof(int));
                dt.Columns.Add("Resumes Found", typeof(int));
                dt.Columns.Add("Resumes Submitted", typeof(int));
            }

            DataRow NewRow;
            foreach (Recruitment r in proj.getRecruitment())
            {
                NewRow = dt.NewRow();
                NewRow[0] = r.position;
                NewRow[1] = DateTime.Now.Subtract(r.position_open).TotalDays;
                NewRow[2] = r.resumes_found;
                NewRow[3] = r.resumes_submitted;
                dt.Rows.Add(NewRow);
            }

            GridView1.DataSource = dt;
            GridView1.DataBind();
            UpdatePanel1.Update();
        }

        protected void Button5_Click(object sender, EventArgs e)
        {
            if (!User.IsInRole("Manager") && !User.IsInRole("Administrators"))
                return;

            GridView1.Visible = false;
            RecruitForm.Visible = true;
            LinkButton1.Visible = true;
            if (Button5 != null)
                Button5.Visible = false;

            TextPosition.Text = "";
            TextResumeFound.Text = "";
            TextResumeSubmitted.Text = "";
            TextOpenDate.Text = "";
            TextOpportunityCost.Text = "";
            TextCausalAnalysis.Text = "";

            UpdatePanel1.Update();
        }

        protected void Button6_Click(object sender, EventArgs e)
        {
            if (!User.IsInRole("Manager") && !User.IsInRole("Administrators"))
                return;

            Project proj = GetCurrentProject();
            if (proj == null)
                return;

            errortext.InnerText = "";

            try
            {
                proj.UpdateRecruiting(TextPosition.Text, int.Parse(TextResumeFound.Text),
                    int.Parse(TextResumeSubmitted.Text),
                    DateTime.Parse(TextOpenDate.Text), decimal.Parse(TextOpportunityCost.Text), 
                    TextCausalAnalysis.Text);
                GridView1.Visible = true;
                Literal1.Visible = false;
                RecruitForm.Visible = false;
                LinkButton1.Visible = false;
                if (Button5 != null)
                    Button5.Visible = true;
            }
            catch (Exception ex)
            {
                errortext.InnerText = "One or more field(s) are invalid.";
            }

            DataTable dt = new DataTable();

            if (dt.Columns.Count == 0)
            {
                dt.Columns.Add("Open Position", typeof(string));
                dt.Columns.Add("Days Open", typeof(int));
                dt.Columns.Add("Resumes Found", typeof(int));
                dt.Columns.Add("Resumes Submitted", typeof(int));
            }

            DataRow NewRow;
            foreach (Recruitment r in proj.getRecruitment())
            {
                NewRow = dt.NewRow();
                NewRow[0] = r.position;
                NewRow[1] = DateTime.Now.Subtract(r.position_open).TotalDays;
                NewRow[2] = r.resumes_found;
                NewRow[3] = r.resumes_submitted;
                dt.Rows.Add(NewRow);
            }

            GridView1.DataSource = dt;
            GridView1.DataBind();
            UpdatePanel1.Update();
        }

        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            e.Cancel = true;
            if (!User.IsInRole("Manager") && !User.IsInRole("Administrators"))
                return;

            Project proj = GetCurrentProject();
            if (proj == null)
                return;

            List<Recruitment> r = proj.getRecruitment().Where(x => x.position == GridView1.Rows[e.NewEditIndex].Cells[0].Text).ToList();
            if (r.Count == 0)
                return;

            GridView1.Visible = false;
            RecruitForm.Visible = true;
            LinkButton1.Visible = true;
            if (Button5 != null)
                Button5.Visible = false;

            TextPosition.Text = r.First().position;
            TextResumeFound.Text = String.Format("{0}", r.First().resumes_found);
            TextResumeSubmitted.Text = String.Format("{0}", r.First().resumes_submitted);
            TextOpenDate.Text = r.First().position_open.ToString("yyyy-MM-dd");
            TextOpportunityCost.Text = String.Format("{0}", r.First().cost);
            TextCausalAnalysis.Text = r.First().causal_analysis;

            UpdatePanel1.Update();
        }
    }
}