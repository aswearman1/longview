﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using Longview.Models;
using MySql.Web.Security;
using System.Web.Security;
using WebMatrix.WebData;

namespace Longview
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void Login1_Authenticate(object sender, System.Web.UI.WebControls.AuthenticateEventArgs e)
        {
            if (Membership.ValidateUser(Login1.UserName, Login1.Password))
            {
                FormsAuthentication.SetAuthCookie(Login1.UserName, true);
                FormsAuthentication.RedirectFromLoginPage(Login1.UserName, true);
            }
            else
                Login1.FailureText = "Login failed. Please check your user name and password and try again.";
        }

        protected void Button1_OnClick(object Source, EventArgs e)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated == true)
            {
                FormsAuthentication.SignOut();
                Response.Cookies[FormsAuthentication.FormsCookieName].Expires = DateTime.Now.AddYears(-1);
                FormsAuthentication.RedirectToLoginPage();
            }
        }
    }
}