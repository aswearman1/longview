﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="employee.aspx.cs" Inherits="Longview.employee" %>

<!DOCTYPE html>

<html>
<head>
    <title></title>
    <link href="Content/bootstrap.css" rel="stylesheet" id="bootstrap-css" />
    <link href="Content/longview.css" rel="stylesheet" id="longview-css" />
    <script type="text/javascript">
        var i = 0;
        function training(e)
        {
            var man = document.getElementById('mandatory');
            var reg = document.getElementById('regular');
            var reg2 = document.getElementById('regular2');
            if (i == 0) {
                setTimeout(function () { man.style.display = 'none'; }, 500);
                man.style.opacity = 0.0;
                setTimeout(function () { reg.style.display = 'flex'; }, 500);
                setTimeout(function () { reg.style.opacity = 1.0; }, 600);
                i = 1;
            }
            else if (i == 1)
            {
                setTimeout(function () { reg.style.display = 'none'; }, 500);
                reg.style.opacity = 0.0;
                setTimeout(function () { reg2.style.display = 'flex'; }, 500);
                setTimeout(function () { reg2.style.opacity = 1.0; }, 600);
                i = 2;
            }
            else if (i == 2)
            {
                setTimeout(function () { reg2.style.display = 'none'; }, 500);
                reg2.style.opacity = 0.0;
                setTimeout(function () { man.style.display = 'flex'; }, 500);
                setTimeout(function () { man.style.opacity = 1.0; }, 600);
                i = 0;
            }
        }
    </script>
</head>
<body style="padding: 8px; line-height: normal; background: linear-gradient(to bottom, rgba(191,191,191,1) 0%,rgba(232,232,232,1) 13%,rgba(244,244,244,1) 100%) no-repeat;">
    <table style="width: 100%;">
        <tr>
            <td style="min-width: 10px; width: 10px;"></td>

            <td align="center" style="padding: 0;">
                <div style="width: 260px; height: 450px; min-width: 260px; margin: 0; font-family: Raleway; border: 1px solid #80808045; box-shadow: 5px 5px 5px #b8b8b8; background-color: white; box-sizing: content-box;">
                    <table cellpadding="0" style="width: 260px; height: 450px; border-collapse: separate; border-spacing: 2px;">
                        <thead>
                            <tr>
                                <td style="width: 100%; height: 72px; vertical-align: middle; text-align: center; border: 2px solid black; background: linear-gradient(to bottom, #242e3a 0%,#061a33 2%,#001e44 4%,#05224e 6%,#061c45 7%,#0a1c40 9%,#05224a 13%,#082044 15%,#09254d 28%,#0d2450 30%,#112f63 52%,#102f65 57%,#0b2c61 59%,#0b2c61 100%);">
                                    <h2 style="font-family: Raleway; font-size: 1.5rem; font-weight: bold; color: white; text-shadow: 2px 2px black; text-align: center; display: inline-block; margin-bottom: 0;">LINKS</h2>
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td style="text-align: center; border: 2px solid black;" onmouseover="this.style.backgroundColor = '#061c45'; this.children[0].style.color = '#fff'; this.children[0].style.textShadow = '2px 2px black'" onmouseout="this.style.backgroundColor = ''; this.children[0].style.color = '#181727'; this.children[0].style.textShadow = ''" >
                                    <a href="Content/helloworld.pdf" style="width: 100%; text-decoration: none; display: block; padding: 9px 0; font-family: Raleway; color: rgb(24, 23, 39); font-weight: bold; font-size: 18pt;">Employee Handbook</a>
                                </td>
                            </tr>

                            <tr>
                                <td style="text-align: center; vertical-align: middle; border: 2px solid black;" onmouseover="this.style.backgroundColor = '#061c45'; this.children[0].style.color = '#fff'; this.children[0].style.textShadow = '2px 2px black'" onmouseout="this.style.backgroundColor = ''; this.children[0].style.color = '#181727'; this.children[0].style.textShadow = ''" >
                                    <a href="https://tcg4.hostedaccess.com/DeltekTC/welcome.msv" target="_parent" style="width: 100%; text-decoration: none; display: block; padding: 9px 0; font-family: Raleway; color: rgb(24, 23, 39); font-weight: bold; font-size: 18pt;">Timesheets</a>
                                </td>
                            </tr>

                            <tr>
                                <td style="text-align: center; vertical-align: middle; border: 2px solid black;" onmouseover="this.style.backgroundColor = '#061c45'; this.children[0].style.color = '#fff'; this.children[0].style.textShadow = '2px 2px black'" onmouseout="this.style.backgroundColor = ''; this.children[0].style.color = '#181727'; this.children[0].style.textShadow = ''" >
                                    <a href="#" style="width: 100%; text-decoration: none; display: block; padding: 9px 0; font-family: Raleway; color: rgb(24, 23, 39); font-weight: bold; font-size: 18pt;">Cyber Recruiter</a>
                                </td>
                            </tr>

                            <tr>
                                <td style="text-align: center; vertical-align: middle; border: 2px solid black;" onmouseover="this.style.backgroundColor = '#061c45'; this.children[0].style.color = '#fff'; this.children[0].style.textShadow = '2px 2px black'" onmouseout="this.style.backgroundColor = ''; this.children[0].style.color = '#181727'; this.children[0].style.textShadow = ''" >
                                    <a href="#" style="width: 100%; text-decoration: none; display: block; padding: 9px 0; font-family: Raleway; color: rgb(24, 23, 39); font-weight: bold; font-size: 18pt;">Human Resources</a>
                                </td>
                            </tr>

                            <tr>
                                <td style="text-align: center; vertical-align: middle; border: 2px solid black;" onmouseover="this.style.backgroundColor = '#061c45'; this.children[0].style.color = '#fff'; this.children[0].style.textShadow = '2px 2px black'" onmouseout="this.style.backgroundColor = ''; this.children[0].style.color = '#181727'; this.children[0].style.textShadow = ''" >
                                    <a href="javascript:void(0);" style="width: 100%; text-decoration: none; display: block; padding: 9px 0; font-family: Raleway; color: rgb(24, 23, 39); font-weight: bold; font-size: 18pt;" >Training</a>
                                </td>
                            </tr>

                            <tr>
                                <td style="text-align: center; vertical-align: middle; border: 2px solid black;" onmouseover="this.style.backgroundColor = '#061c45'; this.children[0].style.color = '#fff'; this.children[0].style.textShadow = '2px 2px black'" onmouseout="this.style.backgroundColor = ''; this.children[0].style.color = '#181727'; this.children[0].style.textShadow = ''" >
                                    <a href="https://www.iso.org/home.html" target="_parent" style="width: 100%; text-decoration: none; display: block; padding: 9px 0; font-family: Raleway; color: rgb(24, 23, 39); font-weight: bold; font-size: 18pt;">ISO</a>
                                </td>
                            </tr>

                            <tr>
                                <td style="text-align: center; vertical-align: middle; border: 2px solid black;" onmouseover="this.style.backgroundColor = '#061c45'; this.children[0].style.color = '#fff'; this.children[0].style.textShadow = '2px 2px black'" onmouseout="this.style.backgroundColor = ''; this.children[0].style.color = '#181727'; this.children[0].style.textShadow = ''" >
                                    <a href="https://cmmiinstitute.com/" target="_parent" style="width: 100%; text-decoration: none; display: block; padding: 9px 0; font-family: Raleway; color: rgb(24, 23, 39); font-weight: bold; font-size: 18pt;">CMMI</a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </td>

            <td style="min-width: 10px; width: 10px;"></td>

            <td style="width: 77%;">
                <div style="width: 100%; height: 450px; min-width: 624px; border: 1px solid #80808045; box-shadow: 5px 5px 5px #b8b8b8; background-color: white; font-family: Raleway; text-align: left;">
                    <input id="button1" class="btn-default btn-sm" type="button" style="background-color: #0d0435; color: white; height: 40px; width: 100px; float: right; margin-top: 5px; margin-right: 20px; font-weight: 500; border: none;" value="More &gt;" onclick="training(this)" />
                    <img src="Images/training.png" style="margin-left: 10px; margin-top: 5px; float: left;" /><h1 style="margin-left: 10px; display: inline-block; margin-top: 10px; font-weight: 500; line-height: 1.2; font-size: 30px;">Mandatory Training</h1><br />
                    <div id="mandatory" style="display: flex; justify-content: space-around; margin: 80px 20px; opacity: 1; transition: all 0.5s;">
                        <div style="display: inline-block; zoom: 1;  text-align: center;">
                            <div>
                                <div style="height: 180px; width: 200px; border: 10px solid #fff; box-shadow: 0px 0px 5px #b8b8b8; overflow: hidden;">
                                    <a href="https://www.osha.gov/dte/grant_materials/fy09/sh-18796-09/fireprotection.pdf" target="_self">
                                        <img src="Images/fire-safety.jpg" style="height: 100%;" onmouseover="this.style.filter = 'blur(4px)'; this.style.WebkitFilter = 'blur(4px)'; this.style.webkitTransition = 'filter 1s'; this.style.transition = 'filter 1s';" onmouseout="this.style.filter = 'blur(0)'; this.style.WebkitFilter = 'blur(0)'; this.style.webkitTransition = 'filter 1s'; this.style.transition = 'filter 1s';" />
                                    </a>
                                </div>
                                <a href="https://www.osha.gov/dte/grant_materials/fy09/sh-18796-09/fireprotection.pdf" target="_self" style="text-decoration: none; color: black;"><h4>Fire Safety</h4></a>
                            </div>
                        </div>
                        <div style="display: inline-block; zoom: 1; text-align: center;">
                            <div>
                                <div style="height: 180px; width: 200px; border: 10px solid #fff; box-shadow: 0px 0px 5px #b8b8b8; overflow: hidden;">
                                    <a href="https://www.dhs.gov/xlibrary/assets/active_shooter_booklet.pdf" target="_self">
                                        <img src="Images/activeshooter.png" style="height: 100%;" onmouseover="this.style.filter = 'blur(4px)'; this.style.WebkitFilter = 'blur(4px)'; this.style.webkitTransition = 'filter 1s'; this.style.transition = 'filter 1s';" onmouseout="this.style.filter = 'blur(0)'; this.style.WebkitFilter = 'blur(0)'; this.style.webkitTransition = 'filter 1s'; this.style.transition = 'filter 1s';" />
                                    </a>
                                </div>
                                <a href="https://www.dhs.gov/xlibrary/assets/active_shooter_booklet.pdf" target="_self" style="text-decoration: none; color: black;"><h4>Active Shooter</h4></a>
                            </div>
                        </div>
                        <div style="display: inline-block; zoom: 1; text-align: center;">
                            <div>
                                <div style="height: 180px; width: 200px; border: 10px solid #fff; box-shadow: 0px 0px 5px #b8b8b8; overflow: hidden;">
                                    <a href="https://www.iso.org/files/live/sites/isoorg/files/archive/pdf/en/pub100304.pdf" target="_self">
                                        <img src="Images/ISO.png" style="height: 100%;" onmouseover="this.style.filter = 'blur(4px)'; this.style.WebkitFilter = 'blur(4px)'; this.style.webkitTransition = 'filter 1s'; this.style.transition = 'filter 1s';" onmouseout="this.style.filter = 'blur(0)'; this.style.WebkitFilter = 'blur(0)'; this.style.webkitTransition = 'filter 1s'; this.style.transition = 'filter 1s';" />
                                    </a>
                                </div>
                                <a href="https://www.iso.org/files/live/sites/isoorg/files/archive/pdf/en/pub100304.pdf" target="_self" style="text-decoration: none; color: black;"><h4>ISO Training</h4></a>
                            </div>
                        </div>
                    </div>

                    <div id="regular" style="display: none; justify-content: space-around; margin: 80px 20px; opacity: 0; transition: all 0.5s;">
                        <div style="display: inline-block; zoom: 1; text-align: center;">
                            <div>
                                <div style="height: 180px; width: 200px; border: 10px solid #fff; box-shadow: 0px 0px 5px #b8b8b8; overflow: hidden;">
                                    <a href="https://elsmar.com/pdf_files/cmmi-overview05.pdf" target="_self">
                                        <img src="Images/cmmi.png" style="height: 100%;" onmouseover="this.style.filter = 'blur(4px)'; this.style.WebkitFilter = 'blur(4px)'; this.style.webkitTransition = 'filter 1s'; this.style.transition = 'filter 1s';" onmouseout="this.style.filter = 'blur(0)'; this.style.WebkitFilter = 'blur(0)'; this.style.webkitTransition = 'filter 1s'; this.style.transition = 'filter 1s';" />
                                    </a>
                                </div>
                                <a href="https://elsmar.com/pdf_files/cmmi-overview05.pdf" target="_self" style="text-decoration: none; color: black;"><h4>CMMI</h4></a>
                            </div>
                        </div>
                        <div style="display: inline-block; zoom: 1; text-align: center;">
                            <div>
                                <div style="height: 180px; width: 200px; border: 10px solid #fff; box-shadow: 0px 0px 5px #b8b8b8; overflow: hidden;">
                                    <a href="/Documents/training/timekeeping.pdf" target="_self">
                                        <img src="Images/time.png" style="height: 100%;" onmouseover="this.style.filter = 'blur(4px)'; this.style.WebkitFilter = 'blur(4px)'; this.style.webkitTransition = 'filter 1s'; this.style.transition = 'filter 1s';" onmouseout="this.style.filter = 'blur(0)'; this.style.WebkitFilter = 'blur(0)'; this.style.webkitTransition = 'filter 1s'; this.style.transition = 'filter 1s';" />
                                    </a>
                                </div>
                                <a href="/Documents/training/timekeeping.pdf" target="_self" style="text-decoration: none; color: black;"><h4>Timekeeping</h4></a>
                            </div>
                        </div>
                        <div style="display: inline-block; zoom: 1; text-align: center;">
                            <div>
                                <div style="height: 180px; width: 200px; border: 10px solid #fff; box-shadow: 0px 0px 5px #b8b8b8; overflow: hidden;">
                                    <a href="/Documents/training/codeofethics.pdf" target="_self">
                                        <img src="Images/ethics.png" style="height: 100%;" onmouseover="this.style.filter = 'blur(4px)'; this.style.WebkitFilter = 'blur(4px)'; this.style.webkitTransition = 'filter 1s'; this.style.transition = 'filter 1s';" onmouseout="this.style.filter = 'blur(0)'; this.style.WebkitFilter = 'blur(0)'; this.style.webkitTransition = 'filter 1s'; this.style.transition = 'filter 1s';" />
                                    </a>
                                </div>
                                <a href="/Documents/training/codeofethics.pdf" target="_self" style="text-decoration: none; color: black;"><h4>Code of Ethics</h4></a>
                            </div>
                        </div>
                    </div>

                    <div id="regular2" style="display: none; justify-content: space-around; margin: 80px 20px; opacity: 0; transition: all 0.5s;">
                        <div style="display: inline-block; zoom: 1; text-align: center;">
                            <div>
                                <div style="height: 180px; width: 200px; border: 10px solid #fff; box-shadow: 0px 0px 5px #b8b8b8; overflow: hidden;">
                                    <a href="https://dbb.defense.gov/Portals/35/Documents/Misc_Documents/Travel_Info/Post%20Foreign%20Travel%20Debriefing%20Form.pdf?ver=2018-05-10-094114-993" target="_self">
                                        <img src="Images/travel.png" style="height: 100%;" onmouseover="this.style.filter = 'blur(4px)'; this.style.WebkitFilter = 'blur(4px)'; this.style.webkitTransition = 'filter 1s'; this.style.transition = 'filter 1s';" onmouseout="this.style.filter = 'blur(0)'; this.style.WebkitFilter = 'blur(0)'; this.style.webkitTransition = 'filter 1s'; this.style.transition = 'filter 1s';" />
                                    </a>
                                </div>
                                <a href="https://dbb.defense.gov/Portals/35/Documents/Misc_Documents/Travel_Info/Post%20Foreign%20Travel%20Debriefing%20Form.pdf?ver=2018-05-10-094114-993" target="_self" style="text-decoration: none; color: black;"><h4>Foreign Travel<br />Debrief</h4></a>
                            </div>
                        </div>
                        <div style="display: inline-block; zoom: 1; text-align: center;">
                            <div>
                                <div style="height: 180px; width: 200px; border: 10px solid #fff; box-shadow: 0px 0px 5px #b8b8b8; overflow: hidden;">
                                    <a href="https://www.cdse.edu/documents/cdse/Receive_and_Maint_Sct_Clnc.pdf" target="_self">
                                        <img src="Images/clearance.png" style="height: 100%;" onmouseover="this.style.filter = 'blur(4px)'; this.style.WebkitFilter = 'blur(4px)'; this.style.webkitTransition = 'filter 1s'; this.style.transition = 'filter 1s';" onmouseout="this.style.filter = 'blur(0)'; this.style.WebkitFilter = 'blur(0)'; this.style.webkitTransition = 'filter 1s'; this.style.transition = 'filter 1s';" />
                                    </a>
                                </div>
                                <a href="https://www.cdse.edu/documents/cdse/Receive_and_Maint_Sct_Clnc.pdf" target="_self" style="text-decoration: none; color: black;"><h4>Security<br />Clearance<br />Refresher</h4></a>
                            </div>
                        </div>
                    </div>
                </div>  
            </td>

            <td style="min-width: 10px; width: 10px;"></td>
        </tr>
    </table>
    <br />
    <table style="width: 100%">
        <tr>
            <td style="min-width: 10px; width: 10px;"></td>

            <td style="width: 50%;">
                <div class="contentBox"  style="width: 100%; height: 450px;">

                </div>
            </td>

            <td style="min-width: 10px; width: 10px;"></td>

            <td style="width: 50%;">
                <div class="contentBox" style="width: 100%; height: 450px;">

                </div>
            </td>

            <td style="min-width: 10px; width: 10px;"></td>
        </tr>
    </table>
</body>
</html>
