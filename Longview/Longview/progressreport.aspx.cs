﻿using Microsoft.AspNet.Identity;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Longview
{
    public partial class ProgressReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string qs = Request.QueryString["project"];
                if (String.IsNullOrEmpty(qs))
                {
                    Response.StatusCode = 404;
                    Response.End();
                    return;
                }

                int ID;
                if (!int.TryParse(qs, out ID))
                {
                    Response.StatusCode = 404;
                    Response.End();
                    return;
                }

                if(ID == 0)
                {
                    Response.StatusCode = 404;
                    Response.End();
                    return;
                }

                if (!IsManager() && !User.IsInRole("Administrators"))
                {
                    Response.StatusCode = 403;
                    Response.End();
                    return;
                }
                             
                DisplayData(ID);

                if (Page.User.Identity.IsAuthenticated)
                {
                    string fullName = (string)HttpContext.Current.Profile.GetPropertyValue("firstname") == "" ? Page.User.Identity.GetUserName() : (string)HttpContext.Current.Profile.GetPropertyValue("firstname");
                    Username.Text = "Hello, " + fullName + "&nbsp;&nbsp;&nbsp;";
                }
            }
        }

        protected void DisplayData(int s)
        {
            string name = ProjectsManager.GetRoleNameById(s);
            if (name == null)
            {
                Response.StatusCode = 404;
                Response.End();
                return;
            }

            Project proj = ProjectsManager.GetRoleByName(name);
            if (proj == null)
            {
                Response.StatusCode = 404;
                Response.End();
                return;
            }

            pName.Text = proj.getName();
            pManager.Text = proj.getContactName();
            pDate.Text = proj.getLastUpdated().ToString("yyyy-MM-dd");
            pAuthor.Text = proj.getAuthor();
            pSummary.Text = proj.getSummary();
            pBudget.Text = String.Format("{0}", proj.getBudget());
            pCost.Text = String.Format("{0:C}", proj.getCost());
            pProfit.Text = String.Format("{0:C}", proj.getProfit());
            pCompletion.Text = proj.getCompleteDate().ToString("yyyy-MM-dd");
            pProjectScope.Text = proj.getProjectScope();
            pProductScope.Text = proj.getProductScope();
            pDetails.Text = proj.getDetails();
            pConclusion.Text = proj.getConclusion();
            pProjectPercentage.Text = String.Format("{0}", proj.getProjectProgress());
            pProjectStart.Text = proj.getStartDate().ToLongDateString();

            ViewState["financial"] = proj.getFinancial();

            SetupTasks(proj);
            SetupMilestones(proj);
            SetupIssues(proj);
            SetupRisks(proj);
            SetupEmployees(proj);

            Session["data"] = new List<MySqlCommand>();
        }

        protected void SetupEmployees(Project proj)
        {
            DataTable dt = new DataTable();
            if (dt.Columns.Count == 0)
            {
                dt.Columns.Add("Name", typeof(string));
                dt.Columns.Add("Position", typeof(string));
                dt.Columns.Add("Type", typeof(string));
                dt.Columns.Add("Weekly Hours", typeof(string));
            }

            DataRow NewRow;
            foreach (KeyValuePair<string, Employee> kvp in proj.getEmployees())
            {
                NewRow = dt.NewRow();
                NewRow[0] = kvp.Key;
                NewRow[1] = kvp.Value.position;
                NewRow[2] = kvp.Value.weeklyhours < 40 ? "Part-time" : "Full-time";
                NewRow[3] = kvp.Value.weeklyhours;
                dt.Rows.Add(NewRow);
            }

            ViewState["employees"] = dt;
            pEmployeesList.DataSource = dt;
            pEmployeesList.DataBind();
        }

        protected void SetupIssues(Project proj)
        {
            DataTable dt = new DataTable();
            if (dt.Columns.Count == 0)
            {
                dt.Columns.Add("Issue", typeof(string));
                dt.Columns.Add("Date Identified", typeof(string));
                dt.Columns.Add("Resolution", typeof(string));
            }

            DataRow NewRow;
            foreach (string[] str in proj.getIssues().Values)
            {
                NewRow = dt.NewRow();
                NewRow[0] = str[0];
                NewRow[1] = DateTime.Parse(str[1]).ToString("yyyy-MM-dd");
                NewRow[2] = str[2];
                dt.Rows.Add(NewRow);
            }

            ViewState["issues"] = dt;
            pIssues.DataSource = dt;
            pIssues.DataBind();
        }

        protected void SetupRisks(Project proj)
        {
            DataTable dt = new DataTable();
            if (dt.Columns.Count == 0)
            {
                dt.Columns.Add("Risk", typeof(string));
                dt.Columns.Add("Date Identified", typeof(string));
                dt.Columns.Add("Risk Level", typeof(string));
                dt.Columns.Add("Owner", typeof(string));
                dt.Columns.Add("Response", typeof(string));
            }

            DataRow NewRow;
            foreach (string[] str in proj.getRisks().Values)
            {
                NewRow = dt.NewRow();
                NewRow[0] = str[0];
                NewRow[1] = DateTime.Parse(str[1]).ToString("yyyy-MM-dd");
                NewRow[2] = str[2];
                NewRow[3] = str[3];
                NewRow[4] = str[4];
                dt.Rows.Add(NewRow);
            }

            ViewState["risks"] = dt;
            pRisks.DataSource = dt;
            pRisks.DataBind();
        }

        protected void SetupMilestones(Project proj)
        {
            DataTable dt = new DataTable();
            if (dt.Columns.Count == 0)
            {
                dt.Columns.Add("Milestone", typeof(string));
                dt.Columns.Add("Completed", typeof(string));
                dt.Columns.Add("Start Date", typeof(string));
                dt.Columns.Add("Complete Date", typeof(string));
            }

            DataRow NewRow;
            foreach (string[] str in proj.getMilestones().Values)
            {
                NewRow = dt.NewRow();
                NewRow[0] = str[0];
                NewRow[1] = str[1];
                NewRow[2] = DateTime.Parse(str[2]).ToString("yyyy-MM-dd");
                NewRow[3] = DateTime.Parse(str[3]).ToString("yyyy-MM-dd"); ;
                dt.Rows.Add(NewRow);
            }

            ViewState["milestones"] = dt;
            pMilestones.DataSource = dt;
            pMilestones.DataBind();
        }

        protected void SetupTasks(Project proj)
        {
            DataTable dt = new DataTable();
            if (dt.Columns.Count == 0)
            {
                dt.Columns.Add("Task", typeof(string));
                dt.Columns.Add("Owner", typeof(string));
                dt.Columns.Add("Priority", typeof(string));
                dt.Columns.Add("Status", typeof(string));
            }

            DataRow NewRow;
            foreach (string[] str in proj.getActions().Values)
            {
                NewRow = dt.NewRow();
                NewRow[0] = str[0];
                NewRow[1] = str[1];
                NewRow[2] = str[2];
                NewRow[3] = str[3];
                dt.Rows.Add(NewRow);
            }

            ViewState["tasks"] = dt;
            pTasks.DataSource = dt;
            pTasks.DataBind();
        }

        protected void pMilestones_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            DataTable dt = ViewState["milestones"] as DataTable;
            if (e.CommandName == "Delete")
            {
                if(index < dt.Rows.Count)
                {
                    List<MySqlCommand> data = Session["data"] as List<MySqlCommand>;
                    MySqlCommand cmd = new MySqlCommand("DELETE FROM project_milestone WHERE project_id = @param1 AND milestone = @param2 AND percent_complete = @param3 AND startdate = @param4 AND completedate = @param5;");
                    cmd.Parameters.AddWithValue("@param1", GetCurrentProject().getId());
                    cmd.Parameters.AddWithValue("@param2", dt.Rows[index].ItemArray[0]);
                    cmd.Parameters.AddWithValue("@param3", dt.Rows[index].ItemArray[1]);
                    cmd.Parameters.AddWithValue("@param4", dt.Rows[index].ItemArray[2]);
                    cmd.Parameters.AddWithValue("@param5", dt.Rows[index].ItemArray[3]);
                    data.Add(cmd);
                    Session["data"] = data;

                    dt.Rows.RemoveAt(index);
                    pMilestones.DataSource = dt;
                    pMilestones.DataBind();
                }
            }

            if (e.CommandName == "Edit")
            {
                TextBox tb = pMilestones.Rows[index].FindControl("TextBox1") as TextBox;
                if (tb != null)
                {
                    tb.Text = pMilestones.Rows[index].Cells[1].Text.Replace("%", "");
                    tb.Visible = true;
                    Literal l = new Literal();
                    l.Text = "%";
                    pMilestones.Rows[index].Cells[2].Controls.Add(l);
                }

                LinkButton lb = ((LinkButton)pMilestones.Rows[index].Cells[5].Controls[0]);
                if (lb != null)
                {
                    lb.Text = "Update";
                    lb.CommandName = "Update";
                }
            }

            if (e.CommandName == "Update")
            {
                TextBox tb = pMilestones.Rows[index].FindControl("TextBox1") as TextBox;
                if (tb != null)
                {
                    dt.Rows[index].SetField(1, tb.Text);
                    pMilestones.DataSource = dt;
                    pMilestones.DataBind();
                    ViewState["milestones"] = dt;
                }
            }
        }

        protected void pMilestones_RowDeleting(object sender, GridViewDeleteEventArgs e) { }

        protected void pMilestones_RowEditing(object sender, GridViewEditEventArgs e)
        {
            e.Cancel = true;
        }

        protected void pMilestones_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            e.Cancel = true;
        }

        protected void pTasks_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            DataTable dt = ViewState["tasks"] as DataTable;
            if (e.CommandName == "Delete")
            {
                if(index < dt.Rows.Count)
                {
                    List<MySqlCommand> data = Session["data"] as List<MySqlCommand>;
                    MySqlCommand cmd = new MySqlCommand("DELETE FROM project_actions WHERE project_id = @param1 AND action = @param2 AND owner = @param3 AND status = @param4 AND priority = @param5;");
                    cmd.Parameters.AddWithValue("@param1", GetCurrentProject().getId());
                    cmd.Parameters.AddWithValue("@param2", dt.Rows[index].ItemArray[0]);
                    cmd.Parameters.AddWithValue("@param3", dt.Rows[index].ItemArray[1]);
                    cmd.Parameters.AddWithValue("@param4", dt.Rows[index].ItemArray[3]);
                    cmd.Parameters.AddWithValue("@param5", dt.Rows[index].ItemArray[2]);
                    data.Add(cmd);
                    Session["data"] = data;

                    dt.Rows.RemoveAt(index);
                    pTasks.DataSource = dt;
                    pTasks.DataBind();
                }
            }

            if(e.CommandName == "Edit")
            {
                DropDownList ddl = pTasks.Rows[index].FindControl("DropDownList1") as DropDownList;
                if(ddl != null)
                {
                    ddl.SelectedValue = pTasks.Rows[index].Cells[3].Text;
                    ddl.Visible = true;
                }

                LinkButton lb = ((LinkButton)pTasks.Rows[index].Cells[5].Controls[0]);
                if (lb != null)
                {
                    lb.Text = "Update";
                    lb.CommandName = "Update";
                }
            }

            if (e.CommandName == "Update")
            {
                DropDownList ddl = pTasks.Rows[index].FindControl("DropDownList1") as DropDownList;
                if (ddl != null)
                {
                    dt.Rows[index].SetField(3, ddl.SelectedValue);
                    pTasks.DataSource = dt;
                    pTasks.DataBind();
                    ViewState["tasks"] = dt;

                    List<MySqlCommand> data = Session["data"] as List<MySqlCommand>;
                    MySqlCommand cmd = new MySqlCommand("UPDATE project_actions SET status = @param1 WHERE project_id = @param2 AND " +
                        "action = @param3;");
                    cmd.Parameters.AddWithValue("@param1", dt.Rows[index].ItemArray[3]);
                    cmd.Parameters.AddWithValue("@param2", GetCurrentProject().getId());
                    cmd.Parameters.AddWithValue("@param3", dt.Rows[index].ItemArray[0]);
                    data.Add(cmd);
                    Session["data"] = data;
                }
            }
        }

        protected void pTasks_RowDeleting(object sender, GridViewDeleteEventArgs e) { }

        protected void pIssues_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            DataTable dt = ViewState["issues"] as DataTable;
            if (e.CommandName == "Delete")
            {
                if(index < dt.Rows.Count)
                {
                    List<MySqlCommand> data = Session["data"] as List<MySqlCommand>;
                    MySqlCommand cmd = new MySqlCommand("DELETE FROM project_issue WHERE project_id = @param1 AND issue = @param2 AND date_identified = @param3 AND resolution = @param4;");
                    cmd.Parameters.AddWithValue("@param1", GetCurrentProject().getId());
                    cmd.Parameters.AddWithValue("@param2", dt.Rows[index].ItemArray[0]);
                    cmd.Parameters.AddWithValue("@param3", dt.Rows[index].ItemArray[1]);
                    cmd.Parameters.AddWithValue("@param4", dt.Rows[index].ItemArray[2]);
                    data.Add(cmd);
                    Session["data"] = data;

                    dt.Rows.RemoveAt(index);
                    pIssues.DataSource = dt;
                    pIssues.DataBind();
                }
            }
        }

        protected void pIssues_RowDeleting(object sender, GridViewDeleteEventArgs e) { }

        protected void pRisks_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            DataTable dt = ViewState["risks"] as DataTable;
            if (e.CommandName == "Delete")
            {
                if(index < dt.Rows.Count)
                {
                    List<MySqlCommand> data = Session["data"] as List<MySqlCommand>;
                    MySqlCommand cmd = new MySqlCommand("DELETE FROM project_risk WHERE project_id = @param1 AND risk = @param2 AND date_identified = @param3 AND risk_level = @param4 AND owner = @param5 AND response = @param6;");
                    cmd.Parameters.AddWithValue("@param1", GetCurrentProject().getId());
                    cmd.Parameters.AddWithValue("@param2", dt.Rows[index].ItemArray[0]);
                    cmd.Parameters.AddWithValue("@param3", dt.Rows[index].ItemArray[1]);
                    cmd.Parameters.AddWithValue("@param4", dt.Rows[index].ItemArray[2]);
                    cmd.Parameters.AddWithValue("@param5", dt.Rows[index].ItemArray[3]);
                    cmd.Parameters.AddWithValue("@param6", dt.Rows[index].ItemArray[4]);
                    data.Add(cmd);
                    Session["data"] = data;

                    dt.Rows.RemoveAt(index);
                    pRisks.DataSource = dt;
                    pRisks.DataBind();
                }
            }
        }

        protected void pRisks_RowDeleting(object sender, GridViewDeleteEventArgs e) { }

		protected void addTask_Click(object sender, EventArgs e)
		{
            if (pTask.Text == "")
            {
                pTaskError.InnerText = "Task cannot be blank.";
                return;
            }

            if (pOwner.Text == "")
            {
                pTaskError.InnerText = "Owner cannot be blank.";
                return;
            }

            List<MySqlCommand> data = Session["data"] as List<MySqlCommand>;
            DataTable dt = ViewState["tasks"] as DataTable;
			DataRow dr = dt.NewRow();
			dr[0] = pTask.Text;
			dr[1] = pOwner.Text;
			dr[2] = pPriority.Text;
			dr[3] = pStatus.Text;

            if (dt.AsEnumerable().Where(x => x.ItemArray.SequenceEqual(dr.ItemArray)).Count() > 0)
            {
                pTaskError.InnerText = "Cannot create duplicate tasks.";
                return;
            }

			dt.Rows.Add(dr);
            MySqlCommand cmd = new MySqlCommand("INSERT INTO project_actions (project_id, action, owner, status, priority) VALUES (@param1, @param2, @param3, @param4, @param5);");
            cmd.Parameters.AddWithValue("@param1", GetCurrentProject().getId());
            cmd.Parameters.AddWithValue("@param2", pTask.Text);
            cmd.Parameters.AddWithValue("@param3", pOwner.Text);
            cmd.Parameters.AddWithValue("@param4", pStatus.Text);
            cmd.Parameters.AddWithValue("@param5", pPriority.Text);
            data.Add(cmd);
            Session["data"] = data;
            pTasks.DataSource = dt;
			pTasks.DataBind();

			pTask.Text = "";
			pOwner.Text = "";
            pTaskError.InnerText = "The task has been added successfully. You must press the 'Submit' button at the bottom of the page to commit the changes.";
		}

		protected void addMilestone_Click(object sender, EventArgs e)
		{
            if (pMilestone.Text == "")
            {
                pMilestoneError.InnerText = "Milestone cannot be blank.";
                return;
            }

            if(pPercentComplete.Text == "")
            {
                pMilestoneError.InnerText = "The field labeled \"Completed\" cannot be blank.";
                return;
            }

            if (pStartDate.Text == "")
            {
                pMilestoneError.InnerText = "Start Date cannot be blank.";
                return;
            }

            if (pEndDate.Text == "")
            {
                pMilestoneError.InnerText = "End Date cannot be blank.";
                return;
            }

			DateTime start;
            if (!DateTime.TryParse(pStartDate.Text, out start))
            {
                pMilestoneError.InnerText = "Invalid Start Date.";
                return;
            }

			DateTime end;
            if (!DateTime.TryParse(pEndDate.Text, out end))
            {
                pMilestoneError.InnerText = "Invalid End Date.";
                return;
            }

			if (end.CompareTo(start) < 0)
            {
                pMilestoneError.InnerText = "Start Date must be earlier than End Date.";
                return;
			}

            List<MySqlCommand> data = Session["data"] as List<MySqlCommand>;
            DataTable dt = ViewState["milestones"] as DataTable;
			DataRow dr = dt.NewRow();
			dr[0] = pMilestone.Text;
            dr[1] = pPercentComplete.Text;
			dr[2] = start.ToString("yyyy-MM-dd");
			dr[3] = end.ToString("yyyy-MM-dd");

            if (dt.AsEnumerable().Where(x => x.ItemArray.SequenceEqual(dr.ItemArray)).Count() > 0)
            {
                pMilestoneError.InnerText = "Cannot create duplicate milestones.";
                return;
            }

            dt.Rows.Add(dr);
            MySqlCommand cmd = new MySqlCommand("INSERT INTO project_milestone (project_id, milestone, percent_complete, startdate, completedate) VALUES (@param1, @param2, @param3, @param4, @param5);");
            cmd.Parameters.AddWithValue("@param1", GetCurrentProject().getId());
            cmd.Parameters.AddWithValue("@param2", pMilestone.Text);
            cmd.Parameters.AddWithValue("@param3", pPercentComplete.Text);
            cmd.Parameters.AddWithValue("@param4", start.ToString("yyyy-MM-dd"));
            cmd.Parameters.AddWithValue("@param5", end.ToString("yyyy-MM-dd"));
            data.Add(cmd);
            Session["data"] = data;
            pMilestones.DataSource = dt;
			pMilestones.DataBind();

			pMilestone.Text = "";
            pMilestoneError.InnerText = "The milestone has been added successfully. You must press the 'Submit' button at the bottom of the page to commit the changes.";
		}

		protected void addIssue_Click(object sender, EventArgs e)
		{
            if (pIssue.Text == "")
            {
                pIssueError.InnerText = "Issue cannot be blank.";
                return;
            }

            if (pResolution.Text == "")
            {
                pIssueError.InnerText = "Resolution cannot be blank.";
                return;
            }

			DateTime dateIdentified;
            if (!DateTime.TryParse(pDateId.Text, out dateIdentified))
            {
                pIssueError.InnerText = "Date Identified field is invalid.";
                return;
            }

            List<MySqlCommand> data = Session["data"] as List<MySqlCommand>;
            DataTable dt = ViewState["issues"] as DataTable;
			DataRow dr = dt.NewRow();
			dr[0] = pIssue.Text;
			dr[1] = dateIdentified.ToString("yyyy-MM-dd");
			dr[2] = pResolution.Text;

            if (dt.AsEnumerable().Where(x => x.ItemArray.SequenceEqual(dr.ItemArray)).Count() > 0)
            {
                pIssueError.InnerText = "Cannot create duplicate issues.";
                return;
            }

            dt.Rows.Add(dr);
            MySqlCommand cmd = new MySqlCommand("INSERT INTO project_issue (project_id, issue, date_identified, resolution) VALUES (@param1, @param2, @param3, @param4);");
            cmd.Parameters.AddWithValue("@param1", GetCurrentProject().getId());
            cmd.Parameters.AddWithValue("@param2", pIssue.Text);
            cmd.Parameters.AddWithValue("@param3", dateIdentified.ToString("yyyy-MM-dd"));
            cmd.Parameters.AddWithValue("@param4", pResolution.Text);
            data.Add(cmd);
            Session["data"] = data;
            pIssues.DataSource = dt;
			pIssues.DataBind();

			pIssue.Text = "";
			pResolution.Text = "";
            pIssueError.InnerText = "The issue has been added successfully. You must press the 'Submit' button at the bottom of the page to commit the changes.";
		}

		protected void addRisk_Click(object sender, EventArgs e)
		{
            if (pRisk.Text == "")
            {
                pRiskError.InnerText = "Risk cannot be blank.";
                return;
            }

            if (pRiskOwner.Text == "")
            {
                pRiskError.InnerText = "Owner cannot be blank.";
                return;
            }

            if (pRiskMitig.Text == "")
            {
                pRiskError.InnerText = "Response cannot be blank.";
                return;
            }

			DateTime riskDate;
            if (!DateTime.TryParse(pRiskDateId.Text, out riskDate))
            {
                pRiskError.InnerText = "Date Identified field is invalid.";
                return;
            }

            List<MySqlCommand> data = Session["data"] as List<MySqlCommand>;
            DataTable dt = ViewState["risks"] as DataTable;
			DataRow dr = dt.NewRow();
			dr[0] = pRisk.Text;
			dr[1] = riskDate.ToString("yyyy-MM-dd");
			dr[2] = pRiskLevel.Text;
			dr[3] = pRiskOwner.Text;
			dr[4] = pRiskMitig.Text;

            if (dt.AsEnumerable().Where(x => x.ItemArray.SequenceEqual(dr.ItemArray)).Count() > 0)
            {
                pRiskError.InnerText = "Cannot create duplicate risks.";
                return;
            }

            dt.Rows.Add(dr);
            MySqlCommand cmd = new MySqlCommand("INSERT INTO project_risk (project_id, risk, date_identified, risk_level, owner, response) VALUES (@param1, @param2, @param3, @param4, @param5, @param6);");
            cmd.Parameters.AddWithValue("@param1", GetCurrentProject().getId());
            cmd.Parameters.AddWithValue("@param2", pRisk.Text);
            cmd.Parameters.AddWithValue("@param3", riskDate.ToString("yyyy-MM-dd"));
            cmd.Parameters.AddWithValue("@param4", pRiskLevel.Text);
            cmd.Parameters.AddWithValue("@param5", pRiskOwner.Text);
            cmd.Parameters.AddWithValue("@param6", pRiskMitig.Text);
            data.Add(cmd);
            Session["data"] = data;
            pRisks.DataSource = dt;
			pRisks.DataBind();

			pRisk.Text = "";
			pRiskOwner.Text = "";
			pRiskMitig.Text = "";
            pRiskError.InnerText = "The risk has been added successfully. You must press the 'Submit' button at the bottom of the page to commit the changes.";

        }

        protected void addEmployee_Click(object sender, EventArgs e)
        {
            if (pEmployeeName.Text == "")
            {
                pEmployeeError.InnerText = "Name cannot be blank.";
                return;
            }

            if (pEmployeePosition.Text == "")
            {
                pEmployeeError.InnerText = "Position cannot be blank.";
                return;
            }

            if (pEmployeeHours.Text == "")
            {
                pEmployeeError.InnerText = "Hours cannot be blank.";
                return;
            }

            decimal hours = 0;
            if(!decimal.TryParse(pEmployeeHours.Text, out hours))
            {
                pEmployeeError.InnerText = "Hours field is invalid.";
                return;
            }

            List<MySqlCommand> data = Session["data"] as List<MySqlCommand>;
            DataTable dt = ViewState["employees"] as DataTable;
            DataRow dr = dt.NewRow();
            dr[0] = pEmployeeName.Text;
            dr[1] = pEmployeePosition.Text;
            dr[2] = hours < 40 ? "Part-time" : "Full-time";
            dr[3] = pEmployeeHours.Text;

            if (dt.AsEnumerable().Where(x => x.ItemArray.SequenceEqual(dr.ItemArray)).Count() > 0)
            {
                pEmployeeError.InnerText = "Cannot create duplicate employees.";
                return;
            }

            dt.Rows.Add(dr);
            MySqlCommand cmd = new MySqlCommand("INSERT INTO project_employee (project_id, employee, position, hours) VALUES (@param1, @param2, @param3, @param4);");
            cmd.Parameters.AddWithValue("@param1", GetCurrentProject().getId());
            cmd.Parameters.AddWithValue("@param2", pEmployeeName.Text);
            cmd.Parameters.AddWithValue("@param3", pEmployeePosition.Text);
            cmd.Parameters.AddWithValue("@param4", pEmployeeHours.Text);
            data.Add(cmd);
            Session["data"] = data;
            pEmployeesList.DataSource = dt;
            pEmployeesList.DataBind();

            pEmployeeName.Text = "";
            pEmployeePosition.Text = "";
            pEmployeeHours.Text = "0";
            pEmployeeError.InnerText = "The employee has been added successfully. You must press the 'Submit' button at the bottom of the page to commit the changes.";
        }

        protected void pSubmitButton_Click(object sender, EventArgs e)
        {
            Project proj = GetCurrentProject();
            if (proj == null)
            {
                Response.StatusCode = 404;
                Response.End();
                return;
            }

            if (!User.IsInRole("Manager") && !User.IsInRole("Administrators"))
            {
                Response.StatusCode = 403;
                Response.End();
                return;
            }

            if(!SaveFinancial())
            {
                errortext.InnerText = "Financial data could not be saved. One or more fields is invalid.";
                return;
            }

            string fullName = HttpContext.Current.Profile.GetPropertyValue("firstname") + " " +
                HttpContext.Current.Profile.GetPropertyValue("lastname");

            PrepareData();
            List<MySqlCommand> data = Session["data"] as List<MySqlCommand>;
            proj.UpdateInfo(pSummary.Text,
                pProjectScope.Text,
                pProductScope.Text,
                pDetails.Text,
                pConclusion.Text,
                DateTime.Parse(pProjectStart.Text),
                DateTime.Parse(pCompletion.Text),
                DateTime.Now,
                fullName,
                decimal.Parse(pBudget.Text),
                int.Parse(pProjectPercentage.Text),
                getDictionary(ViewState["tasks"] as DataTable),
                getDictionary(ViewState["milestones"] as DataTable),
                getDictionary(ViewState["issues"] as DataTable),
                getDictionary(ViewState["risks"] as DataTable),
                ViewState["financial"] as Dictionary<string, decimal[]>,
                getEmployeeDictionary(ViewState["employees"] as DataTable),
                data);
            Response.Redirect("progress?project=" + proj.getId());
        }

        protected void pCancelButton_Click(object sender, EventArgs e)
        {
            //ClientScript.RegisterStartupScript(this.GetType(), "scriptid", "window.parent.location.href='dashboard.aspx';", true);
            Response.Redirect("dashboard.aspx", false);
        }

        protected Dictionary<int, string[]> getDictionary(DataTable dt)
        {
            if (dt == null)
                return null;

            Dictionary<int, string[]> dict = new Dictionary<int, string[]>();
            string[] rowStr;
            int count = 0;
            foreach(DataRow dr in dt.Rows)
            {
                rowStr = dr.ItemArray.Cast<string>().ToArray();
                dict.Add(count, rowStr);
                ++count;
            }

            return dict;
        }

        protected Dictionary<string, Employee> getEmployeeDictionary(DataTable dt)
        {
            if (dt == null)
                return null;

            Dictionary<string, Employee> dict = new Dictionary<string, Employee>();
            string[] rowStr;
            foreach (DataRow dr in dt.Rows)
            {
                rowStr = dr.ItemArray.Cast<string>().ToArray();
                Employee e = new Employee(rowStr[1], decimal.Parse(rowStr[3]));
                dict.Add(rowStr[0], e);
            }

            return dict;
        }

        protected void Calendar1_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
                return;

            Calendar1.SelectedDate = DateTime.Now.Date;
            Calendar1_SelectionChanged(null, EventArgs.Empty);
        }

        protected void Calendar1_SelectionChanged(object sender, EventArgs e)
        {
            Project proj = GetCurrentProject();
            if (proj == null)
            {
                Response.StatusCode = 404;
                Response.End();
                return;
            }

            if (!SaveFinancial())
                return;

            Dictionary<string, decimal[]> dict = ViewState["financial"] as Dictionary<string, decimal[]>;
            string date = Calendar1.SelectedDate.ToString("d MMMM yyyy");
            if(dict.Keys.Contains(date))
            {
                ExpenseProducts.Text = String.Format("{0}", dict[date][0]);
                ExpenseServices.Text = String.Format("{0}", dict[date][1]);
                ExpenseOperation.Text = String.Format("{0}", dict[date][2]);
                ExpenseMarketing.Text = String.Format("{0}", dict[date][3]);
                ExpenseTaxes.Text = String.Format("{0}", dict[date][4]);
                ExpenseOther.Text = String.Format("{0}", dict[date][5]);
                expensetext.InnerText = "";

                ProfitProducts.Text = String.Format("{0}", dict[date][6]);
                ProfitServices.Text = String.Format("{0}", dict[date][7]);
                ProfitOther.Text = String.Format("{0}", dict[date][8]);
                profittext.InnerText = "";
            }
            else
            {
                ExpenseProducts.Text = "0";
                ExpenseServices.Text = "0";
                ExpenseOperation.Text = "0";
                ExpenseMarketing.Text = "0";
                ExpenseTaxes.Text = "0";
                ExpenseOther.Text = "0";
                expensetext.InnerText = "";

                ProfitProducts.Text = "0";
                ProfitServices.Text = "0";
                ProfitOther.Text = "0";
                profittext.InnerText = "";
            }

            ViewState["date"] = Calendar1.SelectedDate.ToString("d MMMM yyyy");
            UpdatePanel2.Update();
        }

        protected bool SaveFinancial()
        {
            profittext.InnerText = "";
            expensetext.InnerText = "";

            if (!IsPostBack)
                return true;

            decimal[] values = new decimal[] { 0,0,0,0,0,0,0,0,0 };
            if (!decimal.TryParse(ExpenseProducts.Text, out values[0]))
            {
                expensetext.InnerText = "Expense products field is invalid.";
                return false;
            }

            if (!decimal.TryParse(ExpenseServices.Text, out values[1]))
            {
                expensetext.InnerText = "Expense services field is invalid.";
                return false;
            }

            if (!decimal.TryParse(ExpenseOperation.Text, out values[2]))
            {
                expensetext.InnerText = "Expense operating costs field is invalid.";
                return false;
            }

            if (!decimal.TryParse(ExpenseMarketing.Text, out values[3]))
            {
                expensetext.InnerText = "Expense marketing field is invalid.";
                return false;
            }

            if (!decimal.TryParse(ExpenseTaxes.Text, out values[4]))
            {
                expensetext.InnerText = "Expense taxes field is invalid.";
                return false;
            }

            if (!decimal.TryParse(ExpenseOther.Text, out values[5]))
            {
                expensetext.InnerText = "Expense other field is invalid.";
                return false;
            }

            if (!decimal.TryParse(ProfitProducts.Text, out values[6]))
            {
                profittext.InnerText = "Profit products field is invalid.";
                return false;
            }

            if (!decimal.TryParse(ProfitServices.Text, out values[7]))
            {
                profittext.InnerText = "Profit services field is invalid.";
                return false;
            }

            if (!decimal.TryParse(ProfitOther.Text, out values[8]))
            {
                profittext.InnerText = "Profit other field is invalid.";
                return false;
            }

            string day = ViewState["date"] as string;
            Dictionary<string, decimal[]> dict = ViewState["financial"] as Dictionary<string, decimal[]>;
            if (dict.Keys.Contains(day))
            {
                if(dict[day].SequenceEqual(values))
                    return true;
            }

            List<MySqlCommand> data = Session["data"] as List<MySqlCommand>;
            DateTime setdate = DateTime.Parse(day);
            if (!dict.Keys.Contains(day))
            {
                dict.Add(day, new decimal[] { values[0], values[1], values[2], values[3], values[4],
                    values[5], values[6], values[7], values[8] });
                MySqlCommand cmd = new MySqlCommand("INSERT INTO project_financial (project_id, year, month, day, expense_products, " +
                    "expense_labor, expense_operation, expense_marketing, expense_taxes, expense_other, profit_products, profit_services," +
                    "profit_other) VALUES (@param1, @param2, @param3, @param4, @param5, @param6, @param7, @param8, @param9, @param10," +
                    "@param11, @param12, @param13);");
                cmd.Parameters.AddWithValue("@param1", GetCurrentProject().getId());
                cmd.Parameters.AddWithValue("@param2", setdate.Year);
                cmd.Parameters.AddWithValue("@param3", setdate.ToString("MMMM"));
                cmd.Parameters.AddWithValue("@param4", setdate.Day);
                cmd.Parameters.AddWithValue("@param5", values[0]);
                cmd.Parameters.AddWithValue("@param6", values[1]);
                cmd.Parameters.AddWithValue("@param7", values[2]);
                cmd.Parameters.AddWithValue("@param8", values[3]);
                cmd.Parameters.AddWithValue("@param9", values[4]);
                cmd.Parameters.AddWithValue("@param10", values[5]);
                cmd.Parameters.AddWithValue("@param11", values[6]);
                cmd.Parameters.AddWithValue("@param12", values[7]);
                cmd.Parameters.AddWithValue("@param13", values[8]);
                data.Add(cmd);
            }
            else
            {
                dict[day][0] = values[0];
                dict[day][1] = values[1];
                dict[day][2] = values[2];
                dict[day][3] = values[3];
                dict[day][4] = values[4];
                dict[day][5] = values[5];
                dict[day][6] = values[6];
                dict[day][7] = values[7];
                dict[day][8] = values[8];
                MySqlCommand cmd = new MySqlCommand("UPDATE project_financial SET expense_products = @param5, expense_labor = @param6, " +
                    "expense_operation = @param7, expense_marketing = @param8, expense_taxes = @param9, expense_other = @param10, " +
                    "profit_products = @param11, profit_services = @param12, profit_other = @param13 WHERE project_id = @param1 AND " +
                    "year = @param2 AND month = @param3 AND day = @param4;");
                cmd.Parameters.AddWithValue("@param1", GetCurrentProject().getId());
                cmd.Parameters.AddWithValue("@param2", setdate.Year);
                cmd.Parameters.AddWithValue("@param3", setdate.ToString("MMMM"));
                cmd.Parameters.AddWithValue("@param4", setdate.Day);
                cmd.Parameters.AddWithValue("@param5", values[0]);
                cmd.Parameters.AddWithValue("@param6", values[1]);
                cmd.Parameters.AddWithValue("@param7", values[2]);
                cmd.Parameters.AddWithValue("@param8", values[3]);
                cmd.Parameters.AddWithValue("@param9", values[4]);
                cmd.Parameters.AddWithValue("@param10", values[5]);
                cmd.Parameters.AddWithValue("@param11", values[6]);
                cmd.Parameters.AddWithValue("@param12", values[7]);
                cmd.Parameters.AddWithValue("@param13", values[8]);
                data.Add(cmd);
            }

            decimal profits = 0;
            decimal expenses = 0;
            foreach(KeyValuePair<string,decimal[]> kvp in dict)
            {
                expenses += (kvp.Value[0] + kvp.Value[1] + kvp.Value[2] + kvp.Value[3] + kvp.Value[4] + kvp.Value[5]);
                profits += (kvp.Value[6] + kvp.Value[7] + kvp.Value[8]);
            }

            pProfit.Text = String.Format("{0:C}", profits);
            pCost.Text = String.Format("{0:C}", expenses);
            UpdatePanel3.Update();
            UpdatePanel4.Update();

            ViewState["financial"] = dict;
            return true;
        }

        protected void Year_SelectedIndexChanged(object sender, EventArgs e)
        {
            DateTime dt;
            if (!DateTime.TryParse(Month.Text + " " + 1 + " " + Year.Text, out dt))
                return;

            try
            {
                Calendar1.VisibleDate = dt;
            }
            catch(Exception ex)
            {

            }
        }

        protected void Year_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
                return;

            Project p = GetCurrentProject();
            if (p == null)
                return;

            int year = p.getStartDate().Year;
            int newYear = DateTime.Now.Year + 5;
            for (int i = year; i < newYear; ++i)
            {
                Year.Items.Add(string.Format("{0}", i));
            }

            Year.Text = DateTime.Now.ToString("yyyy");
        }

        protected void Month_SelectedIndexChanged(object sender, EventArgs e)
        {
            DateTime dt;
            if (!DateTime.TryParse(Month.Text + " " + 1 + " " + Year.Text, out dt))
                return;

            try
            {
                Calendar1.VisibleDate = dt;
            }
            catch (Exception ex)
            {

            }
        }

        protected void Month_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
                return;

            Month.Text = DateTime.Now.ToString("MMMM");
        }

        protected void pTasks_RowEditing(object sender, GridViewEditEventArgs e)
        {
            e.Cancel = true;
        }

        protected void pTasks_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            e.Cancel = true;
        }

        protected void Button1_OnClick(object sender, EventArgs e)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated == true)
            {
                FormsAuthentication.SignOut();
                Response.Cookies[FormsAuthentication.FormsCookieName].Expires = DateTime.Now.AddYears(-1);
                FormsAuthentication.RedirectToLoginPage();
            }
        }

        protected void pEmployeesList_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }

        protected void pEmployeesList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            DataTable dt = ViewState["employees"] as DataTable;
            if (e.CommandName == "Delete")
            {
                if (index < dt.Rows.Count)
                {
                    List<MySqlCommand> data = Session["data"] as List<MySqlCommand>;
                    MySqlCommand cmd = new MySqlCommand("DELETE FROM project_employee WHERE project_id = @param1 AND employee = @param2 AND position = @param3 AND type = @param4 AND hours = @param5;");
                    cmd.Parameters.AddWithValue("@param1", GetCurrentProject().getId());
                    cmd.Parameters.AddWithValue("@param2", dt.Rows[index].ItemArray[0]);
                    cmd.Parameters.AddWithValue("@param3", dt.Rows[index].ItemArray[1]);
                    cmd.Parameters.AddWithValue("@param4", (string)dt.Rows[index].ItemArray[2] == "Part-time" ? 0 : 1);
                    cmd.Parameters.AddWithValue("@param5", dt.Rows[index].ItemArray[3]);
                    data.Add(cmd);
                    Session["data"] = data;

                    dt.Rows.RemoveAt(index);
                    pEmployeesList.DataSource = dt;
                    pEmployeesList.DataBind();
                }
            }
        }

        protected Project GetCurrentProject()
        {
            string qs = Request.QueryString["project"];
            if (String.IsNullOrEmpty(qs))
                return null;

            int ID;
            if (!int.TryParse(qs, out ID))
                return null;

            string name = ProjectsManager.GetRoleNameById(ID);
            if (name == null)
                return null;

            Project proj = ProjectsManager.GetRoleByName(name);

            return proj;
        }

        protected void PrepareData()
        {
            Project proj = GetCurrentProject();
            if(proj == null)
            {
                Response.StatusCode = 404;
                Response.End();
                return;
            }

            List<MySqlCommand> data = Session["data"] as List<MySqlCommand>;
            MySqlCommand cmd = new MySqlCommand();

            if (pSummary.Text != proj.getSummary())
            {
                cmd = new MySqlCommand("UPDATE project SET summary = @param1 WHERE id = @param2;");
                cmd.Parameters.AddWithValue("@param1", pSummary.Text);
                cmd.Parameters.AddWithValue("@param2", proj.getId());
                data.Add(cmd);
                cmd.Dispose();
            }

            if(pBudget.Text != String.Format("{0}", proj.getBudget()))
            {
                cmd = new MySqlCommand("UPDATE project SET budget = @param1 WHERE id = @param2;");
                cmd.Parameters.AddWithValue("@param1", pBudget.Text);
                cmd.Parameters.AddWithValue("@param2", proj.getId());
                data.Add(cmd);
                cmd.Dispose();
            }

            if(pCompletion.Text != proj.getCompleteDate().ToString("yyyy-MM-dd"))
            {
                cmd = new MySqlCommand("UPDATE project SET completion_date = @param1 WHERE id = @param2;");
                cmd.Parameters.AddWithValue("@param1", pCompletion.Text);
                cmd.Parameters.AddWithValue("@param2", proj.getId());
                data.Add(cmd);
                cmd.Dispose();
            }

            if(pProjectScope.Text != proj.getProjectScope())
            {
                cmd = new MySqlCommand("UPDATE project SET project_scope = @param1 WHERE id = @param2;");
                cmd.Parameters.AddWithValue("@param1", pProjectScope.Text);
                cmd.Parameters.AddWithValue("@param2", proj.getId());
                data.Add(cmd);
                cmd.Dispose();
            }

            if(pProductScope.Text != proj.getProductScope())
            {
                cmd = new MySqlCommand("UPDATE project SET product_scope = @param1 WHERE id = @param2;");
                cmd.Parameters.AddWithValue("@param1", pProductScope.Text);
                cmd.Parameters.AddWithValue("@param2", proj.getId());
                data.Add(cmd);
                cmd.Dispose();
            }

            if(pDetails.Text != proj.getDetails())
            {
                cmd = new MySqlCommand("UPDATE project SET details = @param1 WHERE id = @param2;");
                cmd.Parameters.AddWithValue("@param1", pDetails.Text);
                cmd.Parameters.AddWithValue("@param2", proj.getId());
                data.Add(cmd);
                cmd.Dispose();
            }

            if(pConclusion.Text != proj.getConclusion())
            {
                cmd = new MySqlCommand("UPDATE project SET conclusion = @param1 WHERE id = @param2;");
                cmd.Parameters.AddWithValue("@param1", pConclusion.Text);
                cmd.Parameters.AddWithValue("@param2", proj.getId());
                data.Add(cmd);
                cmd.Dispose();
            }

            if(pProjectPercentage.Text != String.Format("{0}", proj.getProjectProgress()))
            {
                cmd = new MySqlCommand("UPDATE project SET progress_percentage = @param1 WHERE id = @param2;");
                cmd.Parameters.AddWithValue("@param1", pProjectPercentage.Text);
                cmd.Parameters.AddWithValue("@param2", proj.getId());
                data.Add(cmd);
                cmd.Dispose();
            }

            Session["data"] = data;
        }

        protected bool IsManager()
        {
            Project proj = GetCurrentProject();

            if (User.IsInRole("Manager") && User.IsInRole(ProjectsManager.GetRoleNameById(proj.getId())))
                return true;

            return false;
        }
    }
}