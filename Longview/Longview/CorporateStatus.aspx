﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CorporateStatus.aspx.cs"  EnableEventValidation="false" Inherits="Longview.CorporateStatus" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="Content/bootstrap.css" rel="stylesheet" id="bootstrap-css" />
    <link href="Content/longview.css" rel="stylesheet" id="longview-css" />
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':[['table'], ['corechart']]});
        google.charts.setOnLoadCallback(drawTable);

      function drawTable() {
            var tableData = new google.visualization.DataTable();
            var services = parseFloat("<%=GetMonthlyFinances(7)%>");
            var products = parseFloat("<%=GetMonthlyFinances(6)%>");
            var other = parseFloat("<%=GetMonthlyFinances(8)%>");
           
            tableData.addColumn('string', 'Source');
            tableData.addColumn('number', 'Profit');
            tableData.addRows([
                ['Service', services],
                ['Products', products],
                ['Other', other],
          ]);

            var formatter = new google.visualization.NumberFormat({
                prefix: '$',
                negativeParens: true
            });

            formatter.format(tableData, 1);

            var table = new google.visualization.Table(document.getElementById('table_div'));

            table.draw(tableData, { showRowNumber: true, width: '100%', height: '100%', title: 'Profits', vAxis: { format: '$###,###,###.00' } });

            var tableData2 = new google.visualization.DataTable();

            var eLabor = parseFloat("<%=GetMonthlyFinances(1)%>");
            var eProducts = parseFloat("<%=GetMonthlyFinances(0)%>");
            var eOperation = parseFloat("<%=GetMonthlyFinances(2)%>");
            var eMarketing = parseFloat("<%=GetMonthlyFinances(3)%>");
            var eTaxes = parseFloat("<%=GetMonthlyFinances(4)%>");
            var eOther = parseFloat("<%=GetMonthlyFinances(5)%>");

            tableData2.addColumn('string', 'Source');
            tableData2.addColumn('number', 'Loss');
            tableData2.addRows([
                ['Products', eProducts],
                ['Labor', eLabor],
                ['Operating Costs', eOperation],
                ['Marketing', eMarketing],
                ['Taxes', eTaxes],
                ['Other', eOther],
          ]);

          formatter.format(tableData2, 1);

          var table2 = new google.visualization.Table(document.getElementById('table2_div'));

          table2.draw(tableData2, { showRowNumber: true, width: '100%', height: '100%', title: 'Losses', vAxis: { format: '$###,###,###.00' } });

          var data = new google.visualization.DataTable();

          var pJan = <%=GetFinancesByMonth(1, true)%>;
          var pFeb = <%=GetFinancesByMonth(2, true)%>;
          var pMar = <%=GetFinancesByMonth(3, true)%>;
          var pApr = <%=GetFinancesByMonth(4, true)%>;
          var pMay = <%=GetFinancesByMonth(5, true)%>;
          var pJun = <%=GetFinancesByMonth(6, true)%>;
          var pJul = <%=GetFinancesByMonth(7, true)%>;
          var pAug = <%=GetFinancesByMonth(8, true)%>;
          var pSep = <%=GetFinancesByMonth(9, true)%>;
          var pOct = <%=GetFinancesByMonth(10, true)%>;
          var pNov = <%=GetFinancesByMonth(11, true)%>;
          var pDec = <%=GetFinancesByMonth(12, true)%>;
          var eJan = <%=GetFinancesByMonth(1, false)%>;
          var eFeb = <%=GetFinancesByMonth(2, false)%>;
          var eMar = <%=GetFinancesByMonth(3, false)%>;
          var eApr = <%=GetFinancesByMonth(4, false)%>;
          var eMay = <%=GetFinancesByMonth(5, false)%>;
          var eJun = <%=GetFinancesByMonth(6, false)%>;
          var eJul = <%=GetFinancesByMonth(7, false)%>;
          var eAug = <%=GetFinancesByMonth(8, false)%>;
          var eSep = <%=GetFinancesByMonth(9, false)%>;
          var eOct = <%=GetFinancesByMonth(10, false)%>;
          var eNov = <%=GetFinancesByMonth(11, false)%>;
          var eDec = <%=GetFinancesByMonth(12, false)%>;
          var Year = <%=DateTime.Now.Year%>;

          data.addColumn('string', 'Month');
          data.addColumn('number', 'Profit');
          data.addColumn('number', 'Expenses');
          data.addRows([
                ['Jan', pJan, eJan],
                ['Feb', pFeb, eFeb],
                ['Mar', pMar, eMar],
                ['Apr', pApr, eApr],
                ['May', pMay, eMay],
                ['Jun', pJun, eJun],
                ['Jul', pJul, eJul],
                ['Aug', pAug, eAug],
                ['Sep', pSep, eSep],
                ['Oct', pOct, eOct],
                ['Nov', pNov, eNov],
                ['Dec', pDec, eDec],
          ]);

          var topLevel = 0;

          var options = {
              hAxis: {
                    title: 'Month'
                },
                vAxis: {
                    title: 'Finances',
                    format: '$###,###,###.00'
                },
              title: 'Finances ' + Year,
              legend: { position: 'top' },
              chartArea: { width: '100%'},
              animation: {
                    duration: 1000,
                    easing: 'out'
              },
          };

          formatter.format(data, 1);
          formatter.format(data, 2);

          var chart = new google.visualization.LineChart(document.getElementById('line_div'));

          chart.draw(data, options);
          google.visualization.events.addListener(chart, 'select', function selectHandler(e) {
                var selectedItem = chart.getSelection()[0];
                switch (topLevel) {
                    case 0:
                        if (selectedItem) {
                            var mo = data.getValue(selectedItem.row, 0);

                            options = {
                                hAxis: {
                                    title: 'Day',
                                    textStyle : { fontSize : 9 },
                                },
                                vAxis: {
                                    title: 'Amount',
                                    format: '$###,###,###.00',
                                },
                                title: mo + ' Finances',
                                legend: { position: 'top' },
                                chartArea: { width: '100%', },
                                animation: {
                                    duration: 1000,
                                    easing: 'out'
                                },
                            };

                            chart.draw(getData(mo), options);
                            table.draw(getTableData(mo, true), { showRowNumber: true, width: '100%', height: '100%', vAxis: { format: '$###,###,###.00' } });
                            table2.draw(getTableData(mo, false), { showRowNumber: true, width: '100%', height: '100%', vAxis: { format: '$###,###,###.00' } });
                            topLevel += 1;
                        }
                        break;

                    case 1:
                        options = {
                            hAxis: {
                                title: 'Month'
                            },
                            vAxis: {
                                title: 'Amount',
                                format: '$###,###,###.00',
                            },
                            title: 'Finances ' + Year,
                            legend: { position: 'top' },
                            chartArea: { width: '100%', },
                            animation: {
                                duration: 1000,
                                easing: 'out'
                            },
                        };

                        chart.draw(data, options);
                        table.draw(tableData, { showRowNumber: true, width: '100%', height: '100%', vAxis: { format: '$###,###,###.00' } });
                        table2.draw(tableData2, { showRowNumber: true, width: '100%', height: '100%', vAxis: { format: '$###,###,###.00' } });
                        topLevel = 0;
                        break;
                }
            });
        }

        function getData(row)
        {
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Day');
            data.addColumn('number', 'Profit');
            data.addColumn('number', 'Expenses')
            var str = "";
            var str2 = "";

            switch(row)
            {
                case "Jan":
                    str = [ <%=GetDayFinances(1)%> ];
                    str2 = [ <%=GetDayFinances(1, true)%> ];
                    break;
                case "Feb":
                    str = [ <%=GetDayFinances(2)%> ];
                    str2 = [ <%=GetDayFinances(2, true)%> ];
                    break;
                case "Mar":
                    str = [ <%=GetDayFinances(3)%> ];
                    str2 = [ <%=GetDayFinances(3, true)%> ];
                    break;
                case "Apr":
                    str = [ <%=GetDayFinances(4)%> ];
                    str2 = [ <%=GetDayFinances(4, true)%> ];
                    break;
                case "May":
                    str = [ <%=GetDayFinances(5)%> ];
                    str2 = [ <%=GetDayFinances(5, true)%> ];
                    break;
                case "Jun":
                    str = [ <%=GetDayFinances(6)%> ];
                    str2 = [ <%=GetDayFinances(6, true)%> ];
                    break;
                case "Jul":
                    str = [ <%=GetDayFinances(7)%> ];
                    str2 = [ <%=GetDayFinances(7, true)%> ];
                    break;
                case "Aug":
                    str = [ <%=GetDayFinances(8)%> ];
                    str2 = [ <%=GetDayFinances(8, true)%> ];
                    break;
                case "Sep":
                    str = [ <%=GetDayFinances(9)%> ];
                    str2 = [ <%=GetDayFinances(9, true)%> ];
                    break;
                case "Oct":
                    str = [ <%=GetDayFinances(10)%> ];
                    str2 = [ <%=GetDayFinances(10, true)%> ];
                    break;
                case "Nov":
                    str = [ <%=GetDayFinances(11)%> ];
                    str2 = [ <%=GetDayFinances(11, true)%> ];
                    break;
                case "Dec":
                    str = [ <%=GetDayFinances(12)%> ];
                    str2 = [ <%=GetDayFinances(12, true)%> ];
                    break;
            }

            for (var i = 0; i < str.length; ++i)
            {
                data.addRow([(i + 1).toString(), parseFloat(str2[i]), parseFloat(str[i])]);
            }

            var formatter = new google.visualization.NumberFormat({
                prefix: '$',
                negativeParens: true
            });

            formatter.format(data, 1);
            formatter.format(data, 2);

            return data;
        }

        function getTableData(row, profit)
        {
            var data = new google.visualization.DataTable();
            var str = "";
            if(profit)
            {
                data.addColumn('string', 'Source');
                data.addColumn('number', 'Profit');

                switch(row)
                {
                    case "Jan":
                        str = [ <%=GetDayFinancialArray(1, true)%> ];
                        break;
                    case "Feb":
                        str = [ <%=GetDayFinancialArray(2, true)%> ];
                        break;
                    case "Mar":
                        str = [ <%=GetDayFinancialArray(3, true)%> ];
                        break;
                    case "Apr":
                        str = [ <%=GetDayFinancialArray(4, true)%> ];
                        break;
                    case "May":
                        str = [ <%=GetDayFinancialArray(5, true)%> ];
                        break;
                    case "Jun":
                        str = [ <%=GetDayFinancialArray(6, true)%> ];
                        break;
                    case "Jul":
                        str = [ <%=GetDayFinancialArray(7, true)%> ];
                        break;
                    case "Aug":
                        str = [ <%=GetDayFinancialArray(8, true)%> ];
                        break;
                    case "Sep":
                        str = [ <%=GetDayFinancialArray(9, true)%> ];
                        break;
                    case "Oct":
                        str = [ <%=GetDayFinancialArray(10, true)%> ];
                        break;
                    case "Nov":
                        str = [ <%=GetDayFinancialArray(11, true)%> ];
                        break;
                    case "Dec":
                        str = [ <%=GetDayFinancialArray(12, true)%> ];
                        break;
                }

                data.addRows([
                ['Service', str[7]],
                ['Products', str[6]],
                ['Other', str[8]],
                ]);
            }
            else
            {
                data.addColumn('string', 'Source');
                data.addColumn('number', 'Loss');

                switch(row)
                {
                    case "Jan":
                        str = [ <%=GetDayFinancialArray(1, false)%> ];
                        break;
                    case "Feb":
                        str = [ <%=GetDayFinancialArray(2, false)%> ];
                        break;
                    case "Mar":
                        str = [ <%=GetDayFinancialArray(3, false)%> ];
                        break;
                    case "Apr":
                        str = [ <%=GetDayFinancialArray(4, false)%> ];
                        break;
                    case "May":
                        str = [ <%=GetDayFinancialArray(5, false)%> ];
                        break;
                    case "Jun":
                        str = [ <%=GetDayFinancialArray(6, false)%> ];
                        break;
                    case "Jul":
                        str = [ <%=GetDayFinancialArray(7, false)%> ];
                        break;
                    case "Aug":
                        str = [ <%=GetDayFinancialArray(8, false)%> ];
                        break;
                    case "Sep":
                        str = [ <%=GetDayFinancialArray(9, false)%> ];
                        break;
                    case "Oct":
                        str = [ <%=GetDayFinancialArray(10, false)%> ];
                        break;
                    case "Nov":
                        str = [ <%=GetDayFinancialArray(11, false)%> ];
                        break;
                    case "Dec":
                        str = [ <%=GetDayFinancialArray(12, false)%> ];
                        break;
                }

                data.addRows([
                  ['Products', str[0]],
                  ['Labor', str[1]],
                  ['Operating Costs', str[2]],
                  ['Marketing', str[3]],
                  ['Taxes', str[4]],
                  ['Other', str[5]],
                ]);
            }

            var formatter = new google.visualization.NumberFormat({
                prefix: '$',
                negativeParens: true
            });

            formatter.format(data, 1);

            return data;
        }
    </script>
    <style>
        .chartHeader
        {
            background: linear-gradient(to bottom, #242e3a 0%,#061a33 2%,#001e44 4%,#05224e 6%,#061c45 7%,#0a1c40 9%,#05224a 13%,#082044 15%,#09254d 28%,#0d2450 30%,#112f63 52%,#102f65 57%,#0b2c61 59%,#0b2c61 100%);
            color: white; 
            text-shadow: 2px 2px black;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>
        <div>
            <table style="width: 100%;">
                <tr>
                    <td style="width: 50%;">
                        <div id="financial" class="contentBox" style="width: 100%; height: 450px;">
                            <img src="Images/financial.png" style="margin-left: 10px; margin-top: 5px; float: left;" /><h1 style="margin-left: 10px; display: inline-block; font-size: 30px; margin-top: 10px;">Company Finances</h1>
                            <!--<div style="width: auto; text-align: center;">
                                <div style="height: 50px; margin: 20px; display: inline-block; text-align: center; line-height: 0.8;">
                                    <b>Full-time Equivalent</b><br />
                                    <b style="font-size: 3vw;"><asp:Literal ID="FullTimeEquivalent" runat="server"></asp:Literal></b>
                                </div>
                                <div style="height: 50px; margin: 20px; display: inline-block; text-align: center; line-height: 0.8;">
                                    <b>Total Employees</b><br />
                                    <b style="font-size: 3vw;"><asp:Literal ID="EmployeeCount" runat="server"></asp:Literal></b>
                                </div>
                                <div style="height: 50px; margin: 20px; display: inline-block; text-align: center; line-height: 0.8;">
                                    <b>Total Revenue</b><br />
                                    <b style="font-size: 3vw;"><asp:Literal ID="TotalRevenue" runat="server"></asp:Literal></b>
                                </div>
                                <div style="height: 50px; margin: 20px; display: inline-block; text-align: center; line-height: 0.8;">
                                    <b>Total Expenses</b><br />
                                    <b style="font-size: 3vw;"><asp:Literal ID="TotalExpenses" runat="server"></asp:Literal></b>
                                </div>
                            </div>-->
                            <div style="margin: 0 20px; height: 370px;">
                                <div id="line_div" style="width: 100%;"></div>
                                <div id="table_div" style="width: 50%; display: inline; float: left;"></div>
                                <div id="table2_div" style="width: 50%; display: inline; float: left;"></div>
                            </div>
                        </div>
                    </td>

                    <td style="width: 50%;">
                        <div id="recruiting" class="contentBox" style="width: 100%; height: 450px;">
                            <img src="Images/recruiting.png" style="margin-left: 10px; margin-top: 5px; float: left;" /><h1 style="margin-left: 10px; display: inline-block; font-size: 30px; margin-top: 10px;">Recruiting</h1>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div id="grid" style="margin: 20px; height: auto; margin-bottom: 20px; overflow-y: auto;" runat="server">
                                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" OnLoad="GridView1_Load" Width="100%" OnRowDataBound="GridView1_RowDataBound" OnSelectedIndexChanged="GridView1_SelectedIndexChanged" GridLines="None" AllowSorting="True">
                                            <Columns>
                                                <asp:BoundField DataField="Project" HeaderText="Project" >
                                                <HeaderStyle Width="150px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Open Position" HeaderText="Open Position" ></asp:BoundField>
                                                <asp:BoundField DataField="Days Open" HeaderText="Days Open" />
                                                <asp:BoundField DataField="Resumes Found" HeaderText="Resumes Found" />
                                                <asp:BoundField DataField="Resumes Submitted" HeaderText="Resumes Submitted" />
                                            </Columns>
                                        </asp:GridView>
                                        <div id="gridContent" runat="server">
                                            <asp:Literal ID="Literal1" runat="server" Visible="false"></asp:Literal>
                                            <asp:TextBox ID="TextProject" runat="server" Visible="false" Enabled="false"></asp:TextBox>
                                            <asp:TextBox ID="TextPosition" runat="server" Visible="false" Enabled="false"></asp:TextBox>
                                            <div style="width: 100%; max-height: 500px;">
                                                <asp:GridView ID="Resumes" runat="server" AutoGenerateColumns="False" GridLines="None" Width="100%" OnRowDataBound="Resumes_RowDataBound">
                                                    <Columns>
                                                        <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" >
                                                        <HeaderStyle Width="100px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="Last Correspondence" HeaderText="Last Correspondence" />
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="TextBox1" runat="server" Visible="false" TextMode="Date" Width="200px"></asp:TextBox>
                                                            </ItemTemplate>
                                                            <ItemStyle Width="250px" />
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="Submitted" HeaderText="Submitted" />
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="" Text="View"></asp:HyperLink>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                            <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click" Visible="false" >← Back</asp:LinkButton>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel> 
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="width: 50%;">
                        <div id="businessdevelopment" class="contentBox" style="width: 100%; height: 450px;">
                            <img src="Images/accountcreate.png" style="margin-left: 10px; margin-top: 5px; float: left;" /><h1 style="margin-left: 10px; display: inline-block; font-size: 30px; margin-top: 10px;">Business Development</h1><br />

                        </div>
                    </td>

                    <td style="width: 50%;">
                        <div id="progressreports" class="contentBox" style="width: 100%; height: 450px;">
                            <img src="Images/survey.png" style="margin-left: 10px; margin-top: 5px; float: left;" /><h1 style="margin-left: 10px; display: inline-block; font-size: 30px; margin-top: 10px;">Progress Reports</h1>
                            <asp:Button ID="UploadButton" runat="server" Height="30px" Text="Upload" CssClass="btn-default btn-sm" Width="80px" BackColor="#0d0435" ForeColor="White" Visible="false" style="float: right; margin-top: 10px; margin-right: 20px; font-weight: 500;" BorderStyle="None" OnClick="UploadButton_Click" />
                            <br />
                                    <div id="PR" runat="server" style="height: 350px; width: auto; margin: 20px; overflow-y: auto;">
                                        <asp:GridView ID="ProgressReports" runat="server" Width="100%" AutoGenerateColumns="False" GridLines="None" OnLoad="ProgressReports_Load" OnSelectedIndexChanging="ProgressReports_SelectedIndexChanging">
                                            <Columns>
                                                <asp:BoundField DataField="Project" HeaderText="Project" />
                                                <asp:BoundField DataField="Date Submitted" HeaderText="Date Submitted" />
                                                <asp:CommandField SelectText="View" ShowSelectButton="True" />
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                    <% if (User.IsInRole("Manager") || User.IsInRole("Administrators"))
                                        { %>
                                    <div id="UploadBox" runat="server" style="height: 350px; width: auto; margin: 20px; overflow-y: auto; display: none;">
                                        <div style="border: 1px solid lightgray; padding: 15px; display: inline-block;">
                                            <table>
                                                <thead>
                                                    <tr>
                                                        <td colspan="7">
                                                            <h3>Upload a Document</h3>
                                                        </td>
                                                    </tr>
                                                </thead>
                                                <tr>
                                                    <td class="auto-style1">
                                                        <asp:Label ID="Label1" runat="server" Text="Project"></asp:Label>
                                                    </td>

                                                    <td></td>

                                                    <td class="auto-style1">
                                                        <asp:Label ID="Label2" runat="server" Text="File Path"></asp:Label>
                                                    </td>
                                                </tr>

                                                <tr style="height: 30px;">
                                                    <td>
                                                        <asp:DropDownList ID="DropDownList1" runat="server" OnLoad="DropDownList1_Load"></asp:DropDownList>
                                                    </td>

                                                    <td></td>

                                                    <td>
                                                        <asp:FileUpload ID="FileUpload1" runat="server" />
                                                    </td>

                                                    <td class="auto-style2">                      
                                                    </td>

                                                    <td>
                                                        <asp:Button ID="Upload" runat="server" Text="Upload" UseSubmitBehavior="False" OnClick="Upload_Click" />
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="height: 15px;"></td>
                                                </tr>

                                                <tr>
                                                    <td colspan="4">
                                                        <p id="errortext" runat="server" style="color: red;"></p>
                                                    </td>

                                                    <td colspan="2" class="auto-style2">

                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <asp:LinkButton ID="BackButton" runat="server" OnClick="BackButton_Click">← Back</asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <% } %>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
