﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Longview
{
    public partial class ProgressOverview : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            decimal monthlyRevenue = 0;
            decimal monthlyExpenses = 0;

            foreach (KeyValuePair<string, Project> kvp in ProjectsManager.GetAllRoles())
            {
                if (kvp.Key == "Overview")
                    continue;

                if (kvp.Value.getSummary() == "")
                    continue;

                monthlyRevenue += kvp.Value.getMonthlyProfit();
                monthlyExpenses += kvp.Value.getMonthlyCost();

                int index = Table1.Rows.Add(new TableRow());
                int cellIndex = Table1.Rows[index].Cells.Add(new TableCell());
                Table1.Rows[index].Cells[cellIndex].ColumnSpan = 4;
                Table1.Rows[index].Cells[cellIndex].Text = @"<b style=""font-size: 18pt;"">" + kvp.Value.getName() + "</b>";

                index = Table1.Rows.Add(new TableRow());
                cellIndex = Table1.Rows[index].Cells.Add(new TableCell());
                Table1.Rows[index].Cells[cellIndex].ColumnSpan = 4;
                Table1.Rows[index].Cells[cellIndex].Text = kvp.Value.getSummary();

                TableRow spacer = new TableRow();
                TableCell tc = new TableCell();
                tc.Height = new Unit("10px");
                spacer.Cells.Add(tc);
                Table1.Rows.Add(spacer);

                if (!User.IsInRole("Customer"))
                {
                    index = Table1.Rows.Add(new TableRow());
                    cellIndex = Table1.Rows[index].Cells.Add(new TableCell());
                    Table1.Rows[index].Cells[cellIndex].Text = "<b>Monthly Profits<b>";
                    cellIndex = Table1.Rows[index].Cells.Add(new TableCell());
                    Table1.Rows[index].Cells[cellIndex].Text = "<b>Monthly Expenses<b>";

                    index = Table1.Rows.Add(new TableRow());
                    cellIndex = Table1.Rows[index].Cells.Add(new TableCell());
                    Table1.Rows[index].Cells[cellIndex].Text = String.Format("{0:C}", kvp.Value.getMonthlyProfit());
                    cellIndex = Table1.Rows[index].Cells.Add(new TableCell());
                    Table1.Rows[index].Cells[cellIndex].Text = String.Format("{0:C}", kvp.Value.getMonthlyCost());

                    spacer = new TableRow();
                    tc = new TableCell();
                    tc.Height = new Unit("10px");
                    spacer.Cells.Add(tc);
                    Table1.Rows.Add(spacer);
                }

                index = Table1.Rows.Add(new TableRow());
                cellIndex = Table1.Rows[index].Cells.Add(new TableCell());
                Table1.Rows[index].Cells[cellIndex].Text = "<b>Completion<b>";

                index = Table1.Rows.Add(new TableRow());
                cellIndex = Table1.Rows[index].Cells.Add(new TableCell());
                Table1.Rows[index].Cells[cellIndex].ColumnSpan = 4;
                Table1.Rows[index].Cells[cellIndex].Text = @"<div class=""progress"" style=""height: 50px;"">" +
                    @"<div class=""progress-bar progress-bar-success"" role=""progressbar"" aria-valuenow=""40"" aria-valuemin=""0"" aria-valuemax=""100"" style=""background-color: #1b377e; width: " + kvp.Value.getProjectProgress() +
                    @"%; font-family: Verdana,sans-serif; font-weight: 400; font-size: 18pt; text-shadow: 2px 2px black;"">" + kvp.Value.getProjectProgress() + @"%</div></div>";

                spacer = new TableRow();
                tc = new TableCell();
                tc.Height = new Unit("10px");
                spacer.Cells.Add(tc);
                Table1.Rows.Add(spacer);

                GridView gv;
                BoundField bf;
                DataTable dt;
                DataRow NewRow;
                if (kvp.Value.getEmployees().Count > 0)
                {
                    index = Table1.Rows.Add(new TableRow());
                    cellIndex = Table1.Rows[index].Cells.Add(new TableCell());
                    Table1.Rows[index].Cells[cellIndex].Text = "<b>Employees<b>";

                    index = Table1.Rows.Add(new TableRow());
                    cellIndex = Table1.Rows[index].Cells.Add(new TableCell());
                    Table1.Rows[index].Cells[cellIndex].ColumnSpan = 4;

                    gv = new GridView();
                    gv.AutoGenerateColumns = false;
                    gv.Width = new Unit("100%");
                    gv.GridLines = GridLines.None;
                    bf = new BoundField();
                    bf.HeaderText = "Employee";
                    bf.DataField = "Employee";
                    gv.Columns.Add(bf);
                    bf = new BoundField();
                    bf.HeaderText = "Position";
                    bf.DataField = "Position";
                    gv.Columns.Add(bf);
                    bf = new BoundField();
                    bf.HeaderText = "Type";
                    bf.DataField = "Type";
                    gv.Columns.Add(bf);
                    bf = new BoundField();
                    bf.HeaderText = "Weekly Hours";
                    bf.DataField = "Weekly Hours";
                    gv.Columns.Add(bf);

                    dt = new DataTable();
                    if (dt.Columns.Count == 0)
                    {
                        dt.Columns.Add("Employee", typeof(string));
                        dt.Columns.Add("Position", typeof(string));
                        dt.Columns.Add("Type", typeof(string));
                        dt.Columns.Add("Weekly Hours", typeof(string));
                    }

                    foreach (KeyValuePair<string, Employee> em in kvp.Value.getEmployees())
                    {
                        NewRow = dt.NewRow();
                        NewRow[0] = em.Key;
                        NewRow[1] = em.Value.position;
                        NewRow[2] = em.Value.weeklyhours >= 40 ? "Full-time" : "Part-time";
                        NewRow[3] = em.Value.weeklyhours;
                        dt.Rows.Add(NewRow);
                    }

                    gv.DataSource = dt;
                    gv.DataBind();

                    Table1.Rows[index].Cells[cellIndex].Controls.Add(gv);

                    spacer = new TableRow();
                    tc = new TableCell();
                    tc.Height = new Unit("10px");
                    spacer.Cells.Add(tc);
                    Table1.Rows.Add(spacer);
                }

                if (kvp.Value.getRecruitment().Count > 0)
                {
                    index = Table1.Rows.Add(new TableRow());
                    cellIndex = Table1.Rows[index].Cells.Add(new TableCell());
                    Table1.Rows[index].Cells[cellIndex].Text = "<b>Open Positions<b>";

                    index = Table1.Rows.Add(new TableRow());
                    cellIndex = Table1.Rows[index].Cells.Add(new TableCell());
                    Table1.Rows[index].Cells[cellIndex].ColumnSpan = 4;
                    Table rt = new Table();
                    rt.Width = new Unit("100%");
                    rt.Style.Add("margin", "0 20px 0 20px");

                    foreach (Recruitment r in kvp.Value.getRecruitment())
                    {
                        int rtIndex = rt.Rows.Add(new TableRow());
                        int rtCellIndex = rt.Rows[rtIndex].Cells.Add(new TableCell());
                        rt.Rows[rtIndex].Cells[rtCellIndex].Text = "<b>Position<br>";
                        rtCellIndex = rt.Rows[rtIndex].Cells.Add(new TableCell());
                        rt.Rows[rtIndex].Cells[rtCellIndex].Text = "<b>Days Open<br>";
                        rtCellIndex = rt.Rows[rtIndex].Cells.Add(new TableCell());
                        rt.Rows[rtIndex].Cells[rtCellIndex].Text = "<b>Resumes Found<br>";
                        rtCellIndex = rt.Rows[rtIndex].Cells.Add(new TableCell());
                        rt.Rows[rtIndex].Cells[rtCellIndex].Text = "<b>Resumes Submitted<br>";
                        rtIndex = rt.Rows.Add(new TableRow());
                        rtCellIndex = rt.Rows[rtIndex].Cells.Add(new TableCell());
                        rt.Rows[rtIndex].Cells[rtCellIndex].Text = r.position;
                        rtCellIndex = rt.Rows[rtIndex].Cells.Add(new TableCell());
                        rt.Rows[rtIndex].Cells[rtCellIndex].Text = Math.Floor(DateTime.Now.Subtract(r.position_open).TotalDays).ToString();
                        rtCellIndex = rt.Rows[rtIndex].Cells.Add(new TableCell());
                        rt.Rows[rtIndex].Cells[rtCellIndex].Text = r.resumes.Where(x => x.submitted == 0).Count().ToString();
                        rtCellIndex = rt.Rows[rtIndex].Cells.Add(new TableCell());
                        rt.Rows[rtIndex].Cells[rtCellIndex].Text = r.resumes.Where(x => x.submitted == 1).Count().ToString();
                        rtIndex = rt.Rows.Add(new TableRow());
                        rtCellIndex = rt.Rows[rtIndex].Cells.Add(new TableCell());
                        rt.Rows[rtIndex].Cells[rtCellIndex].Height = new Unit("10px");

                        if (r.resumes.Count > 0)
                        {
                            rtIndex = rt.Rows.Add(new TableRow());
                            rtCellIndex = rt.Rows[rtIndex].Cells.Add(new TableCell());
                            rt.Rows[rtIndex].Cells[rtCellIndex].Text = "<b>Resumes<br>";
                            rtIndex = rt.Rows.Add(new TableRow());
                            rtCellIndex = rt.Rows[rtIndex].Cells.Add(new TableCell());
                            rt.Rows[rtIndex].Cells[rtCellIndex].ColumnSpan = 4;
                            GridView resumes = new GridView();

                            dt = new DataTable();
                            if (dt.Columns.Count == 0)
                            {
                                dt.Columns.Add("Name", typeof(string));
                                dt.Columns.Add("Last Correspondence", typeof(string));
                                dt.Columns.Add("Submitted", typeof(string));
                            }

                            foreach (Resume res in r.resumes)
                            {
                                NewRow = dt.NewRow();
                                NewRow[0] = res.name;
                                NewRow[1] = res.last_correspondence.ToString("yyyy-MM-dd");
                                NewRow[2] = res.submitted == 0 ? "No" : "Yes";
                                dt.Rows.Add(NewRow);
                            }

                            resumes.DataSource = dt;
                            resumes.DataBind();

                            rt.Rows[rtIndex].Cells[rtCellIndex].Controls.Add(resumes);
                        }
                        rtIndex = rt.Rows.Add(new TableRow());
                        rtCellIndex = rt.Rows[rtIndex].Cells.Add(new TableCell());
                        rt.Rows[rtIndex].Cells[rtCellIndex].ColumnSpan = 4;
                        rt.Rows[rtIndex].Cells[rtCellIndex].Text = "<hr>";
                        rt.Rows[rtIndex].Cells[rtCellIndex].Height = new Unit("30px");
                    }


                    Table1.Rows[index].Cells[cellIndex].Controls.Add(rt);
                }

                spacer = new TableRow();
                tc = new TableCell();
                tc.Height = new Unit("10px");
                spacer.Cells.Add(tc);
                Table1.Rows.Add(spacer);
            }

            List<Company.Employee> PartTime = Company.GetEmployees().Values.Where(x => x.WeeklyHours < 40).ToList();
            List<Company.Employee> FullTime = Company.GetEmployees().Values.Where(x => x.WeeklyHours >= 40).ToList();
            decimal PTE = 0;
            foreach (Company.Employee em in PartTime)
            {
                PTE += em.WeeklyHours;
            }
            pRevenue.Text = String.Format("{0:C}", Company.GetRevenue());
            pExpenses.Text = String.Format("{0:C}", Company.GetExpenses());
            pMonthlyRevenue.Text = String.Format("{0:C}", monthlyRevenue);
            pMonthlyExpenses.Text = String.Format("{0:C}", monthlyExpenses);
            pFullTimeEquivalent.Text = String.Format("{0}", Math.Round((decimal)FullTime.Count + (PTE / 40), 1));

            if (Page.User.Identity.IsAuthenticated)
            {
                string fullName = (string)HttpContext.Current.Profile.GetPropertyValue("firstname") == "" ? Page.User.Identity.GetUserName() : (string)HttpContext.Current.Profile.GetPropertyValue("firstname");
                Username.Text = "Hello, " + fullName + "&nbsp;&nbsp;&nbsp;";
            }
        }

        protected void Button1_OnClick(object sender, EventArgs e)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated == true)
            {
                FormsAuthentication.SignOut();
                Response.Cookies[FormsAuthentication.FormsCookieName].Expires = DateTime.Now.AddYears(-1);
                FormsAuthentication.RedirectToLoginPage();
            }
        }
    }
}