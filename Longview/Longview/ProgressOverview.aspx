﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProgressOverview.aspx.cs" Inherits="Longview.ProgressOverview" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="Content/bootstrap.css" rel="stylesheet" id="bootstrap-css"/>
    <style type="text/css">
        .auto-style1 {
            height: 23px;
        }

            @font-face { font-family: "Raleway"; src: url('/fonts/Raleway-Regular.ttf'); }
            @font-face { font-family: "Raleway"; src: url('/fonts/Raleway-SemiBold.ttf'); font-weight: 500; }
            @font-face { font-family: "Raleway"; src: url('/fonts/Raleway-Bold.ttf'); font-weight: bold; }

            .dropdown {
                position: relative;
                display: inline-block;
            }

            .dropdown-content {
                visibility: hidden;
                position: absolute;
                background-color: #f9f9f9;
                min-width: 160px;
                box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
                z-index: 1;
                right: 0;
                border-color: gray;

                transition: visibility 0.5s;
            }

            .dropdown-item img
            {
                float: left;
            }

            .dropdown-item
            {
                padding: 10px;
            }

            .dropdown-item:hover
            {
                background-color: lightgray;
            }

            .dropdown:focus {
                pointer-events: none;
                background-color: lightgray;
            }

            .dropdown-content:focus
            {
                display: block;
            }

            .dropdown:focus .dropdown-content {
                visibility: visible;
                outline: none;
                pointer-events: auto;

                transition: visibility 0.5s;
            }

        .navItem
        {
            height: 100%;
            font-size: 19px;
            padding: 16px 25px;
            display: block;
            text-align: center;
            color: #2f415f;
        }

        .navItem:hover
        {
            background: rgba(0,0,0,0.3);
            color: #FFF;
            text-align: center;
        }
    </style>
</head>
<body style="margin: 0; background-color: white; font-family: Raleway;">
    <header> 
        <table width="100%" style="font-family: Raleway;">
		    <tr>
			    <td width="5%"></td>
			    <td width="10%" valign="middle" align="left"><a href="/"><img src="/images/longview.png" style="height: 67px;" alt=""/></a></td>
			    <td align="right">
                    <div style="width: auto; height: 100%; display: inline-block; border-right: 1px solid black;">
                        <nav style="height: 100%;">
                            <ul style="list-style: none; margin: 0; padding: 0;">
                                <li>
                                    <a class="navItem" href="dashboard.aspx" style="text-decoration: none;">
                                        Dashboard
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    <% if (Page.User.Identity.IsAuthenticated)
                        { %>
                    <div class="dropdown" tabindex="0" style="outline: none; padding: 0 5px;" onclick="this.focus();">
                        <div style="height: 50px; cursor: pointer;">
                            <asp:Label ID="Username" runat="server" Text="Label" Font-Size="14pt"></asp:Label><img src="Images/user.png" width="50px" height="50px" />
                        </div>
                            
                        <div class="dropdown-content" aria-labelledby="dropdownMenuButton">
                             <a class="dropdown-item" href="Settings" runat="server" style="width: 200px;">&nbsp;&nbsp;&nbsp;<img src="Images/settings.png" />Account Settings</a>
                            <a class="dropdown-item" href="#" runat="server" onserverclick="Button1_OnClick"><img src="Images/logout.png" />Log Out</a>
                        </div>
                    </div>
                    <% } %>
			    </td>
		        <td width="5%"></td>
		    </tr>
	    </table>
    </header>
    <form id="form1" runat="server">
        <div style="padding: 0 50px 50px 50px; height: 100%; color: #383838;">
            <table class="w-100">
                <tr>
				    <td colspan="4">
					    <h1 style="text-align: center; font-weight: 700;">Longeviti LLC<br />Company Progress Report</h1>
				    </td>
			    </tr>

                <tr>
                    <td style="height: 10px;" colspan="4">

                    </td>
                </tr>

                <% if (!User.IsInRole("Customer")) { %>
                <tr>
                    <td colspan="4">
					    <h2 style="margin: 0; font-weight: 700;">Financial</h2><hr />
				    </td>
                </tr>

                <tr>
                    <td>
                        <b style="font-weight: 700; font-size: 14pt;">Total Revenue</b>
                    </td>

                    <td>

                    </td>

                    <td>
                        <b style="font-weight: 700; font-size: 14pt;">Total Expenses</b>
                    </td>
                </tr>

                <tr>
                    <td class="auto-style1">
                        <asp:Literal ID="pRevenue" runat="server"></asp:Literal>
                    </td>

                    <td class="auto-style1">

                    </td>

                    <td class="auto-style1">
                        <asp:Literal ID="pExpenses" runat="server"></asp:Literal>
                    </td>
                </tr>

                <tr>
                    <td colspan="4" style="height: 5px;"></td>
                </tr>

                <tr>
                    <td>
                        <b style="font-weight: 700; font-size: 14pt;">Average Monthly Revenue</b>
                    </td>

                    <td>

                    </td>

                    <td>
                        <b style="font-weight: 700; font-size: 14pt;">Average Monthly Expenses</b>
                    </td>
                </tr>

                <tr>
                    <td>
                        <asp:Literal ID="pMonthlyRevenue" runat="server"></asp:Literal>
                    </td>

                    <td>

                    </td>

                    <td>
                        <asp:Literal ID="pMonthlyExpenses" runat="server"></asp:Literal>
                    </td>
                </tr>

                <tr>
                    <td colspan="4" style="height: 5px;"></td>
                </tr>

                <tr>
                    <td>
                        <b style="font-weight: 700; font-size: 14pt;">Full-time Equivalent</b>
                    </td>
                </tr>

                <tr>
                    <td>
                        <asp:Literal ID="pFullTimeEquivalent" runat="server"></asp:Literal>
                    </td>
                </tr>

                <tr>
                    <td colspan="4" style="height: 20px;"></td>
                </tr>

                <% } %>

                <tr>
                    <td colspan="4">
					    <h2 style="margin: 0; font-weight: 700;">Projects</h2><hr />
				    </td>
                </tr>

                <tr>
                    <td colspan="4">
                        <asp:Table ID="Table1" runat="server" Width="100%">
                        </asp:Table>
                    </td>
                </tr>

                <asp:Literal ID="pProjectSummary" runat="server"></asp:Literal>
            </table>
        </div>
    </form>
</body>
</html>
