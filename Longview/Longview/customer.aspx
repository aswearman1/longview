﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Customer.aspx.cs" Inherits="Longview.customer" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="Content/bootstrap.css" rel="stylesheet" id="bootstrap-css" />
    <link href="Content/longview.css" rel="stylesheet" id="longview-css" />
    <style type="text/css">
        .auto-style1 {
            height: 26px;
        }
        .auto-style2 {
            width: 12px;
        }
    </style>
</head>
<body style="background: linear-gradient(to bottom, rgba(191,191,191,1) 0%,rgba(232,232,232,1) 13%,rgba(244,244,244,1) 100%) no-repeat;">
    <form id="form1" runat="server">
        <div style="background-color: white; padding: 15px; display: inline-block;">
            Select a project: <asp:DropDownList ID="ProjectDropdown" runat="server" AutoPostBack="True" OnLoad="ProjectDropdown_Load" OnSelectedIndexChanged="ProjectDropdown_SelectedIndexChanged"></asp:DropDownList>
        </div>
        <div class="contentBox" style="padding: 20px;">
            <div id="UploadBox" runat="server" style="border: 1px solid lightgray; padding: 15px; display: inline-block;">
                <table>
                    <thead>
                        <tr>
                            <td colspan="7">
                                <h3>Upload a Document</h3>
                            </td>
                        </tr>
                    </thead>
                    <tr>
                        <td class="auto-style1">
                            <asp:Label ID="Label1" runat="server" Text="File Name"></asp:Label>
                        </td>

                        <td></td>

                        <td class="auto-style1">
                            <asp:Label ID="Label2" runat="server" Text="File Path"></asp:Label>
                        </td>

                        <td></td>

                        <td colspan="3">
                            <asp:Label ID="Label3" runat="server" Text="Type"></asp:Label>
                        </td>
                    </tr>

                    <tr style="height: 30px;">
                        <td>
                            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                        </td>

                        <td></td>

                        <td>
                            <asp:FileUpload ID="FileUpload1" runat="server" />
                            <asp:TextBox ID="TextBox2" runat="server" Visible="false" TextMode="Url"></asp:TextBox>
                        </td>

                        <td></td>

                        <td rowspan="2">
                            <asp:RadioButtonList ID="DocumentList" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DocumentList_SelectedIndexChanged">
                                <asp:ListItem Selected="True">Timesheet</asp:ListItem>
                                <asp:ListItem>PPQ Request</asp:ListItem>
                                <asp:ListItem>Progress Report</asp:ListItem>
                                <asp:ListItem>Survey</asp:ListItem>
                                <asp:ListItem>Invoice</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>

                        <td class="auto-style2">                      
                        </td>

                        <td>
                            <asp:Button ID="Upload" runat="server" Text="Upload" OnClick="Upload_Click" UseSubmitBehavior="False" />
                        </td>
                    </tr>

                    <tr>
                        <td colspan="4">
                            <p id="errortext" runat="server" style="color: red;"></p>
                        </td>

                        <td colspan="2" class="auto-style2">

                        </td>
                    </tr>
                </table>
            </div>
            <h2 id="TimesheetLabel" runat="server">Employee Timesheets</h2>
            <asp:Label ID="NoTimesheets" runat="server" Text="No timesheets to show" Visible="false"></asp:Label>
            <div style="max-height: 150px; overflow-y: auto;">
                <asp:GridView ID="Timesheets" runat="server" Width="100%" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None" OnRowDataBound="Timesheets_RowDataBound" OnRowDeleting="Timesheets_RowDeleting" style="overflow: auto;">
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                        <asp:BoundField DataField="Document" HeaderText="Document">
                        <ItemStyle Width="90%" />
                        </asp:BoundField>
                    </Columns>
                    <EditRowStyle BackColor="#2461BF" />
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#EFF3FB" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                </asp:GridView>
            </div>
            <br />
            <h2 id="PPQLabel" runat="server">PPQ Requests</h2>
            <asp:Label ID="NoPPQ" runat="server" Text="No PPQ requests to show" Visible="false"></asp:Label>
            <div style="max-height: 150px; overflow-y: auto;">
                <asp:GridView ID="PPQ" runat="server" Width="100%" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None" OnRowDataBound="PPQ_RowDataBound" OnRowDeleting="PPQ_RowDeleting" style="overflow: auto;">
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                        <asp:BoundField DataField="Document" HeaderText="Document" >
                        <ItemStyle Width="90%" />
                        </asp:BoundField>
                    </Columns>
                    <EditRowStyle BackColor="#2461BF" />
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#EFF3FB" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                </asp:GridView>
            </div>
            <br />
            <h2 id="ProgressLabel" runat="server">Progress Reports</h2>
            <asp:Label ID="NoProgress" runat="server" Text="No progress reports to show" Visible="false"></asp:Label>
            <div style="max-height: 150px; overflow-y: auto;">
                <asp:GridView ID="Progress" runat="server" Width="100%" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None" OnRowDataBound="Progress_RowDataBound" OnRowDeleting="Progress_RowDeleting" style="overflow: auto;">
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                        <asp:BoundField DataField="Document" HeaderText="Document" >
                        <ItemStyle Width="90%" />
                        </asp:BoundField>
                    </Columns>
                    <EditRowStyle BackColor="#2461BF" />
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#EFF3FB" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                </asp:GridView>
            </div>
            <br />
            <h2  id="SurveyLabel" runat="server">Surveys</h2>
            <asp:Label ID="NoSurvey" runat="server" Text="No surveys to show" Visible="false"></asp:Label>
            <div style="max-height: 150px; overflow-y: auto;">
                <asp:GridView ID="Surveys" runat="server" Width="100%" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None" OnRowDataBound="Surveys_RowDataBound" OnRowDeleting="Surveys_RowDeleting" style="overflow: auto;">
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                        <asp:BoundField DataField="Document" HeaderText="Document" >
                        <ItemStyle Width="90%" />
                        </asp:BoundField>
                    </Columns>
                    <EditRowStyle BackColor="#2461BF" />
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#EFF3FB" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                </asp:GridView>
            </div>
            <br />
            <h2 id="InvoiceLabel" runat="server">Invoices</h2>
            <asp:Label ID="NoInvoice" runat="server" Text="No invoices to show" Visible="false"></asp:Label>
            <div style="max-height: 150px; overflow-y: auto;">
                <asp:GridView ID="Invoices" runat="server" Width="100%" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None" OnRowDataBound="Invoices_RowDataBound" OnRowDeleting="Invoices_RowDeleting" style="overflow: auto;">
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                        <asp:BoundField DataField="Document" HeaderText="Document" >
                        <ItemStyle Width="90%" />
                        </asp:BoundField>
                    </Columns>
                    <EditRowStyle BackColor="#2461BF" />
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#EFF3FB" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                </asp:GridView>
            </div>
        </div>
    </form>
</body>
</html>
