﻿using HtmlAgilityPack;
using System;
using System.Linq;
using System.Net;
using System.Text;

namespace Longview
{
    public partial class News : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var html = new HtmlDocument();

            try
            {
                var data = new WebClient().DownloadData("http://www.longevitillc.com/news");
                html.LoadHtml(Encoding.UTF8.GetString(data));
            }
            catch(Exception ex)
            {
                form1.InnerHtml = "<h2>There is no recent news to display.</h2>";
                return;
            }

            html.LoadHtml(html.DocumentNode.SelectSingleNode("//div[@id='content']").Descendants("div").Where(x => x.Id.StartsWith("post-")).First().InnerHtml);

            HtmlNodeCollection nc = html.DocumentNode.SelectNodes("//a");
            foreach(HtmlNode hn in nc)
            {
                hn.SetAttributeValue("target", "_top");
            }

            form1.InnerHtml = html.DocumentNode.InnerHtml;
        }
    }
}