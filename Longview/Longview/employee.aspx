﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Employee.aspx.cs" Inherits="Longview.employee" EnableEventValidation="false" %>

<!DOCTYPE html>

<html>
<head>
    <title></title>
    <link href="Content/bootstrap.css" rel="stylesheet" id="bootstrap-css" />
    <link href="Content/longview.css" rel="stylesheet" id="longview-css" />

    <style>
        .chartHeader
        {
            background: linear-gradient(to bottom, #242e3a 0%,#061a33 2%,#001e44 4%,#05224e 6%,#061c45 7%,#0a1c40 9%,#05224a 13%,#082044 15%,#09254d 28%,#0d2450 30%,#112f63 52%,#102f65 57%,#0b2c61 59%,#0b2c61 100%);
            color: white; 
            text-shadow: 2px 2px black;
            height: 30px;
        }

        svg > g > g:last-child { pointer-events: none }
        .auto-style1 {
            height: 31px;
        }
    </style>

    <script type="text/javascript">
        function confirm_delete()
            {
              if (confirm("Are you sure you want to delete this employee?")==true)
                return true;
              else
                return false;
            }
    </script>
</head>
<body style="padding: 8px; line-height: normal; background: linear-gradient(to bottom, rgba(191,191,191,1) 0%,rgba(232,232,232,1) 13%,rgba(244,244,244,1) 100%) no-repeat;">
    <form id="form1" runat="server">
        <table style="width: 100%;">
            <tr>
                <td style="min-width: 10px; width: 10px;"></td>

                <td align="center" style="padding: 0;">
                    <div style="width: 260px; height: 450px; min-width: 260px; margin: 0; font-family: Raleway; border: 1px solid #80808045; box-shadow: 5px 5px 5px #b8b8b8; background-color: white; box-sizing: content-box;">
                        <table cellpadding="0" style="width: 260px; height: 450px; border-collapse: separate; border-spacing: 2px;">
                            <thead>
                                <tr>
                                    <td style="width: 100%; height: 72px; vertical-align: middle; text-align: center; border: 2px solid black; background: linear-gradient(to bottom, #242e3a 0%,#061a33 2%,#001e44 4%,#05224e 6%,#061c45 7%,#0a1c40 9%,#05224a 13%,#082044 15%,#09254d 28%,#0d2450 30%,#112f63 52%,#102f65 57%,#0b2c61 59%,#0b2c61 100%);">
                                        <h2 style="font-family: Raleway; font-size: 1.5rem; font-weight: bold; color: white; text-shadow: 2px 2px black; text-align: center; display: inline-block; margin-bottom: 0;">LINKS</h2>
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td style="text-align: center; border: 2px solid black;" onmouseover="this.style.backgroundColor = '#061c45'; this.children[0].style.color = '#fff'; this.children[0].style.textShadow = '2px 2px black'" onmouseout="this.style.backgroundColor = ''; this.children[0].style.color = '#181727'; this.children[0].style.textShadow = ''" >
                                        <a href="Content/helloworld.pdf" style="width: 100%; text-decoration: none; display: block; padding: 9px 0; font-family: Raleway; color: rgb(24, 23, 39); font-weight: bold; font-size: 18pt;">Employee Handbook</a>
                                    </td>
                                </tr>

                                <tr>
                                    <td style="text-align: center; vertical-align: middle; border: 2px solid black;" onmouseover="this.style.backgroundColor = '#061c45'; this.children[0].style.color = '#fff'; this.children[0].style.textShadow = '2px 2px black'" onmouseout="this.style.backgroundColor = ''; this.children[0].style.color = '#181727'; this.children[0].style.textShadow = ''" >
                                        <a href="https://tcg4.hostedaccess.com/DeltekTC/welcome.msv" target="_parent" style="width: 100%; text-decoration: none; display: block; padding: 9px 0; font-family: Raleway; color: rgb(24, 23, 39); font-weight: bold; font-size: 18pt;">Timesheets</a>
                                    </td>
                                </tr>

                                <tr>
                                    <td style="text-align: center; vertical-align: middle; border: 2px solid black;" onmouseover="this.style.backgroundColor = '#061c45'; this.children[0].style.color = '#fff'; this.children[0].style.textShadow = '2px 2px black'" onmouseout="this.style.backgroundColor = ''; this.children[0].style.color = '#181727'; this.children[0].style.textShadow = ''" >
                                        <a href="#" style="width: 100%; text-decoration: none; display: block; padding: 9px 0; font-family: Raleway; color: rgb(24, 23, 39); font-weight: bold; font-size: 18pt;">Cyber Recruiter</a>
                                    </td>
                                </tr>

                                <tr>
                                    <td style="text-align: center; vertical-align: middle; border: 2px solid black;" onmouseover="this.style.backgroundColor = '#061c45'; this.children[0].style.color = '#fff'; this.children[0].style.textShadow = '2px 2px black'" onmouseout="this.style.backgroundColor = ''; this.children[0].style.color = '#181727'; this.children[0].style.textShadow = ''" >
                                        <a href="#" style="width: 100%; text-decoration: none; display: block; padding: 9px 0; font-family: Raleway; color: rgb(24, 23, 39); font-weight: bold; font-size: 18pt;">Human Resources</a>
                                    </td>
                                </tr>

                                <tr>
                                    <td style="text-align: center; vertical-align: middle; border: 2px solid black;" onmouseover="this.style.backgroundColor = '#061c45'; this.children[0].style.color = '#fff'; this.children[0].style.textShadow = '2px 2px black'" onmouseout="this.style.backgroundColor = ''; this.children[0].style.color = '#181727'; this.children[0].style.textShadow = ''" >
                                        <a href="javascript:void(0);" style="width: 100%; text-decoration: none; display: block; padding: 9px 0; font-family: Raleway; color: rgb(24, 23, 39); font-weight: bold; font-size: 18pt;" >Training</a>
                                    </td>
                                </tr>

                                <tr>
                                    <td style="text-align: center; vertical-align: middle; border: 2px solid black;" onmouseover="this.style.backgroundColor = '#061c45'; this.children[0].style.color = '#fff'; this.children[0].style.textShadow = '2px 2px black'" onmouseout="this.style.backgroundColor = ''; this.children[0].style.color = '#181727'; this.children[0].style.textShadow = ''" >
                                        <a href="https://www.iso.org/home.html" target="_parent" style="width: 100%; text-decoration: none; display: block; padding: 9px 0; font-family: Raleway; color: rgb(24, 23, 39); font-weight: bold; font-size: 18pt;">ISO</a>
                                    </td>
                                </tr>

                                <tr>
                                    <td style="text-align: center; vertical-align: middle; border: 2px solid black;" onmouseover="this.style.backgroundColor = '#061c45'; this.children[0].style.color = '#fff'; this.children[0].style.textShadow = '2px 2px black'" onmouseout="this.style.backgroundColor = ''; this.children[0].style.color = '#181727'; this.children[0].style.textShadow = ''" >
                                        <a href="https://cmmiinstitute.com/" target="_parent" style="width: 100%; text-decoration: none; display: block; padding: 9px 0; font-family: Raleway; color: rgb(24, 23, 39); font-weight: bold; font-size: 18pt;">CMMI</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </td>

                <td style="min-width: 10px; width: 10px;"></td>

                <td style="width: 77%;">
                    <div style="width: 100%; height: 450px; min-width: 624px; border: 1px solid #80808045; box-shadow: 5px 5px 5px #b8b8b8; background-color: white; font-family: Raleway; text-align: left;">
                        <img src="Images/training.png" style="margin-left: 10px; margin-top: 5px; float: left;" /><h1 style="margin-left: 10px; display: inline-block; margin-top: 10px; font-weight: 500; line-height: 1.2; font-size: 30px;">Mandatory Training</h1><br />
                        <div style="margin: 5px 20px; width: auto; height: 350px; overflow-y: auto;">
                            <% if (User.IsInRole("Administrators")) { %>
                            <table>
                                <tr>
                                    <td colspan="5">
                                        <asp:RadioButtonList ID="RadioButtonList1" runat="server" OnSelectedIndexChanged="RadioButtonList1_SelectedIndexChanged" AutoPostBack="True">
                                            <asp:ListItem Text="Upload Document" Value="Upload" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="Add Link" Value="Link"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        Document Name
                                    </td>

                                    <td style="width: 5px"></td>

                                    <td id="Upload" runat="server">
                                        Upload
                                    </td>
                                </tr>

                                <tr>
                                    <td class="auto-style1">
                                        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                    </td>

                                    <td class="auto-style1">

                                    </td>

                                    <td class="auto-style1">
                                        <asp:FileUpload ID="FileUpload1" runat="server" />
                                        <asp:TextBox ID="TextBox2" runat="server" Visible="false" TextMode="Url"></asp:TextBox>
                                    </td>

                                    <td width="5px" class="auto-style1">
                                    </td>

                                    <td class="auto-style1">
                                        <asp:Button ID="UploadButton" runat="server" Text="Upload" OnClick="UploadButton_Click" />
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="6">
                                        <p id="errortext" runat="server" style="color: red;"></p>
                                    </td>
                                </tr>
                            </table>
                            <% } %>
                            <h2>Documents</h2>
                            <asp:GridView ID="GridView1" Width="100%" style="margin-top: 10px;" runat="server" AutoGenerateColumns="False" BorderStyle="None" GridLines="None" OnRowDataBound="GridView1_RowDataBound" OnSelectedIndexChanged="GridView1_SelectedIndexChanged" OnLoad="GridView1_Load" OnRowDeleting="GridView1_RowDeleting">
                                <Columns>
                                    <asp:BoundField DataField="Document" HeaderText="Document" />
                                </Columns>
                                <HeaderStyle CssClass="chartHeader" />
                            </asp:GridView>
                        </div>
                    </div>  
                </td>

                <td style="min-width: 10px; width: 10px;"></td>
            </tr>

            <% if (User.IsInRole("Manager") || User.IsInRole("Administrators"))
                {  %>
            <tr>
                <td colspan="5" style="height: 10px;"></td>
            </tr>

            <tr>
                <td style="min-width: 10px; width: 10px;"></td>

                <td colspan="3">
                    <div style="width: 100%; height: 450px; min-width: 624px; border: 1px solid #80808045; box-shadow: 5px 5px 5px #b8b8b8; background-color: white; font-family: Raleway; text-align: left;">
                        <img src="Images/recruiting.png" style="margin-left: 10px; margin-top: 5px; float: left;" /><h1 style="font-family: Raleway; margin-left: 10px; display: inline-block; font-size: 30px; margin-top: 10px;">Employees</h1>
                        <div style="display: inline-block; float: right; height: 25px; width: 500px; margin-top: 15px; margin-right: 15px;">
                            <asp:Button ID="AddEmployee" runat="server" Text="Add Employee" CssClass="btn-default btn-sm" BackColor="#0d0435" ForeColor="White" style="font-weight: 500; border: none; float: right;" OnClick="AddEmployee_Click" />
                            <asp:Panel ID="Panel1" runat="server" DefaultButton="SearchButton">
                            <asp:Button ID="SearchButton" runat="server" Text="Search" CssClass="btn-default btn-sm" BackColor="#0d0435" ForeColor="White" style="font-weight: 500; border: none; float: right; margin-right: 8px;" OnClick="SearchButton_Click" />
                            <asp:TextBox ID="SearchBox" runat="server" style="float: right; margin-right: 5px; margin-top: 3px;"></asp:TextBox>
                            </asp:Panel>
                        </div>
                        <div style="margin: 5px 20px; width: auto; height: 370px; overflow-y: auto;">
                            <asp:GridView ID="GridView2" runat="server" Height="100%" Width="100%" AutoGenerateColumns="False" OnLoad="GridView2_Load" OnRowDeleting="GridView2_RowDeleting" OnRowEditing="GridView2_RowEditing" AllowSorting="True" OnSorted="GridView2_Sorted" OnSorting="GridView2_Sorting" OnSelectedIndexChanging="GridView2_SelectedIndexChanging">
                                <AlternatingRowStyle BackColor="#CCCCCC" />
                                <Columns>
                                    <asp:BoundField DataField="ID" HeaderText="ID" />
                                    <asp:BoundField DataField="Last Name" HeaderText="Last Name" SortExpression="Last Name" />
                                    <asp:BoundField DataField="First Name" HeaderText="First Name" />
                                    <asp:BoundField DataField="Position" HeaderText="Position" />
                                    <asp:BoundField DataField="Start Date" HeaderText="Start Date" />
                                    <asp:BoundField DataField="Weekly Hours" HeaderText="Weekly Hours" />
                                    <asp:BoundField DataField="Hourly Rate" HeaderText="Hourly Rate" />
                                    <asp:CommandField SelectText="More Info" ShowSelectButton="True" />
                                    <asp:CommandField ShowEditButton="True" />
                                    <asp:TemplateField ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete" OnClientClick="return confirm_delete();"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>

                            <div id="EmployeeEditor" style="width: 100%; height: auto; display: none;" runat="server">
                                <table style="width: 80%; margin: 20px auto;">
                                    <tr>
                                        <td><b>Employee ID:</b></td>
                                        <td style="width: 10px; min-width: 10px;"></td>
                                        <td><asp:TextBox ID="EmployeeID" runat="server" Enabled="false"></asp:TextBox></td>
                                        <td style="width: 10px; min-width: 10px;"></td>
                                        <td><b>Rewards:</b></td>
                                        <td><asp:TextBox ID="Rewards" runat="server" TextMode="Number"></asp:TextBox></td>
                                    </tr>
                                    <tr><td style="height: 10px;"></td></tr>
                                    <tr>
                                        <td><b>Last Name:</b></td>
                                        <td></td>
                                        <td><asp:TextBox ID="LastName" runat="server"></asp:TextBox></td>
                                        <td style="width: 10px; min-width: 10px;"></td>
                                        <td><b>Recommendations:</b></td>
                                        <td><asp:TextBox ID="Recommendations" runat="server" TextMode="Number"></asp:TextBox></td>
                                    </tr>
                                    <tr><td style="height: 10px;"></td></tr>
                                    <tr>
                                        <td><b>First Name:</b></td>
                                        <td></td>
                                        <td><asp:TextBox ID="FirstName" runat="server"></asp:TextBox></td>
                                        <td style="width: 10px; min-width: 10px;"></td>
                                        <td><b>H&amp;W Rate:</b></td>
                                        <td><asp:TextBox ID="HWRate" runat="server"></asp:TextBox></td>
                                    </tr>
                                    <tr><td style="height: 10px;"></td></tr>
                                    <tr>
                                        <td><b>Position:</b></td>
                                        <td></td>
                                        <td><asp:TextBox ID="Position" runat="server"></asp:TextBox></td>
                                        <td style="width: 10px; min-width: 10px;"></td>
                                        <td><b>Reprimands:</b></td>
                                        <td><asp:TextBox ID="Reprimands" runat="server" TextMode="Number"></asp:TextBox></td>
                                    </tr>
                                    <tr><td style="height: 10px;"></td></tr>
                                    <tr>
                                        <td><b>Start Date:</b></td>
                                        <td></td>
                                        <td><asp:TextBox ID="StartDate" runat="server" TextMode="Date"></asp:TextBox></td>
                                        <td style="width: 10px; min-width: 10px;"></td>
                                        <td><b>Corrective Actions:</b></td>
                                        <td><asp:TextBox ID="CorrectiveActions" runat="server" TextMode="Number"></asp:TextBox></td>
                                    </tr>
                                    <tr><td style="height: 10px;"></td></tr>
                                    <tr>
                                        <td><b>Weekly Hours:</b></td>
                                        <td></td>
                                        <td><asp:TextBox ID="WeeklyHours" runat="server"></asp:TextBox></td>
                                        
                                    </tr>
                                    <tr><td style="height: 10px;"></td></tr>
                                    <tr>
                                        <td><b>Hourly Rate:</b></td>
                                        <td></td>
                                        <td><asp:TextBox ID="HourlyRate" runat="server"></asp:TextBox></td>
                                    </tr>
                                    <tr><td style="height: 10px;"></td></tr>
                                    <tr>
                                        <td>
                                            <asp:Button ID="SaveEmployee" runat="server" Text="Save" CssClass="btn-default btn-sm" BackColor="#0d0435" ForeColor="White" style="font-weight: 500; border: none;" OnClick="SaveEmployee_Click" />
                                            <asp:Button ID="AddNewEmployee" runat="server" Text="Add" CssClass="btn-default btn-sm" BackColor="#0d0435" ForeColor="White" style="font-weight: 500; border: none;" Visible="false" OnClick="AddNewEmployee_Click" />
                                            <asp:Button ID="CancelEmployee" runat="server" Text="Cancel" CssClass="btn-default btn-sm" BackColor="#0d0435" ForeColor="White" style="font-weight: 500; border: none;" OnClick="CancelEmployee_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="5"><p id="errortextemp" runat="server" style="color: red;"></p></td>
                                    </tr>
                                </table>                   
                            </div>
                            <div id="EmployeeView" style="width: 100%; height: auto; display: none;" runat="server">
                                <table style="width: 80%; margin: 20px auto;">
                                    <tr>
                                        <td><b>Employee ID:</b></td>
                                        <td style="width: 10px; min-width: 10px;"></td>
                                        <td><asp:Literal ID="LitEmpID" runat="server"></asp:Literal></td>
                                        <td style="width: 10px; min-width: 10px;"></td>
                                        <td><b>Rewards:</b></td>
                                        <td><asp:Literal ID="LitRew" runat="server"></asp:Literal></td>
                                    </tr>
                                    <tr><td style="height: 10px;"></td></tr>
                                    <tr>
                                        <td><b>Last Name:</b></td>
                                        <td></td>
                                        <td><asp:Literal ID="LitLastName" runat="server"></asp:Literal></td>
                                        <td style="width: 10px; min-width: 10px;"></td>
                                        <td><b>Recommendations:</b></td>
                                        <td><asp:Literal ID="LitRecom" runat="server"></asp:Literal></td>
                                    </tr>
                                    <tr><td style="height: 10px;"></td></tr>
                                    <tr>
                                        <td><b>First Name:</b></td>
                                        <td></td>
                                        <td><asp:Literal ID="LitFirstName" runat="server"></asp:Literal></td>
                                        <td style="width: 10px; min-width: 10px;"></td>
                                        <td><b>H&amp;W Rate:</b></td>
                                        <td><asp:Literal ID="LitHWRate" runat="server"></asp:Literal></td>
                                    </tr>
                                    <tr><td style="height: 10px;"></td></tr>
                                    <tr>
                                        <td><b>Position:</b></td>
                                        <td></td>
                                        <td><asp:Literal ID="LitPosition" runat="server"></asp:Literal></td>
                                        <td style="width: 10px; min-width: 10px;"></td>
                                        <td><b>Reprimands:</b></td>
                                        <td><asp:Literal ID="LitReprimands" runat="server"></asp:Literal></td>
                                    </tr>
                                    <tr><td style="height: 10px;"></td></tr>
                                    <tr>
                                        <td><b>Start Date:</b></td>
                                        <td></td>
                                        <td><asp:Literal ID="LitStartDate" runat="server"></asp:Literal></td>
                                        <td style="width: 10px; min-width: 10px;"></td>
                                        <td><b>Corrective Actions:</b></td>
                                        <td><asp:Literal ID="LitCorrectiveActions" runat="server"></asp:Literal></td>
                                    </tr>
                                    <tr><td style="height: 10px;"></td></tr>
                                    <tr>
                                        <td><b>Weekly Hours:</b></td>
                                        <td></td>
                                        <td><asp:Literal ID="LitWeeklyHours" runat="server"></asp:Literal></td>
                                        
                                    </tr>
                                    <tr><td style="height: 10px;"></td></tr>
                                    <tr>
                                        <td><b>Hourly Rate:</b></td>
                                        <td></td>
                                        <td><asp:Literal ID="LitHourlyRate" runat="server"></asp:Literal></td>
                                    </tr>
                                    <tr><td style="height: 10px;"></td></tr>
                                    <tr>
                                        <td><asp:LinkButton ID="LinkButton2" runat="server" OnClick="LinkButton2_Click">← Back</asp:LinkButton></td>
                                    </tr>
                                </table>     
                            </div>
                        </div>
                    </div>
                </td>

                <td style="min-width: 10px; width: 10px;"></td>

                <% } %>
            </tr>
        </table>
    </form>
</body>
</html>
